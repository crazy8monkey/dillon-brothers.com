<?php


class BlogPost extends BaseObject {
	
	public $blogPostID;
	private $_seoName;
	private $_OptionalSEOURL;
	private $_postID;
	
	public $blogPostName;
	public $blogPostContent;
	public $visible;
	public $blogPhoto;
	public $currentBlogPhoto;
	public $blogPostSEOUrl;
	public $BlogPostPreview;
	public $blogPublishedDateFull;
	public $photoAlbumYearDirectory;
	public $photoAlbumFolderName;
	public $OptionalShortURL;
	
	public $modifiedDateFull;
	
	public $MetaData;

	private $_firstName;
	private $_lastName;
	public $publishedDate;
	
	public $BlogPhotoName;
	public $blogPhotoExt;
	private $ShowFullName;
	public $KeyWords;
	
	public $NotBlogPostName = false;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithID($postID) {
        $instance = new self();
        $instance -> _postID = $postID;
        $instance->loadByID();
        return $instance;
    }
	
	public static function WithSEOName($postName) {
        $instance = new self();
        $instance->_seoName = $postName;
        $instance->loadByID();
        return $instance;
    }
	
	public static function WithOptionalSEOName($postName) {
		$instance = new self();
        $instance-> _OptionalSEOURL = $postName;
        $instance->loadByID();
        return $instance;
		
	}
		
	protected function loadByID() {
		if(!empty($this->_seoName)) {
			$sth = $this -> db -> prepare('SELECT * FROM blogposts LEFT JOIN users ON blogposts.createdByID = users.userID LEFT JOIN photoalbums On blogposts.blogPostID = photoalbums.albumBlogID LEFT JOIN photos ON blogposts.blogPostImage = photos.photoID WHERE blogPostSEOurl = :blogPostSEOurl');
        	$sth->execute(array(':blogPostSEOurl' => $this->_seoName));	
		}
		if(!empty($this -> _OptionalSEOURL)) {
		 	$sth = $this -> db -> prepare('SELECT * FROM blogposts LEFT JOIN users ON blogposts.createdByID = users.userID LEFT JOIN photoalbums On blogposts.blogPostID = photoalbums.albumBlogID LEFT JOIN photos ON blogposts.blogPostImage = photos.photoID WHERE OptionalBlogSEOUrl = :OptionalBlogSEOUrl');
        	$sth->execute(array(':OptionalBlogSEOUrl' => $this -> _OptionalSEOURL));	
		}
    	
		if(!empty($this -> _postID)) {
			$sth = $this -> db -> prepare('SELECT * FROM blogposts LEFT JOIN users ON blogposts.createdByID = users.userID LEFT JOIN photoalbums On blogposts.blogPostID = photoalbums.albumBlogID LEFT JOIN photos ON blogposts.blogPostImage = photos.photoID WHERE blogposts.blogPostID = :blogPostID');
        	$sth->execute(array(':blogPostID' => $this->_postID));	
		}
		
		if($sth->rowCount() > 0) {
			$record = $sth -> fetch();
		    $this->fill($record);
			//return true;
		} else {
			$this -> NotBlogPostName = true;	
			//throw new Exception('Blog Record does not exist (Value: ' . $this->_seoName . ')');	
			//return false;
		}
		
        //$record = $sth -> fetch();
        //$this->fill($record);
    }

    protected function fill(array $row){
    	$this -> blogPostID = $row['blogPostID'];
    	$this -> blogPostName = $row['blogPostName'];
		$this -> currentBlogPhoto = $row['blogPostImage'];
		$this -> blogPostSEOUrl = $row['blogPostSEOurl'];
		$this -> blogPostContent = $row['blogPostContent'];
		$this -> BlogPostPreview = $row['blogPostPreview'];
		$this -> _firstName = $row['firstName'];
		$this -> _lastName = $row['lastName'];
		$this -> publishedDate = $row['publishedDate'];
		$this -> blogPublishedDateFull = $row['publishedDateFull'];
		$this -> modifiedDateFull = $row['dateModifiedDate'];
		$this -> photoAlbumYearDirectory = $row['ParentDirectory'];
		$this -> photoAlbumFolderName = $row['albumFolderName'];
		$this -> BlogPhotoName = $row['photoName'];
		$this -> blogPhotoExt = $row['ext'];
		$this -> ShowFullName = $row['showUserNameOnPublish'];
		$this -> KeyWords = $row['keywords'];
		$this -> OptionalShortURL = $row['OptionalBlogSEOUrl'];
    }
	
	
	public function CreatedByFullName() {
		$creator = NULL;	
		if($this -> ShowFullName == 0) {
			$creator = "Dillon Brothers";	
		} else {
			$creator = $this -> _firstName . ' ' . substr($this -> _lastName, 0, 1);
		}
		return $creator;
	}
	
	
	
	public function GetBlogImage() {
		return PHOTO_URL . $this -> photoAlbumYearDirectory . '/blog/' . $this -> photoAlbumFolderName . '/' . $this -> BlogPhotoName . '-l.' . $this -> blogPhotoExt;
	}	


	

		

}