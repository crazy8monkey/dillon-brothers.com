<?php

class EcommerceCategory extends BaseObject {
	
	private $_seoName;
	public $CategoryID;
	public $CategoryName;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }
	
	public function GetID() {
		return $this -> _productID;
	}
	
    public static function WithSEOName($seoName) {
        $instance = new self();
        $instance-> _seoName = $seoName;
        $instance->loadByName();
        return $instance;
    }

	
	protected function loadByName() {
		$sth = $this -> db -> prepare('SELECT * FROM ecommercesettingcategories WHERE settingCategorySEOName = :settingCategorySEOName');
        $sth->execute(array(':settingCategorySEOName' => $this -> _seoName));	
		$record = $sth -> fetch();
		
		if($sth->rowCount() > 0) {
		 	$this -> fill($record);
		} else {
			throw new Exception('Category Record does not exist (Value: ' . $this -> _seoName . ')');	
		}
	}

    protected function fill(array $row){
    	$this -> CategoryID = $row['settingCategoryID'];
		$this -> CategoryName = $row['settingCategoryName'];
    }	
	
	public function GetPublishedDate() {
		$products = new EcommerceProductList();
		
		return $products -> ProductsByCategory($this -> CategoryID)[count($products -> ProductsByCategory($this -> CategoryID)) - 1]['productPublishedDate'];
	}
	
	public function GetModifiedDate() {
		$products = new EcommerceProductList();
		
		return $products -> ProductsByCategory($this -> CategoryID)[0]['productModifiedTime'];
	}
		



}