<?php

class EcommerceSettingGeneral extends BaseObject {
	
	private $_id;
	public $ShippingRates;	
	public $EmailNotifications;

	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    public function __construct() {
        parent::__construct();
    }

    public static function Init() {
        $instance = new self();
        $instance -> loadById();
        return $instance;
    }
	
	protected function loadById() {
		$sth = $this -> db -> prepare('SELECT * FROM ecommercesettings WHERE ecommerceSettingsID = :ecommerceSettingsID');
	    $sth->execute(array(':ecommerceSettingsID' => 1));	
        $record = $sth -> fetch();
        $this->fill($record);
    }
	

	protected function fill(array $row){
		$this -> ShippingRates = $row['shippingRates'];
		$this -> EmailNotifications = $row['emailnotifications'];
    }	
	
	public function GetFlatRate() {
		return $this -> ShippingRates;
	}

}