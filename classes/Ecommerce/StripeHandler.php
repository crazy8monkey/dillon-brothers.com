<?php

class StripeHandler   {
	
	public $StripeToken;
	public $ChargeID;
	public $CustomerID;
	public $userEmail;

    public function __construct() {
		\Stripe\Stripe::setApiKey(STRIPE_PRIVATE_KEY);
		//set_exception_handler('CardException');
    }
	
	public function CreateCustomer() {
		try {
			$newCustomer = \Stripe\Customer::create(array(
		  		"source" => $this -> StripeToken,
				"email" => $this -> userEmail
			));
			
			$this -> CustomerID = $newCustomer['id'];
			
		} catch(\Stripe\Error\Card $e) {
			$e_json = $e->getJsonBody();
			$this -> SendErrorMessage("Stripe Card Exception",  implode(" - ", $e_json['error']));
		} catch (\Stripe\Error\RateLimit $e) {
  			// Too many requests made to the API too quickly  		
			$e_json = $e->getJsonBody();
  			$this -> SendErrorMessage("Stripe Rate limit Exception",  implode(" - ", $e_json['error']));	
		} catch (\Stripe\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Stripe's API
			$e_json = $e->getJsonBody();
			$this -> SendErrorMessage("Stripe Invalid Request Exception",  implode(" - ", $e_json['error']));	
		} catch (\Stripe\Error\Authentication $e) {			
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			$e_json = $e->getJsonBody();									
			$this -> SendErrorMessage("Stripe Authentication Exception",  implode(" - ", $e_json['error']));
		} catch (\Stripe\Error\ApiConnection $e) {
			// Network communication with Stripe failed
			$e_json = $e->getJsonBody();						
			$this -> SendErrorMessage("Stripe APIConnection Exception",  implode(" - ", $e_json['error']));	
		} catch (\Stripe\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			$e_json = $e->getJsonBody();	
			$this -> SendErrorMessage("Stripe Base Exception",  implode(" - ", $e_json['error']));	
		} catch (Exception $e) {
			$this -> SendErrorMessage("Stripe Charge Normal Exception", $e-> getMessage());		
		}	
	}

	public function UpdateCustomer() {
		try {
			$currentCustomer = \Stripe\Customer::retrieve($this -> CustomerID);
			$currentCustomer->source = $this -> StripeToken; // obtained with Stripe.js
			$currentCustomer->save();
			
			//$currentCustomer -> default_source = 
			//$currentCustomer -> card = ;
			//$currentCustomer -> save();
			
		} catch(\Stripe\Error\Card $e) {
			$e_json = $e->getJsonBody();
			$this -> SendErrorMessage("Stripe Card Exception",  implode(" - ", $e_json['error']));
		} catch (\Stripe\Error\RateLimit $e) {
  			// Too many requests made to the API too quickly  		
			$e_json = $e->getJsonBody();
  			$this -> SendErrorMessage("Stripe Rate limit Exception",  implode(" - ", $e_json['error']));	
		} catch (\Stripe\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Stripe's API
			$e_json = $e->getJsonBody();
			//echo json_encode($e_json['error']);
			$this -> SendErrorMessage("Stripe Invalid Request Exception",  implode(" - ", $e_json['error']));	
		} catch (\Stripe\Error\Authentication $e) {			
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			$e_json = $e->getJsonBody();									
			$this -> SendErrorMessage("Stripe Authentication Exception",  implode(" - ", $e_json['error']));
		} catch (\Stripe\Error\ApiConnection $e) {
			// Network communication with Stripe failed
			$e_json = $e->getJsonBody();						
			$this -> SendErrorMessage("Stripe APIConnection Exception",  implode(" - ", $e_json['error']));	
		} catch (\Stripe\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			$e_json = $e->getJsonBody();	
			$this -> SendErrorMessage("Stripe Base Exception",  implode(" - ", $e_json['error']));	
		} catch (Exception $e) {
			$this -> SendErrorMessage("Stripe Charge Normal Exception", $e-> getMessage());		
		}	
	}

	
	public function GetCustomerID() {
		return $this -> CustomerID;
	}
	
	public function GetChargeID() {
		return $this -> ChargeID;	
	}
	
	
	public function Charge($amount, $ChargeSavedCustomer = false) {
		try {
			if($ChargeSavedCustomer == true) {
				$newCharge = \Stripe\Charge::create(array(
					"amount" => $amount * 100, // amount in cents, again
			  		"currency" => "usd",
	  				"customer" => $this -> CustomerID
				));
			} else {
				$newCharge = \Stripe\Charge::create(array(
					"amount" => $amount * 100, // amount in cents, again
			  		"currency" => "usd",
	  				"source" => $this -> StripeToken
				));
			}
			
			$this -> ChargeID = $newCharge['id'];
			
			
		} catch(\Stripe\Error\Card $e) {
			$e_json = $e->getJsonBody();
			$this -> SendErrorMessage("Stripe Card Exception",  implode(" - ", $e_json['error']));
		} catch (\Stripe\Error\RateLimit $e) {
  			// Too many requests made to the API too quickly  		
			$e_json = $e->getJsonBody();
  			$this -> SendErrorMessage("Stripe Rate limit Exception",  implode(" - ", $e_json['error']));	
		} catch (\Stripe\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Stripe's API
			$e_json = $e->getJsonBody();
			$this -> SendErrorMessage("Stripe Invalid Request Exception", implode(" - ", $e_json['error']));
		} catch (\Stripe\Error\Authentication $e) {			
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			$e_json = $e->getJsonBody();									
			$this -> SendErrorMessage("Stripe Authentication Exception", implode(" - ", $e_json['error']));
		} catch (\Stripe\Error\ApiConnection $e) {
			// Network communication with Stripe failed
			$e_json = $e->getJsonBody();						
			$this -> SendErrorMessage("Stripe APIConnection Exception", implode(" - ", $e_json['error']));	
		} catch (\Stripe\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			$e_json = $e->getJsonBody();	
			$this -> SendErrorMessage("Stripe Base Exception", implode(" - ", $e_json['error']));	
		} catch (Exception $e) {
			$this -> SendErrorMessage("Stripe Charge Normal Exception", $e-> getMessage());		
		}	
	}

	public function ChargeCustomer($amount) {
		try {
			$newCustomerCharge = \Stripe\Charge::create(array(
				"amount" => $amount * 100, // amount in cents, again
				"currency" => "usd",
	  			"customer" => $this -> CustomerID
			));
			
			$this -> ChargeID = $newCustomerCharge['id'];
			
		} catch(\Stripe\Error\Card $e) {
			$e_json = $e->getJsonBody();
			$this -> SendErrorMessage("Stripe Card Exception",  implode(" - ", $e_json['error']));
		} catch (\Stripe\Error\RateLimit $e) {
  			// Too many requests made to the API too quickly  		
			$e_json = $e->getJsonBody();
  			$this -> SendErrorMessage("Stripe Rate limit Exception",  implode(" - ", $e_json['error']));	
		} catch (\Stripe\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Stripe's API
			$e_json = $e->getJsonBody();
			$this -> SendErrorMessage("Stripe Invalid Request Exception", implode(" - ", $e_json['error']));
		} catch (\Stripe\Error\Authentication $e) {			
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			$e_json = $e->getJsonBody();									
			$this -> SendErrorMessage("Stripe Authentication Exception", implode(" - ", $e_json['error']));
		} catch (\Stripe\Error\ApiConnection $e) {
			// Network communication with Stripe failed
			$e_json = $e->getJsonBody();						
			$this -> SendErrorMessage("Stripe APIConnection Exception", implode(" - ", $e_json['error']));	
		} catch (\Stripe\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			$e_json = $e->getJsonBody();	
			$this -> SendErrorMessage("Stripe Base Exception", implode(" - ", $e_json['error']));	
		} catch (Exception $e) {
			$this -> SendErrorMessage("Stripe Charge Normal Exception", $e-> getMessage());		
		}	
	}
	
	
	public function GetCustomer($customerID) {
		return \Stripe\Customer::retrieve($customerID);
	}

	private function SendErrorMessage($exceptionType, $msg) {
		$TrackError = new EmailServerError();
		$TrackError -> message = $exceptionType . ": " . $msg;
		$TrackError -> type = strtoupper($exceptionType);
		$TrackError -> SendMessage();		
	}



}