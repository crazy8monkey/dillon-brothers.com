<?php

class EcommerceOrder extends BaseObject {

	public $firstName;
	public $LastName;
	public $PhoneNumber;
	public $Email;
	public $ShippingAddress;
	public $ShippingCity;
	public $ShippingState;
	public $ShippingZip;
	public $CreateAccount;
	public $StorePickup;
	
	public $TaxRate;
	public $ShippingRate;
	
	public $StripeToken;
	public $orderItems;
	
	public $CustomerStripeID;
	public $ShippingOption;
	
	//0 = no
	//1 = yes
	public $SessionExists;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    public function __construct() {
        parent::__construct();
    }
	
	public function ValidateStripeToken() {
		if(isset($this -> StripeToken)) {
			return true;	
		} else {
			$this -> json -> outputJqueryJSONObject('SecurityTokenPresent', false);	
			return false;
		}
	}
	
	
	public function Validate() {
		$validationErrors = array();
		
		if($this -> SessionExists == 0) {
			if($this -> validate -> emptyInput($this -> firstName)) {
					array_push($validationErrors, array("inputID" => 'shippingFirstNameError',
														'errorMessage' => 'Required'));
			}
			
			if($this -> validate -> emptyInput($this -> LastName)) {
					array_push($validationErrors, array("inputID" => 'shippingLastNameError',
														'errorMessage' => 'Required'));
			}
			
			if($this -> validate -> emptyInput($this -> PhoneNumber)) {
					array_push($validationErrors, array("inputID" => 'shippingPhoneError',
														'errorMessage' => 'Required'));
			}
			
			if($this -> validate -> emptyInput($this -> Email)) {
				array_push($validationErrors, array("inputID" => 'shippingEmailError',
													'errorMessage' => 'Required'));
			} else if($this -> validate -> correctEmailFormat($this -> Email)) {
				array_push($validationErrors, array("inputID" => 'shippingEmailError',
													'errorMessage' => 'Needs to be in Email Format'));
			}
		}
		
		if(!isset($this -> StorePickup)) {
			array_push($validationErrors, array("inputID" => 'storePickupError',
												'errorMessage' => 'Please choose a shipping option'));
		} else {
			if(in_array(1, $this -> StorePickup)) {
				if($this -> validate -> emptyInput($this -> ShippingAddress)) {
					array_push($validationErrors, array("inputID" => 'shippingAddressError',
														'errorMessage' => 'Required'));
				}
					
				if($this -> validate -> emptyInput($this -> ShippingCity)) {
					array_push($validationErrors, array("inputID" => 'shippingCityError',
														'errorMessage' => 'Required'));
				}	
				
				if($this -> ShippingState == '0') {
					array_push($validationErrors, array("inputID" => 'shippingStateError',
														'errorMessage' => 'Required'));
				}
				
				if($this -> validate -> emptyInput($this -> ShippingZip)) {
					array_push($validationErrors, array("inputID" => 'shippingZipError',
														'errorMessage' => 'Required'));
				}	
			}		
		}
		
		
		
		
		//empty first name
		//if($this -> validate -> emptyInput($this -> firstName)) {
		//	array_push($validationErrors, array("inputID" => 1,
		//										'errorMessage' => 'Required'));
		//}

		//if($this -> validate -> emptyInput($this -> LastName)) {
		//	array_push($validationErrors, array("inputID" => 2,
		//										'errorMessage' => 'Required'));
		//}
		
		//if($this -> validate -> emptyInput($this -> userEmail)) {
		//	array_push($validationErrors, array("inputID" => 3,
		//										'errorMessage' => 'Required'));
		//} else if($this -> validate -> correctEmailFormat($this -> userEmail)) {
		//	array_push($validationErrors, array("inputID" => 3,
		//										'errorMessage' => 'Needs to be in email format'));
		//}
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	private function CartTotal() {
		return $_SESSION['CurrentCartObject']['results']['total'];
	}
	
	private function GetGrandTotal() {
		return $_SESSION['CurrentCartObject']['results']['total'] + $this -> ShippingRate + $this -> TaxRate;
	}
	
	public function SaveOrderChargeCustomer() {
		$stripeHandler = new StripeHandler();
		
		$stripeHandler -> CustomerID = $this -> CustomerStripeID;
		$stripeHandler -> ChargeCustomer($this -> GetGrandTotal());
		$insertData = array();
		$insertData['firstName'] = $this -> firstName;
		$insertData['lastName'] = $this -> LastName;
		$insertData['phoneNumber'] = str_replace('-', '', filter_var($this -> PhoneNumber, FILTER_SANITIZE_NUMBER_INT));
		$insertData['orderEmail'] = $this -> Email;
		$insertData['taxRates'] = $this -> TaxRate;
		$insertData['shippingRate'] = $this -> ShippingRate;
		
		$insertData['cartTotal'] = $this -> CartTotal();
		$insertData['grandTotal'] = $this -> GetGrandTotal();
		
		$insertData['stripeCustomerID'] = $this -> CustomerStripeID;
		$insertData['stripeChargeID'] = $stripeHandler -> GetChargeID();
		$insertData['storePickup'] = $this -> StorePickup;
		
		if($this -> StorePickup == 1) {
			$insertData['shippingAddress'] = $this -> ShippingAddress;
			$insertData['shippingCity'] = $this -> ShippingCity;
			$insertData['shippingState'] = $this -> ShippingState;
			$insertData['shippingZip'] = $this -> ShippingZip;	
		} else {
			$insertData['shippedStatus'] = 1;	
		}

		$this -> SendEmail();
		
		$this -> db -> insert('ecommerceorders', $insertData);
		unset($_SESSION['CurrentCartObject']);
		unset($_SESSION['NewOrder']);
		
		$this -> json -> outputJqueryJSONObject('redirect', PATH . 'cart/thanks');	
	}
	
	
	public function Save() {
		$stripeHandler = new StripeHandler();
		
		$insertData = array();
		$insertData['firstName'] = $this -> firstName;
		$insertData['lastName'] = $this -> LastName;
		$insertData['phoneNumber'] = str_replace('-', '', filter_var($this -> PhoneNumber, FILTER_SANITIZE_NUMBER_INT));
		$insertData['orderEmail'] = $this -> Email;
		$insertData['taxRates'] = $this -> TaxRate;
		$insertData['shippingRate'] = $this -> ShippingRate;

		$insertData['cartTotal'] = $this -> CartTotal();
		$insertData['grandTotal'] = $this -> GetGrandTotal();
		
		$insertData['DateEntered'] = parent::TimeStamp();
		
		$insertData['orderSummary'] = json_encode($this -> orderItems);
		
		
		$insertData['storePickup'] = $this -> StorePickup;
		$stripeHandler -> StripeToken = $this -> StripeToken;
		$stripeHandler -> userEmail = $this -> Email;
		
		
		if($this -> CreateAccount == 1) {
			$stripeHandler -> CreateCustomer();
			$stripeHandler -> Charge($this -> GetGrandTotal(), true);
			$insertData['stripeCustomerID'] = $stripeHandler -> GetCustomerID();
			$insertData['stripeChargeID'] = $stripeHandler -> GetChargeID();	
				
			$newAccount = new EcommerceUserAccount();
			$newAccount -> StripeCustomerID = $stripeHandler -> GetCustomerID();
			$newAccount -> FirstName = $this -> firstName;
			$newAccount -> LastName = $this -> LastName;		
			$newAccount -> Email = $this -> Email;
			$newAccount -> StripeCustomerID = $stripeHandler -> GetCustomerID();
			$newAccount -> AccountCreatedSource = 1;
			$newAccount -> Save();	
				
		}  else {
			$stripeHandler -> Charge($this -> GetGrandTotal());
			$insertData['stripeChargeID'] = $stripeHandler -> GetChargeID();	
		}
		
		$this -> SendEmail();
		
		
		if($this -> StorePickup == 1) {
			$insertData['shippingAddress'] = $this -> ShippingAddress;
			$insertData['shippingCity'] = $this -> ShippingCity;
			$insertData['shippingState'] = $this -> ShippingState;
			$insertData['shippingZip'] = $this -> ShippingZip;	
		} else {
			$insertData['shippedStatus'] = 1;	
		}
		
		
		
		
		$this -> db -> insert('ecommerceorders', $insertData);
		unset($_SESSION['CurrentCartObject']);
		unset($_SESSION['NewOrder']);
		
		$this -> json -> outputJqueryJSONObject('redirect', PATH . 'cart/thanks');	
		
	}

	private function SendEmail() {
		if(LIVE_SITE == true) {
			$settings = EcommerceSettingGeneral::Init();
			$receiptArray = array();
			$recieptItems = array();
			
			foreach($this -> orderItems as $itemSingle) {
				$product = EcommerceProduct::WithID($itemSingle['product_id']);
				
				if($itemSingle['colorSelected'] != 0) {
					$color = EcommerceColor::WithID($itemSingle['colorSelected']);
				}
				
				if($itemSingle['sizeSelected'] != 0) {
					$size = EcommerceSize::WithID($itemSingle['sizeSelected']);
				}
				
				
				array_push($recieptItems, array('product-name' => $product -> productName,
												'product-price' => $itemSingle['price'],
												'product-qty' => $itemSingle['quantity'],
												'product-color' => ($itemSingle['colorSelected'] != 0 ? $color -> ColorText : 0),
												'product-size' => ($itemSingle['sizeSelected'] != 0 ? $size -> SizeText : 0)));	
			}
			
			
			if($this -> StorePickup == 1) {				
				$receiptArray['street'] = $this -> ShippingAddress; 
				$receiptArray['city'] = $this -> ShippingCity;
				$receiptArray['state'] = $this -> ShippingState;
				$receiptArray['zip'] = $this -> ShippingZip;	
			}
			
			$email = new Email();
			$email -> to = $this -> Email;
			$email -> Bcc = $settings -> EmailNotifications;
			$email -> subject = "Your Receipt";
			$email -> EcommerceReceipt($recieptItems, 
									   $receiptArray, 
									   $this -> CartTotal(), 
									   $this -> ShippingRate, 
									   $this -> TaxRate, 
									   number_format($this -> GetGrandTotal(), 2));
		}
	}

	public function SavePersistentOrder() {
		Session::init();
		$newOrderArray = array();
		
		$newOrderArray['create-account'] = $this -> CreateAccount;
		$newOrderArray['user-first-name'] = $this -> firstName;
		$newOrderArray['user-first-last'] = $this -> LastName;;
		$newOrderArray['user-email'] = $this -> Email;
		$newOrderArray['user-phone'] = $this -> PhoneNumber;
		
		$newOrderArray['ship-option'] = $this -> StorePickup;
		
		$newOrderArray['ship-address'] = $this -> ShippingAddress;
		$newOrderArray['ship-city'] = $this -> ShippingCity;
		$newOrderArray['ship-state'] = $this -> ShippingState;
		$newOrderArray['ship-zip'] = $this -> ShippingZip;
		$newOrderArray['customer-id'] = $this -> CustomerStripeID;		
		$newOrderArray['create-account'] = $this -> CreateAccount;
		
		if(isset($this -> StripeToken)) {
			$newOrderArray['token'] = $this -> StripeToken;
		}
		
		$_SESSION['NewOrder'] = $newOrderArray;
		
		$this -> json -> outputJqueryJSONObject('redirect', PATH . 'cart/confirm');	
	}
	
	




}