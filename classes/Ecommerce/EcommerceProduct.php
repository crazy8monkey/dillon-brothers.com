<?php

class EcommerceProduct extends BaseObject {
	
	private $_seoName;
	private $_productID;
	
	public $productName;
	public $SKUNumber;
	public $Description;
	public $Price;
	public $MSRPPrice;
	public $ModifiedDate;
	public $PublishedDate;
	public $FolderName;
	public $ProductCategory;
	public $ProductCategoryLink;
	public $AverageRating;
	
	public $Photos = array();
	public $Colors = array();
	public $Sizes = array();
	
	public $ProductSelect;
	public $ColorSelect;
	public $SizeSelect;
	
	public $CartKey;
	public $ProductReviews = array();
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }
	
	public function GetID() {
		return $this -> _productID;
	}
	
	public static function WithID($id) {
		$instance = new self();
        $instance-> ProductSelect = $id;
        $instance->GetSelfByID();
        return $instance;
	}
	
	
    public static function WithSEOName($seoName) {
        $instance = new self();
        $instance-> _seoName = $seoName;
        $instance->loadByName();
        return $instance;
    }
	
	private function GetSelfByID() {
		$sth = $this -> db -> prepare('SELECT *, (SELECT ROUND(AVG(rating), 1)FROM reviews WHERE reviewActive = 1 AND reviewItemID = :productID) AverageRating FROM ecommerceproducts LEFT JOIN ecommercesettingcategories ON ecommerceproducts.productCategory = ecommercesettingcategories.settingCategoryID WHERE productID = :productID');
        $sth->execute(array(':productID' => $this -> ProductSelect));
		$record = $sth -> fetch();
		$this -> fill($record);
	}
	
	protected function loadByName() {
		$sth = $this -> db -> prepare('SELECT *, (SELECT ROUND(AVG(rating), 1)FROM reviews WHERE reviewActive = 1 AND reviewItemID = ecommerceproducts.productID) AverageRating FROM ecommerceproducts LEFT JOIN ecommercesettingcategories ON ecommerceproducts.productCategory = ecommercesettingcategories.settingCategoryID WHERE productSEOName = :productSEOName');
        $sth->execute(array(':productSEOName' => $this -> _seoName));	
		$record = $sth -> fetch();
		
		if($sth->rowCount() > 0) {
		 	$this -> fill($record);
			$this -> GetInventoryPhotos();
			$this -> GetProductColors();
			$this -> GetProductSizes();
			$this -> GetProductReviews();
		} else {
			throw new Exception('Product Record does not exist (Value: ' . $this -> _seoName . ')');	
		}
	}

    protected function fill(array $row){
    	$this -> _productID = $row['productID'];
    	$this -> productName = $row['productName'];
		$this -> SKUNumber = $row['SKUNumber'];
		$this -> FolderName = $row['productFolderName'];
		$this -> Description = $row['productDescription'];
		$this -> Price = $row['productPrice'];
		$this -> ModifiedDate = $row['productModifiedTime'];
		$this -> PublishedDate = $row['productPublishedDate'];
		$this -> ProductCategory = $row['settingCategoryName'];
		$this -> ProductCategoryLink = $row['settingCategorySEOName'];
		$this -> MSRPPrice = $row['productMSRP'];
		$this -> AverageRating = $row['AverageRating'];
    }	

	public function GetURL() {
		return PATH . 'shop/product/' . $this -> _seoName;
	}
	
	private function GetInventoryPhotos() {
		$photos = new EcommercePhotosList();
		$this -> Photos = $photos -> PhotosByProduct($this -> _productID);
	}
	
	private function GetProductColors() {
		$colors = new EcommerceColorList();
		$this -> Colors = $colors -> ByProduct($this -> _productID);
	}
	
	private function GetProductSizes() {
		$sizes = new EcommerceSizeList();
		$this -> Sizes = $sizes -> ByProduct($this -> _productID);
	}
	
	public function Validate() {
		$validationErrors = array();
		
	
		if(isset($this -> ColorSelect)) {
			if($this -> ColorSelect == 0) {
				array_push($validationErrors, array("inputID" => 'ColorSelectedError',
													'errorMessage' => 'Required'));
			}	
		}
		

		if(isset($this -> SizeSelect)) {
			if($this -> SizeSelect == 0) {
				array_push($validationErrors, array("inputID" => 'SizeSelectedError',
													'errorMessage' => 'Required'));
			}	
		}

		
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	private function GenerateCartKey() {
		if(isset($this -> ColorSelect) && isset($this -> SizeSelect)) {
			$cartKey = sha1($this -> ProductSelect . $this -> ColorSelect . $this -> SizeSelect);
		} else if(isset($this -> ColorSelect)) {
			$cartKey = sha1($this -> ProductSelect . $this -> ColorSelect);
		} else if(isset($this -> SizeSelect)) {
			$cartKey = sha1($this -> ProductSelect . $this -> SizeSelect);
		} else {
			$cartKey = sha1($this -> ProductSelect);
		}
		return $cartKey;
	}
	
	public function AddToCart() {
		$this -> GetSelfByID();
		Session::init();
		if(isset($_SESSION['CurrentCartObject'])) {
			$cartObject = $_SESSION['CurrentCartObject'];
			$cartItems = $_SESSION['CurrentCartObject']['results']['cartItems'];
		} else {
			$cartObject = array();
			$cartItems = array();
		}
		
		$cartKey = $this -> GenerateCartKey();
		
		if(isset($this -> SizeSelect)) {
			$productSizes = array();	
			$sizes = new EcommerceSizeList();
			$productSizes = $sizes -> ByProduct($this -> ProductSelect);
			
			$sizekey = array_search($this -> SizeSelect, array_column($productSizes, 'relatedSizeID'));
			
			$partNumber	= $productSizes[$sizekey]['PartNumber'];
		} else {
			$partNumber	= $this -> SKUNumber;
		}
				
		$newCartItem = array('product_id' => $this -> ProductSelect, 
							 'quantity' => 1, 
							 'price' => $this -> Price, 
							 'colorSelected' => (isset($this -> ColorSelect) ? $this -> ColorSelect : 0), 
							 'sizeSelected' => (isset($this -> SizeSelect) ? $this -> SizeSelect : 0),
							 'PartNumber' => $partNumber);
		
		$cartItems[$cartKey] = $newCartItem;
		
		$count = 0;
		$total = 0;
		foreach($cartItems as $itemSingle) {
			$count += $itemSingle['quantity'];	
			$total += $itemSingle['quantity'] * $itemSingle['price'];
		}
		$_SESSION['CurrentCartObject'] = array('results' => array('cartItems' => $cartItems,
											   					  'count' => $count,
																  'total' => number_format($total, 2)));
		
		return $_SESSION['CurrentCartObject'];
	}

	public function RemoveCartItem() {
		Session::init();
		$cartItems = $_SESSION['CurrentCartObject']['results']['cartItems'];
		
		if(isset($cartItems[$this -> CartKey])) {
			unset($cartItems[$this -> CartKey]);
			
			$count = 0;
			$total = 0;
			
			foreach($cartItems as $itemSingle) {
				$count += $itemSingle['quantity'];	
				$total += $itemSingle['quantity'] * $itemSingle['price'];
			}
			
			$_SESSION['CurrentCartObject'] = array('results' => array('cartItems' => $cartItems,
												   					  'count' => $count,
																	  'total' => number_format($total, 2)));
				
		}
				
		return $_SESSION['CurrentCartObject'];
	}
	
	public function GetProductPhoto() {
		return PHOTO_URL . $this -> FolderName . '/' . $this -> Photos[0]['PhotoName'] . '-l.' . $this -> Photos[0]['PhotoExt'];
	}

	private function GetProductReviews() {
		$reviewsList = new ReviewList();
		$this -> ProductReviews = $reviewsList -> ActiveReviewsByEcommerceProduct($this -> _productID);
	}



}