<?php

class EcommerceSize extends BaseObject {
	
	private $_id;
	public $SizeText;

    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    public function __construct() {
        parent::__construct();
    }
	
	public static function WithID($id) {
		$instance = new self();
        $instance-> _id = $id;
        $instance->GetSelfByID();
        return $instance;
	}
		
	private function GetSelfByID() {
		$sth = $this -> db -> prepare('SELECT * FROM ecommercesettingsizes WHERE settingSizeID = :settingSizeID');
        $sth->execute(array(':settingSizeID' => $this -> _id));
		$record = $sth -> fetch();
		$this -> fill($record);
	}
	

    protected function fill(array $row){
    	$this -> SizeText = $row['settingSizeText'];
    }	
	


}