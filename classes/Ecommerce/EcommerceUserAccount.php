<?php

class EcommerceUserAccount extends BaseObject {
	
	public $_id;
	private $_initialPassword;
	private $_password;
	
	public $FirstName;
	public $LastName;		
	public $Email;
	public $Phone;
	public $Password;
	public $SecurityToken;
	public $StripeCustomerID;
	
	private $_salt;
	
	//1 = from checkout page
	//2 = created from shop main Page
	//3 = submitted from create new credentials
	//4 = from signup page
	public $AccountCreatedSource;
	
    public function __sleep() {
        parent::__sleep();
		return array('_id', 'FirstName', 'LastName', 'Email', 'StripeCustomerID');
    }

    public function __wakeup() {
        parent::__wakeup();
		return array('_id', 'FirstName', 'LastName', 'Email', 'StripeCustomerID');
    }

    public function __construct() {
        parent::__construct();
    }

    public static function WithID($id) {
        $instance = new self();
		$instance -> _id = $id;
        $instance -> loadById();
        return $instance;
    }
	
	protected function loadById() {
		$sth = $this -> db -> prepare('SELECT * FROM ecommerceuseraccounts WHERE ecommerceAccountID = :ecommerceAccountID');
	    $sth->execute(array(':ecommerceAccountID' => $this -> _id));	
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
	public function GetID() {
		return $this -> _id;
	}

	protected function fill(array $row){
		$this -> FirstName = $row['accountFirstName'];
		$this -> LastName = $row['accountLastName'];
		$this -> Email = $row['accountEmail'];
		$this -> StripeCustomerID = $row['stripeCustomerID'];
    	$this -> SecurityToken = $row['securityToken'];
		$this -> Phone = $row['accountPhone'];
		$this -> _salt = $row['salt'];
		$this -> _password = $row['password'];
    }	
	
	public function Validate() {
		$validationErrors = array();
		
		
		if($this -> AccountCreatedSource == 3) {
			
			if($this -> validate -> emptyInput($this -> _initialPassword)) {
				array_push($validationErrors, array('inputID' => 2,
													'errorMessage' => 'Required'));
			} else if($this -> validate -> passwordLength($this -> _initialPassword, 6)) {
				array_push($validationErrors, array("inputID" => 2,
													'errorMessage' => $this -> msg -> passwordLengthMessage("6")));
			}
		}
				
		if($this -> AccountCreatedSource == 4) {
			if(!isset($this->_id)) {
	            $emailCheck = $this->db->prepare('SELECT accountEmail FROM ecommerceuseraccounts WHERE accountEmail = ?');
	            $emailCheck->execute(array($this->Email));
	
	        }else{
	            $emailCheck = $this->db->prepare('SELECT accountEmail FROM users WHERE accountEmail = :accountEmail AND ecommerceAccountID <> :ecommerceAccountID');
	            $emailCheck->execute(array(':ecommerceAccountID' => $this->_id, ':accountEmail' => $this->Email));
	        }
			
			
			
			if($this -> validate -> emptyInput($this -> FirstName)) {
				array_push($validationErrors, array('inputID' => 1,
													'errorMessage' => 'Required'));
			}
			
			if($this -> validate -> emptyInput($this -> LastName)) {
				array_push($validationErrors, array('inputID' => 2,
													'errorMessage' => 'Required'));
			}
			
			if($this -> validate -> emptyInput($this -> Email)) {
				array_push($validationErrors, array('inputID' => 3,
													'errorMessage' => 'Required'));
			} else if($this -> validate -> correctEmailFormat($this->Email)) {
				array_push($validationErrors, array("inputID" => 3,
													'errorMessage' => $this -> msg -> emailFormatRequiredMessage()));			
	        } else if($emailCheck -> fetchAll()) {
				array_push($validationErrors, array("inputID" => 3,
													'errorMessage' => $this->msg->duplicateUsername()));
			}
			
			
			if($this -> validate -> emptyInput($this -> Phone)) {
				array_push($validationErrors, array('inputID' => 4,
													'errorMessage' => 'Required'));
			}
			
		
			if($this -> validate -> emptyInput($this -> _initialPassword)) {
				array_push($validationErrors, array('inputID' => 5,
													'errorMessage' => 'Required'));
			} else if($this -> validate -> passwordLength($this -> _initialPassword, 6)) {
				array_push($validationErrors, array("inputID" => 5,
													'errorMessage' => $this -> msg -> passwordLengthMessage("6")));
			}
			
			
			
		}		
				
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function ValidateAccountInfo() {
		$validationErrors = array();
				
		$emailCheck = $this->db->prepare('SELECT accountEmail FROM ecommerceuseraccounts WHERE accountEmail = :accountEmail AND ecommerceAccountID <> :ecommerceAccountID');
	    $emailCheck->execute(array(':ecommerceAccountID' => $this->_id, ':accountEmail' => $this->Email));
			
			
		if($this -> validate -> emptyInput($this -> FirstName)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Required'));
		}
			
		if($this -> validate -> emptyInput($this -> LastName)) {
			array_push($validationErrors, array('inputID' => 2,
												'errorMessage' => 'Required'));
		}
			
		if($this -> validate -> emptyInput($this -> Email)) {
			array_push($validationErrors, array('inputID' => 3,
												'errorMessage' => 'Required'));
		} else if($this -> validate -> correctEmailFormat($this->Email)) {
			array_push($validationErrors, array("inputID" => 3,
												'errorMessage' => $this -> msg -> emailFormatRequiredMessage()));			
	    } else if($emailCheck -> fetchAll()) {
			array_push($validationErrors, array("inputID" => 3,
												'errorMessage' => $this->msg->duplicateUsername()));
		}

		if($this -> validate -> emptyInput($this -> Phone)) {
			array_push($validationErrors, array('inputID' => 6,
												'errorMessage' => 'Required'));
		}
			
				
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function ValidateSecurityToken() {
		if(isset($this -> SecurityToken)) {
			return true;	
		} else {
			$this -> json -> outputJqueryJSONObject('SecurityTokenPresent', false);	
			return false;
		}
	}
	
	public function SaveSignup() {
		//$securityToken = Hash::generateSecurityToken();
		$newAccountArray = array();
		$newAccountArray['accountFirstName'] = $this -> FirstName;
		$newAccountArray['accountLastName'] = $this -> LastName;
		$newAccountArray['accountEmail'] = $this -> Email;
		$newAccountArray['accountPhone'] = $this -> Phone;
		//$newAccountArray['securityToken'] = $securityToken;
				
		$newAccountArray['password'] = $this -> _password;	
		$newAccountArray['salt'] = $this -> _salt;
		
		$stripe = new StripeHandler();
		$stripe -> StripeToken = $this -> SecurityToken;
		$stripe -> userEmail = $this -> Email;			
		$stripe -> CreateCustomer();
		
		$newAccountArray['stripeCustomerID'] = $stripe -> CustomerID;		
		$newAccountArray['accountCreatedDate'] = parent::TimeStamp();	
								
		$newAccountID = $this -> db -> insert('ecommerceuseraccounts', $newAccountArray);
		$this -> StartUserSession($newAccountID);
		
		$this -> json -> outputJqueryJSONObject('AccountSaved', true);
	}


	public function SaveSingleAccountInfo() {
		Session::init();
		$updatedData['accountFirstName'] = $this -> FirstName;
		$updatedData['accountLastName'] = $this -> LastName;	
		$updatedData['accountEmail'] = $this -> Email;	
		$updatedData['accountPhone'] = str_replace('-', '', filter_var($this -> Phone, FILTER_SANITIZE_NUMBER_INT));			
		
		$this->db->update('ecommerceuseraccounts', $updatedData, array('ecommerceAccountID' => $this -> _id));
		
		$this -> json -> outputJqueryJSONObject('AccountUpdated', true);
	}
	
	public function SaveBillingInfo() {
		$stripeHandler = new StripeHandler();
		$stripeHandler -> CustomerID = $this -> StripeCustomerID;
		$stripeHandler -> StripeToken = $this -> SecurityToken;
		$stripeHandler -> UpdateCustomer();
		
		$this -> json -> outputJqueryJSONObject('BillingSaved', true);
	}

	private function StartUserSession($accountID) {
		Session::init();	
		Session::set('EcommerceUserAccountLoggedIn', true);
		
		Session::set('EcommerceUserAccount', EcommerceUserAccount::WithID($accountID));	
		
	}
	
	public function Save() {
		if(isset($this -> _id)) {
			$updatedData = array();
				
			$updatedData['accountFirstName'] = $this -> FirstName;
			$updatedData['accountLastName'] = $this -> LastName;	
			$updatedData['accountEmail'] = $this -> Email;	
				
			if($this -> AccountCreatedSource == 3) {
				$updatedData['password'] = $this -> _password;
				$updatedData['salt'] = $this -> _salt; 
				$updatedData['securityToken'] = '';  				
			}	
						
			$this->db->update('ecommerceuseraccounts', $updatedData, array('ecommerceAccountID' => $this -> _id));
		} else {
			$duplicateEmailCheck = $this -> db -> prepare('SELECT * FROM ecommerceuseraccounts WHERE accountEmail = :accountEmail');
			$duplicateEmailCheck -> execute(array(':accountEmail' => $this -> Email));		
			
			//if an email doesn't exist
			if($duplicateEmailCheck->rowCount() == 0) {
				$securityToken = Hash::generateSecurityToken();
				$newAccountArray = array();
				$newAccountArray['accountFirstName'] = $this -> FirstName;
				$newAccountArray['accountLastName'] = $this -> LastName;
				$newAccountArray['accountEmail'] = $this -> Email;
				$newAccountArray['securityToken'] = $securityToken;
				
				
				if($this -> AccountCreatedSource == 4) {
					$newAccountArray['password'] = $this -> _password;	
					$newAccountArray['salt'] = $this -> _salt;
					$stripe = new StripeHandler();
					$stripe -> StripeToken = $this -> SecurityToken;
					$stripe -> userEmail = $this -> Email;
					
					$stripe -> CreateCustomer();
					$newAccountArray['stripeCustomerID'] = $stripe -> CustomerID;
				} else {
					$newAccountArray['stripeCustomerID'] = $this -> StripeCustomerID;	
				}
				
				$newAccountArray['accountCreatedDate'] = parent::TimeStamp();	
				
								
				$newUser = $this -> db -> insert('ecommerceuseraccounts', $newAccountArray);
				
				if($this -> AccountCreatedSource == 1) {
					if(LIVE_SITE == true) {
						$email = new Email();
						$email -> to = $this -> Email;
						$email -> subject = "Create your Dillon Brothers Ecommerce Account";
						$url = PATH . 'shop/newcredentials?u=' . $newUser . '&token=' . $securityToken;
						$email -> EcommerceCreateCredentials($url);	
					}
				}
			}
																				
																				
		}
				
	}

	public function AccountSaveResponse() {
		$this -> json -> outputJqueryJSONObject('AccountSaved', true);
	}

	public function userLogin() {
		$sth = $this->db->prepare("SELECT * FROM ecommerceuseraccounts WHERE accountEmail = :accountEmail");
		$sth->execute(array(':accountEmail' => $this -> Email));
		
		$data = $sth->fetch();							 
		$count =  $sth->rowCount();
		
		if ($count > 0) {
			
			$userLogin = EcommerceUserAccount::WithID($data['ecommerceAccountID']);
            if($userLogin -> ValidatePasswordAttempt($this -> Password)) {
            		
            	$this -> StartUserSession($data['ecommerceAccountID']);
				//redirect to main app section
				$this -> json -> outputJqueryJSONObject('redirect', PATH . "shop");	
				
				
			} else {
				$this -> json -> outputJqueryJSONObject('errorMessage', "Wrong Email and password combination");
			}
		} else {
			$this -> json -> outputJqueryJSONObject('errorMessage', "Wrong Email and password combination. No Email");			
		}
	}

	   public function ValidatePasswordAttempt($pass) {
        return  Hash::create('sha256', $pass, $this->_salt) == $this->_password;
    }

	public function SetPassword($password) {
        $this->_salt = Hash::generateSalt();
        $this->_initialPassword = $password;
        $this->_password = Hash::create('sha256', $password, $this->_salt);
    }

}