<?php

class EcommerceColor extends BaseObject {
	
	private $_id;
	public $ColorText;

    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    public function __construct() {
        parent::__construct();
    }
	
	public static function WithID($id) {
		$instance = new self();
        $instance-> _id = $id;
        $instance->GetSelfByID();
        return $instance;
	}
		
	private function GetSelfByID() {
		$sth = $this -> db -> prepare('SELECT * FROM ecommercesettingcolors WHERE settingColorID = :settingColorID');
        $sth->execute(array(':settingColorID' => $this -> _id));
		$record = $sth -> fetch();
		$this -> fill($record);
	}
	

    protected function fill(array $row){
    	$this -> ColorText = $row['settingColorText'];
    }	
	


}