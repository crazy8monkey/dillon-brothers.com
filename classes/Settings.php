<?php

class SettingsObject extends BaseObject {

	public $DonationEmail;
	
    public function __sleep()
    {
        parent::__sleep();
    
    }

    public function __wakeup()
    {
        parent::__wakeup();
	}


    public function __construct(){
        parent::__construct();
	}

    public static function Init(){
        $instance = new self();
        $instance->loadSettings();
        return $instance;
    }


    protected function loadSettings() {
        $sth = $this -> db -> prepare('SELECT * FROM settings WHERE settingsID = 1');
        $sth -> execute();
        $record = $sth -> fetch();
        $this->fill($record);
    }

    protected function fill(array $row){
		$this -> DonationEmail = $row['DonationEmail'];
	}

	

}