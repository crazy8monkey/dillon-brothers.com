<?php


class ShortLinks extends BaseObject {
	
	private $_shortLinkSEOName;
	private $_blogID;
	private $_ShortLinkID;
	private $_replaceShortLink = false;
	
	public $BlogID;
	public $EventID;
	public $NewShortLink;
	public $ShortCodeLinkActive;
	public $ActionStatus;
	public $OriginatedSource;
	public $SubDomain;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
		
	public static function WIthSEOName($name) {
        $instance = new self();
        $instance -> _shortLinkSEOName = $name;
        $instance -> loadRow();
        return $instance;
    }
		
	protected function loadRow() {
		$sth = $this -> db -> prepare('SELECT * FROM shortlinks WHERE shortUrl = :shortUrl');
        $sth->execute(array(':shortUrl' => $this-> _shortLinkSEOName));	
		
		if($sth->rowCount() > 0) {
			$record = $sth -> fetch();
		    $this->fill($record);
		} else {
			throw new Exception('Short Link Value Does not Exists (Value: ' . $this->_shortLinkSEOName . ')');	
		}
    }
	
    protected function fill(array $row){
    	$this -> _ShortLinkID = $row['shortLinkID'];
    	$this -> BlogID = $row['relatedBlogID'];
		$this -> EventID = $row['relatedEventID'];
		$this -> ShortCodeLinkActive = $row['shortLinkActive'];
		$this -> OriginatedSource = $row['OriginatedSource'];
		$this -> ActionStatus = $row['Action'];
		$this -> SubDomain = $row['isSubDomain'];
    }
	
	
	
	

	
	

		

}