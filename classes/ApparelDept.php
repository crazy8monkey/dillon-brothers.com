<?php


class ApparelDept extends BaseObject {
	
	public $Store;
	public $FirstName;
	public $LastName;
	public $HomePhone;
	public $CellPhone;
	public $EmailAddress;
	public $StreetAddress;
	public $City;
	public $state;
	public $Zip;
	//vehicle information
	public $AdditionalComments;
	
	public $CaptchaCheck;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public function Validate() {
		$validationErrors = array();
		
		$captcha = new GoogleCaptaHandler();
		$captcha -> response = $_POST['g-recaptcha-response'];
		
		$googleCheckResponse = json_decode($captcha -> GetResponse(), true);
		
		if(!isset($this -> Store)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Please select a store'));
		}
		
		//empty first name
		if($this -> validate -> emptyInput($this -> FirstName)) {
			array_push($validationErrors, array("inputID" => 2,
												'errorMessage' => 'Required'));	
		}

		if($this -> validate -> emptyInput($this -> LastName)) {
			array_push($validationErrors, array("inputID" => 3,
												'errorMessage' => 'Required'));	
		}
		
		if($this -> validate -> emptyInput($this -> HomePhone)) {
			array_push($validationErrors, array("inputID" => 4,
												'errorMessage' => 'Required'));	
		}
		
		
		if(!empty($this -> EmailAddress)) {
			if($this -> validate -> correctEmailFormat($this -> EmailAddress)) {
				array_push($validationErrors, array("inputID" => 12,
													'errorMessage' => 'Needs to be in email format'));		
			}
			
		}
		
		if(!$googleCheckResponse['success']) {
			array_push($validationErrors, array("inputID" => 13,
												'errorMessage' => 'Required'));
		}
				
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}

	public function Save() {
		$storeLocation = explode(":",$this -> Store[0]);
		$emailContent = array();
		$emailContent['StoreLocation'] = $storeLocation[1];
		$emailContent['ContactInformation'] = array("FullName" => $this -> FirstName . ' ' . $this -> LastName,
													"HomePhone" => $this -> HomePhone,
													"CellPhone" => $this -> CellPhone,
													"EmailAddress" => $this -> EmailAddress,
													"Address" => $this -> StreetAddress . '<br />' . $this -> City . ', '. $this -> state . ' ' .$this -> Zip);
		
		$emailContent['AdditionalInformation'] = $this -> AdditionalComments;
		
			
		$email = new EmailLeadObject();
		$email -> VehicleInfo = "NA";
		$email -> StoreID = $storeLocation[0];
		$email -> Source = 1;
		$email -> type = 9;
		$email -> Content = json_encode($emailContent);	
		$email -> Save();
		
		if(LIVE_SITE == TRUE) {
			$store = Store::WithID($storeLocation[0]);
			$serviceContent = array();	
			$serviceContent['StoreName'] = $store -> StoreName . ' ' .$storeLocation[1];
			$serviceContent['FullName'] = $this -> FirstName . ' ' . $this -> LastName;
			$serviceContent['HomePhone'] = $this -> HomePhone;
			$serviceContent['CellPhone'] = $this -> CellPhone;
			$serviceContent['Address'] = $this -> StreetAddress . '<br />' . $this -> City . ', '. $this -> state . ' ' .$this -> Zip;
			$serviceContent['Comments'] = $this -> AdditionalComments;
			
			
			
			$sendEmail = new Email();
			$sendEmail -> subject = "New Apparel Department Request";
			$sendEmail -> to = $store -> ApparelEmail;
			$sendEmail -> ApparelRequest($serviceContent);	
		}
		
		
		$this -> json -> outputJqueryJSONObject('testResponse', $emailContent);	
		
	}
	
}