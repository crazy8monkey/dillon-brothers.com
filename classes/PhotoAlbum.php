<?php


class PhotoAlbum extends BaseObject {
	
	private $_id;
	private $_uploadProgress;
	private $_albumSEOName;
	
	
	public $albumID; 
		
	public $folderName;
	public $year;
	
	public $_albumName;
	
	public $fileUploads;
	public $photoName;
	public $photoExt;
	public $altText;
	public $titleText;
	
	
	public $photoCount;
	public $UploadCount;
	
	public $StoreName;
	
	public $photoNames;
	public $photoAltTexts;
	public $photoTitleTexts;
	
	public $MainPhotoAlbumID;
	public $ParentDirectory;

	public $folerAlbumName;	
	
	//1 = event album
	//2 = misc album
	public $AlbumType;
	public $albumSEOName;
	
	public $modifiedDate;
	public $publishedDate;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithSEOName($year, $typeValue, $albumSEOName) {
        $instance = new self();
        $instance-> _albumSEOName = $albumSEOName;
		$instance -> ParentDirectory = $year;
		$instance -> AlbumType = $typeValue;
        $instance->loadByName();
        return $instance;
    }
	
	protected function loadByName() {
    	$sth = $this -> db -> prepare('SELECT * FROM photoalbums LEFT JOIN photos ON photoalbums.albumThumbNail = photos.photoID WHERE albumSEOurl = :albumSEOurl AND ParentDirectory = :ParentDirectory AND AlbumType = :AlbumType');
        $sth->execute(array(':albumSEOurl' => $this->_albumSEOName, 
        					":ParentDirectory" => $this -> ParentDirectory,
							":AlbumType" => $this -> AlbumType));
							
		if($sth->rowCount() > 0) {
			$record = $sth -> fetch();
        	$this->fill($record);
		} else {
			throw new Exception('Photo Album Record does not exist (album seo url: ' . $this->_albumSEOName . ' - parent directory: ' . $this -> ParentDirectory.' - album type: ' . $this -> AlbumType. ')');	
		//exit;
		}					
							
        
    }
	

    protected function fill(array $row){
    	$this -> albumID = $row['albumID'];
		$this -> year = $row['ParentDirectory'];
		$this -> _albumName = $row['albumName'];
		$this -> createdDate = $row['createdDate'];
		$this -> createdTime = $row['createdTime'];
		$this -> UploadCount = $row['UploadCount'];
		$this -> AlbumType = $row['AlbumType'];
		$this -> MainPhotoAlbumID = $row['photoID'];
		$this -> folerAlbumName = $row['albumFolderName'];
		$this -> photoName = $row['photoName'];
		$this -> photoExt = $row['ext'];
		$this -> modifiedDate = $row['albumModifedDate'];
		$this -> publishedDate = $row['albumPublishedDate'];
		$this -> albumSEOName = $row['albumSEOurl'];
    }
	
	


	


}