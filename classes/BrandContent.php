<?php


class BrandContent extends BaseObject {
	
	private $_brandSEOName;
	private $_pageSEOName;
	
	public $Content;
	public $LandingPageTitle;
	public $ParentBrandName;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	
	public static function WithSEOName($brandSEO, $postName) {
        $instance = new self();
		$instance -> _brandSEOName = $brandSEO;
		$instance -> _pageSEOName = $postName;
        $instance->loadByName();
        return $instance;
    }
	
		
	protected function loadByName() {
		$sth = $this -> db -> prepare('SELECT * FROM (
													SELECT (
													    CASE
													    	WHEN brandpostcontent.IsOEMPost = 1 THEN brands.brandNameSEOUrl
													    	ELSE partaccessorybrand.brandSEOUrl
													    END 
													    
													    ) BrandSEOName,
													    (
													     CASE
													    	WHEN brandpostcontent.IsOEMPost = 1 THEN CONCAT(brands.BrandName, " Vehicles")
													    	ELSE partaccessorybrand.brandName
													     END 
													    ) BrandName,
													    brandpostcontent.ContentTitle,
													    brandpostcontent.contentUrl,
													    brandpostcontent.Content,
													    brandpostcontent.isPageActive FROM brandpostcontent 
													    	LEFT JOIN brands ON brandpostcontent.OEMBrandID = brands.BrandID 
													    	LEFT JOIN partaccessorybrand ON brandpostcontent.PartBrandID = partaccessorybrand.partAccessoryBrandID) BrandWeCarryContent WHERE 
													    		contentUrl = :contentUrl AND 
													    		BrandSEOName = :BrandSEOName AND 
													    		isPageActive = 1');
        
        $sth->execute(array(':contentUrl' => $this -> _pageSEOName,
							':BrandSEOName' => $this -> _brandSEOName));	
		
		if($sth->rowCount() > 0) {
			$record = $sth -> fetch();
		    $this->fill($record);
			//return true;
		} else {
			throw new Exception('Landing Page does not exist (content url: ' . $this->_pageSEOName . ' / Brand SEO Url: ' . $this -> _brandSEOName. ')');	
		}		
    }

	public function GetBrandUrl() {
		return $this -> _brandSEOName;	
	}
	
    protected function fill(array $row){
    	$this -> Content = $row['Content'];
		$this -> LandingPageTitle = $row['ContentTitle'];
		$this -> ParentBrandName = $row['BrandName'];
    }
	
	

}