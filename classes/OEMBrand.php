<?php


class OEMBrand extends BaseObject {
	
	private $_brandID;
	
	public $BrandName;
	public $AlternateBrandImage;
	public $BrandImage;
	public $BrandDescription;
	public $SEOUrl;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithID($brandID) {
        $instance = new self();
        $instance -> _brandID = $brandID;
        $instance->loadByID();
        return $instance;
    }
	
		
	protected function loadByID() {
		$sth = $this -> db -> prepare('SELECT * FROM brands WHERE BrandID = :BrandID');
        $sth->execute(array(':BrandID' => $this->_brandID));	
		
		if($sth->rowCount() > 0) {
			$record = $sth -> fetch();
		    $this->fill($record);
			//return true;
		} else {
			$this -> NotBlogPostName = true;	
			//throw new Exception('Blog Record does not exist (Value: ' . $this->_seoName . ')');	
			//return false;
		}
		
        //$record = $sth -> fetch();
        //$this->fill($record);
    }

    protected function fill(array $row){
    	$this -> BrandName = $row['BrandName'];
		$this -> BrandImage = $row['BrandImage'];
		$this -> BrandDescription = $row['BrandDescription'];
		$this -> SEOUrl = $row['brandNameSEOUrl'];
		$this -> AlternateBrandImage = $row['AlternateBrandImage'];
    }
	
	
	
		

}