<?php


class BlogPostComment extends BaseObject {
	
	public $fullName;
	public $CommentDescription;
	public $postID;	
	public $Capta;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public function Validate() {
		$validationErrors = array();
		
		$captcha = new GoogleCaptaHandler();
		$captcha -> response = $_POST['g-recaptcha-response'];
		
		$googleCheckResponse = json_decode($captcha -> GetResponse(), true);
				
		if($this -> validate -> emptyInput($this -> CommentDescription)) {
			array_push($validationErrors, array('inputID' => 1,
												'errorMessage' => 'Required'));
		}
		
		
		if(!$googleCheckResponse['success']) {
			array_push($validationErrors, array("inputID" => 2,
												'errorMessage' => 'Required'));
		}
						
		//$this -> Capta
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}
	
	public function Save() {
	
				
				
		$this -> db -> insert('blogcomments', array("commentFullName" => $this -> fullName,
													"commentNotesSection" => $this -> CommentDescription,
													"commentEnteredDate" => date("Y-m-d", $this -> time -> NebraskaTime()),
													"commentEnteredTime" => date("H:i:s", $this -> time -> NebraskaTime()),
													"postID" => $this -> postID,
													'commentVisible' => 0));	
													
		$this -> json -> outputJqueryJSONObject('success', "Thank you for the comment, this will be reviewed");	
													
	}
		

}