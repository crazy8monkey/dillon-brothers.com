<?php


class EmailLeadObject extends BaseObject {
	
	private $_NewEmail;
	public $VehicleInfo;
	public $StoreID;
	//1 = dillon-brothers.com
	public $Source;
	//1 = Send To Friend
	//2 = Online Offer
	//3 = Trade in Value
	//4 = Schedule Test Ride
	//5 = Contact Us
	//6 = Service Department
	//7 = Parts Department
	//8 = Donation Request
	//9 = Apparel Department
	public $type;
	
	public $Content;
	
	public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }

	public function Save($createDirectory = false) {
		$timeStamp = parent::TimeStamp();
		
		$newEmailLogDB = array();
		$newEmailLogDB['VehiclelInfo'] = $this -> VehicleInfo;
		$newEmailLogDB['LeadStoreID'] = $this -> StoreID;
		$newEmailLogDB['SourceOfLead'] = $this -> Source;
		$newEmailLogDB['TypeOfLead'] = $this -> type;
		$newEmailLogDB['ContentOfLead'] = $this -> Content;
		$newEmailLogDB['EnteredTime'] = $timeStamp;
		
		$this -> _NewEmail = $this -> db -> insert('emaillogs', $newEmailLogDB);
		
		if($createDirectory == true) {
			mkdir(PHOTO_PATH . 'DonationSubmissions/' . $this -> _NewEmail);
		}
		
	}
	
	public function SaveDonationRequestFiles($files = array(), $filesTemp = array()) {
		$count = 1;
		foreach($files as $key => $fileSingle) {
			$ext = explode('.', $fileSingle);
			$UploadeDirectory = PHOTO_PATH . 'DonationSubmissions/' . $this -> _NewEmail  . '/';
			PHOTO_PATH . 'DonationSubmissions/' . $this -> _NewEmail  . '/fileNumber' . $ext[1];	
			Image::moveImage($fileSingle, 
							 $filesTemp[$key], 
							 $UploadeDirectory);
			
			Image::renameImage($UploadeDirectory, $UploadeDirectory . $fileSingle, 'fileNumber'. $count);
			//Image::renameImage($count);
			$count++;
		}	

	}
	
	public function GetLastedSavedID() {
		return $this -> _NewEmail;
	}
		

}