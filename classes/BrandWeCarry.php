<?php


class BrandWeCarry extends BaseObject {
	
	private $_seoName;
	public $ISOEMBrand;
	public $BrandID;
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	
	public static function WithSEOName($postName) {
        $instance = new self();
        $instance->_seoName = $postName;
        $instance->loadByName();
        return $instance;
    }
	
		
	protected function loadByName() {
		$sth = $this -> db -> prepare('SELECT * FROM (SELECT brands.BrandName, 
											(1) ISOEMBrand, 
											(1) BrandType, 
											brands.BrandImage, 
											brands.BrandID,
               								brands.brandNameSEOUrl FROM brands 
									  UNION 
									  SELECT partaccessorybrand.brandName, 
									  		(0) ISOEMBrand, (partaccessorybrand.BrandType) BrandType, 
									  		partaccessorybrand.partBrandImage, 
									  		partaccessorybrand.partAccessoryBrandID,
               								partaccessorybrand.brandSEOUrl FROM partaccessorybrand) TotalBrands WHERE brandNameSEOUrl = :brandNameSEOUrl');
        $sth->execute(array(':brandNameSEOUrl' => $this -> _seoName));	
		
		if($sth->rowCount() > 0) {
			$record = $sth -> fetch();
		    $this->fill($record);
			//return true;
		} else {
			$this -> NotBlogPostName = true;	
			//throw new Exception('Blog Record does not exist (Value: ' . $this->_seoName . ')');	
			//return false;
		}
		
        //$record = $sth -> fetch();
        //$this->fill($record);
    }

    protected function fill(array $row){
    	$this -> ISOEMBrand = $row['ISOEMBrand'];
		$this -> BrandID = $row['BrandID'];
    }
	
	

}