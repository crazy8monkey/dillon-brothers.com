<?php


class Event extends BaseObject {
	
	private $_id;
	private $_seoName;
	
	public $eventName;
	public $eventHtmlJSON;
	
	public $eventImage;
	public $startDate;
	public $startTime;
	public $endDate;
	public $endTime;
	
	public $albumName;
	public $AlbumID;
	public $ParentDirectory;
	
	public $Store;
	public $storeIDS;
	
	public $EventPhoto;
	public $GoogleEventID;
	
	public $SeoURL;
	
	public $visible;
	
	public $address;
	public $city;
	public $state;
	public $zip;
	
	public $GoogleEventDescription;
	
	public $StructuredData;
	
	public $SuperEvent;
	public $SubEvent;
	
	public $SuperEventDescription;
	
	public $metaDescription;
	public $metaURL;
	public $metaImageURL;

	public $MetaData;
	
	public $photoName;
	public $ext;
	
	public $SuperEventName;
	public $SuperEventStartDate;
	public $SuperEventStartTime;
	public $SuperEventID;
	
	public $SuperEventEndDate;
	public $SuperEventEndTime;
	public $SuperEventStructuredData;
	
	public $PublishedDate;
	public $ModifiedDate;
	
	public $notEventPage;
	
	public $altText;
	public $titleText;
	
	
	public $SuperEventImage;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithSEOname($name) {
		$instance = new self();
	    $instance->_seoName = $name;
	    $instance->loadByName();
	    return $instance;	
    }
	
	public static function WithID($id) {
        $instance = new self();
        $instance->_id = $id;
        $instance->loadByID();
        return $instance;
    }
	
	public static function PreviewID($id) {
		$instance = new self();
        $instance->_id = $id;
        $instance->loadByPreviewID();
        return $instance;
	}
		
	protected function loadByPreviewID() {
		$sth = $this -> db -> prepare('SELECT *, (SELECT eventName FROM events WHERE eventID = subevents.SuperEventID) SuperEventName,
												 (SELECT seoUrl FROM events WHERE eventID = subevents.SuperEventID) SuperEventSEOUrl, 
										         (SELECT SuperEventDescription FROM events WHERE eventID = subevents.SuperEventID) SuperEventDescription,
										         (SELECT startDate FROM events WHERE eventID = subevents.SuperEventID) SuperEventStartDate,
										         (SELECT startTime FROM events WHERE eventID = subevents.SuperEventID) SuperEventStartTime,
	                                             (SELECT endDate FROM events WHERE eventID = subevents.SuperEventID) SuperEventEndDate,
	                                             (SELECT endTime FROM events WHERE eventID = subevents.SuperEventID) SuperEventEndTime,
	                                             (SELECT StructuredDataInfo FROM events WHERE eventID = subevents.SuperEventID) SuperEventStructuredData,
	                                             (SELECT eventImage FROM events WHERE eventID = subevents.SuperEventID) SuperEventImage FROM events 
												 LEFT JOIN subevents ON subevents.SubEventID = events.eventID
												 LEFT JOIN photoalbums ON events.albumID = photoalbums.albumID LEFT JOIN photos ON photoalbums.albumID = photos.photoAlbumID WHERE events.eventID = :eventID');
        $sth->execute(array(':eventID' => $this -> _id));
        $record = $sth -> fetch();
        $this->fillPreview($record);
	}	
		
		
		
	protected function loadByName() {
		
		$sth = $this -> db -> prepare('SELECT * FROM events 
												    	 LEFT JOIN photoalbums ON events.albumID = photoalbums.albumID 
		    											 LEFT JOIN photos ON photoalbums.albumID = photos.photoAlbumID WHERE seoUrl = :seoUrl');
		$sth->execute(array(':seoUrl' => $this->_seoName)); 
		
		if($sth->rowCount() > 0) {
			$record = $sth -> fetch();
		    $this->fill($record);
		} else {
			//throw new Exception('Event Record does not exist (Value: ' . $this->_seoName . ')');	
			$this -> notEventPage = true;
			//exit;
		}
	
		    
		
    }
	
	protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT *, (SELECT eventName FROM events WHERE eventID = subevents.SuperEventID) SuperEventName,
												 (SELECT seoUrl FROM events WHERE eventID = subevents.SuperEventID) SuperEventSEOUrl, 
										         (SELECT SuperEventDescription FROM events WHERE eventID = subevents.SuperEventID) SuperEventDescription,
										         (SELECT startDate FROM events WHERE eventID = subevents.SuperEventID) SuperEventStartDate,
										         (SELECT startTime FROM events WHERE eventID = subevents.SuperEventID) SuperEventStartTime,
	                                             (SELECT endDate FROM events WHERE eventID = subevents.SuperEventID) SuperEventEndDate,
	                                             (SELECT endTime FROM events WHERE eventID = subevents.SuperEventID) SuperEventEndTime,
	                                             (SELECT StructuredDataInfo FROM events WHERE eventID = subevents.SuperEventID) SuperEventStructuredData,
	                                             (SELECT eventImage FROM events WHERE eventID = subevents.SuperEventID) SuperEventImage FROM events 
												 LEFT JOIN subevents ON subevents.SubEventID = events.eventID
												 LEFT JOIN photoalbums ON events.albumID = photoalbums.albumID LEFT JOIN photos ON photoalbums.albumID = photos.photoAlbumID WHERE events.eventID = :eventID');
        $sth->execute(array(':eventID' => $this->_id));
        $record = $sth -> fetch();
        $this->fill($record);
    }

    protected function fill(array $row){
    	$this -> _id = $row['eventID'];
    	$this -> eventName = $row['eventName'];
    	$this -> eventHtmlJSON = $row['eventHTMLJSON'];
		$this -> city = $row['eventCity'];
		$this -> state = $row['eventState'];
		$this -> zip = $row['eventZip'];
		$this -> address = $row['eventAddress'];
		$this -> photoName = $row['photoName'];
		$this -> ext = $row['ext'];
		$this -> ParentDirectory = $row['ParentDirectory'];
		$this -> albumName = $row['albumFolderName'];
		$this -> startDate = $row['startDate'];
		$this -> startTime = $row['startTime'];
		$this -> endDate = $row['endDate'];
		$this -> endTime = $row['endTime'];
		$this -> eventImage = $row['eventImage'];
		$this -> SuperEvent = $row['SuperEvent'];
		$this -> SubEvent = $row['SubEvent'];
		$this -> SuperEventDescription = $row['SuperEventDescription'];
		$this -> GoogleEventDescription = $row['GoogleEventDescription'];
		$this -> PublishedDate = $row['publishDateFull'];
		$this -> ModifiedDate = $row['modifiedDateFull'];
		$this -> photoName = $row['photoName'];
		$this -> ext = $row['ext'];
		$this -> altText = $row['altText'];
		$this -> titleText = $row['title'];
		$this -> SeoURL = $row['seoUrl'];
		$this -> StructuredData = $row['StructuredDataInfo'];
    }

	protected function fillPreview(array $row){
    	$this -> _id = $row['eventID'];
    	$this -> eventName = $row['eventName'];
    	$this -> eventHtmlJSON = $row['eventHTMLJSON'];
		$this -> city = $row['eventCity'];
		$this -> state = $row['eventState'];
		$this -> zip = $row['eventZip'];
		$this -> address = $row['eventAddress'];
		$this -> photoName = $row['photoName'];
		$this -> ext = $row['ext'];
		$this -> ParentDirectory = $row['ParentDirectory'];
		$this -> albumName = $row['albumFolderName'];
		$this -> startDate = $row['startDate'];
		$this -> startTime = $row['startTime'];
		$this -> endDate = $row['endDate'];
		$this -> endTime = $row['endTime'];
		$this -> eventImage = $row['eventImage'];
		$this -> SuperEvent = $row['SuperEvent'];
		$this -> SubEvent = $row['SubEvent'];
		$this -> SuperEventDescription = $row['SuperEventDescription'];
		$this -> GoogleEventDescription = $row['GoogleEventDescription'];
		$this -> PublishedDate = $row['publishDateFull'];
		$this -> ModifiedDate = $row['modifiedDateFull'];
		$this -> photoName = $row['photoName'];
		$this -> ext = $row['ext'];
		$this -> altText = $row['altText'];
		$this -> titleText = $row['title'];
		$this -> SeoURL = $row['seoUrl'];
		$this -> StructuredData = $row['StructuredDataInfo'];
		$this -> SuperEventImage = $row['SuperEventImage'];
		$this -> SuperEventName = $row['SuperEventName'];
		$this -> SuperEventStructuredData = $row['SuperEventStructuredData'];
		$this -> SuperEventID = $row['SuperEventID'];
    }
	
	
	public function GetEventID() {
		return $this -> _id;
	}
	
	
	
	public function GetSEOUrl() {
		return $this -> _seoName;
	}
	
	public function GetFullImage() {
		return PHOTO_URL . PHOTO_PATH. 'events/'. $this -> ParentDirectory . '/'. $this -> albumName . '/'. $this -> photoName . '.' .$this -> ext;		
	}
	
	public function GetScheduledTime() {
		$startDate = new DateTime($this -> startDate);
		$endDate = new DateTime($this -> endDate);	
		if($startDate == $endDate) {
			return $this -> time -> formatDate($this -> startDate) . ' / ' . $this -> time -> formatTime($this -> startTime) . ' - ' . $this -> time -> formatTime($this -> endTime);
		} else {
			return $this -> time -> formatDate($this -> startDate) . ' / ' . $this -> time -> formatTime($this -> startTime) . ' - ' . $this -> time -> formatDate($this -> endDate) . ' / ' . $this -> time -> formatTime($this -> endTime);	
		}
	}

	public function PreviewGetBreadcrumbName() {
		$name = NULL;	
		if($this -> SubEvent == 1) {
			$name = $this -> SuperEventName;
		} else {
			$name = $this -> eventName;
		}
		
		return $name;
	}	
	
	
	
	public function GetHtml() {
		return file_get_contents(JSON_EVENT_URL . $this -> eventHtmlJSON);
	}
	
	public function GetMainEventPhoto() {
		$imageURL = NULL;	
		if($this -> SuperEvent == 1) {
			$imageURL = PHOTO_URL . 'SuperEvents/'. $this -> eventImage;
		} else {
			$imageURL = PHOTO_URL . $this -> ParentDirectory . '/events/'. $this -> albumName . '/'. $this -> photoName . '-l.' . $this -> ext;
		}
		
		return $imageURL;
	}
	
	public function GetMainPreviewEventPhoto() {
		$imageURL = NULL;	
		if($this -> SubEvent == 1) {
			$imageURL = PHOTO_URL . 'SuperEvents/'. $this -> SuperEventImage;
		} else {
			$imageURL = PHOTO_URL . $this -> ParentDirectory . '/events/'. $this -> albumName . '/'. $this -> photoName . '-l.' . $this -> ext;
		}
		
		return $imageURL;
	}
	
	
	
	public function GetEventLocation() {
		return $this -> address . '<br />' . $this -> city . ', ' . $this -> state . ' ' . $this -> zip;		
	}
	

		

}