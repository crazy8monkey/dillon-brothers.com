<?php


class PhotoYearDirectory extends BaseObject {
	
	public $year;
	public $YearPhoto;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithYear($year) {
        $instance = new self();
		$instance -> year = $year;
        $instance->loadByYear();
        return $instance;
    }
	
	protected function loadByYear() {
    	$sth = $this -> db -> prepare('SELECT * FROM yearphotos WHERE YearText = :YearText');
        $sth->execute(array(':YearText' => $this -> year));
		
		if($sth->rowCount() > 0) {
			$record = $sth -> fetch();
        	$this->fill($record);
		} else {
			throw new Exception('Year Record does not exist (Value: ' . $this->year . ')');	
		}
		
		
        
    }
	

    protected function fill(array $row){
    	$this -> YearPhoto = $row['YearPhotoThumb'];
    }
	
	
	
	

}