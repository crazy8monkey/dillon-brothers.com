<?php


class GoogleCaptaHandler {
	
	public $response;
	
    public function GetResponse() {
    	$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
		curl_setopt($ch, CURLOPT_POST, 1);
		if(LIVE_SITE == true) {
			curl_setopt($ch, CURLOPT_POSTFIELDS,"secret=6Lfi8C4UAAAAAAwc8xmX9hN2MfBk6hlV4bJOPAxy&response=" . $this -> response);
		} else {
			curl_setopt($ch, CURLOPT_POSTFIELDS,"secret=6LcQ9i4UAAAAAMS0yMeE1XoQ-swJ1w0FNgFrpkl3&response=" . $this -> response);	
		}
		
		
		
		// in real life you should use something like:
		// curl_setopt($ch, CURLOPT_POSTFIELDS, 
		//          http_build_query(array('postvar1' => 'value1')));
		
		// receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$server_output = curl_exec ($ch);
		
		curl_close ($ch);
		
		return $server_output;
    }
}