<?php

class ReviewSingle extends BaseObject {
	
	//1 = ecommer products
	public $ReviewType;
	public $inventoryItemID;
	public $RatingNumber;
	public $firstName;
	public $lastName;
	public $ReviewText;
	public $CaptaCheck;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }
	
	public function Validate() {
		$validationErrors = array();
		
		$captcha = new GoogleCaptaHandler();
		$captcha -> response = $_POST['g-recaptcha-response'];
		
		$googleCheckResponse = json_decode($captcha -> GetResponse(), true);
		
	
		if(!isset($this -> RatingNumber)) {
			array_push($validationErrors, array("inputID" => 1,
												"errorMessage" => "Required"));
		}

		if(isset($this -> firstName)) {
			if($this -> validate -> emptyInput($this -> firstName)) {
				array_push($validationErrors, array("inputID" => 2,
													"errorMessage" => "Required"));
			}	
		}
		
		if(isset($this -> lastName)) {
			if($this -> validate -> emptyInput($this -> lastName)) {
				array_push($validationErrors, array("inputID" => 3,
													"errorMessage" => "Required"));
			}	
		}
		
		if($this -> validate -> emptyInput($this -> ReviewText)) {
			array_push($validationErrors, array("inputID" => 4,
												"errorMessage" => "Required"));
		}
				
		if(!$googleCheckResponse['success']) {
			array_push($validationErrors, array("inputID" => 5,
												'errorMessage' => 'Required'));
		}
		
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}

	public function Save() {
		$this -> db -> insert('reviews', array('DateEntered' => parent::TimeStamp(),
											   'reviewedByFirstName' => $this -> firstName,
											   'reviewedByLastName' => $this -> lastName,
											   'reviewText' => str_replace("\n", '<br />',$this -> ReviewText),
											   'reviewType' => $this -> ReviewType,
											   'rating' => $this -> RatingNumber,
											   'reviewItemID' => $this -> inventoryItemID));
											   
		$this -> json -> outputJqueryJSONObject('ReviewedSaved', true);									   
	}	 



}