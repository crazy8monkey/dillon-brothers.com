<?php


class ServiceDept extends BaseObject {
	
	public $Store;
	public $FirstName;
	public $LastName;
	public $HomePhone;
	public $CellPhone;
	public $EmailAddress;
	public $StreetAddress;
	public $City;
	public $state;
	public $Zip;
	//vehicle information
	public $VehicleYear;
	public $VehicleMake;
	public $VehicleModel;
	public $Miles;
	public $VinNumber;
	//service details
	public $PastServiceYesNo;
	public $LastServiceDate;
	public $LastWorkDone;
	public $NewAppointmentDate;
	public $NewWorkNeedsDone;
	
	public $CaptchaCheck;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public function Validate() {
		$validationErrors = array();
		
		$captcha = new GoogleCaptaHandler();
		$captcha -> response = $_POST['g-recaptcha-response'];
		
		$googleCheckResponse = json_decode($captcha -> GetResponse(), true);
		
		
		if(!isset($this -> Store)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Please select a store'));
		}
		
		//empty first name
		if($this -> validate -> emptyInput($this -> FirstName)) {
			array_push($validationErrors, array("inputID" => 2,
												'errorMessage' => 'Required'));	
		}

		if($this -> validate -> emptyInput($this -> LastName)) {
			array_push($validationErrors, array("inputID" => 3,
												'errorMessage' => 'Required'));	
		}
		
		if($this -> validate -> emptyInput($this -> VehicleYear)) {
			array_push($validationErrors, array("inputID" => 4,
												'errorMessage' => 'Required'));	
		}
		
		if($this -> validate -> emptyInput($this -> VehicleMake)) {
			array_push($validationErrors, array("inputID" => 5,
												'errorMessage' => 'Required'));	
		}
		
		if($this -> validate -> emptyInput($this -> VehicleModel)) {
			array_push($validationErrors, array("inputID" => 6,
												'errorMessage' => 'Required'));	
		}
		
		if(!empty($this -> EmailAddress)) {
			if($this -> validate -> correctEmailFormat($this -> EmailAddress)) {
				array_push($validationErrors, array("inputID" => 12,
													'errorMessage' => 'Needs to be in email format'));		
			}
			
		}
		
		if(!isset($this -> PastServiceYesNo)) {
			array_push($validationErrors, array("inputID" => 7,
												'errorMessage' => 'Please specify if we previously serviced your vehicle'));
		} else if(isset($this -> PastServiceYesNo)) {
			if($this -> PastServiceYesNo[0] == 1) {
				if($this -> validate -> emptyInput($this -> LastServiceDate)) {
					array_push($validationErrors, array("inputID" => 8,
														'errorMessage' => 'Required'));	
				}
				
				if($this -> validate -> emptyInput($this -> LastWorkDone)) {
					array_push($validationErrors, array("inputID" => 9,
														'errorMessage' => 'Required'));	
				}		
				
				
			}
		}
		
		if($this -> validate -> emptyInput($this -> NewAppointmentDate)) {
			array_push($validationErrors, array("inputID" => 10,
												'errorMessage' => 'Required'));	
		}
		if($this -> validate -> emptyInput($this -> NewWorkNeedsDone)) {
			array_push($validationErrors, array("inputID" => 11,
												'errorMessage' => 'Required'));	
		}
		
		if(!$googleCheckResponse['success']) {
			array_push($validationErrors, array("inputID" => 13,
												'errorMessage' => 'Required'));
		}
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}

	public function Save() {
		$storeLocation = explode(":",$this -> Store[0]);
		$emailContent = array();
		$emailContent['StoreLocaiton'] = $storeLocation[1];
		$emailContent['ContactInformation'] = array("FullName" => $this -> FirstName . ' ' . $this -> LastName,
													"HomePhone" => $this -> HomePhone,
													"CellPhone" => $this -> CellPhone,
													"EmailAddress" => $this -> EmailAddress,
													"Address" => $this -> StreetAddress . '<br />' . $this -> City . ', '. $this -> state . ' ' .$this -> Zip);
		
		$emailContent['VehicleInformation'] = array("VehicleYear" => $this -> VehicleYear,
													"VehicleMake" => $this -> VehicleMake,
													"VehicleModel" => $this -> VehicleModel,
													"Miles" => $this -> Miles,
													"VinNumber" => $this -> VinNumber);
													
		$serviceDetails = array();
		
		$serviceDetails['PreviousServiced'] = $this -> PastServiceYesNo[0];
		if($this -> PastServiceYesNo[0] == 1) {
			$serviceDetails['LastServicedDate'] = $this -> LastServiceDate;
			$serviceDetails['LastServiceWorkDone'] = $this -> LastWorkDone;
		}
		
		$serviceDetails['NewAppointmentDate'] = $this -> NewAppointmentDate;
		$serviceDetails['NewServiceWorkNeed'] = $this -> NewWorkNeedsDone;
		
		$emailContent['ServiceDetailInformation'] = $serviceDetails;
			
		$email = new EmailLeadObject();
		$email -> VehicleInfo = "NA";
		$email -> StoreID = $storeLocation[0];
		$email -> Source = 1;
		$email -> type = 6;
		$email -> Content = json_encode($emailContent);	
		$email -> Save();
		
		if(LIVE_SITE == TRUE) {
			$store = Store::WithID($storeLocation[0]);
			$serviceContent = array();	
			$serviceContent['StoreName'] = $store -> StoreName . ' ' .$storeLocation[1];
			$serviceContent['FullName'] = $this -> FirstName . ' ' . $this -> LastName;
			$serviceContent['HomePhone'] = $this -> HomePhone;
			$serviceContent['CellPhone'] = $this -> CellPhone;
			$serviceContent['Address'] = $this -> StreetAddress . '<br />' . $this -> City . ', '. $this -> state . ' ' .$this -> Zip;
			$serviceContent['VehicleYear'] = $this -> VehicleYear;
			$serviceContent['VehicleMake'] = $this -> VehicleMake;
			$serviceContent['VehicleModel'] = $this -> VehicleModel;
			$serviceContent['VehicleMiles'] = $this -> Miles;
			$serviceContent['VehicleVIN'] = $this -> VinNumber;
			$serviceContent['PastServiced'] = $this -> PastServiceYesNo[0];
			
			if(!empty($this -> LastServiceDate)) {
				$PrevMySQLDate = $this -> time -> FormatExpectedDate($this -> LastServiceDate);
				$serviceContent['PastServicedDate'] = $this -> time -> formatDate($PrevMySQLDate);
				$serviceContent['PastWorkDone'] = $this -> LastWorkDone;	
			}
			
			
			$NewMySQLDate = $this -> time -> FormatExpectedDate($this -> NewAppointmentDate);
			
			$serviceContent['NewAppointmentDate'] = $this -> time -> formatDate($NewMySQLDate);
			$serviceContent['NewWorkDone'] = $this -> NewWorkNeedsDone;
			
			$sendEmail = new Email();
			$sendEmail -> subject = "New Service Request";
			$sendEmail -> to = $store -> ServiceEmail;
			$sendEmail -> ServiceEmail($serviceContent);	
		}
		
		
		$this -> json -> outputJqueryJSONObject('testResponse', $emailContent);	
		
	}
	
}