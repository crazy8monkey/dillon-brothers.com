<?php


class DonationReq extends BaseObject {
	
	public $DillonBrotherCustomer;
	public $ContactName;
	public $ContactPhone;
	public $ContactEmail;
	//event info
	public $NameOfEvent;
	public $TypeOfEvent;
	public $TaxID;
	public $Benifiting;
	public $DateOfEvent;
	public $AnticipatedAttendance;
	//addional info
	public $Attachments = array();
	public $AttachmentsTemp;
	public $AdditionalComments;
	
	public $CaptchaCheck;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public function Validate() {
		$validationErrors = array();
		
		$captcha = new GoogleCaptaHandler();
		$captcha -> response = $_POST['g-recaptcha-response'];
		
		$googleCheckResponse = json_decode($captcha -> GetResponse(), true);
		
		if($this -> validate -> emptyInput($this -> ContactName)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> ContactPhone)) {
			array_push($validationErrors, array("inputID" => 2,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> ContactEmail)) {
			array_push($validationErrors, array("inputID" => 3,
												'errorMessage' => 'Required'));
		} else if($this -> validate -> correctEmailFormat($this -> ContactEmail)) {
			array_push($validationErrors, array("inputID" => 3,
												'errorMessage' => 'Needs to be in email format'));		
		}
		
		if($this -> validate -> emptyInput($this -> NameOfEvent)) {
			array_push($validationErrors, array("inputID" => 4,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> TypeOfEvent)) {
			array_push($validationErrors, array("inputID" => 5,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> TaxID)) {
			array_push($validationErrors, array("inputID" => 6,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> Benifiting)) {
			array_push($validationErrors, array("inputID" => 7,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> DateOfEvent)) {
			array_push($validationErrors, array("inputID" => 8,
												'errorMessage' => 'Required'));
		}	
		
		if($this -> validate -> emptyInput($this -> AnticipatedAttendance)) {
			array_push($validationErrors, array("inputID" => 9,
												'errorMessage' => 'Required'));
		}		
		
		if(!$googleCheckResponse['success']) {
			array_push($validationErrors, array("inputID" => 10,
												'errorMessage' => 'Required'));
		}
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}

	public function Save() {
		$emailContent = array();
		
		$emailContent['ContactInformation'] = array("CustomerRelation" => $this -> DillonBrotherCustomer,
													"EventContactName" => $this -> ContactName,
													"EventContactPhone" => $this -> ContactPhone,
													"EventContactEmail" => $this -> ContactEmail);
		
		$emailContent['FundraisingEventInformation'] = array("NameOfEvent" => $this -> NameOfEvent,
															 "TypeOfEvent" => $this -> TypeOfEvent,
															 "TaxIDNumber" => $this -> TaxID,
															 "EventBenefiting" => $this -> Benifiting,
															 "DateOfEvent" => $this -> DateOfEvent,
															 "AnticipatedAttendance" => $this -> AnticipatedAttendance);
		
		
		$additionalInformation = array();
		$additionalInformation['Comments'] = $this -> AdditionalComments;
		//if(count($this -> Attachments) > 0) {
		//if(!empty($this -> Attachments)) {
		//if($_FILES['donationAttachments']['size'] != 0) {	
		if($_FILES['donationAttachments']['name'][0] != null) {		
			
			$filePath = array();
			$count = 1;
			foreach($this -> Attachments as $fileSingle) {
				$ext = explode('.', $fileSingle);	
				array_push($filePath, 'fileNumber' . $count . '.' . $ext[1]);
				$count++;
			}	
			$additionalInformation['attachments'] = $filePath;
		}
		
		$emailContent['AdditionalInformation'] = $additionalInformation;
													
			
		$email = new EmailLeadObject();
		$email -> VehicleInfo = "NA";
		$email -> StoreID = 0;
		$email -> Source = 1;
		$email -> type = 8;
		$email -> Content = json_encode($emailContent);	
		$email -> Save(true);
		if($_FILES['donationAttachments']['name'][0] != null) {		
		//if($_FILES['donationAttachments']['size'] != 0) {		
			$email -> SaveDonationRequestFiles($this -> Attachments, $this -> AttachmentsTemp);	
		}
		
		
		if(LIVE_SITE == TRUE) {
			$settings = SettingsObject::Init();
			$serviceContent = array();	
			if(!empty($this -> DillonBrotherCustomer)) {
				$serviceContent['DillonBrothersBeneficiary'] = $this -> DillonBrotherCustomer;	
			} else {
				$serviceContent['DillonBrothersBeneficiary'] = "Nothing was entered";
			}
			
			$serviceContent['EventContactPerson'] = $this -> ContactName;
			$serviceContent['EventContactPhone'] = $this -> ContactPhone;
			$serviceContent['NameOfEvent'] = $this -> NameOfEvent;
			$serviceContent['TypeOfEvent'] = $this -> TypeOfEvent;
			$serviceContent['TaxIDNumber'] = $this -> TaxID;
			$serviceContent['EventBenefiting'] = $this -> Benifiting;
			
			$DateOfEventMySQL = $this -> time -> FormatExpectedDate($this -> DateOfEvent);
			$serviceContent['DateOfEvent'] = $this -> time -> formatDate($DateOfEventMySQL);
			$serviceContent['AnticipatedAttendance'] = $this -> AnticipatedAttendance;
			
			if(!empty($this -> AdditionalComments)) {
				$serviceContent['AdditionalComments'] = $this -> AdditionalComments;
			} else {
				$serviceContent['AdditionalComments'] = "Nothing Was entered";
			}
			
			if($_FILES['donationAttachments']['name'][0] != null) {
				$serviceContent['LastedEmailID'] = $email -> GetLastedSavedID();
				$serviceContent['Attachments'] = $filePath;
			} 
			
			
			
			$sendEmail = new Email();
			$sendEmail -> subject = "New Donation Request";
			$sendEmail -> to = $settings -> DonationEmail;
			$sendEmail -> DonationRequest($serviceContent);	
		}
		
		
		$this -> json -> outputJqueryJSONObject('testResponse', $emailContent);	
		//$this -> json -> outputJqueryJSONObject('testResponse', count($_FILES['donationAttachments']['name']));	
		
		
	}
	
}