<?php


class EventPDF extends BaseObject {
	
	private $_id;
	private $year;
	public $ShowLink;
	
	public $pdfName;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithCurrentYear($year) {
        $instance = new self();
        $instance -> year = $year;
        $instance -> loadByYear();
        return $instance;
    }
		
	protected function loadByYear() {
    	$sth = $this -> db -> prepare('SELECT * FROM eventpdflist WHERE eventYearList = :eventYearList');
        $sth->execute(array(':eventYearList' => $this->year));
        if($sth->rowCount() > 0) {
			$this -> ShowLink = "show";
			$record = $sth -> fetch();
		    $this->fill($record);
		} else {
			$this -> ShowLink = "hide";
		}
    }

	protected function fill(array $row){
		$this -> pdfName = $row['eventYearListName'];
    }

    public function GetUploadNotificaitonText() {
    	return $this -> NotificationUploadText;
    }

	public function GetUploadLabel() {
		return $this -> UploadLabel;
	}	
	
	
	
	

	


		

}