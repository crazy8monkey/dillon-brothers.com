<?php


class BlogCategory extends BaseObject {
	
	private $seoName;
	public $categoryID;
	
	public $blogCategoryName;
	public $PublishedTime;
	
	
	
		
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithSEOName($categoryName) {
        $instance = new self();
        $instance-> seoName = $categoryName;
        $instance->loadByID();
        return $instance;
    }
		
	protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM blogpostcategories WHERE blogCategorySEOUrl = :blogCategorySEOUrl');
        $sth->execute(array(':blogCategorySEOUrl' => $this -> seoName));
        $record = $sth -> fetch();
        $this->fill($record);
    }

    protected function fill(array $row){
    	$this -> categoryID = $row['blogPostCategoryID'];	
		$this -> blogCategoryName = $row['blogCategoryName'];	
		$this -> PublishedTime = $row['blogCategoryPublishedTime'];
    }
	
	public function GetPublishedTime() {
		
	}
	
	
	
		

}