<?php

class ChatConversationMessage extends BaseObject {
		
	public $FromCustomer;
	public $RelatedConversationID;
	public $Message;
	public $RelatedWorker;
	
	public $ChannelID;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

	public function Save() {
		try {
			$twilio = new TwilioHandler();
			$twilio -> init();
			$twilio -> ChatChannelID = $this -> ChannelID;
			$twilio -> NewMessage = $this -> Message;
			$twilio -> NewChatMessage();

			$newChatMessageDB = array();
			
			$newChatMessageDB['fromCustomer'] = 1;
			$newChatMessageDB['relatedConversationID'] = $this -> RelatedConversationID;
			$newChatMessageDB['message'] = $this -> Message;
			$newChatMessageDB['relatedWorder'] = 0;
			$newChatMessageDB['dateentered'] = parent::TimeStamp();
			
			$this -> db -> insert('chatconversationmessages', $newChatMessageDB);
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Save Twilio Chat Message Error: " . $e->getMessage();
			$TrackError -> type = "TWILIO CHAT MESSAGE SAVE ERROR";
			$TrackError -> SendMessage();
		}
	}
	




}