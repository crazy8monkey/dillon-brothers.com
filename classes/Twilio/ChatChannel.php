<?php


class ChatChannel extends BaseObject {
		
	private $_channelObj;	
	private $_twilioChannelID;
	
	public $RelatedStoreID;
	public $StoreImage;
	
	private $_ifChannelExists = false;
	private $_ifWorkerExists = false;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithTwilioChannelID($channelID) {
		$instance = new self();
	    $instance -> _twilioChannelID = $channelID;
	    $instance->loadByTwilioChannelID();
	    return $instance;	
	}
	
	protected function loadByTwilioChannelID() {
		$sth = $this -> db -> prepare('SELECT * FROM twiliochatchannels LEFT JOIN stores ON twiliochatchannels.relatedStoreID = stores.storeID WHERE twilioChannelID = :twilioChannelID');
        $sth->execute(array(':twilioChannelID' => $this -> _twilioChannelID));
        $record = $sth -> fetch();
		$this->fill($record);	
	}
	
	protected function fill(array $row){
 		$this -> RelatedStoreID = $row['relatedStoreID'];
		$this -> StoreImage = $row['storeImage'];
    }
	
	public function CheckifExists($type) {
		switch($type) {
			case "channel":
				$channel = new ChatChannelList();
				if($channel -> GetChannelSingle() != NULL) {
					$this -> _ifChannelExists = true;	
				}
				break;
			case "worker":
				$channelUser = new ChatChannelUsersList();
				if($channelUser -> GetChannelUser($this -> _channelObj[0]['chatChannelID']) != NULL) {
					$this -> _ifWorkerExists = true;
				}
				break;
		}
	}
	
	public function GetChatChannelDetails() {
		$channelResponse = array();
		$channelResponse['response'] = false;
		
		$channel = new ChatChannelList();
		if($channel -> GetChannelSingle() != NULL) {
			$this -> _channelObj = $channel -> GetChannelSingle();
			$channelUser = new ChatChannelUsersList();
			if($channelUser -> GetChannelUser($this -> _channelObj[0]['chatChannelID']) != NULL) {
				$channelResponse['response'] = true;
				$channelResponse['channel'] = $this -> _channelObj[0]['twilioChannelID'];
				$channelResponse['worker'] = $this -> GetWorker()[0]['twilioMemberID'];
			}
		}
		
		return $channelResponse;
	}
	
	public function GetChatChannel() {
		$channel = new ChatChannelList();
		$this -> _channelObj = $channel -> GetChannelSingle();
		return $channel -> GetChannelSingle();	
				
	}
	
	public function GetIfChannelExists() {
		return $this -> _ifChannelExists;
	}
	
	public function GetIfWorkerExists() {
		return $this -> _ifWorkerExists;
	}
	
	public function GetWorker() {
		$channelUser = new ChatChannelUsersList();
		return $channelUser -> GetChannelUser($this -> _channelObj[0]['chatChannelID']);	
		
	}
}