<?php


class ChatChannelUser extends BaseObject {
		
	private $_workerID;
	
	public $relatedUserID;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithTwilioWorkerID($workerID) {
		$instance = new self();
	    $instance -> _workerID = $workerID;
	    $instance->loadByTwilioWorkerID();
	    return $instance;	
	}
	
	protected function loadByTwilioWorkerID() {
		$sth = $this -> db -> prepare('SELECT * FROM twiliochatmembers WHERE twilioMemberID = :twilioMemberID');
        $sth->execute(array(':twilioMemberID' => $this -> _workerID));
        $record = $sth -> fetch();
        $this->fill($record);
	}
	
	protected function fill(array $row){
		$this -> relatedUserID = $row['relatedUserID'];
    }
	
	
	public function GetChatChannel() {
		$channel = new ChatChannelList();
		$this -> _channelObj = $channel -> GetChannelSingle();
		return $channel -> GetChannelSingle();
	}
	
	public function GetWorker() {
		$channelUser = new ChatChannelUsersList();
		return $channelUser -> GetChannelUser($this -> _channelObj[0]['chatChannelID']);
	}
}