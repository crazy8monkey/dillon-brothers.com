<?php

require_once TWILIO_API_PATH. 'autoload.php'; // Loads the library
use Twilio\Rest\Client;	

use Twilio\Twiml;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\IpMessagingGrant;
use Twilio\Jwt\TaskRouter\WorkerCapability;
use Twilio\Jwt\TaskRouter\WorkspaceCapability;
use Twilio\Jwt\TaskRouter\TaskQueueCapability;


//http://c2cd82e6.ngrok.io/dillonbrothers/updates/twiliocatchresponse?Twiml=%3CResponse%3E%3C%2FResponse%3E


//?Twiml=%3CResponse%3E%3C%2FResponse%3E
class TwilioHandler {
	
	private $TwilioClient;
	
	public $ChannelMemberName;
	
	
	public $ChannelFriendlyName;
	public $ChannelUniqueName;
	
	public $DeviceID;
	
	public $PersonName;
	public $PersonPhone;
	public $PersonEmail;
	public $ChatChannelID;
	
	public $acessTokenCustomerName;
	
	private $NewTaskClient;
	private $_NewTaskID;
	
	public $CurrentTaskID;
	public $NewMessage;
	
    public function __construct() {
		
    }
	
	public function init() {
		$this -> TwilioClient = new Client('AC23289f8d8a0e5a87ae60349eb24ee1b8', 'fe80215bdc30955d0be7a06d18351fbb');
	}
	
	public function GenerateAccessToken() {
		// Required for IP messaging grant
		$ipmServiceSid = 'IScef31cfda2374e4aa4f3f9eb4678a377';
		// An identifier for your app - can be anything you'd like
		$appName = 'DillonBrothersSalesChatService';
		// choose a random username for the connecting user
		$identity = "Adam Schmidt";
		// A device ID should be passed as a query string parameter to this script
		$deviceId = $this -> DeviceID;
		$endpointId = $appName . ':' . $identity . ':' . $deviceId;
		
		// Create access token, which we will serialize and send to the client
		$token = new AccessToken(
		    'AC23289f8d8a0e5a87ae60349eb24ee1b8',
		    'SK193190932553a8e3ba71ff149b58cc64',
		    'A2YYwbu0Jjvv1vlb0VAB0tHosifQNsj6',
		    3600,
		    $identity
		);
		
		// Create IP Messaging grant
		$ipmGrant = new IpMessagingGrant();
		$ipmGrant->setServiceSid($ipmServiceSid);
		$ipmGrant->setEndpointId($endpointId);
		
		// Add grant to token
		$token->addGrant($ipmGrant);
		
		// render token to string
		return $token->toJWT();
		
		
	}

	public function CreateNewTask() {
		$this -> NewTaskClient = $this -> TwilioClient -> taskrouter
		    ->workspaces('WS47103271c50b41b49342a83c28ec6f4a')
		    ->tasks
		    ->create(array(
		      'workflowSid' => 'WW61d91abc2983ac99d29ae3848a24bd04',
		      'attributes' => json_encode(array('fullName' => $this -> PersonName,
			  								    'email' => $this -> PersonEmail,
												'phone' => $this -> PersonPhone,
												'channelID' => $this -> ChatChannelID)),
				'TaskChannel' => 'chat'								
		      ));
	}
	
	public function AcceptTask() {
		//$reservations = $this -> TwilioClient -> taskrouter
		//    ->workspaces('WS47103271c50b41b49342a83c28ec6f4a')
		//    ->tasks($this -> NewTaskClient -> sid)
		//    ->reservations
		//    ->read();
		
		
		//$this -> TwilioClient -> taskrouter
		//    ->workspaces('WS47103271c50b41b49342a83c28ec6f4a')
		//    ->tasks($this -> NewTaskClient -> sid)
		//    ->reservations($reservations[0] -> sid)
		//    ->update(
		//        array('reservationStatus' => 'accepted')
		//    );
	}
	
	
	public function RetrieveTask() {
		$this -> TwilioClient->taskrouter
		    ->workspaces('WS47103271c50b41b49342a83c28ec6f4a')
		    ->tasks($this -> CurrentTaskID)
		    ->fetch();
	}
	
	public function SetNewTaskID() {
		$this -> _NewTaskID = $this -> NewTaskClient -> sid;
	}
	
	public function GetNewTaskID() {
		return $this -> _NewTaskID;
	}
	
	public function NewChatMessage() {
		$this -> TwilioClient -> chat -> 
								 services("IScef31cfda2374e4aa4f3f9eb4678a377") -> 
								 channels($this -> ChatChannelID) -> 
								 messages -> 
								 create($this -> NewMessage);
	}


	public function GenerateWorkerTaskRouterToken() {
		$accountSid = "AC23289f8d8a0e5a87ae60349eb24ee1b8";
		$authToken = "fe80215bdc30955d0be7a06d18351fbb";
		$workspaceSid = "WS47103271c50b41b49342a83c28ec6f4a";
		$workerSid = "WK173024ac7c8972f293b83a71ea0504e4";
		
		$capability = new WorkerCapability($accountSid, $authToken, $workspaceSid, $workerSid);
		$capability->allowActivityUpdates();
		$capability->allowReservationUpdates();
		
		return $capability->generateToken();
		// By default, tokens are good for one hour.
		// Override this default timeout by specifiying a new value (in seconds).
		// For example, to generate a token good for 8 hours:
		//$token = $capability->generateToken(28800);  // 60 * 60 * 8
	}
	
	public function GenerateWorkspaceTaskRouterToken() {
		
		$capability = new WorkspaceCapability("AC23289f8d8a0e5a87ae60349eb24ee1b8",
											  "fe80215bdc30955d0be7a06d18351fbb",
											  "WS47103271c50b41b49342a83c28ec6f4a");
		$capability->allowFetchSubresources();
		$capability->allowUpdatesSubresources();
		$capability->allowDeleteSubresources();
		$token = $capability->generateToken();
		// By default, tokens are good for one hour.
		// Override this default timeout by specifiying a new value (in seconds).
		// For example, to generate a token good for 8 hours:
		return $capability->generateToken(28800);  // 60 * 60 * 8
	}
	
	public function GenerateTaskQueueRouterToken() {
		$accountSid = "AC23289f8d8a0e5a87ae60349eb24ee1b8";
		$authToken = "fe80215bdc30955d0be7a06d18351fbb";
		$workspaceSid = "WS47103271c50b41b49342a83c28ec6f4a";
		$taskQueueSid = "WQ4b7e3c87a265c4e4cc47cf8b84d2dfdd";
		
		$capability = new TaskQueueCapability($accountSid, $authToken, $workspaceSid, $taskQueueSid);
		$capability->allowFetchSubresources();
		$capability->allowUpdates();
		$token = $capability->generateToken();
		// By default, tokens are good for one hour.
		// Override this default timeout by specifiying a new value (in seconds).
		// For example, to generate a token good for 8 hours:
		return $capability->generateToken(28800);  // 60 * 60 * 8
	}
	
	
	public function GrabMessagesByTaskID() {
		$MessagesByTask = array();
		//Retrieve the messages
		$messages = $this -> TwilioClient -> chat
										  -> services("IScef31cfda2374e4aa4f3f9eb4678a377")
										  -> channels($this -> CurrentChannelID)
										  -> messages
										  -> read();
		
		//
		
		foreach($messages as $msgSingle) {
			$attributes = json_decode($msgSingle -> attributes, true);
			if(isset($attributes['taskID'])) {
				if($attributes['taskID'] == $this -> CurrentTaskID) {
					array_push($MessagesByTask, $msgSingle);
				}	
			}
			
		}
		
		return $MessagesByTask;
	}
	
	
	

}