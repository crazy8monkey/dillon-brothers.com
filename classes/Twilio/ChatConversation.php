<?php


class ChatConversation extends BaseObject {
		
	private $_id;
	private $_taskID;
	
	public $CustomerFirstName;
	public $CustomerLastname;
	
	public $CustomerName;
	public $CustomerPhone;
	public $CustomerEmail;
	public $ChannelID;
	public $WorkerID;
	public $ChatActive;
	public $taskID;
	
	public $StoreImage;
	public $ChannelFriendlyName;
	public $ChannelUniqueName;
	
	
	
	public $StoreID;
	
	public $UserProfile;
	public $UserFirstName;
	public $UserLastname;
	
	public $FromCustomerValue;
	public $NewMessage;
	
	public $CurrentMessages = array();
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithTaskID($taskID) {
		$instance = new self();
		$instance -> _taskID = $taskID;
        $instance -> loadByTaskID();
        return $instance;
	}
	
	protected function loadByTaskID() {
		$sth = $this -> db -> prepare('SELECT * FROM chatconversations 
												LEFT JOIN twiliochatchannels ON chatconversations.twilioChannelID = twiliochatchannels.twilioChannelID 
												LEFT JOIN stores ON twiliochatchannels.relatedStoreID = stores.storeID 
												LEFT JOIN users ON chatconversations.WorkerName = users.userID
												LEFT JOIN twiliochatmembers ON chatconversations.WorkerName = twiliochatmembers.relatedUserID
												WHERE twilioTaskID = :twilioTaskID');
        $sth->execute(array(':twilioTaskID' => $this -> _taskID));	
        $record = $sth -> fetch();
        $this->fill($record);
		$this -> _grabCurrentMessages();
	}
	
	protected function fill(array $row){
		$this -> _id = $row['chatConversationID'];
		$this -> CustomerName = $row['CustomerName'];
		$this -> ChannelID = $row['twilioChannelID'];
		$this -> StoreImage = $row['storeImage'];
		$this -> StoreID = $row['storeID'];
		$this -> ChannelFriendlyName = $row['friendlyName'];
		$this -> ChannelUniqueName = $row['uniqueName'];
		$this -> ChatActive = $row['taskStatus'];
		$this -> UserProfile = $row['userProfile'];
		$this -> UserFirstName = $row['firstName'];
		$this -> UserLastname = $row['lastName'];
		$this -> WorkerID = $row['twilioMemberID'];
    }	
	
	public function GetSalesmanFullName() {
		return $this -> UserFirstName . ' ' . $this -> UserLastname;
	}
	
	private function _grabCurrentMessages() {
		if($this -> ChatActive == 1) {
			$twilio = new TwilioHandler();
			$twilio -> init();
			$twilio -> CurrentChannelID = $this -> ChannelID;
			$twilio -> CurrentTaskID = $this -> _taskID;
			$this -> CurrentMessages = $twilio -> GrabMessagesByTaskID();
		}
	}
	
	public function Validate() {
		session_start();	
		$validationErrors = array();
		
		if($this -> validate -> emptyInput($this -> CustomerFirstName)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> CustomerLastname)) {
			array_push($validationErrors, array("inputID" => 2,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> CustomerPhone)) {
			array_push($validationErrors, array("inputID" => 3,
												'errorMessage' => 'Required'));
		}

		if($this -> validate -> emptyInput($this -> CustomerPhone)) {
			array_push($validationErrors, array("inputID" => 4,
												'errorMessage' => 'Required'));
		}
		
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}

	public function StartChatConversation() {
		
		$taskCheck = $this -> db -> prepare('SELECT * FROM chatconversations WHERE CustomerName = :CustomerName AND phone = :phone AND customerEmail = :customerEmail AND taskStatus = 1');
		$taskCheck -> execute(array(':CustomerName' => $this -> CustomerFirstName . ' ' . $this -> CustomerLastname,
									':phone' => preg_replace("/[^0-9]/", "", $this -> CustomerPhone),
									':customerEmail' => $this -> CustomerEmail));
		$grabCurrentTask = $taskCheck -> fetch();
		$twilio = new TwilioHandler();
		$twilio -> init();
		
		
		if($taskCheck -> rowCount() == 0) {
			$twilio -> PersonName = $this -> CustomerFirstName . ' ' . $this -> CustomerLastname;
			$twilio -> PersonPhone = preg_replace("/[^0-9]/", "", $this -> CustomerPhone);
			$twilio -> PersonEmail = $this -> CustomerEmail;
			$twilio -> ChatChannelID = $this -> ChannelID;
			$twilio -> CreateNewTask();
			$twilio -> SetNewTaskID();
			$newTaskID = $twilio -> GetNewTaskID();
			
			//$twilio -> AcceptTask();
			
			$channelUser = ChatChannelUser::WithTwilioWorkerID($this -> WorkerID);
			
			$newConversationChainDB = array();
			$newConversationChainDB['twilioTaskID'] = $newTaskID;
			$newConversationChainDB['twilioChannelID'] = $this -> ChannelID;
			$newConversationChainDB['WorkerName'] = $channelUser -> relatedUserID;
			$newConversationChainDB['CustomerName'] = $this -> CustomerFirstName . ' ' . $this -> CustomerLastname;
			$newConversationChainDB['phone'] = preg_replace("/[^0-9]/", "", $this -> CustomerPhone);
			$newConversationChainDB['customerEmail'] = $this -> CustomerEmail;
			
			$this -> db -> insert('chatconversations', $newConversationChainDB);
			
			//if(LIVE_SITE == true) {
			//	$channelUsers = new ChatChannelUsersList();
			//	$sendEmails = '';
				
			//	foreach($channelUsers -> All() as $userSingle) {	
			//		$sendEmails .= $userSingle['UserEmail'] . ', ';
			//	}	
					
			//	$email = new Email();
			//	$email -> to = rtrim($sendEmails, ', ');
			//	$email -> subject = "New Chat Request";
			//	$email -> ChatNotification();	
			//}
			
			$redirectTask = $newTaskID;
		} else {
			$redirectTask = $grabCurrentTask['twilioTaskID'];
			//$twilio -> CurrentTaskID = $grabCurrentTask['twilioTaskID'];
			//$twilio -> RetrieveTask();
			
			
		}
		
		
		$this -> json -> outputJqueryJSONObject('redirect', PATH . 'chat/conversation?id=' . $redirectTask);	
		
	}

	public function SaveMessage() {
		$message = new ChatConversationMessage();
		$message -> RelatedConversationID = $this -> _id;
		$message -> Message = $this -> NewMessage;
		$message -> ChannelID = $this -> ChannelID;
		$message -> Save();
		
		$this -> json -> outputJqueryJSONObject('hi', $this -> ChannelID);	
	}

	
}