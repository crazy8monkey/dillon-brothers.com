<?php


class PartApparelBrand extends BaseObject {
	
	private $_brandID;
	private $_seoName;
	
	public $BrandName;
	public $BrandImage;
	public $BrandID;
	public $BrandType;
	public $PartDescription;
	public $ApparelDescription;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	//public static function WithID($brandID) {
    //    $instance = new self();
    //    $instance -> _brandID = $brandID;
    //    $instance->loadByID();
    //    return $instance;
    //}
	
	public static function WithSEOName($seoName) {
		$instance = new self();
        $instance -> _seoName = $seoName;
        $instance -> loadByName();
        return $instance;
	}
	
		
	protected function loadByName() {
		$sth = $this -> db -> prepare('SELECT * FROM partaccessorybrand WHERE brandSEOUrl = :brandSEOUrl');
        $sth->execute(array(':brandSEOUrl' => $this -> _seoName));	
		
		if($sth->rowCount() > 0) {
			$record = $sth -> fetch();
		    $this->fill($record);
			//return true;
		} else {
			$this -> NotBlogPostName = true;	
			//throw new Exception('Blog Record does not exist (Value: ' . $this->_seoName . ')');	
			//return false;
		}
		
        //$record = $sth -> fetch();
        //$this->fill($record);
    }

	public function GetSEOUrl() {
		return $this -> _seoName;
	}

    protected function fill(array $row){
    	$this -> BrandID = $row['partAccessoryBrandID'];
    	$this -> BrandName = $row['brandName'];
		$this -> BrandImage = $row['partBrandImage'];
		$this -> BrandType = $row['BrandType'];
		$this -> PartDescription = $row['partDescription'];
		$this -> ApparelDescription = $row['apparelDescription'];
		
    }
	
	
	
		

}