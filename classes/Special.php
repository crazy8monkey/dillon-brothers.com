<?php


class Special extends BaseObject {
	
	private $_id;
	private $_seoName;
	private $_storeNameSEO;
	public $_OEMBrandID;
	public $_OEMBrandName;
	public $_OEMBrandDescription;
	public $OEMUrl;
	
	public $PartBrandID;
	
	public $PartBrandDescription;
	public $ApparelBrandDescription;
	
	public $SpecialID;
	public $SpecialTitle;
	public $SpecialImage;
	public $seoUrl;
	public $Description;
	public $MetaData;
	public $StoreID;
	public $StoreName;
	public $startDate;
	public $endDate;
	public $BrandName;
	public $folderName;
	public $SpecialDescriptionFooter;	
	public $SpecialType;
	
	public $PartBrandName;
	public $PartBrandImage;
	
	public $OEMBrandImage;
	
	public $VehicleSpecialBrand;
	public $PartsSpecialBrand;
	public $IsServiceSpecial;
	
	public $SpecialModifiedDate;
	public $SpecialPublishedDate;
	public $RelatedInventoryType;
	
	
	
	
	public $relatedStores = array();
		
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
	public function __construct() {
        parent::__construct();
    }
	
	public static function WithSEOname($name) {
        $instance = new self();
        $instance->_seoName = $name;
        $instance->loadByName();
        return $instance;
    }
		
	protected function loadByName() {
    	$sth = $this -> db -> prepare('SELECT * FROM specials LEFT JOIN brands ON specials.OEMBrandID = brands.brandID LEFT JOIN partaccessorybrand ON specials.PartsApparelID = partaccessorybrand.partAccessoryBrandID WHERE specialSEOurl = :seoUrl');
        $sth->execute(array(':seoUrl' => $this->_seoName));
		
		if($sth->rowCount() > 0) {
			$record = $sth -> fetch();
	        $this->fill($record);
			$this -> GetStores();
		} else {
			throw new Exception('Special Record does not exist (Value: ' . $this->_seoName . ')');	
		}
		
    }
	
	private function GetStores() {
		$sth = $this -> db -> prepare('SELECT * FROM specialrelatedstores LEFT JOIN stores ON specialrelatedstores.RelatedStoreID = stores.storeID WHERE CreatedSpecialID = :specialID');
		$sth -> execute(array(":specialID" => $this -> SpecialID));
		$this -> relatedStores = $sth -> fetchAll();
	}

    protected function fill(array $row){
    	$this -> SpecialID = $row['SpecialID'];
		$this -> SpecialTitle = $row['SpecialTitle'];
		$this -> SpecialImage = $row['SpecialMainImage'];
		$this -> Description = $row['SpecialDescription'];
    	$this -> seoUrl = $row['specialSEOurl'];
		$this -> startDate = $row['SpecialStartDate'];
		$this -> endDate = $row['SpecialEndDate'];
		$this -> folderName = $row['FolderName'];
		$this -> SpecialDescriptionFooter = $row['SpecialDescriptionFooter'];
		$this -> SpecialModifiedDate = $row['specialModified'];
		$this -> SpecialPublishedDate = $row['specialPublishedDate'];
		$this -> endDate = $row['SpecialEndDate'];
		$this -> RelatedInventoryType = $row['RelatedInventoryType'];
		$this -> SpecialType = $row['specialType'];
		$this -> _OEMBrandID = $row['OEMBrandID'];
		$this -> _OEMBrandName = $row['BrandName'];
		$this -> OEMBrandImage = $row['BrandImage'];
		$this -> _OEMBrandDescription = $row['BrandDescription'];
		$this -> OEMUrl = $row['OfferPageRef'];
		$this -> PartBrandDescription = $row['partDescription'];
		$this -> ApparelBrandDescription = $row['apparelDescription'];
		$this -> PartBrandName = $row['brandName'];
		$this -> PartBrandImage = $row['partBrandImage'];
		$this -> PartBrandID = $row['PartsApparelID'];
    }
	
	
	
	public function GetURL() {
		$type = NULL;
		if(count($this -> relatedStores) == 1) {
			switch(true) {
				case stripos($this -> relatedStores[0]['StoreName'], "MotorSports"):
					$type = "motorsport/";			
					break;
				case stripos($this -> relatedStores[0]['StoreName'], "Harley"):
					$type = "harley/";
					break;
				case stripos($this -> relatedStores[0]['StoreName'], "Indian"):
					$type = "indian/";
					break;	
			}
			
		} else {
			switch($this -> SpecialType) {
				case 2:
					$type = 'apparelspecial/';
					break;		
				case 3:
					$type = 'partsspecial/';
					break;		
				case 4:
					$type = 'servicespecial/';
					break;
				case 5:
					$type = "storespecial/";
					break;
					
	
					
			}
		}
		
		return PATH . 'specials/' . $type . $this -> seoUrl;
		
	}
	

	public function GetSpecialTitle() {
		$specialName = NULL;	
		switch($this -> SpecialType) {
			case 1:
				$specialName = $this -> _OEMBrandName . ' Manufacturer Incentive: '. $this -> SpecialTitle;
				break;			
			case 2:
				if($this -> PartBrandID != 0) {
					$specialName = $this -> PartBrandName . ' Apparel Incentive: '. $this -> SpecialTitle;	
				} else {
					$specialName = 'Apparel Incentive: '. $this -> SpecialTitle;
				}
				break;		
			case 3:
				if($this -> PartBrandID != 0) {
					$specialName = $this -> PartBrandName . ' Parts Incentive: '. $this -> SpecialTitle;
				} else {
					$specialName = 'Parts Incentive: '. $this -> SpecialTitle;
				}
				break;		
			case 4:
				$specialName = 'Service Incentive: '. $this -> SpecialTitle;
				break;
			case 5:
				$specialName = 'Store Incentive: '. $this -> SpecialTitle;
				break;
					
	
					
		}
	
		return $specialName;
	}
	
	

		

}