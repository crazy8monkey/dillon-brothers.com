<?php

class Store extends BaseObject {
	
	
	private $_id;

	public $StoreName;
	public $ContactUsEmail;
	public $ScheduleTestRide;
	public $OnlineOffer;
	public $TradeValue;
	public $SendToFriendBcc;
	public $ServiceEmail;
	public $PartsEmail;
	public $ApparelEmail;
	public $StoreImage;
	
    public function __sleep() {
        parent::__sleep();
		 return array('_id');
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($storeID) {
        $instance = new self();
        $instance -> _id = $storeID;
        $instance -> loadById();
        return $instance;
    }
	
    protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM stores WHERE storeID = :storeID');
        $sth->execute(array(':storeID' => $this->_id));	
        $record = $sth -> fetch();
        $this->fill($record);
    }

	
    protected function fill(array $row){
    	$this -> StoreName = $row['StoreName'];
    	$this -> ContactUsEmail = $row['contactUsEmail'];
		$this -> ScheduleTestRide = $row['ScheduleTestRideEmail'];
		$this -> OnlineOffer = $row['OnlineOfferEmail'];
		$this -> TradeValue = $row['TradeValueEmail'];
		$this -> SendToFriendBcc = $row['SendToFriendBcc'];
		$this -> StoreImage = $row['storeImage'];
		$this -> ServiceEmail = $row['ServiceEmailNotification'];
		$this -> PartsEmail = $row['PartsEmailNotification'];
		$this -> ApparelEmail = $row['ApparelEmailNotification'];
    }	


	public function GetStoreImage() {
		return PHOTO_URL . 'Stores/' . $this -> StoreImage;
	}
}