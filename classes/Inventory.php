<?php

class InventoryObject extends BaseObject {
	
	private $_vinNumberCheck;
	
	public $inventoryID;
	public $stockNumber;
	public $year;
	public $manufactureText;
	public $ModelName;
	public $FriendlyModelName;
	public $MSRP;
	public $Conditions;
	public $Category;
	public $VIN;
	public $Color;
	public $storeLocation;
	public $Description;
	public $Mileage;
	public $OverlayText;
	
	public $categoryID;
	public $SpecJSON;
	public $seoUrl;
	private $minPriceRange;
	private $maxPriceRange;
	
	public $EngineSizeCC;
	
	public $PublishedTime;
	public $ModifiedTime;
	
	public $StoreID;
	public $MarkAsSoldDate;
	public $storePhone;
	public $storeEmail;
	
	private $mainImage = array();
	
	public $firstName;
	public $lastName;
	public $userEmail;
	public $friendsEmail;
	public $PersonalMessage;
	public $CaptchaCheck;
	
	public $StoreName;
	
	public $TradeInYear;
	public $TradeInMake;
	public $TradeInModel;
	public $TradeInMileage;
	public $phone;
	
	public $addedAccessories;
	public $TradeComments;
	
	public $DateofRide;
	public $CommentsConcerns;
	
	public $RelatedSpecialName;
	public $RelatedIncentiveValue;
	public $RelatedIncentiveType;
	
	public $YoutubeURL;
	
	//filter content
	public $RowNumber;
	public $FilterCondition;
	public $FilterStore;
	public $FilterManufactures;
	public $FilterColors;
	public $FilterYears;
	public $FilterCategory;
	public $PageNumber;
	public $FilterMileage;
	public $FilterPrice;
	public $FilterEngineSizeCCRange;
	public $FilterOrderBy;
	public $FilterByString;
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }
	
	

    public static function WithVin($vinNumberCheck) {
        $instance = new self();
        $instance-> _vinNumberCheck = strtoupper($vinNumberCheck);
        $instance->loadByVin();
        return $instance;
    }
	
	protected function loadByVin() {
		$sth = $this -> db -> prepare('SELECT *, GROUP_CONCAT(colors.ColorText) InventoryColors FROM inventory 
												LEFT JOIN (
									                SELECT MIN(MSRP) MinMSRP, MAX(MSRP) MaxMSRP, inventoryStoreID FROM inventory WHERE IsInventoryActive = 1 GROUP BY inventoryStoreID
									            ) PriceRages ON inventory.InventoryStoreID = PriceRages.inventoryStoreID
												LEFT JOIN 
															(
																SELECT relatedMUInventoryID, GROUP_CONCAT(relatedMUIColorID) ColorIDS FROM muirelatedcolors GROUP BY relatedMUInventoryID
															) InventoryColors ON inventory.inventoryID = InventoryColors.relatedMUInventoryID 
															LEFT JOIN colors ON FIND_IN_SET(colors.colorID, InventoryColors.ColorIDS)  
												LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
                                                LEFT JOIN inventoryrelatedspecials ON inventory.inventoryID = inventoryrelatedspecials.RelatedInventoryID
                                                LEFT JOIN specials ON inventoryrelatedspecials.RelatedSpecialID = specials.SpecialID
                                                LEFT JOIN relatedspecialitems ON inventoryrelatedspecials.RelatedIncentiveID = relatedspecialitems.RelatedSpecialItemID
												LEFT JOIN stores ON inventory.inventoryStoreID = stores.storeID WHERE inventory.VinNumber LIKE :VinCheck');
        $sth->execute(array(':VinCheck' => '%' . $this -> _vinNumberCheck . '%'));	
		$record = $sth -> fetch();
		
		if($record['InventoryColors'] != NULL) {
		 	$this->fill($record);
			$this -> GetMainImage();
		} else {
			throw new Exception('Inventory Single Record does not exist (Value: ' . $this -> _vinNumberCheck . ')');	
		}
		
       
	}

    protected function fill(array $row){
    	$this -> inventoryID = $row['inventoryID'];
   		$this -> year = $row['Year'];
		$this -> manufactureText = $row['Manufacturer'];
		$this -> ModelName = $row['ModelName'];
		$this -> FriendlyModelName = $row['FriendlyModelName'];
		$this -> MSRP = $row['MSRP'];
		$this -> Conditions = $row['Conditions'];
		$this -> Category = $row['inventoryCategoryName'];
		$this -> VIN = $row['VinNumber'];
		$this -> Color = $row['InventoryColors'];
		$this -> storeLocation = $row['StoreLocation'];
		$this -> Description = $row['InventoryDescription'];
		$this -> categoryID = $row['Category'];
		$this -> SpecJSON = $row['SpecJSON'];
		$this -> seoUrl = $row['inventorySeoURL'];
		$this -> minPriceRange = $row['MinMSRP'];
		$this -> maxPriceRange = $row['MaxMSRP'];
		$this -> Mileage = $row['Mileage'];
		$this -> PublishedTime = $row['inventoryPublishedDate'];
		$this -> ModifiedTime = $row['inventoryModifiedTime'];
		$this -> StoreID = $row['InventoryStoreID'];
		$this -> MarkAsSoldDate = $row['MarkAsSoldDate'];
		$this -> StoreImage = $row['storeImage'];
		$this -> storePhone = $row['storePhone'];
		$this -> storeEmail = $row['StoreEmail'];
		$this -> StoreName = $row['StoreName'];
		$this -> OverlayText = $row['OverlayText'];
		$this -> RelatedSpecialName = $row['SpecialTitle'];
		$this -> RelatedIncentiveValue = $row['Value'];
		$this -> RelatedIncentiveType = $row['RelatedItemSpecialType'];
		$this -> YoutubeURL = $row['youtubeURL'];
		$this -> EngineSizeCC = $row['EngineSizeCC'];
		$this -> stockNumber = $row['Stock'];
		$this -> IsDummyInventoryFlag = $row['IsDummyInventory'];
    }	
	
	
	private function GetMainImage() {
		$photo = $this -> db -> prepare('SELECT * FROM inventoryphotos WHERE relatedInventoryID = :inventoryID AND MainProductPhoto = 1');
		$photo -> execute(array(':inventoryID' => $this -> inventoryID));
		$this -> mainImage = $photo -> fetch();
	}
	
	public function GetMainPhoto() {
		return PHOTO_URL . 'inventory/'. $this -> VIN . '/' . $this -> mainImage['inventoryPhotoName'] .'-l.' . $this -> mainImage['inventoryPhotoExt'];
	}
	
	public function GetBikeName() {
		$bikeName = NULL;
		if(!empty($this -> FriendlyModelName)) {
			$bikeName = $this -> year . ' ' . $this -> manufactureText . ' ' . htmlentities($this -> FriendlyModelName);
		} else {
			$bikeName = $this -> year . ' ' . $this -> manufactureText . ' ' . $this -> ModelName;
		}
		
		return $bikeName;
	}
	
	public function GenerateMSRPText() {
		$msrpString = '';
		
		if($this -> MSRP > CALLTOPRICE_THRESHOLD) {
			$msrpString = '$' . number_format($this -> MSRP, 0);
		} else {
			$msrpString = "Call To Price";
		}
		
		return $msrpString;
	}
	
	public function GetPriceRange() {
		return number_format($this -> minPriceRange, 2) . ' - '. number_format($this -> maxPriceRange, 2);
	}
	
	public function GetID() {
		return $this -> _id;
	}

	public function GetCondition() {
		switch($this -> Conditions) {
			case 1:
				return "Used";
				break;
			case 0:
				return "New";
				break;
		}
	}
	
	public function GetStoreAddress() {
		$storelocation = NULL;	
		
		if(!empty($this -> storeLocation)) {
			switch($this -> storeLocation) {
				case 'A':
					$storelocation = "Dillon Brothers Harley-Davidson<br />3838 N HWS Cleveland Blvd<br />Omaha, NE 68116";
					break;
				case 'B':
					$storelocation = "Dillon Brothers Harley-Davidson<br />2440 East 23rd Street <br />Fremont, NE 68025";
					break;		
				case 'C':
					$storelocation = "Dillon Brothers MotorSports<br />3848 N HWS Cleveland Blvd<br />Omaha, NE 68116";
					break;
				case 'D':
					$storelocation = "Dillon Brothers Indian<br />3840 N 174th Ave.<br />Omaha, NE 68116";
					break;	
			}	
		} else {
			switch($this -> StoreID) {
				case 2:
					$storelocation = "Dillon Brothers MotorSports<br />3848 N HWS Cleveland Blvd<br />Omaha, NE 68116";
					break;
				case 3:
					$storelocation = "Dillon Brothers Harley<br />3838 N HWS Cleveland Blvd<br />Omaha, NE 68116";
					break;		
				case 4:
					$storelocation = "Dillon Brothers Indian<br />3840 N 174th Ave.<br />Omaha, NE 68116";
					break;	
			}	
		}
		
		
		switch($this -> storeLocation) {
			
		}
		
		return $storelocation;
	}

	public function GetStoreName() {
		$storeName = NULL;	
		
		if(!empty($this -> storeLocation)) {
			switch($this -> storeLocation) {
				case 'A':
					$storeName = "Dillon Brothers Harley Omaha, NE 68116";
					break;
				case 'B':
					$storeName = "Dillon Brothers Harley Fremont, NE 68025";
					break;		
				case 'C':
					$storeName = "Dillon Brothers MotorSports Omaha, NE 68116";
					break;
				case 'D':
					$storeName = "Dillon Brothers Indian Omaha, NE 68116";
					break;	
			}	
		} else {
			switch($this -> StoreID) {
				case 2:
					$storeName = "Dillon Brothers MotorSports Omaha, NE 68116";
					break;
				case 3:
					$storeName = "Dillon Brothers Harley Omaha, NE 68116";
					break;		
				case 4:
					$storeName = "Dillon Brothers Indian Omaha, NE 68116";
					break;	
			}	
		}
		
		return $storeName;
	}
	
	public function GetStorePhoneNumber() {
		$storePhone = NULL;	
		switch($this -> storeLocation) {
			case 'A':
				$stores = explode(" / ", $this -> storePhone);
				$storePhone = str_replace("A:", "", $stores[0]);
				break;
			case 'B':
				$stores = explode(" / ", $this -> storePhone);
				$storePhone = str_replace("B:", "", $stores[1]);
				break;		
			case 'C':
				$storePhone = $this -> storePhone;
				break;
			case 'D':
				$storePhone = $this -> storePhone;
				break;	
		}
		
		return $storePhone;
	}
	
	public function GetStoreEmail() {
		$storeEmail = NULL;	
		switch($this -> storeLocation) {
			case 'A':
				$stores = explode(" / ", $this -> storeEmail);
				$storeEmail = str_replace("A:", "", $stores[0]);
				break;
			case 'B':
				$stores = explode(" / ", $this -> storeEmail);
				$storeEmail = str_replace("B:", "", $stores[1]);
				break;		
			case 'C':
				$storeEmail = $this -> storeEmail;
				break;
			case 'D':
				$storeEmail = $this -> storeEmail;
				break;	
		}
		
		return $storeEmail;
	}
	

	public function GetInventorySpecs() {
		return file_get_contents(INVENTORY_VEHICLE_INFO_URL . $this -> SpecJSON);
	}
	
	public function GetURL() {
		$condition = NULL;	
		switch($this -> Conditions) {
			case 1:
				$condition = "used/";
				break;
			case 0:
				$condition = "new/";
				break;
		}
		
		
		return PATH . 'inventory/' . $condition . $this -> seoUrl;
	}

	public function GetMetaDescription() {
		switch($this -> Conditions) {
			case '1':
				$conditionText = "New";
				break;
			case '0':
				$conditionText = "Used";
				break;
		}
		
		
		if($this -> FriendlyModelName != NULL) {
			$BikeName = $this -> manufactureText . ' ' . preg_replace('/[^A-Za-z0-9\-]/', '', $this -> FriendlyModelName) . ' ' . $this -> year;
		} else {
			$BikeName = $this -> manufactureText . ' ' . $this -> ModelName . ' ' . $this -> year;
		}
		if(!empty($this -> storeLocation)) {
			switch($this -> storeLocation) {
				case 'A':
					$store = "Harley";
					$location = "Omaha";
					break;
				case 'B':
					$store = "Harley";
					$location = "Fremont";
					break;		
				case 'C':
					$store = "Motorsports";
					$location = "Omaha";
					break;
				case 'D':
					$store = "Indian";
					$location = "Omaha";
					break;	
			}	
		} else {
			switch($this -> StoreID) {
				case 2:
					$store = "Motorsports";
					$location = "Omaha";
					
					break;
				case 3:
					$store = "Harley";
					$location = "Omaha";
					break;		
				case 4:
					$store = "Indian";
					$location = "Omaha";
					break;	
			}	
		}
		
		
		
		
		
		return "Dillon Brothers " . $conditionText . " Inventory Search " . $BikeName . " " . $location . " " . $store . " Nebraska";
	}


	public function ValidateLeadEmail($type) {
		session_start();
		$validationErrors = array();
		
		$captcha = new GoogleCaptaHandler();
		$captcha -> response = $_POST['g-recaptcha-response'];
		
		$googleCheckResponse = json_decode($captcha -> GetResponse(), true);
	
		//empty first name
		if($this -> validate -> emptyInput($this -> firstName)) {
			array_push($validationErrors, array("inputID" => 1,
												'errorMessage' => 'Required'));
		}

		if($this -> validate -> emptyInput($this -> lastName)) {
			array_push($validationErrors, array("inputID" => 2,
												'errorMessage' => 'Required'));
		}
		
		if($this -> validate -> emptyInput($this -> userEmail)) {
			array_push($validationErrors, array("inputID" => 3,
												'errorMessage' => 'Required'));
		} else if($this -> validate -> correctEmailFormat($this -> userEmail)) {
			array_push($validationErrors, array("inputID" => 3,
												'errorMessage' => 'Needs to be in email format'));
		}
		
		if($type == "sendToFriend") {
			if($this -> validate -> emptyInput($this -> friendsEmail)) {
				array_push($validationErrors, array("inputID" => 4,
													'errorMessage' => 'Required'));
			} else if($this -> validate -> correctEmailFormat($this -> friendsEmail)) {
				array_push($validationErrors, array("inputID" => 4,
													'errorMessage' => 'Needs to be in email format'));
			}	
		}
		
		if($type == "tradeinvalue" || $type == "testride" || $type == "contactus") {
			if($this -> validate -> emptyInput($this -> phone)) {
				array_push($validationErrors, array("inputID" => 6,
													'errorMessage' => 'Required'));
			} 
		}
		if($type == "testride") {
			if($this -> validate -> emptyInput($this -> DateofRide)) {
				array_push($validationErrors, array("inputID" => 7,
													'errorMessage' => 'Required'));
			}
		}
		
		
		
		if(!$googleCheckResponse['success']) {
			array_push($validationErrors, array("inputID" => 5,
												'errorMessage' => 'Required'));
		}
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}


	public function SendEmail($type) {
		$emailSave = new EmailLeadObject();
		
		
		switch($type) {
			case "sendToFriend":
				
				$emailSave -> type = 1;
				
				if(LIVE_SITE == true) {
					
					$content = array();
					$content['company-logo'] = $this -> StoreImage;
					$content['referred-person'] = $this -> firstName . ' ' . $this -> lastName; 
					$content['store-id'] = $this -> StoreID;
                    $content['store-name'] = $this -> StoreName;
					$content['personal-message'] = $this -> PersonalMessage;
					$content['vehicle-name']  = $this -> GetCondition() . ' ' . $this -> GetBikeName();
					$content['MSRP'] = '$'. number_format($this -> MSRP, 0);
					$content['MainImage'] = $this -> GetMainPhoto();
					$content['bikeURL'] = $this -> GetURL();
					$content['ModelNumber'] = $this -> ModelName;
					$content['category'] = $this -> Category;
					$content['VIN'] = $this -> VIN;
					$content['Mileage']  = number_format($this -> Mileage, 0);
					$content['Stock'] = $this-> stockNumber;
					$content['Color'] = $this -> Color;
					$content['StoreLocation'] = $this -> GetStoreAddress();		
					$content['StoreContactInfo'] = $this -> GetStorePhoneNumber() . "<br />" . $this -> storeEmail;		
					
					$sendToFriend = new Email();
					$sendToFriend -> to = $this -> friendsEmail;
					$sendToFriend -> Bcc = $store -> SendToFriendBcc;
					$sendToFriend -> subject = "A Friend Recommended a vehicle to you: Dillon Brothers";
					
					
					$sendToFriend -> SendToFriend($content);
					
					
				}
				
				$emailSave -> Content = json_encode(array("ReferredPerson" => $this -> firstName . ' ' . $this -> lastName,
														  "ReferredPersonEmail" => $this -> userEmail,
														  "FriendsEmail" => $this -> friendsEmail,
														  "PersonalMessage" => $this -> PersonalMessage));
				
					
				
				
				break;
		case "onlineoffer":
			$emailSave -> type = 2;
			
			$emailSave -> Content = json_encode(array("OnlineOfferPerson" => $this -> firstName . ' ' . $this -> lastName,
													  "OnlineOfferEmail" => $this -> userEmail,
													  "Comments" => $this -> PersonalMessage));
													  
			if(LIVE_SITE == true) {
				$this -> LeadEmailSend("onlineoffer");
			}										  
													  
			
			break;
						
		case "tradeinvalue":
			$emailSave -> type = 3;
			
			$emailSave -> Content = json_encode(array("FullName" => $this -> firstName . ' ' . $this -> lastName,
													  "ContactInfo" => $this -> userEmail . ' / ' . $this -> phone,
													  "TradeInInformation" => $this -> TradeInYear . ' ' . $this -> TradeInMake . ' ' . $this -> TradeInModel . ' / ' . $this -> TradeInMileage,
													  "AddedAccessoriesInfo" => $this -> addedAccessories,
													  "TradeComments" => $this -> TradeComments));
													  
			if(LIVE_SITE == true) {
				$this -> LeadEmailSend("tradeinvalue");
			}												  
													  
													  
			
			break;		
		case "testride":
			$emailSave -> type = 4;
			
			$emailSave -> Content = json_encode(array("FullName" => $this -> firstName . ' ' . $this -> lastName,
													  "ContactInfo" => $this -> userEmail . ' / ' . $this -> phone,
													  "DateOfRide" => $this -> DateofRide));
			
			if(LIVE_SITE == true) {
				$this -> LeadEmailSend("testride");
			}											
			
			break;
			
		case "contactus":
			$emailSave -> type = 5;
			
			$emailSave -> Content = json_encode(array("FullName" => $this -> firstName . ' ' . $this -> lastName,
													  "ContactInfo" => $this -> userEmail . ' / ' . $this -> phone,
													  "CommentsConcerns" => $this -> CommentsConcerns));
													  
													  
			if(LIVE_SITE == true) {
				$this -> LeadEmailSend("contactus");
			}											  
			
			break;
		
		}

		$this -> json -> outputJqueryJSONObject('emailsent', array("StockNumber" => $this-> stockNumber));

		$emailSave -> VehicleInfo = json_encode(array("AddressLocation" => $this -> GetStoreAddress(),	
													  "Category" => $this -> Category,
													  "MSRP" => '$'. number_format($this -> MSRP, 0),
													  "VehicleName" => $this -> GetCondition() . ' ' . $this -> GetBikeName(),
													  "VIN" => $this -> VIN,
													  "Color" => $this -> Color,
													  "StockNumber" => $this-> stockNumber,
													  "Mileage" => number_format($this -> Mileage, 0)));
													  
		$emailSave -> StoreID = $this -> StoreID;
		
		$emailSave -> Source = 1;
		$emailSave -> Save();
	}

	private function LeadEmailSend($type) {
		$store = Store::WithID($this -> StoreID);
		
		$leadEmail = new Email();
		
		$content = array();
		$content['logo'] = $store -> GetStoreImage();
		
		$content['fullname'] = $this -> firstName . ' ' . $this -> lastName;
		switch($type) {
			case "onlineoffer":
				$content['useremail'] = $this -> userEmail;
				$content['comments'] = $this -> PersonalMessage;
				
				$leadEmail -> subject = "Online Offer Lead";
				$leadEmail -> to = $store -> OnlineOffer;
				
				//send lead email
				if($this -> StoreID == 2) {
					$this -> ImportLeadToCRM(MOTORSPORT_DEALERSHIP_ID, 'Online Offer');
				} else if($this -> StoreID == 3) {
					$this -> ImportLeadToCRM(HARLEY_DEALERSHIP_ID, 'Online Offer');
				}
				
				break;
				
			case "tradeinvalue":
				$content['contactInfo'] = $this -> userEmail . ' / ' . $this -> phone;
				
				if(!empty($this -> TradeInMileage)) {
					$content['TradeInVehicle'] = $this -> TradeInYear . ' ' . $this -> TradeInMake . ' ' . $this -> TradeInModel . ' / ' . $this -> TradeInMileage . ' Miles';	
				} else {
					$content['TradeInVehicle'] = $this -> TradeInYear . ' ' . $this -> TradeInMake . ' ' . $this -> TradeInModel;
				}
				
				$content['AddedAccessoriesText'] = $this -> addedAccessories;
				$content['TradeInComments'] = $this -> TradeComments;
				
				$leadEmail -> subject = "Trade In Value Lead";
				$leadEmail -> to = $store -> TradeValue;
				
				//send lead email
				if($this -> StoreID == 2) {
					$this -> ImportLeadToCRM(MOTORSPORT_DEALERSHIP_ID, 'Trade In Value');
				} else if($this -> StoreID == 3) {
					$this -> ImportLeadToCRM(HARLEY_DEALERSHIP_ID, 'Trade In Value');
				}
				
				break;
			case "testride":
				$content['contactInfo'] = $this -> userEmail . ' / ' . $this -> phone;
				$content['date-of-ride'] = $this -> DateofRide;
				
				$leadEmail -> subject = "Test Ride Lead";
				$leadEmail -> to = $store -> ScheduleTestRide;
				
				//send lead email
				if($this -> StoreID == 2) {
					$this -> ImportLeadToCRM(MOTORSPORT_DEALERSHIP_ID, 'Test Ride');
				} else if($this -> StoreID == 3) {
					$this -> ImportLeadToCRM(HARLEY_DEALERSHIP_ID, 'Test Ride');
				}
				
				break;
			case "contactus":
				$content['contactInfo'] = $this -> userEmail . ' / ' . $this -> phone;
				$content['CommentsConcerns']= $this -> CommentsConcerns;
				
				$leadEmail -> subject = "Contact Us Lead";
				$leadEmail -> to = $store -> ContactUsEmail;
				
				//send lead email
				if($this -> StoreID == 2) {
					$this -> ImportLeadToCRM(MOTORSPORT_DEALERSHIP_ID, 'Contact Us');
				} else if($this -> StoreID == 3) {
					$this -> ImportLeadToCRM(HARLEY_DEALERSHIP_ID, 'Contact Us');
				}
				
				break;
		}
		
				
		$content['VehicleName'] = $this -> GetCondition() . ' ' . $this -> GetBikeName();
		$content['category'] = $this -> Category;
		$content['MSRP'] = '$'. number_format($this -> MSRP, 0);
		$content['VIN'] = $this -> VIN;
		$content['Color'] = $this -> Color;
		$content['Stock'] = $this-> stockNumber;
		$content['Mileage'] = number_format($this -> Mileage, 0);
		$content['Address'] = $this -> GetStoreAddress();		
								
		$leadEmail -> Lead($type, $content);
		
		
		
	}

	private function ImportLeadToCRM($dealershipID, $leadType) {
		$xml_data ='<ProspectImport>
					<Item>
					<SourceProspectId>dillon-brothers.com</SourceProspectId>
					<DealershipId>'. $dealershipID . '</DealershipId>
					<Email>' . $this -> userEmail . '</Email>
					<Name>' . $this -> firstName . ' ' . $this -> lastName . '</Name>
					<Phone>' . $this -> phone . '</Phone>
					<AltPhone />
					<SourceDate></SourceDate>
					<Address1></Address1>
					<Address2 />
					<City></City>
					<State></State>
					<ZipCode></ZipCode>
					<VehicleType>' . $this -> Category . '</VehicleType>
					<VehicleMake>' . $this -> manufactureText . '</VehicleMake>
					<VehicleModel>' . $this -> ModelName . ' | ' . $this -> FriendlyModelName . '</VehicleModel>
					<VehicleYear>' . $this -> year . '</VehicleYear>
					<Notes>Source: dillon-brothers.com | Lead Type: '. $leadType . '</Notes>
					<ProspectType></ProspectType>
					<BirthDate></BirthDate>
					<CAEmployerName></CAEmployerName>
					<CAJobTitle></CAJobTitle>
					<CAHireDate></CAHireDate>
					<CAMonthlyIncome></CAMonthlyIncome>
					<CATimeAtAddress></CATimeAtAddress>
					<CARentOrMortgagePayment></CARentOrMortgagePayment>
					<CARentOrOwn></CARentOrOwn>
					</Item>
					</ProspectImport>';
	 	
		$url = 'http://pch.v-sept.com/VSEPTPCHPostService.aspx?method=AddProspect&sourceid=DillonBros';
		
		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
		// Following line is compulsary to add as it is:
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "ProspectXML=" . $xml_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
        $data = curl_exec($ch);
        curl_close($ch);
	}


	public function ToggleInventory($updateFilters) {
		$inventoryListHtmlString = NULL;
		
		
		
		
		$inventory = new InventoryList();
		
		if(isset($this -> FilterCondition)) {
			$inventory -> Condition = $this -> FilterCondition;
		}
		
		if(isset($this -> FilterStore)) {
			$inventory -> Stores = $this -> FilterStore;
		}
		
		if(isset($this -> FilterManufactures)) {
			$inventory -> Manufactures = $this -> FilterManufactures;
		}
		
		if(isset($this -> FilterColors)) {
			$inventory -> Colors = $this -> FilterColors;
		}
		
		if(isset($this -> FilterYears)) {
			$inventory -> Years = $this -> FilterYears;
		}
		
		if(isset($this -> FilterCategory)) {
			$inventory -> Categories = $this -> FilterCategory;
		}
			
		if(isset($this -> FilterPrice)) {
			$inventory -> PriceRange = $this -> FilterPrice;
		}
		
		if(isset($this -> FilterEngineSizeCCRange)) {
			$inventory -> EngineSizeCC = $this -> FilterEngineSizeCCRange;
		}	
		
		if(isset($this -> FilterByString)) {
			$inventory -> SearchString = $this -> FilterByString;	
		}
		
		
		
		$inventory -> pageNumber = $this -> PageNumber;
			
		$inventory -> MileageRange = $this -> FilterMileage;
		$inventory -> UpdateFilters	= $updateFilters;	
		$inventory -> OrderByInventory = $this -> FilterOrderBy;
		
		
	
		$inventoryHTML = '';
		
		$appendURL = '';
		
		foreach(array_chunk($inventory -> AllLiveInventory($this -> PageNumber), $this -> RowNumber, true) as $inventoryDuece) {
			$inventoryHTML .= '<div class="row">';			
			foreach($inventoryDuece as $inventorySingle) {
				$bikeURL = PATH . 'inventory/' . $inventorySingle['InventoryURLString'];
				
				$styleString ='';
				if($inventorySingle['InventoryMainPhoto'] == NULL) {
					$styleString .= 'style="height:185px"';	
				}
				
				 
				
				if($this -> RowNumber == '2') {
					
					
					$inventoryHTML .='<div class="col-md-6">
									<div class="inventorySingleObject">
										<a href="' . $bikeURL . '"><div class="inventoryTitle" style="color:black;">'. $inventorySingle['InventoryBikeName']. '</div></a>
										<div class="row" style="margin-left: 0px; margin-right: 0px;">
											<a href="'. $bikeURL . '">
												<div class="col-md-5 MainInventoryPhoto" '. $styleString . '>';
													if($inventorySingle['InventoryMainPhoto'] != NULL) {
														$bikeMainPhoto = PHOTO_URL . 'inventory/' . $inventorySingle['InventoryMainPhoto'];
										$inventoryHTML .='<div class="inventoryPhotoSingle" style="background:url('. $bikeMainPhoto .') no-repeat center">
															<img src="'. $bikeMainPhoto .'" alt="'. $inventorySingle['inventoryAltTag'] . '" title="'. $inventorySingle['inventoryTitleTag'] . '" />	
														</div>';
														
													}
													if($inventorySingle['MarkAsSoldDate'] != NULL) {
														$inventoryHTML .='<div class="SoldOutText" style="font-size:16px">SOLD</div>';
													}
													$inventoryHTML .='<div class="NoPicture"></div></div></a>';
							$inventoryHTML .='<div class="col-md-7">';
											
							
								$inventoryHTML .='<div class="row">
													<div class="col-md-12">';
													if($inventorySingle['OverlayText'] != NULL) {
														$inventoryHTML .='<div class="OverlayTextList">'. $inventorySingle['OverlayText'] .'</div>';
													}
								$inventoryHTML .='<div class="CarDetailInfo">
															<div class="MSRPPriceList">$' . number_format($inventorySingle['MSRP'], 0) . '</div>
															<table>';
																$inventoryHTML .='<tr><td style="padding-right:15px;"><strong>Condition:</strong></td><td>' . $inventorySingle['ConditionText'] .'</td></tr>';
																if(!empty($inventorySingle['Stock'])) {
																	$inventoryHTML .='<tr><td style="padding-right:15px;"><strong>Stock:</strong></td><td>'. $inventorySingle['Stock'] . '</td></tr>';
																}
																
																$inventoryHTML .='<tr><td style="padding-right:15px;"><strong>Color:</strong></td><td>'. str_replace(',', '/', $inventorySingle['SelectedColor']). '</td></tr>
																<tr><td style="padding-right:15px;"><strong>Mileage:</strong></td><td>'. number_format($inventorySingle['Mileage'], 0) .'</td></tr>
															</table>
														</div>
													</div>
														
												</div>
											</div>
										</div>
										<div style="border-top: 1px solid #eaeaea;"></div>
										<div class="row">
											<div class="col-md-8">
												<a href="'. $bikeURL .'">';
													switch($inventorySingle['InventoryStoreID']) {
														case 2:
															$inventoryHTML .='<div class="ViewDetailsButton MotorSportShoppingTools">More Info.</div>';
															break;
														case 3:
															$inventoryHTML .='<div class="ViewDetailsButton HarleyColorShoppingTools">More Info.</div>';
															break;
														case 4:
															$inventoryHTML .='<div class="ViewDetailsButton IndianColorShoppingTools">More Info.</div>';
															break;
													}
													
												$inventoryHTML .='</a>
										
											</div>
										</div>
									</div>
								</div>';
					
				} else {
					
					$inventoryHTML .='<div class="col-md-4">
									<div class="inventorySingleObject">
										<a href="' . $bikeURL . '"><div class="inventoryTitle" style="color:black;">'. $inventorySingle['InventoryBikeName']. '</div></a>
										<div class="row" style="margin-left: 0px; margin-right: 0px;">
											<a href="'. $bikeURL . '">
												<div class="col-md-12 MainInventoryPhotoThirds" '. $styleString . '>';
													if($inventorySingle['InventoryMainPhoto'] != NULL) {
														$bikeMainPhoto = PHOTO_URL . 'inventory/' . $inventorySingle['InventoryMainPhoto'];
										$inventoryHTML .='<div class="inventoryPhotoSingle">
															<img src="'. $bikeMainPhoto .'" alt="'. $inventorySingle['inventoryAltTag'] . '" title="'. $inventorySingle['inventoryTitleTag'] . '" />	
														</div>';
														
													}
													if($inventorySingle['MarkAsSoldDate'] != NULL) {
														$inventoryHTML .='<div class="SoldOutText" style="font-size:16px">SOLD</div>';
													}
													$inventoryHTML .='<div class="NoPicture"></div></div></a>';
							$inventoryHTML .='<div class="col-md-12">';
											if($inventorySingle['OverlayText'] != NULL) {
												$inventoryHTML .='<div class="OverlayTextListThirds">'. $inventorySingle['OverlayText'] .'</div>';
											}
							
								$inventoryHTML .='<div class="row">
													<div class="col-md-12">
														<div class="CarDetailInfo">
															<div class="MSRPPriceList">$' . number_format($inventorySingle['MSRP'], 0) . '</div>
															<table>
																<tr><td style="padding-right:15px;"><strong>Condition:</strong></td><td>' . $inventorySingle['ConditionText'] .'</td></tr>';
																if(!empty($inventorySingle['Stock'])) {
																	$inventoryHTML .='<tr><td style="padding-right:15px;"><strong>Stock:</strong></td><td>'. $inventorySingle['Stock'] . '</td></tr>';
																}
																$inventoryHTML .='<tr><td style="padding-right:15px;"><strong>Color:</strong></td><td>'. str_replace(',', '/', $inventorySingle['SelectedColor']). '</td></tr>
																<tr><td style="padding-right:15px;"><strong>Mileage:</strong></td><td>'. number_format($inventorySingle['Mileage'], 0) .'</td></tr>
															</table>
														</div>
													</div>
														
												</div>
											</div>
										</div>
										<div style="border-top: 1px solid #eaeaea;"></div>
										<div class="row">
											<div class="col-md-8">
												<a href="'. $bikeURL .'">
													<div class="ViewDetailsButton">
														More Info.
													</div>
												</a>
										
											</div>
										</div>
									</div>
								</div>';
				}
				
				
								
								
			}
			$inventoryHTML .='</div>';
		}
		
		if($this -> RowNumber == '2') {
			$appendURL .= '&View=2By2';
		} else {
			$appendURL .= '&View=3By3';
		}
		
		$filteredInventory = array();
		
		
		$filteredInventory['html'] = $inventoryHTML;
		$filteredInventory['count'] = $inventory -> Count();
		$filteredInventory['PageNumber'] = $this -> PageNumber;
		
		$filteredInventory['AppendUrl'] = '#FilerApplied' . $inventory -> AppendURL() . $appendURL;
		$filteredInventory['Pagination'] = $inventory -> PaginationLinks();
		$filteredInventory['Mobile-Pagination'] = $inventory -> MobilePagination();
		
		if($updateFilters == 'true') {
			$filteredInventory['YearFilter'] = $inventory -> Years(true);	
			$filteredInventory['StoreFilter'] = $inventory -> Stores(true);
			$filteredInventory['CategoryFilter'] = $inventory -> Categories(true);
			$filteredInventory['ManufactureFilter'] = $inventory -> manufactures(true);
			$filteredInventory['ColorFilter'] = $inventory -> Colors(true);	
			$filteredInventory['MileageFilter'] = $inventory -> Mileage(true);
			$filteredInventory['ConditionFilter'] = "";
			$filteredInventory['PriceFilter'] = $inventory -> PriceRange(true);
			$filteredInventory['EngineSizeCCFilter'] = $inventory -> EngineSizeCC(true);
				
			/**
			$filteredInventory['StoreFilter'] = $inventory -> Stores(true);
			$filteredInventory['CategoryFilter'] = $inventory -> Categories(true);
			$filteredInventory['ManufactureFilter'] = $inventory -> manufactures(true);
			$filteredInventory['ColorFilter'] = $inventory -> Colors(true);	
			$filteredInventory['MileageFilter'] = $inventory -> Mileage(true);
			$filteredInventory['PriceFilter'] = $inventory -> PriceRange(true);
			$filteredInventory['EngineSizeCCFilter'] = $inventory -> EngineSizeCC(true);
			
			
			$filteredInventory['ConditionFilter'] = $inventory -> Condition(true);
			
			**/
		} else {
			$filteredInventory['CategoryFilter'] = '';
			$filteredInventory['YearFilter'] = '';
			$filteredInventory['StoreFilter'] = '';
			$filteredInventory['ManufactureFilter'] = '';
			$filteredInventory['ColorFilter'] = '';	
			$filteredInventory['MileageFilter'] = '';
			$filteredInventory['ConditionFilter'] = '';
			$filteredInventory['PriceFilter'] = '';
			$filteredInventory['EngineSizeCCFilter'] = '';
		}
				
		
		return $filteredInventory;
	

	}

	public function QuickSearch() {
		$inventory = new InventoryList();
		
		if(!empty($this -> FilterByString)) {
			$inventory -> SearchString = $this -> FilterByString;	
		}
		
		
		
		$inventoryHTML = '';
		foreach(array_chunk($inventory -> AllLiveInventory(1), 2, true) as $inventoryDuece) {
			$inventoryHTML .= '<div class="row">';			
			foreach($inventoryDuece as $inventorySingle) {
				$bikeURL = PATH . 'inventory/' . $inventorySingle['InventoryURLString'];
				
				$styleString ='';
				if($inventorySingle['InventoryMainPhoto'] == NULL) {
					$styleString .= 'style="height:185px"';	
				}
						
					
					$inventoryHTML .='<div class="col-md-6">
									<div class="inventorySingleObject">
										<a href="' . $bikeURL . '"><div class="inventoryTitle" style="color:black;">'. $inventorySingle['InventoryBikeName']. '</div></a>
										<div class="row" style="margin-left: 0px; margin-right: 0px;">
											<a href="'. $bikeURL . '">
												<div class="col-md-5 MainInventoryPhoto" '. $styleString . '>';
													if($inventorySingle['InventoryMainPhoto'] != NULL) {
														$bikeMainPhoto = PHOTO_URL . 'inventory/' . $inventorySingle['InventoryMainPhoto'];
										$inventoryHTML .='<div class="inventoryPhotoSingle" style="background:url('. $bikeMainPhoto .') no-repeat center">
															<img src="'. $bikeMainPhoto .'" alt="'. $inventorySingle['inventoryAltTag'] . '" title="'. $inventorySingle['inventoryTitleTag'] . '" />	
														</div>';
														
													}
													if($inventorySingle['MarkAsSoldDate'] != NULL) {
														$inventoryHTML .='<div class="SoldOutText" style="font-size:16px">SOLD</div>';
													}
													$inventoryHTML .='<div class="NoPicture"></div></div></a>';
							$inventoryHTML .='<div class="col-md-7">';
											
							
								$inventoryHTML .='<div class="row">
													<div class="col-md-12">';
													if($inventorySingle['OverlayText'] != NULL) {
														$inventoryHTML .='<div class="OverlayTextList">'. $inventorySingle['OverlayText'] .'</div>';
													}
								$inventoryHTML .='<div class="CarDetailInfo">
															<div class="MSRPPriceList">$' . number_format($inventorySingle['MSRP'], 0) . '</div>
															<table>
																<tr><td style="padding-right:15px;"><strong>Condition:</strong></td><td>' . $inventorySingle['ConditionText'] .'</td></tr>';
																if(!empty($inventorySingle['Stock'])) {
																	$inventoryHTML .='<tr><td style="padding-right:15px;"><strong>Stock:</strong></td><td>'. $inventorySingle['Stock'] . '</td></tr>';
																}
																$inventoryHTML .='<tr><td style="padding-right:15px;"><strong>Color:</strong></td><td>'. str_replace(',', '/', $inventorySingle['SelectedColor']). '</td></tr>
																<tr><td style="padding-right:15px;"><strong>Mileage:</strong></td><td>'. number_format($inventorySingle['Mileage'], 0) .'</td></tr>
															</table>
														</div>
													</div>
														
												</div>
											</div>
										</div>
										<div style="border-top: 1px solid #eaeaea;"></div>
										<div class="row">
											<div class="col-md-8">
												<a href="'. $bikeURL .'">
													<div class="ViewDetailsButton">
														More Info.
													</div>
												</a>
										
											</div>
										</div>
									</div>
								</div>';
					
				
								
								
			}
			$inventoryHTML .='</div>';
		}

		$filteredInventory = array();

		$filteredInventory['html'] = $inventoryHTML;
		$filteredInventory['count'] = $inventory -> Count();		
		$filteredInventory['Pagination'] = $inventory -> PaginationLinks();

		return $filteredInventory;
		
		
	}

	 



}