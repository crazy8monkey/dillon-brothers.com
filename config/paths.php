<?php

define ('PATH', 'http://127.0.0.1/DillonBrothersMain/');
define('LIBS', 'libs/');
define('CLASSES', 'classes/');
define('LISTS', 'lists/');

define('PHOTO_URL', 'http://localhost/DillonBrothersPhotos/');
define('INVENTORY_VEHICLE_INFO_URL', 'http://localhost/Inventory/');

define("NEWSLETTER_URL", "http://localhost/DillonBrothers/view/photos/newsletter/");
define('BRANDS_URL', 'http://localhost/DillonBrothers/view/photos/brands/');
define('PARTS_BRANDS_URL', 'http://localhost/DillonBrothers/view/photos/PartsBrand/');
define("MISC_PHOTO_URL", "http://localhost/DillonBrothers/view/photos/miscellaneous/");



define("JSON_EVENT_URL", "http://localhost/DillonBrothers/view/PageEventJSON/");

define('PHOTO_PATH', '../DillonBrothersPhotos/');
define('TWILIO_API_PATH', '../Twilio/');

define("PHOTO_EVENT_PATH", '../DillonBrothers/view/photos/events/');
define('NEWSLETTER_PATH', '../DillonBrothers/view/photos/newsletter/');
define('EMPLOYEE_PATH', '../DillonBrothers/view/photos/employees/');
define('SPECIALS_PATH', '../DillonBrothers/view/photos/specials/');
define('BRANDS_PATH', '../DillonBrothers/view/photos/brands/');

define('ENCRYPTION_KEY', 'd0a7e7997b6d5fcd55f4b5c32611b87cd923e88837b63bf2941ef819dc8ca282');
//dev testing
define('GOOGLE_ANALYTICS_ID', 'UA-96783488-1');

define("LIVE_SITE", false);
define("EMAIL_SUBJECT", " | Testing");

define('IP_LOOKUP', 'http://ip-api.com/#');

// email header types
define ('MYSQL_ERROR_TYPE', 'MYSQL ERROR');

define('STRIPE_PUBLISH_KEY', 'pk_test_gH9s5ZbjMOtznCSVcwnLgqE0');
define('STRIPE_PRIVATE_KEY', 'sk_test_rb9BDBs8S0njtDxx1pDHRAI7');

//system default error message
define ('SYSTEM_ERROR_MESSAGE', 'Something went wrong with our system. Please try again');


