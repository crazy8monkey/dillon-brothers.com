<?php

class EmployeeList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function MotorSportEmployees() {
		return $this -> db -> select('SELECT * FROM employees LEFT JOIN employeestorerelation On employees.EmployeeID = employeestorerelation.EmployeeID LEFT JOIN stores On employeestorerelation.RelatedStoreID = stores.storeID WHERE employeestorerelation.RelatedStoreID = 2 LIMIT 6');
	}
	
	public function HarleyDavidsonEmployees() {
		return $this -> db -> select('SELECT * FROM employees LEFT JOIN employeestorerelation On employees.EmployeeID = employeestorerelation.EmployeeID LEFT JOIN stores On employeestorerelation.RelatedStoreID = stores.storeID WHERE employeestorerelation.RelatedStoreID = 3 LIMIT 6');
	}

	public function IndianEmployees() {
		return $this -> db -> select('SELECT * FROM employees LEFT JOIN employeestorerelation On employees.EmployeeID = employeestorerelation.EmployeeID LEFT JOIN stores On employeestorerelation.RelatedStoreID = stores.storeID WHERE employeestorerelation.RelatedStoreID = 4 LIMIT 6');
	}
}