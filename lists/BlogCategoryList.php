<?php

class BlogCategoryList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function AllBlogCategories() {
		return $this -> db -> select('SELECT * FROM blogpostcategories');
	}

}