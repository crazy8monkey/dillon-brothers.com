<?php

class SpecialsList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function AllSpecials() {
		$specialsList = $this -> db -> prepare("SELECT *,
												       RelatedStoresTable.StoreIDS,
												       GROUP_CONCAT(stores.storeName) StoreNames,
												 	   GROUP_CONCAT(stores.storeSpecialImage) storeSpecialImages
												FROM specials LEFT JOIN
														(SELECT DISTINCT CreatedSpecialID, GROUP_CONCAT(RelatedStoreID) StoreIDS FROM specialrelatedstores GROUP BY CreatedSpecialID) RelatedStoresTable ON specials.SpecialID = RelatedStoresTable.CreatedSpecialID 
														 LEFT JOIN stores ON FIND_IN_SET(RelatedStoresTable.StoreIDS, stores.storeID) 
														 LEFT JOIN brands ON specials.OEMBrandID = brands.brandID 
														 LEFT JOIN partaccessorybrand ON specials.PartsApparelID = partaccessorybrand.partAccessoryBrandID WHERE specials.isSpecialsActive = 1 AND specials.SpecialEndDate >= :TodaysDate GROUP BY specials.SpecialID");
		
		$specialsList -> execute(array(":TodaysDate" => date('Y-m-d', $this -> time -> NebraskaTime())));
		return $specialsList -> fetchAll();												
	}

	public function GetRandomSpecial() {
		$randomSpecial = $this -> db -> prepare("SELECT *,
												       RelatedStoresTable.StoreIDS,
												       GROUP_CONCAT(stores.storeName) StoreNames,
												 	   GROUP_CONCAT(stores.storeSpecialImage) storeSpecialImages
												FROM specials LEFT JOIN
														(SELECT DISTINCT CreatedSpecialID, GROUP_CONCAT(RelatedStoreID) StoreIDS FROM specialrelatedstores GROUP BY CreatedSpecialID) RelatedStoresTable ON specials.SpecialID = RelatedStoresTable.CreatedSpecialID 
														 LEFT JOIN stores ON FIND_IN_SET(RelatedStoresTable.StoreIDS, stores.storeID) 
														 LEFT JOIN brands ON specials.OEMBrandID = brands.brandID 
														 LEFT JOIN partaccessorybrand ON specials.PartsApparelID = partaccessorybrand.partAccessoryBrandID WHERE specials.isSpecialsActive = 1 AND specials.SpecialEndDate >= :TodaysDate GROUP BY specials.SpecialID ORDER BY RAND() LIMIT 1");	
		
		$randomSpecial -> execute(array(":TodaysDate" => date('Y-m-d', $this -> time -> NebraskaTime())));
		return $randomSpecial -> fetchAll();
	}
	
	public function MotorSportSpecials() {
		//SELECT DISTINCT specialrelatedstores.SpecialID, GROUP_CONCAT(IF(specials.VehicleSpecialBrand IS NOT NULL, vehiclespecials.VehicleCategory, NULL)) RelatedIncentiveCategories, GROUP_CONCAT(IF(specials.VehicleSpecialBrand IS NOT NULL, vehiclespecials.VehicleName, NULL)) RelatedIncentiveName, specials.SpecialStartDate, specials.specialEndDate, specials.SpecialMainImage, specials.SpecialDescription, specials.specialSEOurl, specials.FolderName, specials.SpecialTitle, brands.BrandName FROM specialrelatedstores LEFT JOIN specials ON specialrelatedstores.SpecialID = specials.SpecialID LEFT JOIN stores ON specialrelatedstores.RelatedStoreID = stores.storeID LEFT JOIN brands On specials.VehicleSpecialBrand = brands.brandID LEFT JOIN vehiclespecials ON specials.SpecialID = vehiclespecials.MainSpecialID WHERE specialrelatedstores.RelatedStoreID = 2 AND specials.SpecialEndDate >= :TodaysDate GROUP BY specialrelatedstores.SpecialID	
		
		$specialsList = $this -> db -> prepare("SELECT DISTINCT specialrelatedstores.CreatedSpecialID, 
				GROUP_CONCAT(
                    CASE
                    WHEN specials.VehicleSpecialBrand IS NOT NULL THEN vehiclespecials.VehicleCategory
                    WHEN specials.PartsSpecialBrand IS NOT NULL THEN partsaccessoryspecials.partAccessoryType
                    WHEN specials.IsServiceSpecial IS NOT NULL THEN serviceincentives.serviceIncentiveType
                    END) 
                RelatedIncentiveCategories, 
                GROUP_CONCAT(
                    CASE
                    WHEN specials.VehicleSpecialBrand IS NOT NULL THEN vehiclespecials.VehicleName
                    WHEN specials.PartsSpecialBrand IS NOT NULL THEN partsaccessoryspecials.partAccessoryName
                    WHEN specials.IsServiceSpecial IS NOT NULL THEN serviceincentives.serviceIncentiveName
                    END) 
                RelatedIncentiveName, 
                specials.SpecialStartDate, 
                specials.SpecialEndDate, 
                specials.SpecialMainImage, 
                specials.SpecialDescription, 
                specials.specialSEOurl, 
                specials.FolderName, 
                specials.SpecialTitle FROM specialrelatedstores 
                LEFT JOIN specials ON specialrelatedstores.CreatedSpecialID = specials.SpecialID 
                LEFT JOIN stores ON specialrelatedstores.RelatedStoreID = stores.storeID 
                LEFT JOIN brands On specials.VehicleSpecialBrand = brands.brandID 
                LEFT JOIN vehiclespecials ON specials.SpecialID = vehiclespecials.MainSpecialID
                LEFT JOIN serviceincentives ON specials.SpecialID = serviceincentives.MainSpecialID
                LEFT JOIN partsaccessoryspecials ON specials.SpecialID = partsaccessoryspecials.MainSpecialID 
                WHERE specialrelatedstores.RelatedStoreID = 2 AND specials.SpecialEndDate >= :TodaysDate GROUP BY specialrelatedstores.CreatedSpecialID");
		
		
		//$specialsList = $this -> db -> prepare("SELECT DISTINCT specialrelatedstores.SpecialID, specials.SpecialStartDate, specials.specialEndDate, specials.SpecialMainImage, specials.SpecialDescription, specials.specialSEOurl, specials.FolderName, specials.SpecialTitle, brands.BrandName FROM specialrelatedstores LEFT JOIN specials ON specialrelatedstores.SpecialID = specials.SpecialID LEFT JOIN stores ON specialrelatedstores.RelatedStoreID = stores.storeID LEFT JOIN brands On specials.VehicleSpecialBrand = brands.brandID LEFT JOIN vehiclespecials ON specials.SpecialID = vehiclespecials.MainSpecialID WHERE specialrelatedstores.RelatedStoreID = 2 AND specials.SpecialEndDate >= :TodaysDate GROUP BY SpecialID");
		$specialsList -> execute(array(":TodaysDate" => date('Y-m-d')));
		return $specialsList -> fetchAll();
	}
	
	public function HarleySpecials() {
		$specialsList = $this -> db -> prepare("SELECT DISTINCT specialrelatedstores.CreatedSpecialID, 
				GROUP_CONCAT(
                    CASE
                    WHEN specials.VehicleSpecialBrand IS NOT NULL THEN vehiclespecials.VehicleCategory
                    WHEN specials.PartsSpecialBrand IS NOT NULL THEN partsaccessoryspecials.partAccessoryType
                    WHEN specials.IsServiceSpecial IS NOT NULL THEN serviceincentives.serviceIncentiveType
                    END) 
                RelatedIncentiveCategories, 
                GROUP_CONCAT(
                    CASE
                    WHEN specials.VehicleSpecialBrand IS NOT NULL THEN vehiclespecials.VehicleName
                    WHEN specials.PartsSpecialBrand IS NOT NULL THEN partsaccessoryspecials.partAccessoryName
                    WHEN specials.IsServiceSpecial IS NOT NULL THEN serviceincentives.serviceIncentiveName
                    END) 
                RelatedIncentiveName, 
                specials.SpecialStartDate, 
                specials.SpecialEndDate, 
                specials.SpecialMainImage, 
                specials.SpecialDescription, 
                specials.specialSEOurl, 
                specials.FolderName, 
                specials.SpecialTitle FROM specialrelatedstores 
                LEFT JOIN specials ON specialrelatedstores.CreatedSpecialID = specials.SpecialID 
                LEFT JOIN stores ON specialrelatedstores.RelatedStoreID = stores.storeID 
                LEFT JOIN brands On specials.VehicleSpecialBrand = brands.brandID 
                LEFT JOIN vehiclespecials ON specials.SpecialID = vehiclespecials.MainSpecialID
                LEFT JOIN serviceincentives ON specials.SpecialID = serviceincentives.MainSpecialID
                LEFT JOIN partsaccessoryspecials ON specials.SpecialID = partsaccessoryspecials.MainSpecialID 
                WHERE specialrelatedstores.RelatedStoreID = 3 AND specials.SpecialEndDate >= :TodaysDate GROUP BY specialrelatedstores.CreatedSpecialID");
		$specialsList -> execute(array(":TodaysDate" => date('Y-m-d')));
		return $specialsList -> fetchAll();
	}
	
	public function IndianSpecials() {
		$specialsList = $this -> db -> prepare("SELECT DISTINCT specialrelatedstores.CreatedSpecialID, 
				GROUP_CONCAT(
                    CASE
                    WHEN specials.VehicleSpecialBrand IS NOT NULL THEN vehiclespecials.VehicleCategory
                    WHEN specials.PartsSpecialBrand IS NOT NULL THEN partsaccessoryspecials.partAccessoryType
                    WHEN specials.IsServiceSpecial IS NOT NULL THEN serviceincentives.serviceIncentiveType
                    END) 
                RelatedIncentiveCategories, 
                GROUP_CONCAT(
                    CASE
                    WHEN specials.VehicleSpecialBrand IS NOT NULL THEN vehiclespecials.VehicleName
                    WHEN specials.PartsSpecialBrand IS NOT NULL THEN partsaccessoryspecials.partAccessoryName
                    WHEN specials.IsServiceSpecial IS NOT NULL THEN serviceincentives.serviceIncentiveName
                    END) 
                RelatedIncentiveName, 
                specials.SpecialStartDate, 
                specials.SpecialEndDate, 
                specials.SpecialMainImage, 
                specials.SpecialDescription, 
                specials.specialSEOurl, 
                specials.FolderName, 
                specials.SpecialTitle FROM specialrelatedstores 
                LEFT JOIN specials ON specialrelatedstores.CreatedSpecialID = specials.SpecialID 
                LEFT JOIN stores ON specialrelatedstores.RelatedStoreID = stores.storeID 
                LEFT JOIN brands On specials.VehicleSpecialBrand = brands.brandID 
                LEFT JOIN vehiclespecials ON specials.SpecialID = vehiclespecials.MainSpecialID
                LEFT JOIN serviceincentives ON specials.SpecialID = serviceincentives.MainSpecialID
                LEFT JOIN partsaccessoryspecials ON specials.SpecialID = partsaccessoryspecials.MainSpecialID 
                WHERE specialrelatedstores.RelatedStoreID = 4 AND specials.SpecialEndDate >= :TodaysDate GROUP BY specialrelatedstores.CreatedSpecialID");
		$specialsList -> execute(array(":TodaysDate" => date('Y-m-d')));
		return $specialsList -> fetchAll();
	}
	
	
	public function SpecialsByInventory($stock) {
		$inventorySpecials = $this -> db -> prepare('SELECT * From inventoryrelatedspecials 
															LEFT JOIN inventory ON inventoryrelatedspecials.RelatedInventoryID = inventory.inventoryID 
															LEFT JOIN specials ON inventoryrelatedspecials.RelatedSpecialID = specials.SpecialID 
															LEFT JOIN relatedspecialitems ON inventoryrelatedspecials.RelatedIncentiveID = relatedspecialitems.RelatedSpecialItemID 
															LEFT JOIN partaccessorybrand ON specials.PartsApparelID = partaccessorybrand.partAccessoryBrandID 
    														LEFT JOIN brands ON specials.OEMBrandID = brands.BrandID
															WHERE inventory.Stock = :stock AND specials.SpecialEndDate >= :TodaysDate ORDER BY specials.SpecialTitle ASC');			
		$inventorySpecials -> execute(array(":stock" => $stock,
											":TodaysDate" => date('Y-m-d')));		
		return $inventorySpecials -> fetchAll();			
		
	}

}