<?php

class ReviewsList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function MotorSportReviews() {
		//require_once 'GoogleAPI/autoload.php';
		
		//$googleClient = new Google_Client();
		
		//http://stackoverflow.com/questions/35638497/curl-error-60-ssl-certificate-prblm-unable-to-get-local-issuer-certificate
		//$guzzleClient = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
		//$googleClient->setHttpClient($guzzleClient);		
		//$googleClient->setApplicationName("Dillon Brothers");
  		//$googleClient->setDeveloperKey(GOOGLE_PLACES_API_KEY);
  		
  		//https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJN1t_tDeuEmsRUsoyG83frY4&key=YOUR_API_KEY
		//$placeUrl = json_encode
		$ch = curl_init();
        $url = 'https://maps.googleapis.com/maps/api/place/details/json?placeid=' . MOTORSPORT_PLACE_ID . '&key=' . GOOGLE_PLACES_API_KEY;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $server_output = curl_exec($ch);
        curl_close($ch);
        return $server_output;
		
		
		//return json_encode("https://maps.googleapis.com/maps/api/place/details/json?placeid=" . MOTORSPORT_PLACE_ID . "&key=". GOOGLE_PLACES_API_KEY);
	}
	


}