<?php

class BlogPostList extends BaseObjectList {
	
	public $Page;
	private $Start;
	private $UserLimit;
	public $PaginationPath;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
		$this -> UserLimit = "10";
    }
	
	private function InitializeStartProperty() {
		if($this -> Page) {
			//First Item to display on this page
			$this -> Start = ($this -> Page - 1) * $this -> UserLimit;
		} else {
			//if no page variable is given, set start to 0
			$this -> Start = 0;
		}
	}
	
	public function AllBlogPlosts() {		
		$this -> InitializeStartProperty();
		
		$blogPostsArray = array();
		$blogPostsArray['pagination'] = $this -> pagination -> paginationLinks($this -> db -> query('SELECT COUNT(blogposts.blogPostID) FROM blogposts 
										     WHERE blogposts.blogVisible = 1') -> fetchColumn(), $this -> UserLimit, PATH . "blog/page", $this -> Page);
		
		$blogPostsArray['mobile-pagination'] = $this -> pagination -> MobilePagination();
		$blogPostsArray['blogposts'] = $this -> db -> select('SELECT blogposts.blogPostID,
											 blogposts.blogPostName,
											 blogposts.blogPostImage, 
											 blogposts.publishedDate,
											 blogposts.blogPostPreview,
											 blogposts.publishedDateFull,
											 blogposts.dateModifiedDate,
											 blogposts.blogPostSEOurl,
											 blogposts.showUserNameOnPublish,
											 blogposts.OptionalBlogSEOUrl,
											 users.firstName,
											 users.lastName,
											 users.userID,
											 COALESCE((SELECT COUNT(*) FROM blogcomments WHERE postID = blogposts.blogPostID AND blogcomments.commentVisible = 1 GROUP BY blogcomments.postID), 0) CommentCount,
                                             BlogCategoryTable.BlogCategoryNames,
                                             BlogCategoryTable.BlogCategorySEOUrl,
                                             BlogCommentDetailsTable.CommentFullNames,
											 BlogCommentDetailsTable.CommentDatePublishes,
											 BlogCommentDetailsTable.CommentNotesTotal,
											 photoalbums.ParentDirectory,
											 photoalbums.albumFolderName,
											 photos.photoName,
											 photos.ext,
											 photos.altText,
											 photos.title
											 FROM blogposts 
										     LEFT JOIN users ON blogposts.createdByID = users.userID
										     LEFT JOIN photoalbums On blogposts.blogPostID = photoalbums.albumBlogID
										     LEFT JOIN photos ON blogposts.blogPostImage = photos.photoID
										     LEFT JOIN (
										     	SELECT RelatedBlogPostID, 
										     		   GROUP_CONCAT(BlogCategoryID) BlogCategories, 
										     		   COALESCE(GROUP_CONCAT(blogpostcategories.blogCategoryName), "Uncategorized") BlogCategoryNames, 
										     		   COALESCE(GROUP_CONCAT(blogpostcategories.blogCategorySEOUrl), "uncategorized") BlogCategorySEOUrl FROM blogcatories 
										     		   LEFT JOIN blogpostcategories ON blogcatories.BlogCategoryID = blogpostcategories.blogPostCategoryID GROUP BY RelatedBlogPostID
											) BlogCategoryTable ON blogposts.blogPostID = BlogCategoryTable.RelatedBlogPostID 
										    LEFT JOIN blogcomments ON blogposts.blogPostID = blogcomments.postID 
										    LEFT JOIN (
										    	SELECT DISTINCT postID, 
										    					GROUP_CONCAT(commentFullName) CommentFullNames, 
										    					GROUP_CONCAT(commentDatePublished) CommentDatePublishes, 
										    					GROUP_CONCAT(commentNotesSection) CommentNotesTotal FROM blogcomments WHERE commentVisible = 1
											) BlogCommentDetailsTable ON blogposts.blogPostID = BlogCommentDetailsTable.postID 
											WHERE blogposts.blogVisible = 1 GROUP BY blogposts.blogPostID ORDER BY blogposts.publishedDateFull DESC LIMIT ' . $this -> Start . ', ' . $this -> UserLimit);	
			
			
		return $blogPostsArray;
		//virtual table for related categories
		//SELECT RelatedBlogPostID, GROUP_CONCAT(BlogCategoryID) BlogCategories, COALESCE(GROUP_CONCAT(blogpostcategories.blogCategoryName), 'Uncategorized') BlogCatoryNames, COALESCE(GROUP_CONCAT(blogpostcategories.blogCategorySEOUrl), 'uncategorized') BlogCategorSEOUrl FROM blogcatories LEFT JOIN blogpostcategories ON blogcatories.BlogCategoryID = blogpostcategories.blogPostCategoryID GROUP BY RelatedBlogPostID													   
		
		
		//SELECT DISTINCT postID, GROUP_CONCAT(commentFullName) CommentFullNames, GROUP_CONCAT(commentDatePublished) CommentDatePublishes FROM blogcomments WHERE commentVisible = 1													   
	}

	public function PostsByNotShownAuthor() {
		$this -> InitializeStartProperty();
		
		$blogPostsArray = array();
		$blogPostsArray['pagination'] = $this -> pagination -> paginationLinks(
			$this -> db -> query('SELECT COUNT(blogposts.blogPostID) FROM blogposts WHERE blogposts.blogVisible = 1 AND blogposts.showUserNameOnPublish = 0') -> fetchColumn(), 
			$this -> UserLimit, 
			$this -> PaginationPath, 
			$this -> Page);
		
		$blogPostsArray['blogposts'] = $this -> db -> select('SELECT blogposts.blogPostID,
											 blogposts.blogPostName,
											 blogposts.blogPostImage, 
											 blogposts.publishedDate,
											 blogposts.blogPostPreview,
											 blogposts.publishedDateFull,
											 blogposts.dateModifiedDate,
											 blogposts.blogPostSEOurl,
											 blogposts.showUserNameOnPublish,
											 blogposts.OptionalBlogSEOUrl,
											 users.firstName,
											 users.lastName,
											 COALESCE((SELECT COUNT(*) FROM blogcomments WHERE postID = blogposts.blogPostID AND blogcomments.commentVisible = 1 GROUP BY blogcomments.postID), 0) CommentCount,
                                             BlogCategoryTable.BlogCategoryNames,
                                             BlogCategoryTable.BlogCategorySEOUrl,
                                             BlogCommentDetailsTable.CommentFullNames,
											 BlogCommentDetailsTable.CommentDatePublishes,
											 BlogCommentDetailsTable.CommentNotesTotal,
											 photoalbums.ParentDirectory,
											 photoalbums.albumFolderName,
											 photos.photoName,
											 photos.ext,
											 photos.altText,
											 photos.title
											 FROM blogposts 
										     LEFT JOIN users ON blogposts.createdByID = users.userID
										     LEFT JOIN photoalbums On blogposts.blogPostID = photoalbums.albumBlogID
										     LEFT JOIN photos ON blogposts.blogPostImage = photos.photoID
										     LEFT JOIN (
										     	SELECT RelatedBlogPostID, 
										     		   GROUP_CONCAT(BlogCategoryID) BlogCategories, 
										     		   COALESCE(GROUP_CONCAT(blogpostcategories.blogCategoryName), "Uncategorized") BlogCategoryNames, 
										     		   COALESCE(GROUP_CONCAT(blogpostcategories.blogCategorySEOUrl), "uncategorized") BlogCategorySEOUrl FROM blogcatories 
										     		   LEFT JOIN blogpostcategories ON blogcatories.BlogCategoryID = blogpostcategories.blogPostCategoryID GROUP BY RelatedBlogPostID
											) BlogCategoryTable ON blogposts.blogPostID = BlogCategoryTable.RelatedBlogPostID 
										    LEFT JOIN blogcomments ON blogposts.blogPostID = blogcomments.postID 
										    LEFT JOIN (
										    	SELECT DISTINCT postID, 
										    					GROUP_CONCAT(commentFullName) CommentFullNames, 
										    					GROUP_CONCAT(commentDatePublished) CommentDatePublishes, 
										    					GROUP_CONCAT(commentNotesSection) CommentNotesTotal FROM blogcomments WHERE commentVisible = 1
											) BlogCommentDetailsTable ON blogposts.blogPostID = BlogCommentDetailsTable.postID 
											WHERE blogposts.blogVisible = 1 AND blogposts.showUserNameOnPublish = 0 GROUP BY blogposts.blogPostID ORDER BY blogposts.publishedDateFull DESC LIMIT ' . $this -> Start . ', ' . $this -> UserLimit);	
			
		return $blogPostsArray;	
		
		//virtual table for related categories
		//SELECT RelatedBlogPostID, GROUP_CONCAT(BlogCategoryID) BlogCategories, COALESCE(GROUP_CONCAT(blogpostcategories.blogCategoryName), 'Uncategorized') BlogCatoryNames, COALESCE(GROUP_CONCAT(blogpostcategories.blogCategorySEOUrl), 'uncategorized') BlogCategorSEOUrl FROM blogcatories LEFT JOIN blogpostcategories ON blogcatories.BlogCategoryID = blogpostcategories.blogPostCategoryID GROUP BY RelatedBlogPostID													   
		
		
		//SELECT DISTINCT postID, GROUP_CONCAT(commentFullName) CommentFullNames, GROUP_CONCAT(commentDatePublished) CommentDatePublishes FROM blogcomments WHERE commentVisible = 1													   
	}

	public function PostsByAuthor($id, $name) {
		$this -> InitializeStartProperty();
		
		$blogPostsArray = array();
		$blogPostsArray['pagination'] = $this -> pagination -> paginationLinks(
			$this -> db -> query('SELECT COUNT(blogposts.blogPostID) FROM blogposts WHERE blogposts.blogVisible = 1 AND blogposts.showUserNameOnPublish = 1 AND blogposts.createdByID = ' . $id) -> fetchColumn(), 
			$this -> UserLimit, 
			PATH . 'blog/author/' . $name, 
			$this -> Page);
		
		$blogPostsArray['blogposts'] = $this -> db -> select('SELECT blogposts.blogPostID,
											 blogposts.blogPostName,
											 blogposts.blogPostImage, 
											 blogposts.publishedDate,
											 blogposts.blogPostPreview,
											 blogposts.publishedDateFull,
											 blogposts.dateModifiedDate,
											 blogposts.blogPostSEOurl,
											 blogposts.showUserNameOnPublish,
											 blogposts.OptionalBlogSEOUrl,
											 users.firstName,
											 users.lastName,
											 COALESCE((SELECT COUNT(*) FROM blogcomments WHERE postID = blogposts.blogPostID AND blogcomments.commentVisible = 1 GROUP BY blogcomments.postID), 0) CommentCount,
                                             BlogCategoryTable.BlogCategoryNames,
                                             BlogCategoryTable.BlogCategorySEOUrl,
                                             BlogCommentDetailsTable.CommentFullNames,
											 BlogCommentDetailsTable.CommentDatePublishes,
											 BlogCommentDetailsTable.CommentNotesTotal,
											 photoalbums.ParentDirectory,
											 photoalbums.albumFolderName,
											 photos.photoName,
											 photos.ext,
											 photos.altText,
											 photos.title
											 FROM blogposts 
										     LEFT JOIN users ON blogposts.createdByID = users.userID
										     LEFT JOIN photoalbums On blogposts.blogPostID = photoalbums.albumBlogID
										     LEFT JOIN photos ON blogposts.blogPostImage = photos.photoID
										     LEFT JOIN (
										     	SELECT RelatedBlogPostID, 
										     		   GROUP_CONCAT(BlogCategoryID) BlogCategories, 
										     		   COALESCE(GROUP_CONCAT(blogpostcategories.blogCategoryName), "Uncategorized") BlogCategoryNames, 
										     		   COALESCE(GROUP_CONCAT(blogpostcategories.blogCategorySEOUrl), "uncategorized") BlogCategorySEOUrl FROM blogcatories 
										     		   LEFT JOIN blogpostcategories ON blogcatories.BlogCategoryID = blogpostcategories.blogPostCategoryID GROUP BY RelatedBlogPostID
											) BlogCategoryTable ON blogposts.blogPostID = BlogCategoryTable.RelatedBlogPostID 
										    LEFT JOIN blogcomments ON blogposts.blogPostID = blogcomments.postID 
										    LEFT JOIN (
										    	SELECT DISTINCT postID, 
										    					GROUP_CONCAT(commentFullName) CommentFullNames, 
										    					GROUP_CONCAT(commentDatePublished) CommentDatePublishes, 
										    					GROUP_CONCAT(commentNotesSection) CommentNotesTotal FROM blogcomments WHERE commentVisible = 1
											) BlogCommentDetailsTable ON blogposts.blogPostID = BlogCommentDetailsTable.postID 
											WHERE blogposts.blogVisible = 1 AND blogposts.showUserNameOnPublish = 1 AND blogposts.createdByID = ' . $id . ' GROUP BY blogposts.blogPostID ORDER BY blogposts.publishedDateFull DESC LIMIT ' . $this -> Start . ', ' . $this -> UserLimit);	
			
			
		return $blogPostsArray;	
		
		//virtual table for related categories
		//SELECT RelatedBlogPostID, GROUP_CONCAT(BlogCategoryID) BlogCategories, COALESCE(GROUP_CONCAT(blogpostcategories.blogCategoryName), 'Uncategorized') BlogCatoryNames, COALESCE(GROUP_CONCAT(blogpostcategories.blogCategorySEOUrl), 'uncategorized') BlogCategorSEOUrl FROM blogcatories LEFT JOIN blogpostcategories ON blogcatories.BlogCategoryID = blogpostcategories.blogPostCategoryID GROUP BY RelatedBlogPostID													   
		
		
		//SELECT DISTINCT postID, GROUP_CONCAT(commentFullName) CommentFullNames, GROUP_CONCAT(commentDatePublished) CommentDatePublishes FROM blogcomments WHERE commentVisible = 1													   
	}
	
	
	public function CategorizedBlogPosts($id) {
		$this -> InitializeStartProperty();
		
		$blogPostsArray = array();
		$blogPostsArray['pagination'] = $this -> pagination -> paginationLinks(
			$this -> db -> query('SELECT COUNT(blogposts.blogPostID) FROM blogposts 
													LEFT JOIN blogcatories ON blogposts.blogPostID = blogcatories.RelatedBlogPostID 
                                                    LEFT JOIN blogpostcategories ON blogcatories.BlogCategoryID = blogpostcategories.blogPostCategoryID
													
                                                    WHERE blogposts.blogVisible = 1 AND blogcatories.BlogCategoryID = ' . $id) -> fetchColumn(), 
			$this -> UserLimit, 
			$this -> PaginationPath, 
			$this -> Page);
		
		
		$category = $this -> db -> prepare("SELECT *, COALESCE((SELECT COUNT(*) FROM blogcomments WHERE postID = blogposts.blogPostID AND blogcomments.commentVisible = 1 GROUP BY blogcomments.postID), 0) CommentCount,
		  (blogpostcategories.blogCategoryName) CategoryNames FROM blogposts 
													LEFT JOIN users ON blogposts.createdByID = users.userID 
													LEFT JOIN photoalbums On blogposts.blogPostID = photoalbums.albumBlogID
													LEFT JOIN blogcatories ON blogposts.blogPostID = blogcatories.RelatedBlogPostID 
                                                    LEFT JOIN blogpostcategories ON blogcatories.BlogCategoryID = blogpostcategories.blogPostCategoryID
													LEFT JOIN blogcomments ON blogposts.blogPostID = blogcomments.postID 
													LEFT JOIN photos ON blogposts.blogPostImage = photos.photoID
                                                    LEFT JOIN (SELECT DISTINCT postID, GROUP_CONCAT(commentFullName) CommentFullNames, GROUP_CONCAT(commentDatePublished) CommentDatePublishes, GROUP_CONCAT(commentNotesSection) CommentNotesTotal FROM blogcomments WHERE commentVisible = 1) BlogCommentDetailsTable ON blogposts.blogPostID = BlogCommentDetailsTable.postID
                                                    WHERE blogposts.blogVisible = 1 AND blogcatories.BlogCategoryID = :categoryID ORDER BY blogposts.publishedDateFull DESC  LIMIT " . $this -> Start . ", " . $this -> UserLimit);	
		$category -> execute(array(":categoryID" => $id));
		
		$blogPostsArray['blogposts'] = $category -> fetchAll();
		
		return $blogPostsArray;	
	}
	
	public function UncategorizedPosts() {
		return $this -> db -> select('SELECT * FROM blogcatories LEFT JOIN blogposts ON blogposts.blogPostID = blogcatories.RelatedBlogPostID WHERE blogcatories.BlogCategoryID = 0 AND blogposts.blogVisible = 1 ORDER BY blogposts.dateModifiedDate ASC');
	}
	
	public function PostsByCategory($id) {
		return $this -> db -> select('SELECT * FROM blogcatories LEFT JOIN blogposts ON blogposts.blogPostID = blogcatories.RelatedBlogPostID WHERE blogcatories.BlogCategoryID = ' . $id . ' AND blogposts.blogVisible = 1 ORDER BY blogposts.dateModifiedDate ASC');
	}

}