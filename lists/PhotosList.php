<?php

class PhotosList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function PhotosByAlbum($id, $eventView = false) {
		if($eventView == false) {
			return $this -> db -> select('SELECT * FROM photos LEFT JOIN photoalbums ON photos.photoAlbumID = photoalbums.albumID WHERE photos.visible = 1 AND photoAlbumID = ' . $id);	
		} else {
			$photoSwipe = array();	
			foreach($this -> db -> select('SELECT * FROM photos LEFT JOIN photoalbums ON photos.photoAlbumID = photoalbums.albumID WHERE photos.visible = 1 AND photoAlbumID = ' . $id) as $photoSingle) {
				switch($photoSingle['AlbumType']) {
					case 1:
						$path = "/events/";	
						break;
					case 2:
						$path = "/blog/";	
						break;
					case 3:
						$path = "/misc/";
						break;
				}	
				$largeImage = PHOTO_URL . $photoSingle['ParentDirectory'] . $path . $photoSingle['albumFolderName'] . '/'. $photoSingle['photoName'] . '-l.' . $photoSingle['ext'];
				
				list($imageWidth, $imageHeight) = getimagesize($largeImage);
				
				array_push($photoSwipe, array('src' => $largeImage,
											  'w' => $imageWidth,
											  'h' => $imageHeight,
											  'html' => 'hi'));	
			}
			return $photoSwipe;
		}
		
	}
	
	public function PhotosByAlbumStatic($id) {
		return $this -> db -> select('SELECT * FROM photos LEFT JOIN photoalbums ON photos.photoAlbumID = photoalbums.albumID WHERE photos.visible = 1 AND photoAlbumID = ' .$id);
	}
	
	public function LoadPhotosByAlbum($start, $limit, $id) {
		
		if($limit == 0) {
			$photos = $this -> db -> prepare('SELECT * FROM photos LEFT JOIN photoalbums ON photos.photoAlbumID = photoalbums.albumID WHERE photos.visible = 1 AND photoAlbumID = :photoAlbumID LIMIT :start');	
			$photos -> bindValue(':start', (int) $start, PDO::PARAM_INT);
		} else {
			$photos = $this -> db -> prepare('SELECT * FROM photos LEFT JOIN photoalbums ON photos.photoAlbumID = photoalbums.albumID WHERE photos.visible = 1 AND photoAlbumID = :photoAlbumID LIMIT :start, :limit');	
			$photos -> bindValue(':start', (int) $start, PDO::PARAM_INT);
			$photos -> bindValue(':limit', (int) $limit, PDO::PARAM_INT);
		}
		
		
		
		$photos -> bindValue(':photoAlbumID', $id, PDO::PARAM_INT);
		
		$photos -> execute();
		//return $photos -> fetchAll();
		$html = '';
								 
		foreach ($photos -> fetchAll() as $photo) {
			
			switch($photo['AlbumType']) {
				case 1:
					$photoPath = '/events/';
					break;
				case 2:
					$photoPath = '/blog/';
					break;
				case 3:
					$photoPath = '/misc/';
					break;
			}
			
				
			$largeImage = PHOTO_URL . $photo['ParentDirectory'] . $photoPath . $photo['albumFolderName'] . '/' . $photo['photoName'] . '-l.' . $photo['ext'];
			$mediumImage = PHOTO_URL . $photo['ParentDirectory'] . $photoPath . $photo['albumFolderName'] . '/' . $photo['photoName'] . '-s.' . $photo['ext'];
			
			list($imageWidth, $imageHeight) = getimagesize($largeImage);

			//<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">	
			//	<a href="" itemprop="contentUrl" data-size="">			
			//		<div class="photoSingle">
			//			<img src="" data-scale="best-fill" data-align="center" title="" />
			//		</div>					
			//	</a>
			//</figure>
			
			
			$html .= '<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
						<a href="' . $largeImage . '" itemprop="contentUrl" data-size="' . $imageWidth . 'x' . $imageHeight . '">			
							<div class="photoSingle">
								<img src="' . $mediumImage .'" data-scale="best-fill" data-align="center" alt="' . $photo['altText'] .' " title="'. $photo['title'] .'" />
							</div>					
						</a>
					
					</figure>';	
		}			
		
		return $html;			 
	}
	
	
}