<?php

class BlogPostCommentsList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function CommentsByPost($postID) {
		return $this -> db -> select("SELECT * FROM blogcomments WHERE commentVisible = 1 AND postID = " . $postID);	
	}
	

}