<?php

class RelatedSpecialItemsList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	
	public function RelatedItemsBySpecial($id, $inventory = false) {
		$specialItems = array();
		
		$vehicleList = $this -> db -> prepare("SELECT * FROM relatedspecialitems WHERE relatedSpecialID = " . $id . " AND ExpiredDate >= :TodaysDate ORDER BY relatedspecialitems.RelatedItemSpecialType ASC");
		$vehicleList -> execute(array(":TodaysDate" => date('Y-m-d')));
		
		$specialItems['incentives'] = $vehicleList -> fetchAll();
		if($inventory == true) {
			$specialItems['inventory'] = $this -> db -> select('SELECT * FROM inventoryrelatedspecials INNER JOIN inventory ON inventoryrelatedspecials.RelatedInventoryID = inventory.inventoryID LEFT JOIN inventoryphotos ON inventory.inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1 WHERE inventoryrelatedspecials.RelatedSpecialID = ' . $id);
		}
		return $specialItems;
	}	
	

}