<?php

class VehicleSpecialsList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function VehicleSpecials() {
		return $this -> db -> select("SELECT * FROM vehiclespecials");
	}
	
	public function VehiclesBySpecial($id) {		
		$vehicleList = $this -> db -> prepare("SELECT * FROM vehiclespecials WHERE MainSpecialID = " . $id . " AND ExpiredDate >= :TodaysDate ORDER BY VehicleCategory");
		$vehicleList -> execute(array(":TodaysDate" => date('Y-m-d')));
		return $vehicleList -> fetchAll();
	}	

}