<?php

class ChatChannelUsersList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    public function __construct(){
        parent::__construct();
	}
	
	public function All() {
		return $this -> db -> select('SELECT * FROM twiliochatmembers LEFT JOIN users on twiliochatmembers.relatedUserID = users.userID');
	}
	
	public function GetChannelUser($id) {
		return $this -> db -> select('SELECT * FROM twiliochatmembers WHERE relatedTwilioChatID = ' . $id . ' ORDER BY RAND() LIMIT 1');
	}
	
	
	
	
}