<?php

class BrandContentList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
	}
	
	public function ContentByOEMBrand($id) {
		return $this -> db -> select('SELECT * FROM brandpostcontent WHERE isPageActive = 1 AND OEMBrandID = ' . $id . ' ORDER BY ContentTitle DESC');
	}
	
	public function ContentByPartApparelBrand($id) {
		return $this -> db -> select('SELECT * FROM brandpostcontent WHERE isPageActive = 1 AND PartBrandID = ' . $id . ' ORDER BY ContentTitle DESC');
	}
	
	public function PartApparelBrands() {
		return $this -> db -> select('SELECT * FROM partaccessorybrand');
	}
	
	
}