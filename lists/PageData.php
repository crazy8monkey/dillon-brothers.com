<?php

class PageData extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function EventSinglePhotos($id) {
		return $this -> db -> select("SELECT * FROM pagedata LEFT JOIN photos ON pagedata.photosSelected = photos.photoID LEFT JOIN photoalbums ON photos.photoAlbumID = photoalbums.albumID WHERE linkedPageTypeID = ". $id . " AND pageType = 1");						
	}	
	
	public function SuperEventPhotos($ids) {
		return $this -> db -> select("SELECT * FROM pagedata LEFT JOIN photos ON pagedata.photosSelected = photos.photoID LEFT JOIN photoalbums ON photos.photoAlbumID = photoalbums.albumID WHERE linkedPageTypeID IN (". $ids . ") AND pageType = 1");
	}
	
	public function EventSingleAlbums($id) {
		return $this -> db -> select("SELECT * FROM pagedata LEFT JOIN photoalbums ON pagedata.albumsSelected = photoalbums.albumID LEFT JOIN photos ON photos.photoAlbumID = photoalbums.albumID WHERE linkedPageTypeID = ". $id . " AND pageType = 1 GROUP BY pagedata.pageID");						
	}	
	
	public function SuperEventAlbums($ids) {
		return $this -> db -> select("SELECT * FROM pagedata LEFT JOIN photoalbums ON pagedata.albumsSelected = photoalbums.albumID LEFT JOIN photos ON photos.photoAlbumID = photoalbums.albumID WHERE linkedPageTypeID IN (". $ids . ") AND pageType = 1 GROUP BY pagedata.pageID");
	}

}