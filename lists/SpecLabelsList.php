<?php

class SpecLabelsList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function Groups() {
		return $this -> db -> select('SELECT * FROM specgroups WHERE GroupVisible = 1 ORDER BY GroupOrder ASC');
	}
	
	public function ByCategory($categoryID) {
		return $this -> db -> select('SELECT * FROM speclabels WHERE FIND_IN_SET (' . $categoryID. ', LinkedInventoryCategories) ORDER BY labelOrder ASC');	
		
		
		//$limit = $this->db->prepare("SET @@group_concat_max_len = 200000;");
		//$limit -> execute();
		
		//$specLabels = $this -> db -> prepare('SELECT DISTINCT specgroups.specGroupText, GROUP_CONCAT(speclabels.labelText) AssociatedLabels, 
		//													  GROUP_CONCAT(speclabels.labelOrder) LabelOrders,
		//													  GROUP_CONCAT(speclabels.specLabelID) LabelIDS FROM speclabels 
		//													  LEFT JOIN specgroups ON speclabels.relatedSpecGroupID = specgroups.specGroupID WHERE LinkedInventoryCategories LIKE :categoryID GROUP BY specgroups.specGroupText ORDER BY specgroups.GroupOrder ASC');
		
		//$specLabels -> bindValue(':categoryID', "%" . $categoryID . "%");
		//$specLabels -> execute();
		
		//return $specLabels -> fetchAll();
		
	}
	

}