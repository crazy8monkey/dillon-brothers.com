<?php

class InventoryPhotoList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function PhotosByInventory($id) {
		return $this -> db -> select("SELECT * FROM inventoryphotos WHERE relatedInventoryID = " .$id . " ORDER BY MainProductPhoto DESC");
	}
	


}