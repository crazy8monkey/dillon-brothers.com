<?php

class ServiceSpecialsList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function VehicleSpecials() {
		return $this -> db -> select("SELECT * FROM vehiclespecials");
	}
	
	public function ServiceBySpecial($id) {		
		$vehicleList = $this -> db -> prepare("SELECT * FROM serviceincentives WHERE MainSpecialID = " . $id . " AND serviceIncentiveExpiredDate >= :TodaysDate");
		$vehicleList -> execute(array(":TodaysDate" => date('Y-m-d')));
		return $vehicleList -> fetchAll();
	}	

}