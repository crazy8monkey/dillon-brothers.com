<?php

class InventoryList extends BaseObjectList {
	
	
	
	public $Condition;
	public $Stores;
	public $Manufactures;
	public $Colors;
	public $Years;
	public $Categories;
	public $PageNumber;
	public $MileageRange;
	public $PriceRange;
	public $EngineSizeCC;
	
	public $UpdateFilters;
	
	public $pageNumber;
	public $UserLimit;
	public $AppendURL;
	public $OrderByInventory;
	public $SearchString;
	
	private $ConditionIDS;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	private function GenerateConditionQuery($appendURL = false) {
		$conditonSqlString = '';	
		$conditionValue = NULL;
		foreach($this -> Condition as $conditionSingle) {
			$conditionValue .=  $conditionSingle . ',';	
			
		}
		$conditionQuery = rtrim($conditionValue, ',');
		$this -> ConditionIDS = rtrim($conditionValue, ',');
		
		if($appendURL == true) {
			$newStringReplace = str_replace('0', 'New', $conditionQuery);
			$usedString = str_replace('1', 'Used', $newStringReplace);
			$this -> AppendURL .= '&Conditions=' . $usedString;
		}
							
		$conditonSqlString .= " inventory.Conditions IN (" . $conditionQuery . ") AND";
		return $conditonSqlString;
		
	}
	
	private function GenerateYearQuery($appendURL = false) {
		$yearSqlString = '';
		$yearValue = NULL;
		foreach($this -> Years as $yearSingle) {
			$yearValue .=  '"'. $yearSingle . '",';	
		}
		$yearQuery = rtrim($yearValue, ',');
		
		if($appendURL == true) {
			$this -> AppendURL .= '&Years=' . str_replace('"', '', $yearQuery);
		}
							
		$yearSqlString .= " inventory.Year IN (" . $yearQuery . ") AND";
		return $yearSqlString;
	}
	
	
	private function GenerateStoreQuery($appendURL = false) {
		$storeSqlString = '';	
		$storeValue = NULL;
		foreach($this -> Stores as $storeSingle) {
			$storeValue .=  '"'. $storeSingle . '",';	
		}
		$storeQuery = rtrim($storeValue, ',');
		
		
		if($appendURL == true) {
			$this -> AppendURL .= '&Stores=' . str_replace('"', '', $storeQuery);
		}
		
												
		$storeSqlString .= " stores.StoreName IN (" . $storeQuery . ") AND";
		return $storeSqlString;			
	}
	
	private function GenerateManufactureQuery($appendURL = false) {
		$manufatureSqlString = '';
		$manufactureValue = NULL;
		foreach($this -> Manufactures as $manufactureSingle) {
			$manufactureValue .=  '"'. $manufactureSingle . '",';	
		}
		$manufactureQuery = rtrim($manufactureValue, ',');
		
		if($appendURL == true) {
			$this -> AppendURL .= '&Manufactures=' . str_replace('"', '', $manufactureQuery);
		}
							
		$manufatureSqlString .= " inventory.Manufacturer IN (" . $manufactureQuery . ") AND";
		return $manufatureSqlString;			
	}
	
	private function GenerateCategoryQuery($appendURL = false) {
		$categorySqlString = '';	
		$categoryValue = NULL;
		foreach($this -> Categories as $categorySingle) {
			$categoryValue .=  '"'. $categorySingle . '",';	
		}
		$categoryQuery = rtrim($categoryValue, ',');
		
		
		if($appendURL == true) {
			$this -> AppendURL .= '&Categories=' . str_replace('"', '', $categoryQuery);
		}
		
					
		$categorySqlString .= " inventorycategories.inventoryCategoryName IN (" . $categoryQuery . ") AND";
		return $categorySqlString;
	}
	
	public function GeneratePriceRangeQuery($appendURL = false) {
		//echo $this -> PriceRange;
			
		$msrpSqlString = '';	
		$urlString = '';
		$msrpRangeValue = NULL;
		foreach($this -> PriceRange as $msrpRangeSingle) {
			$singleRange = explode(' - ', $msrpRangeSingle);		
			
			$urlString .= $singleRange[0] . "-" . $singleRange[1] . ',';
			 //1 AND 5000 OR inventory.MSRP BETWEEN 10001 AND 15000
			
			$msrpRangeValue .=  " inventory.MSRP BETWEEN " . $singleRange[0] . " AND " . $singleRange[1] . ' OR';	
		}
		$msrpSqlString = " (" . rtrim($msrpRangeValue, ' OR') . ') AND';
		
		if($appendURL == true) {
			$this -> AppendURL .= '&PriceRange=' . rtrim($urlString, ',');
		}
		
		
		return $msrpSqlString;
	}
	
	
	public function GenerateEngineSizeCCRangeQuery($appendURL = false) {
		//echo $this -> PriceRange;
		$urlString = '';	
		$engineccSqlString = '';	
		$ccRangeValue = NULL;
		foreach($this -> EngineSizeCC as $engineRangeSingle) {
			$singleRange = explode(' - ', $engineRangeSingle);		
			
			$urlString .= $singleRange[0] . "-" . $singleRange[1] . ',';
			 //1 AND 5000 OR inventory.MSRP BETWEEN 10001 AND 15000
			
			$ccRangeValue .=  " inventory.EngineSizeCC BETWEEN " . $singleRange[0] . " AND " . $singleRange[1] . ' OR';	
		}
		$engineccSqlString = " (" . rtrim($ccRangeValue, ' OR') . ') AND';
		
		if($appendURL == true) {
			$this -> AppendURL .= '&EngineSizeCCRange=' . rtrim($urlString, ',');
		}
		
		return $engineccSqlString;
	}
	
	private function GenerateMileageRangeQuery($appendURL = false) {
		//echo $this -> PriceRange;
		$urlString = '';	
		$mileageSqlString = '';	
		$mileageRangeValue = NULL;
		foreach($this -> MileageRange as $MileageRangeSingle) {
			$singleRange = explode(' - ', $MileageRangeSingle);		
			
			$urlString .= $singleRange[0] . "-" . $singleRange[1] . ',';
			 //1 AND 5000 OR inventory.MSRP BETWEEN 10001 AND 15000
			
			$mileageRangeValue .=  " inventory.Mileage BETWEEN " . $singleRange[0] . " AND " . $singleRange[1] . ' OR';	
		}
		$mileageSqlString = " (" . rtrim($mileageRangeValue, ' OR') . ') AND';
		
		if($appendURL == true) {
			$this -> AppendURL .= '&MileageRange=' . rtrim($urlString, ',');
		}
		
		return $mileageSqlString;
	
	}
	
	private function GenerateSearchStringQuery() {
		return " CONCAT(Year, '', Manufacturer, '', ModelName, '', VinNumber, '', inventoryCategoryName, '', Stock, '', SelectedColor, '', SelectedFilteredColor, '', FriendlyModelName) LIKE '%" . $this -> SearchString . "%' AND";
	}
	
	private function GenerateFilterWHERE($type) {
		$sqlWhere = NULL;	
		switch($type) {
			
		
		
		}

		return $sqlWhere;
	}
	
	private function GenerateFilterColor($appendURL = false) {
		$havingColorSql = NULL;
		$urlString = '';
		if(isset($this -> Colors)) {
			$colorValue = NULL;
			foreach($this -> Colors as $colorSingle) {
				$colorValue .=  $colorSingle . '|';	
				$urlString .= $colorSingle . ',';
			}
			$colorQuery = rtrim($colorValue, '|');
						
			$havingColorSql .= " HAVING SelectedFilteredColor REGEXP '" . $colorQuery . "'";
		}		
		
		if($appendURL == true) {
			$this -> AppendURL .= '&EngineSizeRange=' . rtrim($urlString, ',');
		}
		
		return $havingColorSql;
	}
	
	private function GenerateFilterColor2($appendURL = false) {
		$havingColorSql = NULL;
		$urlString = '';
		if(isset($this -> Colors)) {
			$colorValue = NULL;
			foreach($this -> Colors as $colorSingle) {
				$colorValue .=  $colorSingle . '|';	
				$urlString .= $colorSingle . ',';
			}
			$colorQuery = rtrim($colorValue, '|');
						
			$havingColorSql .= " AND SelectedFilteredColor REGEXP '" . $colorQuery . "'";
		}		
		
		if($appendURL == true) {
			$this -> AppendURL .= '&EngineSizeRange=' . rtrim($urlString, ',');
		}
		
		return $havingColorSql;
	}
	
	public function Condition($filtered = false) {
		if($filtered == true) {
			$conditionFiltered = $this -> db -> select('SELECT inventory.Conditions, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor, COALESCE(colors.FilterColor, inventory.Color) SelectedFilteredColor FROM inventory 
														LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
														LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
														LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
														LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
														WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL' . $this -> GenerateFilterWHERE('condition') . ' GROUP BY inventory.inventoryID' . $this -> GenerateFilterColor());
			
			
			$conditions = array_column($conditionFiltered, 'Conditions');
		
			$conditionFilterString = '';
			
			foreach(array_unique($conditions, SORT_REGULAR) as $key => $conditionSingle) {
				$conditionChecked = '';
				if(isset($this -> Condition)) {
					if(in_array($conditionSingle, $this -> Condition)) {
						$conditionChecked = ' checked';	
					}	
				} else {
					$conditionChecked = ' checked';
				}

				//if(!isset($this -> Stores)) {
				//	$conditionChecked = ' checked';		
				//}
				
				//$conditionFilterString .='<div class="line"><input type="checkbox" class="ConditionCheck" checked name="Condition[]" value="' .$conditionSingle . '" /> '. ($conditionSingle == 0 ? "New": "Used") .'</div>';
				
				$conditionFilterString .='<div class="line"><input type="checkbox" class="ConditionCheck" id="ConditionCheck'.  $key .'"'. $conditionChecked . ' name="Condition[]" value="' . $conditionSingle .'" /> 	
										  <label for="ConditionCheck' . $key . '"></label>' . ($conditionSingle == 0 ? "New": "Used") .'</div>';
				
				
			}
			
			return $conditionFilterString;
			
		} else {
			return $this -> db -> select('SELECT DISTINCT inventory.IsInventoryActive, inventory.Conditions FROM inventory 
											LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
											LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
											WHERE inventory.IsInventoryActive= 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL');	
		}
		
		
	}
	
	
	public function Years($filtered = false) {
		if($filtered == true) {
			$yearFilterResults = $this -> db -> select('SELECT inventory.Year, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor, COALESCE(GROUP_CONCAT(colors.FilterColor), inventory.Color) SelectedFilteredColor
              FROM inventory
                   LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
                   LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
                   LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
                   LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
                   WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL'. $this -> GenerateSqlWhereClauses('YearsFiltered'). ' GROUP BY inventory.inventoryID' . $this -> GenerateFilterColor());	
			$Years = array_column($yearFilterResults, 'Year');		
			
			$YearFilterString = NULL;
			$YearsArray = array_unique($Years, SORT_REGULAR);
			arsort($YearsArray);
			foreach($YearsArray as $key => $yearSingle) {
				$checkedString = '';	
				if(isset($this -> Years)) {
					if(in_array($yearSingle, $this -> Years)) {
						$checkedString = ' checked';	
					}	
				}
				
				$YearFilterString .= '<div class="line"><input type="checkbox" id="YearCheck' . $key . '" class="YearCheck" name="Year[]"' . $checkedString .' value="' . $yearSingle . '" /><label for="YearCheck' . $key . '"></label>' . $yearSingle . '</div>';					
			}	
						
			return $YearFilterString;	
			
		} else {
			return $this -> db -> select('SELECT DISTINCT Year FROM inventory WHERE IsInventoryActive = 1 ORDER BY Year DESC');			
		}
			
		
	}
	
	public function Categories($filtered = false) {
		if($filtered == true) {
			$categoryFiltered = $this -> db -> select('SELECT inventorycategories.inventoryCategoryName, COALESCE(colors.FilterColor, inventory.Color) SelectedFilteredColor
              FROM inventory
                   LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
                   LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
                   LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
                   LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
                   WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL'. $this -> GenerateSqlWhereClauses('CategoriesFiltered') .' GROUP BY inventory.inventoryID');
			
			$categoryFilterString = '';
			
			$categories = array_column($categoryFiltered, 'inventoryCategoryName');
			$sortedCategories = array_unique($categories, SORT_REGULAR);
			
			sort($sortedCategories);
			
			foreach($sortedCategories as $key => $CategorySingle) {
				if(!empty($CategorySingle)) {
					$categoryChecked = false;
					if(isset($this -> Categories)) {
						if(in_array($CategorySingle, $this -> Categories)) {
							$categoryChecked = ' checked';	
						}	
					}
					
					$categoryFilterString .='<div class="line"><input type="checkbox" class="CategoryCheck" id="CategoryCheck' . $key . '" name="category[]"' . $categoryChecked . ' value="' . $CategorySingle . '" /><label for="CategoryCheck' . $key . '"></label>' . $CategorySingle . '</div>';
					
				}
			}	
			
			return $categoryFilterString;
			
		} else {
			return $this -> db -> select('SELECT DISTINCT inventorycategories.inventoryCategoryName FROM inventory INNER JOIN inventorycategories ON inventorycategories.inventoryCategoryID = inventory.Category WHERE inventory.IsInventoryActive = 1');	
		}	
	}

	public function Stores($filtered = false) {
		if($filtered == true) {
			$storesFiltered = $this -> db -> select('SELECT stores.StoreName, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor, COALESCE(GROUP_CONCAT(colors.FilterColor), inventory.Color) SelectedFilteredColor
              FROM inventory
                   LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
                   LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
                   LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
                   LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
                   WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL' . $this -> GenerateSqlWhereClauses('StoresFiltered') .' GROUP BY inventory.inventoryID');
			//stores
			$stores = array_column($storesFiltered, 'StoreName');
			$storeID = array_column($storesFiltered, 'InventoryStoreID');
			
			$sortedStores = array_unique($stores, SORT_REGULAR);
			
			sort($sortedStores);
			
			$storeFilterString = NULL;
			
			foreach($sortedStores as $key => $storeSingle) {
				$storeChecked = '';
				if(isset($this -> Stores)) {
					if(in_array($storeSingle, $this -> Stores)) {
						$storeChecked = ' checked';	
					}	
				}
				
				$storeFilterString .='<div class="line"><input type="checkbox" class="StoreCheck" id="StoreCheck' . $key . '" name="store[]"' . $storeChecked . ' value="' . $storeSingle . '" /><label for="StoreCheck' . $key . '"></label>' .  $storeSingle . '</div>';
				
			}	
				
			return $storeFilterString;
		
		} else {
			return $this -> db -> select('SELECT DISTINCT inventory.InventoryStoreID, stores.StoreName, inventory.IsInventoryActive, stores.storeID FROM inventory LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID WHERE IsInventoryActive = 1');	
		}
		
	}

	public function manufactures($filtered = false) {
		if($filtered == true) {
			$manufacturedFiltered = $this -> db -> select('SELECT inventory.Manufacturer, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor, COALESCE(GROUP_CONCAT(colors.FilterColor), inventory.Color) SelectedFilteredColor
              FROM inventory
                   LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
                   LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
                   LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
                   LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
                   WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL'. $this -> GenerateSqlWhereClauses('ManufacturesFiltered') .' GROUP BY inventory.inventoryID');
			
			$manufactures = array_column($manufacturedFiltered, 'Manufacturer');
			$sortedManufactures = array_unique($manufactures, SORT_REGULAR);
			
			sort($sortedManufactures);
		
			$manufactureFilterString = '';
			
			foreach($sortedManufactures as $key => $ManufactureSingle) {
				$manufactureChecked = '';
				if(isset($this -> Manufactures)) {
					if(in_array($ManufactureSingle, $this -> Manufactures)) {
						$manufactureChecked = ' checked';	
					}	
				}
				
				$manufactureFilterString .='<div class="line"><input type="checkbox" class="ManufactureCheck" id="ManufactureCheck' . $key . '" name="manufacture[]"' . $manufactureChecked . ' value="' . $ManufactureSingle . '" /><label for="ManufactureCheck' . $key . '"></label>' .  $ManufactureSingle . '</div>';
				
			}
			
			return $manufactureFilterString;
		
		} else {
			return $this -> db -> select('SELECT DISTINCT Manufacturer FROM inventory WHERE IsInventoryActive = 1');	
		}
		
		
	}

	public function Colors($filtered = false) {
		if($filtered == true) {
			$colorFiltered = $this -> db -> select('SELECT *, COALESCE(colors.FilterColor, inventory.Color) SelectedFilteredColor
              FROM inventory
                   LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
                   LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
                   LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
                   LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
                   WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL' . $this -> GenerateSqlWhereClauses('ColorsFiltered'). ' GROUP BY inventory.inventoryID');
													
			$colors = array_column($colorFiltered, 'SelectedFilteredColor');
			$sortedColors = array_unique($colors, SORT_REGULAR);
			
			sort($sortedColors);
		
			$colorFilterString = '';
			
			foreach($sortedColors as $key =>  $colorSingle) {
				if(!empty($colorSingle)) {
					$colorChecked = '';
					if(isset($this -> Colors)) {
						if(in_array($colorSingle, $this -> Colors)) {
							$colorChecked = ' checked';	
						}
					}	
					
					
					$colorFilterString .='<div class="line"><input type="checkbox" class="ColorCheck" id="ColorCheck' . $key . '" name="color[]"' . $colorChecked . ' value="' . $colorSingle . '" /><label for="ColorCheck' . $key . '"></label>' . $colorSingle . '</div>';
				}	
			}
			
			return $colorFilterString;										
													
		} else {
			return $this -> db -> select('SELECT DISTINCT COALESCE(colors.FilterColor, inventory.Color) FilterColor FROM inventory LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) WHERE inventory.IsInventoryActive = 1');	
		}
		
	}

	
	public function Mileage($filtered = false) {
		
		$MaxMileage = $this -> db -> prepare('SELECT MAX(Mileage) MaxMileage FROM (
				SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor, COALESCE(colors.FilterColor, inventory.Color) SelectedFilteredColor
              FROM inventory
                   LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
                   LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
                   LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
                   LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
                   WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL'. $this -> GenerateSqlWhereClauses('MileageFiltered'). ' GROUP BY inventory.inventoryID) CurrentInventoryTable');
		
		$MaxMileage -> execute();
		$FinalMaxMileage = $MaxMileage -> fetch();	
		
		
		$mileageRangesFiltered = $this -> db -> prepare("CALL MileageRanges(" . $FinalMaxMileage['MaxMileage'] . ")");
		$mileageRangesFiltered -> execute();
		
		$MileageRangesFinal = $mileageRangesFiltered -> fetchAll();
		$mileageRangesFiltered->closeCursor();
		
			
		if($filtered == true) {
			$mileageRangeFilteredString = '';
			
			foreach($MileageRangesFinal as $key => $mileageRangeFinal) {
				$values = explode(' - ', $mileageRangeFinal['MileageRanges']);
				$mileageRangesCheck = $this -> db -> query('SELECT COUNT(*) FROM (
																SELECT *,
													              COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor,
												              COALESCE(colors.FilterColor, inventory.Color) SelectedFilteredColor
												              FROM inventory
												                   LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
												                   LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
												                   LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
												                   LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
												                   WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL' . $this -> GenerateSqlWhereClauses('MileageFiltered') . ' GROUP BY inventory.inventoryID) CurrentInventoryTable WHERE Mileage BETWEEN ' . $values[0] . ' AND ' . $values[1]) -> fetchColumn();
				   
				   
					
				
				if($mileageRangesCheck > 0) {
					$mileageRangeChecked = '';			
					if(isset($this -> MileageRange)) {
						
						if(in_array($mileageRangeFinal['MileageRanges'], $this -> MileageRange)) {
							$mileageRangeChecked = ' checked';	
						}	
					}
					
											
					$mileageRangeFilteredString.= '<div class="line"><input type="checkbox" name="mileageRange[]" class="MileageRange"'. $mileageRangeChecked . ' id="MileageRange' . $key . '" value="'. $mileageRangeFinal['MileageRanges'] .'" /> 
									<label for="MileageRange' . $key . '"></label>' . $mileageRangeFinal['MileageRangeText'] . '</div>';	
									
				}	
				
				
			}
				
			return $mileageRangeFilteredString; 
														
		} else {
			return $MileageRangesFinal;	
		}
		
	}
		

	


	public function PriceRange($filtered = false) {
		
		$MaxMSRP = $this -> db -> prepare('SELECT MAX(MSRP) FinalMaxMSRP FROM (
				SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor, COALESCE(colors.FilterColor, inventory.Color) SelectedFilteredColor
              FROM inventory
                   LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
                   LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
                   LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
                   LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
                   WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL' . $this -> GenerateSqlWhereClauses('MSRPFiltered'). ' GROUP BY inventory.inventoryID) CurrentInventoryTable');
		$MaxMSRP -> execute();
		$FinalMaxMSRP = $MaxMSRP -> fetch();	
		
		$priceRangesFiltered = $this -> db -> prepare("CALL GETPRICERANGES(" . $FinalMaxMSRP['FinalMaxMSRP'] . ")");
		$priceRangesFiltered -> execute();
		
		$PriceRangesFinal = $priceRangesFiltered -> fetchAll();
		$priceRangesFiltered->closeCursor();
		
		if($filtered == true) {
			
			
			
			$priceFilterString = '';
			//sort($priceFiltered);
			
			foreach($PriceRangesFinal as $key => $priceRangeSingle) {
				$values = explode(' - ', $priceRangeSingle['PriceRanges']);
				
				$PriceRangesCheck = $this -> db -> query('SELECT COUNT(*) FROM (
				SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor, COALESCE(colors.FilterColor, inventory.Color) SelectedFilteredColor
              FROM inventory
                   LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
                   LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
                   LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
                   LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
                   WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL' . $this -> GenerateSqlWhereClauses('MSRPFiltered') .' GROUP BY inventory.inventoryID) CurrentInventoryTable WHERE MSRP BETWEEN ' . $values[0] . ' AND ' . $values[1]) -> fetchColumn();
				
									
				
				if($PriceRangesCheck > 0) {
					$priceRangeChecked = '';			
					if(isset($this -> PriceRange)) {
						
						if(in_array($priceRangeSingle['PriceRanges'], $this -> PriceRange)) {
							$priceRangeChecked = ' checked';	
						}	
					}
					
					
					$priceFilterString	.= '<div class="line"><input type="checkbox" name="priceRange[]" class="PriceRange" id="PriceRange'. $key .'"' . $priceRangeChecked .' value="' . $priceRangeSingle['PriceRanges'] .'" /> 
											<label for="PriceRange' . $key .'"></label>' . $priceRangeSingle['PriceRangeText'] . '</div>';
				
				//$priceFilterString .= $PriceRangesCheck;	
				}	
				
			}
					
			return $priceFilterString;
			
		} else {
			
			return $PriceRangesFinal;	
		}
			
	}

	public function EngineSizeCC($filtered = false) {
		
		
		$MaxEngineSizeCC = $this -> db -> prepare('SELECT MAX(EngineSizeCC) FinalMaxCC FROM (
						SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor, COALESCE(colors.FilterColor, inventory.Color) SelectedFilteredColor
		              FROM inventory
		                   LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
		                   LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
		                   LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
		                   LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
		                   WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL' . $this -> GenerateSqlWhereClauses('EngineSizeCCFiltered') . ' GROUP BY inventory.inventoryID) CurrentInventoryTable');
		$MaxEngineSizeCC -> execute();
		$FinalMaxCC = $MaxEngineSizeCC -> fetch();	
		
		$EngineSizeRanges = $this -> db -> prepare("CALL EngineSizeCCRanges(" . $FinalMaxCC['FinalMaxCC'] . ")");
		$EngineSizeRanges -> execute();
		
		$EngineRangesFinal = $EngineSizeRanges -> fetchAll();
		$EngineSizeRanges -> closeCursor();
		
		
		if($filtered == true) {
			$EngineSizeCCFilteredString = '';
			foreach($EngineRangesFinal as $key => $engineRange) {
				$values = explode(' - ', $engineRange['EngineSizeCCRanges']);
				
				$EngineRangesCheck = $this -> db -> query('SELECT COUNT(*) FROM (
						SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor, COALESCE(colors.FilterColor, inventory.Color) SelectedFilteredColor
		              FROM inventory
		                   LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
		                   LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
		                   LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
		                   LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
		                   WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL' . $this -> GenerateSqlWhereClauses('EngineSizeCCFiltered') . ' GROUP BY inventory.inventoryID) CurrentInventoryTable WHERE EngineSizeCC BETWEEN ' . $values[0] . ' AND ' . $values[1]) -> fetchColumn();
				
				
				
				if($EngineRangesCheck > 0) {				
					$engineRangeChecked = '';
					
					if(isset($this -> EngineSizeCC)) {
						if(in_array($engineRange['EngineSizeCCRanges'], $this -> EngineSizeCC)) {
							$engineRangeChecked = ' checked';	
						}	
					}
							
					$EngineSizeCCFilteredString .='<div class="line">	
											<input type="checkbox" name="engineSizeCCRange[]" class="EngineCCRange" id="EngineRange' . $key . '"' . $engineRangeChecked . ' value="' . $engineRange['EngineSizeCCRanges'] . '" /> 
											<label for="EngineRange'. $key  . '"></label>'. $engineRange['EngineSizeCCRangeText'] . '</div>';
											
					//$EngineSizeCCFilteredString .=$EngineRangesCheck;						
					
				}						

			}
			
			
			
			
		
			
			return $EngineSizeCCFilteredString;
			
		} else {
			
			return $EngineRangesFinal;	
		}	
		
	}

	public function Count() {
		return $this -> db -> query('SELECT COUNT(*) FROM (SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor,
						              COALESCE(GROUP_CONCAT(colors.FilterColor), inventory.Color) SelectedFilteredColor FROM inventory
						                   LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
						                   LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
						                   LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
						                   LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
						                   WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL' . $this -> GenerateSqlWhereClauses('Count') . ' GROUP BY inventory.inventoryID' . $this -> GenerateFilterColor() . ') CurrentInventoryTable') -> fetchColumn();
	}
	
	
	private function GenerateSqlWhereClauses($type) {
		$sqlWhere = '';	
		switch($type) {
			case "InventoryList":
				//if(!empty($this -> SearchString)) {$sqlWhere .= $this -> GenerateSearchStringQuery();}
				if(isset($this -> Condition)) {$sqlWhere .= $this -> GenerateConditionQuery(true);}
				if(isset($this -> Years)) {$sqlWhere .= $this -> GenerateYearQuery(true);}
				if(isset($this -> Categories)) {$sqlWhere .= $this -> GenerateCategoryQuery(true);}	
				if(isset($this -> Stores)) {$sqlWhere .= $this -> GenerateStoreQuery(true);}
				if(isset($this -> Manufactures)) {$sqlWhere .= $this -> GenerateManufactureQuery(true);}	
				//if(isset($this -> Colors)) {$sqlWhere .= $this -> GenerateFilterColor(true);}
				if(isset($this -> MileageRange)) {$sqlWhere .= $this -> GenerateMileageRangeQuery(true);}
				if(isset($this -> PriceRange)) {$sqlWhere .= $this -> GeneratePriceRangeQuery(true);}
				if(isset($this -> EngineSizeCC)) {$sqlWhere .= $this -> GenerateEngineSizeCCRangeQuery(true);}
				break;
			case "Count":
				//if(!empty($this -> SearchString)) {$sqlWhere .= $this -> GenerateSearchStringQuery();}
				if(isset($this -> Condition)) {$sqlWhere .= $this -> GenerateConditionQuery();}
				if(isset($this -> Years)) {$sqlWhere .= $this -> GenerateYearQuery();}
				if(isset($this -> Categories)) {$sqlWhere .= $this -> GenerateCategoryQuery();}	
				if(isset($this -> Stores)) {$sqlWhere .= $this -> GenerateStoreQuery();}
				if(isset($this -> Manufactures)) {$sqlWhere .= $this -> GenerateManufactureQuery();}	
				//if(isset($this -> Colors)) {$sqlWhere .= $this -> GenerateFilterColor();}	
				if(isset($this -> MileageRange)) {$sqlWhere .= $this -> GenerateMileageRangeQuery();}	
				if(isset($this -> PriceRange)) {$sqlWhere .= $this -> GeneratePriceRangeQuery();}
				if(isset($this -> EngineSizeCC)) {$sqlWhere .= $this -> GenerateEngineSizeCCRangeQuery();}
				break;
			case "YearsFiltered":
				if(!empty($this -> SearchString)) {$sqlWhere .= $this -> GenerateSearchStringQuery();}
				if(isset($this -> Condition)) {$sqlWhere .= $this -> GenerateConditionQuery();}
				if(isset($this -> Categories)) {$sqlWhere .= $this -> GenerateCategoryQuery();}	
				if(isset($this -> Stores)) {$sqlWhere .= $this -> GenerateStoreQuery();}
				if(isset($this -> Manufactures)) {$sqlWhere .= $this -> GenerateManufactureQuery();}	
				//if(isset($this -> Colors)) {$sqlWhere .= $this -> GenerateFilterColor();}	
				if(isset($this -> MileageRange)) {$sqlWhere .= $this -> GenerateMileageRangeQuery();}	
				if(isset($this -> PriceRange)) {$sqlWhere .= $this -> GeneratePriceRangeQuery();}
				if(isset($this -> EngineSizeCC)) {$sqlWhere .= $this -> GenerateEngineSizeCCRangeQuery();}
				break;
			case "CategoriesFiltered":
				//if(!empty($this -> SearchString)) {$sqlWhere .= $this -> GenerateSearchStringQuery();}
				if(isset($this -> Condition)) {$sqlWhere .= $this -> GenerateConditionQuery();}
				if(isset($this -> Years)) {$sqlWhere .= $this -> GenerateYearQuery();}
				if(isset($this -> Stores)) {$sqlWhere .= $this -> GenerateStoreQuery();}
				if(isset($this -> Manufactures)) {$sqlWhere .= $this -> GenerateManufactureQuery();}	
			//	//if(isset($this -> Colors)) {$sqlWhere .= $this -> GenerateFilterColor();}
				if(isset($this -> MileageRange)) {$sqlWhere .= $this -> GenerateMileageRangeQuery();}
				if(isset($this -> PriceRange)) {$sqlWhere .= $this -> GeneratePriceRangeQuery();}
				if(isset($this -> EngineSizeCC)) {$sqlWhere .= $this -> GenerateEngineSizeCCRangeQuery();}
				break;
			case "ManufacturesFiltered":
				//if(!empty($this -> SearchString)) {$sqlWhere .= $this -> GenerateSearchStringQuery();}
				if(isset($this -> Condition)) {$sqlWhere .= $this -> GenerateConditionQuery();}
				if(isset($this -> Years)) {$sqlWhere .= $this -> GenerateYearQuery();}
				if(isset($this -> Categories)) {$sqlWhere .= $this -> GenerateCategoryQuery();}	
				if(isset($this -> Stores)) {$sqlWhere .= $this -> GenerateStoreQuery();}
				//if(isset($this -> Colors)) {$sqlWhere .= $this -> GenerateFilterColor();}
				if(isset($this -> MileageRange)) {$sqlWhere .= $this -> GenerateMileageRangeQuery();}
				if(isset($this -> PriceRange)) {$sqlWhere .= $this -> GeneratePriceRangeQuery();}
				if(isset($this -> EngineSizeCC)) {$sqlWhere .= $this -> GenerateEngineSizeCCRangeQuery();}
				break;
			case "StoresFiltered":
				//if(!empty($this -> SearchString)) {$sqlWhere .= $this -> GenerateSearchStringQuery();}
				if(isset($this -> Condition)) {$sqlWhere .= $this -> GenerateConditionQuery(true);}
				if(isset($this -> Years)) {$sqlWhere .= $this -> GenerateYearQuery(true);}
				if(isset($this -> Categories)) {$sqlWhere .= $this -> GenerateCategoryQuery(true);}	
				if(isset($this -> Manufactures)) {$sqlWhere .= $this -> GenerateManufactureQuery(true);}	
				//if(isset($this -> Colors)) {$sqlWhere .= $this -> GenerateFilterColor(true);}
				if(isset($this -> MileageRange)) {$sqlWhere .= $this -> GenerateMileageRangeQuery(true);}
				if(isset($this -> PriceRange)) {$sqlWhere .= $this -> GeneratePriceRangeQuery(true);}
				if(isset($this -> EngineSizeCC)) {$sqlWhere .= $this -> GenerateEngineSizeCCRangeQuery(true);}
				break;
			case "MileageFiltered":
				//if(!empty($this -> SearchString)) {$sqlWhere .= $this -> GenerateSearchStringQuery();}
				if(isset($this -> Condition)) {$sqlWhere .= $this -> GenerateConditionQuery();}
				if(isset($this -> Years)) {$sqlWhere .= $this -> GenerateYearQuery();}
				if(isset($this -> Categories)) {$sqlWhere .= $this -> GenerateCategoryQuery();}	
				if(isset($this -> Stores)) {$sqlWhere .= $this -> GenerateStoreQuery();}
				if(isset($this -> Manufactures)) {$sqlWhere .= $this -> GenerateManufactureQuery();}	
				//if(isset($this -> Colors)) {$sqlWhere .= $this -> GenerateFilterColor();}
				if(isset($this -> PriceRange)) {$sqlWhere .= $this -> GeneratePriceRangeQuery();}
				if(isset($this -> EngineSizeCC)) {$sqlWhere .= $this -> GenerateEngineSizeCCRangeQuery();}
				break;	
			case "MSRPFiltered":
				//if(!empty($this -> SearchString)) {$sqlWhere .= $this -> GenerateSearchStringQuery();}
				if(isset($this -> Condition)) {$sqlWhere .= $this -> GenerateConditionQuery();}
				if(isset($this -> Years)) {$sqlWhere .= $this -> GenerateYearQuery();}
				if(isset($this -> Categories)) {$sqlWhere .= $this -> GenerateCategoryQuery();}	
				if(isset($this -> Stores)) {$sqlWhere .= $this -> GenerateStoreQuery();}
				if(isset($this -> Manufactures)) {$sqlWhere .= $this -> GenerateManufactureQuery();}	
				//if(isset($this -> Colors)) {$sqlWhere .= $this -> GenerateFilterColor();}
				if(isset($this -> MileageRange)) {$sqlWhere .= $this -> GenerateMileageRangeQuery();}
				if(isset($this -> EngineSizeCC)) {$sqlWhere .= $this -> GenerateEngineSizeCCRangeQuery();}
				break;
				
			case "ColorsFiltered":
				//if(!empty($this -> SearchString)) {$sqlWhere .= $this -> GenerateSearchStringQuery();}
				if(isset($this -> Condition)) {$sqlWhere .= $this -> GenerateConditionQuery();}
				if(isset($this -> Years)) {$sqlWhere .= $this -> GenerateYearQuery();}
				if(isset($this -> Categories)) {$sqlWhere .= $this -> GenerateCategoryQuery();}	
				if(isset($this -> Stores)) {$sqlWhere .= $this -> GenerateStoreQuery();}
				if(isset($this -> Manufactures)) {$sqlWhere .= $this -> GenerateManufactureQuery();}	
				if(isset($this -> MileageRange)) {$sqlWhere .= $this -> GenerateMileageRangeQuery();}
				if(isset($this -> PriceRange)) {$sqlWhere .= $this -> GeneratePriceRangeQuery();}
				if(isset($this -> EngineSizeCC)) {$sqlWhere .= $this -> GenerateEngineSizeCCRangeQuery();}
				break;
			
			case "EngineSizeCCFiltered":
				//if(!empty($this -> SearchString)) {$sqlWhere .= $this -> GenerateSearchStringQuery();}
				if(isset($this -> Condition)) {$sqlWhere .= $this -> GenerateConditionQuery();}
				if(isset($this -> Years)) {$sqlWhere .= $this -> GenerateYearQuery();}
				if(isset($this -> Categories)) {$sqlWhere .= $this -> GenerateCategoryQuery();}	
				if(isset($this -> Stores)) {$sqlWhere .= $this -> GenerateStoreQuery();}
				if(isset($this -> Manufactures)) {$sqlWhere .= $this -> GenerateManufactureQuery();}	
				//if(isset($this -> Colors)) {$sqlWhere .= $this -> GenerateFilterColor();}
				if(isset($this -> MileageRange)) {$sqlWhere .= $this -> GenerateMileageRangeQuery();}
				if(isset($this -> PriceRange)) {$sqlWhere .= $this -> GeneratePriceRangeQuery();}
				break;
			/**	 * 
				 */
		}
		
		if(!empty($sqlWhere)) {
			return ' AND' . rtrim($sqlWhere, ' AND');
		}
		
	}
	
	
	public function AllLiveInventory($page) {
		
		$this -> pageNumber = $page;
		$this -> UserLimit = "30";
		
		if($this -> pageNumber) {
			//First Item to display on this page
			$start = ($this -> pageNumber - 1) * $this -> UserLimit;
		} else {
			//if no page variable is given, set start to 0
			$start = 0;
		}
		
		
		$url = "#FilerApplied";
		
		$results = array();
		
		
		
		
	
		$count = $this -> Count();									
		
		switch($this -> OrderByInventory) {
			case "YearsDesc":
				$orderBySql = 'inventory.Year DESC';
				break;	
			case "YearsAsc":
				$orderBySql = 'inventory.Year ASC';
				break;
			case "PriceDesc":
				$orderBySql = 'inventory.MSRP DESC';
				break;
			case "PriceAsc":
				$orderBySql = 'inventory.MSRP ASC';
				break;
			case "RecentlyAdded":
				$orderBySql = 'inventory.TimeInserted DESC';
				break;
			case "ManufactureDesc":
				$orderBySql = 'inventory.Manufacturer DESC';		
				break;
			case "ManufactureAsc":
				$orderBySql = 'inventory.Manufacturer ASC';
				break;
			case "MilesDesc":
				$orderBySql = 'inventory.Mileage DESC';
				break;
			case "MilesAsc":
				$orderBySql = 'inventory.Mileage ASC';
				break;
			default:
				$orderBySql = 'inventory.Year DESC';
				break;
		}
											
		
		$inventorySql = $this -> db -> prepare('SELECT inventory.inventoryID, 
			   (
                   CASE WHEN inventory.FriendlyModelName != " " THEN 
                   CONCAT(inventory.Year, " ", inventory.Manufacturer, " ", inventory.FriendlyModelName)
                   ELSE
                   CONCAT(inventory.Year, " ", inventory.Manufacturer, " ", inventory.ModelName)
                    END
                ) InventoryBikeName,
              (CASE 
               WHEN inventory.Conditions = 0 THEN 
                  "New"
               ELSE
                  "Used"
               END
              ) ConditionText,
              inventory.Conditions,
              inventory.Year,
              inventory.ModelName,
              inventory.FriendlyModelName,
              inventory.Manufacturer,
              inventory.Stock,
              inventory.MSRP,
              inventory.Mileage,
              inventorycategories.inventoryCategoryName,
              inventory.MarkAsSoldDate,
              inventory.OverlayText,
              inventory.VinNumber,
              inventory.InventoryDescription,
              inventory.TimeInserted,
              inventory.EngineSizeCC,
              stores.StoreName,
              COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor,
              COALESCE(GROUP_CONCAT(colors.FilterColor), inventory.Color) SelectedFilteredColor,
              (CASE 
              WHEN inventory.Conditions = 0 THEN 
                  CONCAT("new/", inventory.inventorySeoURL)
               ELSE
                  CONCAT("used/", inventory.inventorySeoURL)
               END
              ) InventoryURLString,
              (
			  	CASE 
                  WHEN inventoryphotos.inventoryPhotoName != " " THEN 
                       CONCAT(inventory.VinNumber, "/", inventoryphotos.inventoryPhotoName, "-s.", inventoryphotos.inventoryPhotoExt)
                  ELSE
                  NULL
                  END
			  
			  
			  ) InventoryMainPhoto,
			  inventoryphotos.inventoryAltTag,
              inventoryphotos.inventoryTitleTag
              FROM inventory
                   LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
                   LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
                   LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
                   LEFT JOIN inventoryphotos ON inventory.inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1 
                   LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
                   WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL' . $this -> GenerateSqlWhereClauses('InventoryList') . ' GROUP BY inventory.inventoryID ' . $this -> GenerateFilterColor() . ' ORDER BY '. $orderBySql . ' LIMIT :start, :end');
		
		
				   
				   
		$inventorySql -> bindValue(':start', (int) $start, PDO::PARAM_INT);
		$inventorySql -> bindValue(':end', (int) $this -> UserLimit, PDO::PARAM_INT);		   
		
		$inventorySql -> execute();
		
		
		
		
		
		return $inventorySql -> fetchAll();
		
		//excluded harley new inventory
		//SELECT * FROM inventory LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID WHERE HarleyNewInventory.HarleyNewInventoryID IS NULL
	}

	public function PaginationLinks() {
		return $this -> pagination -> paginationLinks($this -> Count(), $this -> UserLimit, PATH . "inventory/page/", $this -> pageNumber);
	}
	
	public function AppendURL() {
		$urlString = '';	
		if(!empty($this -> AppendURL)) {
			$urlString .= '#FilerApplied' . $this -> AppendURL;	
		}
		
		if($urlString != '') {
			return $urlString;	
		} else {
			return '';
		}
		
	}


	public function RelatedInventory($currentInventoryVIN, $currentStoreID, $CurrentCategoryID) {
		try {	
			$currentInventoryObject = InventoryObject::WithVin($currentInventoryVIN);
			
			$MaxCCRange = ($currentInventoryObject -> EngineSizeCC + 200);	
			$MinCCRange = ($currentInventoryObject -> EngineSizeCC - 200);
			
			
			//SELECT * FROM ( SELECT * FROM inventory WHERE Category = 8 AND inventoryID <> 8 AND inventoryStoreID = 2 AND IsInventoryActive = 1 AND CASE WHEN EngineSizeCC IS NOT NULL THEN EngineSizeCC > 400 AND EngineSizeCC < 200 ELSE TRUE END ) RelatedInventoryTable LEFT JOIN inventoryphotos ON RelatedInventoryTable.inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1 ORDER BY RAND() LIMIT 4
			
			$relatedInventoryCC = $this -> db -> prepare('SELECT * FROM 
																	(SELECT * FROM inventory WHERE Category = :currentInventoryCategory AND Stock <> :currentInventoryStock AND inventoryStoreID = :currentStoreID AND IsInventoryActive = 1 AND CASE WHEN EngineSizeCC IS NOT NULL THEN EngineSizeCC > :MaxRange AND EngineSizeCC < :MinRange ELSE TRUE END ) RelatedInventoryTable 
																	LEFT JOIN inventoryphotos ON RelatedInventoryTable .inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1 
																	ORDER BY RAND() LIMIT 4');	
			$relatedInventoryCC -> execute(array(':currentInventoryStock' => $currentInventoryObject -> stockNumber,
											   ':currentStoreID' => $currentStoreID,
											   ':currentInventoryCategory' => $CurrentCategoryID,
											   ':MaxRange' => $MaxCCRange,
											   ':MinRange' => $MinCCRange));
											   
			$relatedInventory = $this -> db -> prepare('SELECT * FROM 
																	(SELECT * FROM inventory WHERE Category = :currentInventoryCategory AND Stock <> :currentInventoryStock AND inventoryStoreID = :currentStoreID AND IsInventoryActive = 1) RelatedInventoryTable 
																	LEFT JOIN inventoryphotos ON RelatedInventoryTable .inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1 
																	ORDER BY RAND() LIMIT 4');	
			$relatedInventory -> execute(array(':currentInventoryStock' => $currentInventoryObject -> stockNumber,
											   ':currentStoreID' => $currentStoreID,
											   ':currentInventoryCategory' => $CurrentCategoryID));								   
			
			if($relatedInventoryCC -> rowCount() > 0) {
				return $relatedInventoryCC -> fetchAll();	
			} else {
				return $relatedInventory -> fetchAll();
			}								   
						
		} catch (PDOException $e) { // Right now it is only catching SQL errors.
            $this->_error($e, "PDO EXCEPTION");
        } catch (Exception $e){
            // You can choose what to do here for other exceptions.... right now I am doing nothing. Just leaving this here to show how it works
            $this->_error($e, "Normal Exception");
        }
	}

  	private function _error($e, $errorType) {
    	//print_r($e->getMessage());
        $TrackError = new EmailServerError();
        $TrackError -> message = 'Related Inventory error: ' . $e->getMessage();
        $TrackError -> type = $errorType;
        $TrackError -> SendMessage();
        
    }
	
	
	public function FeaturedInventory() {
		return $this -> db -> select('SELECT inventory.inventoryID, 
			   (
                   CASE WHEN inventory.FriendlyModelName != " " THEN 
                   CONCAT(inventory.Year, " ", inventory.Manufacturer, " ", inventory.FriendlyModelName)
                   ELSE
                   CONCAT(inventory.Year, " ", inventory.Manufacturer, " ", inventory.ModelName)
                    END
                ) InventoryBikeName,
              (CASE 
               WHEN inventory.Conditions = 0 THEN 
                  "New"
               ELSE
                  "Used"
               END
              ) ConditionText,
              inventory.Year,
              inventory.ModelName,
              inventory.Manufacturer,
              inventory.Stock,
              inventory.MSRP,
              inventory.Mileage,
              inventory.MarkAsSoldDate,
              inventory.OverlayText,
              inventory.VinNumber,
              inventory.InventoryDescription,
              (CASE 
              WHEN inventory.Conditions = 0 THEN 
                  CONCAT("new/", inventory.inventorySeoURL)
               ELSE
                  CONCAT("used/", inventory.inventorySeoURL)
               END
              ) InventoryURLString,
              (
			  	CASE 
                  WHEN inventoryphotos.inventoryPhotoName != " " THEN 
                       CONCAT(inventory.VinNumber, "/", inventoryphotos.inventoryPhotoName, "-s.", inventoryphotos.inventoryPhotoExt)
                  ELSE
                  NULL
                  END
			  
			  
			  ) InventoryMainPhoto,
			  inventoryphotos.inventoryAltTag,
              inventoryphotos.inventoryTitleTag
              FROM inventory
                   LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
                   LEFT JOIN inventoryphotos ON inventory.inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1 
                   WHERE inventory.IsInventoryActive=1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL AND inventory.isFeaturedProduct = 1');
	}
	

}