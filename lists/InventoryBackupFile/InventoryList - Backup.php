<?php

class InventoryList extends BaseObjectList {
	
	
	public $Condition;
	public $Stores;
	public $Manufactures;
	public $Colors;
	public $Years;
	public $Categories;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function manufactures() {
		return $this -> db -> select('SELECT DISTINCT Manufacturer FROM inventory WHERE IsInventoryActive = 1');
	}
	
	public function Stores() {
		return $this -> db -> select('SELECT DISTINCT inventory.InventoryStoreID, stores.StoreName, inventory.IsInventoryActive, stores.storeID FROM inventory LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID WHERE IsInventoryActive = 1');
	}
	
	public function Years() {
		return $this -> db -> select('SELECT DISTINCT Year FROM inventory WHERE IsInventoryActive = 1 ORDER BY Year DESC');
	}
	
	public function Categories() {
		return $this -> db -> select('SELECT DISTINCT inventory.Category, inventorycategories.inventoryCategoryName FROM inventory INNER JOIN inventorycategories ON inventorycategories.inventoryCategoryID = inventory.Category WHERE inventory.IsInventoryActive = 1');
	}
	
	
	
	public function Newsletters() {
		return $this -> db -> select('SELECT * FROM newsletter ORDER BY DateSent DESC');
	}
	
	public function Colors() {
		return $this -> db -> select('SELECT DISTINCT COALESCE(colors.FilterColor, inventory.Color) FilterColor FROM inventory LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) WHERE inventory.IsInventoryActive = 1');
	}
	
	public function Condition() {
		return $this -> db -> select('SELECT DISTINCT IsInventoryActive, Conditions FROM inventory WHERE IsInventoryActive = 1');
	}
	
	public function AllLiveInventory() {
		$sqlWhere = NULL;
		$havingColorSql = NULL;
		
		$url = "#FilerApplied";
		
		$results = array();
		
		
		if(isset($this -> Condition)) {
			$conditionValue = NULL;
			foreach($this -> Condition as $conditionSingle) {
				$conditionValue .=  $conditionSingle . ',';	
			}
			$conditionQuery = rtrim($conditionValue, ',');
			$url .= '&Condition=' . $conditionQuery;
					
			$sqlWhere .= " AND inventory.Conditions IN (" . $conditionQuery . ")";
			
		}

		if(isset($this -> Stores)) {
			$storeValue = NULL;
			foreach($this -> Stores as $storeSingle) {
				$storeValue .=  '"'. $storeSingle . '",';	
			}
			$storeQuery = rtrim($storeValue, ',');
					
					
			$sqlWhere .= " AND inventory.InventoryStoreID IN (" . $storeQuery . ")";
		}
		
		if(isset($this -> Manufactures)) {
			$manufactureValue = NULL;
			foreach($this -> Manufactures as $manufactureSingle) {
				$manufactureValue .=  '"'. $manufactureSingle . '",';	
			}
			$manufactureQuery = rtrim($manufactureValue, ',');
					
			$sqlWhere .= " AND inventory.Manufacturer IN (" . $manufactureQuery . ")";
		}		
		
		if(isset($this -> Colors)) {
			$colorValue = NULL;
			foreach($this -> Colors as $colorSingle) {
				$colorValue .=  $colorSingle . '|';	
			}
			$colorQuery = rtrim($colorValue, '|');
					
			$havingColorSql .= " HAVING SelectedFilteredColor REGEXP '" . $colorQuery . "'";
		}		
		
		if(isset($this -> Years)) {
			$yearValue = NULL;
			foreach($this -> Years as $yearSingle) {
				$yearValue .=  '"'. $yearSingle . '",';	
			}
			$yearQuery = rtrim($yearValue, ',');
					
			$sqlWhere .= " AND inventory.Year IN (" . $yearQuery . ")";
		}
		
		if(isset($this -> Categories)) {
			$categoryValue = NULL;
			foreach($this -> Categories as $categorySingle) {
				$categoryValue .=  '"'. $categorySingle . '",';	
			}
			$categoryQuery = rtrim($categoryValue, ',');
					
			$sqlWhere .= " AND inventory.Category IN (" . $categoryQuery . ")";
		}	
	
		$sqlWHEREYears = NULL;
		$sqlWHEREStores = NULL;
		$sqlWHERECategories = NULL;
		$sqlWHEREManufacture = NULL;
		$sqlWHEREColors = NULL;
		$havingColorYears = NULL;
		$havingColorStores = NULL;
		$havingColorCategories = NULL;
		$havingColorManufactures = NULL;
		
		
		$inventorySql = $this -> db -> select('SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor, COALESCE(colors.FilterColor, inventory.Color) SelectedFilteredColor FROM inventory 
													LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
													LEFT JOIN inventoryphotos ON inventory.inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1 
													LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
													LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
													LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
													WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL' . $sqlWhere . ' GROUP BY inventory.inventoryID' . $havingColorSql . ' LIMIT 0, 20');
		
		if(isset($storeQuery)) {
			$sqlWHEREYears .= " AND inventory.InventoryStoreID IN (" . $storeQuery . ")";
			$sqlWHERECategories	.= " AND inventory.InventoryStoreID IN (" . $storeQuery . ")";
			$sqlWHEREManufacture .= " AND inventory.InventoryStoreID IN (" . $storeQuery . ")";
			$sqlWHEREColors .= " AND inventory.InventoryStoreID IN (" . $storeQuery . ")";
		}
		
		if(isset($yearQuery)) {
			$sqlWHERECategories	.= " AND inventory.Year IN (" . $yearQuery . ")";
			$sqlWHEREStores .= " AND inventory.Year IN (" . $yearQuery . ")";
			$sqlWHEREManufacture  .= " AND inventory.Year IN (" . $yearQuery . ")";
			$sqlWHEREColors .= " AND inventory.Year IN (" . $yearQuery . ")";
		}
		
		if(isset($categoryQuery)) {
			$sqlWHEREStores .= " AND inventory.Category IN (" . $categoryQuery . ")";
			$sqlWHEREYears .= " AND inventory.Category IN (" . $categoryQuery . ")";
			$sqlWHEREManufacture .= " AND inventory.Category IN (" . $categoryQuery . ")";
			$sqlWHEREColors .= " AND inventory.Category IN (" . $categoryQuery . ")";
		}
		
		if(isset($manufactureQuery)) {
			$sqlWHEREStores .= " AND inventory.Manufacturer IN (" . $manufactureQuery . ")";
			$sqlWHEREYears .= " AND inventory.Manufacturer IN (" . $manufactureQuery . ")";
			$sqlWHERECategories	 .= " AND inventory.Manufacturer IN (" . $manufactureQuery . ")";
			$sqlWHEREColors .= " AND inventory.Manufacturer IN (" . $manufactureQuery . ")";
		}

		if(isset($colorQuery)) {
			$havingColorYears  .= " HAVING SelectedFilteredColor REGEXP '" . $colorQuery . "'";
			$havingColorStores .= " HAVING SelectedFilteredColor REGEXP '" . $colorQuery . "'";
			$havingColorCategories .= " HAVING SelectedFilteredColor REGEXP '" . $colorQuery . "'";
			$havingColorManufactures .= " HAVING SelectedFilteredColor REGEXP '" . $colorQuery . "'";
		}		
		
		
		
		
		
		
													
		$yearFiltered = $this -> db -> select('SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor, COALESCE(colors.FilterColor, inventory.Color) SelectedFilteredColor FROM inventory 
													LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
													LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
													LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
													LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
													WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL' . $sqlWHEREYears . ' GROUP BY inventory.inventoryID' . $havingColorYears);											
		
		$storesFiltered = $this -> db -> select('SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor, COALESCE(colors.FilterColor, inventory.Color) SelectedFilteredColor FROM inventory 
													LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
													LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
													LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
													LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
													WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL' . $sqlWHEREStores . ' GROUP BY inventory.inventoryID' . $havingColorStores);
													
													
		$categoryFiltered = $this -> db -> select('SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor, COALESCE(colors.FilterColor, inventory.Color) SelectedFilteredColor FROM inventory 
													LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
													LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
													LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
													LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
													WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL' . $sqlWHERECategories . ' GROUP BY inventory.inventoryID' . $havingColorCategories);
													
		$manufacturedFiltered = $this -> db -> select('SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor, COALESCE(colors.FilterColor, inventory.Color) SelectedFilteredColor FROM inventory 
													LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
													LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
													LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
													LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
													WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL' . $sqlWHEREManufacture . ' GROUP BY inventory.inventoryID' . $havingColorManufactures);
													
		$colorFiltered = $this -> db -> select('SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor, COALESCE(colors.FilterColor, inventory.Color) SelectedFilteredColor FROM inventory 
													LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
													LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
													LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
													LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
													WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL' . $sqlWHEREColors . ' GROUP BY inventory.inventoryID');	
													
													
													
																															
		
													
												
		$results['items'] = $inventorySql;
		$results['count'] = count($this -> db -> select('SELECT *, COALESCE(GROUP_CONCAT(colors.ColorText), inventory.Color) SelectedColor, COALESCE(colors.FilterColor, inventory.Color) SelectedFilteredColor FROM inventory 
													LEFT JOIN colors ON FIND_IN_SET(colors.colorID, inventory.Color) 
													LEFT JOIN inventoryphotos ON inventory.inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1 
													LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID 
													LEFT JOIN inventorycategories ON inventory.Category = inventorycategories.inventoryCategoryID
													LEFT JOIN stores ON inventory.InventoryStoreID = stores.storeID
													WHERE inventory.IsInventoryActive = 1 AND HarleyNewInventory.HarleyNewInventoryID IS NULL' . $sqlWhere . ' GROUP BY inventory.inventoryID' . $havingColorSql));		
		$results['AppendUrl'] = $url;
		
		$ConditionFilterString = NULL;
		
		$categoryFilterString = NULL;
		$storeFilterString = NULL;
		$manufactureFilterString = NULL;
		$colorFilterString = NULL;
		
		$Conditions = array_column($inventorySql, 'Conditions');
		
		//year results
		$Years = array_column($yearFiltered, 'Year');		
		
		$YearFilterString = NULL;
		$YearsArray = array_unique($Years, SORT_REGULAR);
		arsort($YearsArray);
		foreach($YearsArray as $yearSingle) {
			$checkedString = NULL;	
			if(isset($this -> Years)) {
				if(in_array($yearSingle, $this -> Years)) {
					$checkedString = ' checked ';	
				}	
			}
			$YearFilterString .= '<div class="line"><input type="checkbox" name="Year[]"' . $checkedString . ' value="'. $yearSingle . '"> ' . $yearSingle . '</div>';	
		}	
					
		$results['Years'] = $YearFilterString;	
		
		//stores
		$stores = array_column($storesFiltered, 'StoreName');
		$storeID = array_column($storesFiltered, 'InventoryStoreID');
		
		foreach(array_unique($stores, SORT_REGULAR) as $key => $storeSingle) {
			$storeChecked = NULL;
			if(isset($this -> Stores)) {
				if(in_array($storeID[$key], $this -> Stores)) {
					$storeChecked = ' checked ';	
				}	
			}
			$storeFilterString .= '<div class="line"><input type="checkbox" name="store[]"' . $storeChecked . '  value="'. $storeID[$key] . '"> ' . $storeSingle . '</div>';	
		}	
			
		$results['Stores'] = $storeFilterString;
		
		
		
		$categories = array_column($categoryFiltered, 'inventoryCategoryName');
		$categoryID = array_column($categoryFiltered, 'inventoryCategoryID');
		
		foreach(array_unique($categories, SORT_REGULAR) as $key => $CategorySingle) {
			if(!empty($CategorySingle)) {
				$categoryChecked = NULL;
				if(isset($this -> Categories)) {
					if(in_array($categoryID[$key], $this -> Categories)) {
						$categoryChecked = ' checked ';	
					}	
				}
				$categoryFilterString .= '<div class="line"><input type="checkbox" name="category[]"' . $categoryChecked . ' value="'. $categoryID[$key] . '"> ' . $CategorySingle . '</div>';		
			}
		}	
			
		$results['Categories'] = $categoryFilterString;
		
		
		$manufactures = array_column($manufacturedFiltered, 'Manufacturer');
		
		foreach(array_unique($manufactures, SORT_REGULAR) as  $ManufactureSingle) {
			//if(!empty($CategorySingle)) {
				$manufactureChecked = NULL;
				if(isset($this -> Manufactures)) {
					if(in_array($ManufactureSingle, $this -> Manufactures)) {
						$manufactureChecked = ' checked ';	
					}	
				}
				$manufactureFilterString .= '<div class="line"><input type="checkbox" name="manufacture[]"' . $manufactureChecked . ' value="'. $ManufactureSingle . '"> ' . $ManufactureSingle . '</div>';		
			//}
		}
		
		$results['Manufactures'] = $manufactureFilterString;
		
		$colors = array_column($colorFiltered, 'SelectedFilteredColor');
		
		foreach(array_unique($colors, SORT_REGULAR) as  $colorSingle) {
			if(!empty($colorSingle)) {
				$colorChecked = NULL;
				if(isset($this -> Colors)) {
					if(in_array($colorSingle, $this -> Colors)) {
						$colorChecked = ' checked ';	
					}
				}	
			
				$colorFilterString .= '<div class="line"><input type="checkbox" name="color[]"' . $colorChecked . ' value="'. $colorSingle . '"> '. $colorSingle . '</div>';	
			}	
		}
		
		$results['Colors'] = $colorFilterString;
		
		
		
		if(!isset($this -> Condition)) {
		
			foreach(array_unique($Conditions, SORT_REGULAR) as $conditionFilter) {
				$ConditionFilterString .= '<div class="line">
										<input type="checkbox" class="ConditionCheck" name="Condition[]" value="'. $conditionFilter . '"> ' . ($conditionFilter == 0 ? 'New' : 'Used') . '
									</div>';
			}
			
			$results['Conditions'] = $ConditionFilterString;		
			
		} else {
			$results['Conditions'] = '';		
		}

		
		
		
		
		
		
		
	
		
		
		return $results;
		
		//excluded harley new inventory
		//SELECT * FROM inventory LEFT JOIN (SELECT (inventoryID) HarleyNewInventoryID FROM inventory WHERE inventoryStoreID = 3 AND Conditions = 0) HarleyNewInventory ON inventory.inventoryID = HarleyNewInventory.HarleyNewInventoryID WHERE HarleyNewInventory.HarleyNewInventoryID IS NULL
	}

	public function RelatedInventory($currentInventoryVIN, $currentStoreID, $CurrentCategoryID) {
		try {	
			$currentInventoryObject = InventoryObject::WithVin($currentInventoryVIN);
			
			$MaxCCRange = ($currentInventoryObject -> EngineSizeCC + 200);	
			$MinCCRange = ($currentInventoryObject -> EngineSizeCC - 200);
			
			
			//SELECT * FROM ( SELECT * FROM inventory WHERE Category = 8 AND inventoryID <> 8 AND inventoryStoreID = 2 AND IsInventoryActive = 1 AND CASE WHEN EngineSizeCC IS NOT NULL THEN EngineSizeCC > 400 AND EngineSizeCC < 200 ELSE TRUE END ) RelatedInventoryTable LEFT JOIN inventoryphotos ON RelatedInventoryTable.inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1 ORDER BY RAND() LIMIT 4
			
			$relatedInventoryCC = $this -> db -> prepare('SELECT * FROM 
																	(SELECT * FROM inventory WHERE Category = :currentInventoryCategory AND Stock <> :currentInventoryStock AND inventoryStoreID = :currentStoreID AND IsInventoryActive = 1 AND CASE WHEN EngineSizeCC IS NOT NULL THEN EngineSizeCC > :MaxRange AND EngineSizeCC < :MinRange ELSE TRUE END ) RelatedInventoryTable 
																	LEFT JOIN inventoryphotos ON RelatedInventoryTable .inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1 
																	ORDER BY RAND() LIMIT 4');	
			$relatedInventoryCC -> execute(array(':currentInventoryStock' => $currentInventoryObject -> stockNumber,
											   ':currentStoreID' => $currentStoreID,
											   ':currentInventoryCategory' => $CurrentCategoryID,
											   ':MaxRange' => $MaxCCRange,
											   ':MinRange' => $MinCCRange));
											   
			$relatedInventory = $this -> db -> prepare('SELECT * FROM 
																	(SELECT * FROM inventory WHERE Category = :currentInventoryCategory AND Stock <> :currentInventoryStock AND inventoryStoreID = :currentStoreID AND IsInventoryActive = 1) RelatedInventoryTable 
																	LEFT JOIN inventoryphotos ON RelatedInventoryTable .inventoryID = inventoryphotos.relatedInventoryID AND inventoryphotos.MainProductPhoto = 1 
																	ORDER BY RAND() LIMIT 4');	
			$relatedInventory -> execute(array(':currentInventoryStock' => $currentInventoryObject -> stockNumber,
											   ':currentStoreID' => $currentStoreID,
											   ':currentInventoryCategory' => $CurrentCategoryID));								   
			
			if($relatedInventoryCC -> rowCount() > 0) {
				return $relatedInventoryCC -> fetchAll();	
			} else {
				return $relatedInventory -> fetchAll();
			}								   
						
		} catch (PDOException $e) { // Right now it is only catching SQL errors.
            $this->_error($e, "PDO EXCEPTION");
        } catch (Exception $e){
            // You can choose what to do here for other exceptions.... right now I am doing nothing. Just leaving this here to show how it works
            $this->_error($e, "Normal Exception");
        }
	}

  	private function _error($e, $errorType) {
    	//print_r($e->getMessage());
        $TrackError = new EmailServerError();
        $TrackError -> message = 'Related Inventory error: ' . $e->getMessage();
        $TrackError -> type = $errorType;
        $TrackError -> SendMessage();
        
    }
	
	

}