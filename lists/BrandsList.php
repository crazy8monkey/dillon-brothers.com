<?php

class BrandsList extends BaseObjectList {
	
	public $Page;
	private $Start;
	private $UserLimit;
	public $PaginationPath;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
	}
	
	public function OEMBrands() {
		return $this -> db -> select('SELECT * FROM brands');
	}
	
	public function PartApparelBrands() {
		return $this -> db -> select('SELECT * FROM partaccessorybrand');
	}
	
	public function AllOEMAndPartApparelBrands() {
		return $this -> db -> select('SELECT brands.BrandName, 
											(1) ISOEMBrand, 
											(1) BrandType, 
											brands.BrandImage, 
											brands.BrandID FROM brands 
									  UNION 
									  SELECT partaccessorybrand.brandName, 
									  		(0) ISOEMBrand, (partaccessorybrand.BrandType) BrandType, 
									  		partaccessorybrand.partBrandImage, 
									  		partaccessorybrand.partAccessoryBrandID FROM partaccessorybrand');
	}
}