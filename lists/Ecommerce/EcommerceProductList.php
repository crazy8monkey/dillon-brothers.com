<?php

class EcommerceProductList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function AllProducts() {
		return $this -> db -> select('SELECT *, (SELECT ROUND(AVG(rating), 1)FROM reviews WHERE reviewActive = 1 AND reviewItemID = ecommerceproducts.productID) AverageRating FROM ecommerceproducts LEFT JOIN ecommercesettingcategories ON ecommerceproducts.productCategory = ecommercesettingcategories.settingCategoryID LEFT JOIN ecommercephotos ON ecommerceproducts.productID = ecommercephotos.relatedProductID AND ecommercephotos.productMainProductPhoto = 1 WHERE ecommerceproducts.productVisible = 1 ORDER BY productPublishedDate DESC');
	}
	
	public function ProductsByCategory($id) {
		return $this -> db -> select('SELECT *, (SELECT ROUND(AVG(rating), 1)FROM reviews WHERE reviewActive = 1 AND reviewItemID = ecommerceproducts.productID) AverageRating FROM ecommerceproducts LEFT JOIN ecommercesettingcategories ON ecommerceproducts.productCategory = ecommercesettingcategories.settingCategoryID LEFT JOIN ecommercephotos ON ecommerceproducts.productID = ecommercephotos.relatedProductID AND ecommercephotos.productMainProductPhoto = 1 WHERE ecommerceproducts.productVisible = 1 AND ecommerceproducts.productCategory = ' . $id . ' ORDER BY productPublishedDate DESC');
	}

	public function SelectedProducts($ids) {
		return $this -> db -> select('SELECT * FROM ecommerceproducts LEFT JOIN ecommercephotos ON ecommerceproducts.productID = ecommercephotos.relatedProductID AND ecommercephotos.productMainProductPhoto = 1 WHERE ecommerceproducts.productID IN (' . $ids . ')');
	}

	public function FeaturedProducts() {
		return $this -> db -> select('SELECT * FROM ecommerceproducts LEFT JOIN ecommercephotos ON ecommerceproducts.productID = ecommercephotos.relatedProductID AND ecommercephotos.productMainProductPhoto = 1 WHERE ecommerceproducts.featuredProduct = 1');
	}

}