<?php

class EcommerceColorList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function ByProduct($id) {
		return $this -> db -> select('SELECT * FROM ecommercesavedproductcolors LEFT JOIN ecommercesettingcolors ON ecommercesavedproductcolors.relatedColorID = ecommercesettingcolors.settingColorID WHERE ecommercesavedproductcolors.relatedProductID = ' . $id);
	}
	
	public function SelectedColors($ids) {
		return $this -> db -> select('SELECT * FROM ecommercesettingcolors WHERE settingColorID IN (' . $ids . ')');
	}
}