<?php

class EcommercePhotosList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function PhotosByProduct($id) {
		return $this -> db -> select('SELECT * FROM ecommercephotos WHERE relatedProductID = ' . $id . ' ORDER BY productMainProductPhoto DESC');
	}
}