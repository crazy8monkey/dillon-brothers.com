<?php

class EcommerceCategoryList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function ActiveCategories() {
		return $this -> db -> select('SELECT ecommercesettingcategories.settingCategoryID, 
											 ecommercesettingcategories.settingCategoryName, 
											 ecommercesettingcategories.settingCategorySEOName, 
											 COALESCE((SELECT COUNT(*) FROM ecommerceproducts WHERE productCategory = ecommercesettingcategories.settingCategoryID AND productVisible = 1), 0) ProductCount FROM ecommercesettingcategories 
											 	LEFT JOIN ecommerceproducts ON ecommercesettingcategories.settingCategoryID = ecommerceproducts.productID HAVING ProductCount > 0');
	}

}