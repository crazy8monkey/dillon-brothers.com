<?php

class EcommerceSizeList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function ByProduct($id) {
		return $this -> db -> select('SELECT * FROM ecommercesavedsizes LEFT JOIN ecommercesettingsizes ON ecommercesavedsizes.relatedSizeID = ecommercesettingsizes.settingSizeID WHERE ecommercesavedsizes.relatedProductID = ' . $id . '  GROUP BY ecommercesavedsizes.relatedSizeID');
	}
	
	public function SelectedSizes($ids) {
		return $this -> db -> select('SELECT * FROM ecommercesettingsizes WHERE settingSizeID IN (' . $ids . ')');
	}
}