<?php

class ReviewList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function AllActiveReviewsByEcommerceProducts() {
		return $this -> db -> select('SELECT * FROM reviews WHERE reviewActive = 1 ORDER BY DateEntered DESC');
	}
	
	public function ActiveReviewsByEcommerceProduct($id) {
		return $this -> db -> select('SELECT * FROM reviews WHERE reviewActive = 1 AND reviewItemID = ' . $id . ' ORDER BY DateEntered DESC');
	}



}