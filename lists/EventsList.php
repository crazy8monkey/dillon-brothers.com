<?php

class EventsList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function LatestEvents() {
		$latestEvents = $this -> db -> prepare("SELECT *, (SELECT eventName FROM events WHERE eventID = subevents.SuperEventID) SuperEventName, 
									            (SELECT seoUrl FROM events WHERE eventID = subevents.SuperEventID) SuperEventSEOUrl, 
									            (SELECT SuperEventDescription FROM events WHERE eventID = subevents.SuperEventID) SuperEventDescription,
									            (SELECT startDate FROM events WHERE eventID = subevents.SuperEventID) SuperEventStartDate,
									            (SELECT startTime FROM events WHERE eventID = subevents.SuperEventID) SuperEventStartTime,
                                                (SELECT endDate FROM events WHERE eventID = subevents.SuperEventID) SuperEventEndDate,
                                                (SELECT endTime FROM events WHERE eventID = subevents.SuperEventID) SuperEventEndTime,
                                                (SELECT StructuredDataInfo FROM events WHERE eventID = subevents.SuperEventID) SuperEventStructuredData,
                                                (SELECT eventImage FROM events WHERE eventID = subevents.SuperEventID) SuperEventImage,
                                                (CONCAT(EventMainImage.photoName, '-m.', EventMainImage.ext)) MainEventImageImage,
    											(CONCAT(EventThumbImage.photoName, '-l.', EventThumbImage.ext)) ThumbNailImage
									            FROM events 
									            LEFT JOIN photoalbums ON events.albumID = photoalbums.albumID
									            LEFT JOIN photos AS EventMainImage ON events.eventImage = EventMainImage.photoID 
									            LEFT JOIN photos AS EventThumbImage ON events.eventThumbImage = EventThumbImage.photoID 
									            LEFT JOIN subevents ON events.eventID = subevents.SubEventID WHERE events.eventVisible = 1 AND events.SuperEvent <> 1 AND events.endDate >= :endDate");
		$latestEvents -> execute(array(":endDate" => date("Y-m-d", $this -> time -> NebraskaTime())));							            
		
		return $latestEvents -> fetchAll();
		
		
	}
	
	//SELECT MIN(events.startDate) FROM subevents LEFT JOIN events on subevents.SubEventID = events.eventID WHERE subevents.SuperEventID = 3
	
	
	public function GetRandomEvent() {
		$RandomEvent = $this -> db -> prepare("SELECT *, (SELECT eventName FROM events WHERE eventID = subevents.SuperEventID) SuperEventName, 
									            (SELECT seoUrl FROM events WHERE eventID = subevents.SuperEventID) SuperEventSEOUrl, 
									            (SELECT SuperEventDescription FROM events WHERE eventID = subevents.SuperEventID) SuperEventDescription,
									            (SELECT startDate FROM events WHERE eventID = subevents.SuperEventID) SuperEventStartDate,
									            (SELECT startTime FROM events WHERE eventID = subevents.SuperEventID) SuperEventStartTime,
                                                (SELECT endDate FROM events WHERE eventID = subevents.SuperEventID) SuperEventEndDate,
                                                (SELECT endTime FROM events WHERE eventID = subevents.SuperEventID) SuperEventEndTime,
                                                (SELECT StructuredDataInfo FROM events WHERE eventID = subevents.SuperEventID) SuperEventStructuredData,
                                                (SELECT eventImage FROM events WHERE eventID = subevents.SuperEventID) SuperEventImage,
                                                (CONCAT(EventThumbImage.photoName, '-l.', EventThumbImage.ext)) ThumbNailImage
									            FROM events 
									            LEFT JOIN photoalbums ON events.albumID = photoalbums.albumID
									            LEFT JOIN photos AS EventMainImage ON events.eventImage = EventMainImage.photoID 
									            LEFT JOIN photos AS EventThumbImage ON events.eventThumbImage = EventThumbImage.photoID  
									            LEFT JOIN subevents ON events.eventID = subevents.SubEventID WHERE events.eventVisible = 1 AND events.SuperEvent <> 1 AND events.endDate >= :TodaysDate ORDER BY RAND() LIMIT 1");	
		
		$RandomEvent -> execute(array(":TodaysDate" => date("Y-m-d", $this -> time -> NebraskaTime())));
		
		return $RandomEvent -> fetchAll();
	}
	
	public function GetSubEvents($superEventID) {
		return $this -> db -> select("SELECT * FROM subevents LEFT JOIN events ON subevents.SubEventID = events.eventID LEFT JOIN photoalbums ON events.albumID = photoalbums.albumID LEFT JOIN photos ON events.eventImage = photos.photoID WHERE events.eventVisible = 1 AND SuperEventID = " . $superEventID);						
	}
	
	
	public function GetSuperEventRelatedAlbums($id) {
		return $this -> db -> select('SELECT *, (SELECT COUNT(*) FROM photos WHERE photoalbumID = photoalbums.albumID) PhotoCount FROM photoalbums LEFT JOIN photos ON photoalbums.albumThumbNail = photos.photoID WHERE photoalbums.linkedSuperEventID = '. $id . ' HAVING PhotoCount > 0 ORDER BY photoalbums.ParentDirectory DESC');
	}
	
	public function GetEventRelatedAlbums($seoUrl) {
		return $this -> db -> select('SELECT *, (SELECT COUNT(*) FROM photos WHERE photoalbumID = photoalbums.albumID) PhotoCount FROM photoalbums LEFT JOIN photos ON photoalbums.albumThumbNail = photos.photoID WHERE photoalbums.linkedSuperEventID = "'. $seoUrl . '" HAVING PhotoCount > 0 ORDER BY photoalbums.ParentDirectory DESC');
	}
	
	
	public function GetSuperEventYearPhotos($superEventUrl) {
		//SELECT DISTINCT events.EventDirectory FROM subevents LEFT JOIN events On subevents.SubEventID = events.eventID WHERE SuperEventID = (SELECT eventID FROM events WHERE seoUrl = 'throttlethursday')
		$years = $this -> db -> prepare("SELECT DISTINCT events.EventDirectory FROM subevents LEFT JOIN events On subevents.SubEventID = events.eventID WHERE SuperEventID = (SELECT eventID FROM events WHERE seoUrl = :seoUrl)");
		$years -> execute(array(":seoUrl" => $superEventUrl));
		return $years -> fetchAll();
	}
	
	public function GetPastSubEvents($id, $currentSubEvent) {
		$sth = $this -> db -> prepare("SELECT * FROM subevents LEFT JOIN events ON subevents.SubEventID = events.eventID WHERE subevents.SuperEventID = :id AND events.endDate <= :todaysDate AND events.endTime <= :todaysTime AND events.eventID <> :subEventID");
		$sth -> execute(array(":id" => $id,
							  ":todaysDate" => date('Y-m-d'),
							  ":todaysTime" => date('H:i'),
							  ":subEventID" => $currentSubEvent));
		return $sth -> fetchAll(); 
	}
	
	public function GetFutureSubEvents($id, $currentSubEvent) {
		$sth = $this -> db -> prepare("SELECT * FROM subevents LEFT JOIN events ON subevents.SubEventID = events.eventID WHERE subevents.SuperEventID = :id AND events.endDate >= :todaysDate AND events.endTime >= :todaysTime AND events.eventID <> :subEventID AND events.eventVisible = 1"); 
		$sth -> execute(array(":id" => $id,
							  ":todaysDate" => date('Y-m-d', $this -> time -> NebraskaTime()),
							  ":todaysTime" => date('H:i', $this -> time -> NebraskaTime()),
							  ":subEventID" => $currentSubEvent));
		return $sth -> fetchAll(); 
	}
	
	
	public function GoogleEvents() {
		
		require_once 'libs/GoogleAPI/autoload.php';
		
		
		
		
		$googleClient = new Google_Client();
		
		//http://stackoverflow.com/questions/35638497/curl-error-60-ssl-certificate-prblm-unable-to-get-local-issuer-certificate
		$guzzleClient = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
		$googleClient->setHttpClient($guzzleClient);		
		$googleClient->setApplicationName("Dillon Brothers");
  		$googleClient->setDeveloperKey("AIzaSyDJFMAj5nlpg3ndTNv1p7ZSkZN6IcBJEbc");
		
		$calendar = new Google_Service_Calendar($googleClient);
		
		$calendarId = CALENDAR_ID;
		
		$eventsItems = $calendar -> events -> listEvents($calendarId);
		
		
		$eventsList = array();
		foreach($eventsItems -> getItems() as $event) {
			array_push($eventsList, array("start" => $event->start->date, 
										  "end" => $event->end->date,
										  "color", "#257e4a"));	
		}
		
		return $eventsList;
	}

}