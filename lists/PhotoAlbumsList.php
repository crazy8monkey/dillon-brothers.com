<?php

class PhotoAlbumsList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function AlbumsByYear($year) {
		$albumsList = $this -> db -> prepare('SELECT *, (SELECT COUNT(*) FROM photos WHERE photoAlbumID = photoalbums.albumID) PhotoCount FROM photoalbums LEFT JOIN photos ON photoalbums.albumThumbNail = photos.photoID WHERE ParentDirectory = :ParentDirectory AND photoAlbumVisible = 1');
		$albumsList -> execute(array(":ParentDirectory" => $year));
		return $albumsList -> fetchAll();
	}
	
	public function AllAlbums() {
		return $this -> db -> select('SELECT * FROM photoalbums WHERE photoAlbumVisible = 1');
	}


}