<?php

class NewsletterList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function Newsletters() {
		return $this -> db -> select('SELECT * FROM newsletter WHERE newsletterVisible = 1 ORDER BY DateSent DESC');
	}

}