<?php

class BaseObjectList {
    public function __construct() {
    	$this -> time = new Time();   
		$this -> pagination = new Pagination(); 	
        $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_CHARSET, DB_USER, DB_PASS);
    }

    public function __sleep() {
    	$this -> time = new Time();    	
        return array();
    }

    public function __wakeup() {
    	$this -> time = new Time();    	
        $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_CHARSET, DB_USER, DB_PASS);
    }

}