<?php

class Model {


	private $cartItems;
	private $totalBoxes = array();
	private $totalCartWeight = null;
	
	public function __construct() {
		//database connection
		$this -> msg = new Message();
		$this -> json = new JSONparse();
		$this -> validate = new Validation();
		$this -> redirect = new Redirect();
		$this -> currentTime = new Time(); 
		$this -> pagination = new Pagination();
		$this -> settings = Settings::GetSettings();
        $this -> setShippingCompany();
		$this -> db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_CHARSET, DB_USER, DB_PASS);
	}


	




}