<?php

class Bootstrap {

    private $_url = null;
    private $_controller = null;
    
    private $_controllerPath = 'controller/'; // Always include trailing slash
    private $_modelPath = 'model/'; // Always include trailing slash
    private $_errorFile = 'error.php';
    private $_defaultFile = 'index.php';
    
	private $_defaultControllers = array();
	
	private $_isEventSingle = false;
	
    /**
     * Starts the Bootstrap
     * 
     * @return boolean
     */
    public function init()
    {
    	$this -> _defaultControllers = array("inventory", 
    										 "specials", 
    										 "newsletters", 
    										 "rewards", 
    										 "photos",
    										 "contact",
											 "ajax",
											 "textclub",
											 "blog",
											 "shop",
											 "cart",
											 "preview",
											 "events",
											 "chat",
											 "service",
											 "apparel",
											 "parts",
											 "donation",
											 "sitemap",
											 "brand",
											 "privacy");
		
        // Sets the protected $_url
        $this->_getUrl();
		
	    // Load the default controller if no URL is set
        // eg: Visit http://localhost it loads Default Controller
        if (empty($this->_url[0])) {
            $this->_loadDefaultController();
            return false;
        }
		
        $this->_loadExistingController();        
    }
    
	
 
    /**
     * (Optional) Set a custom path to controllers
     * @param string $path
     */
    public function setControllerPath($path)
    {
        $this->_controllerPath = trim($path, '/') . '/';
    }
    
    /**
     * (Optional) Set a custom path to models
     * @param string $path
     */
    public function setModelPath($path)
    {
        $this->_modelPath = trim($path, '/') . '/';
    }
    
    /**
     * (Optional) Set a custom path to the error file
     * @param string $path Use the file name of your controller, eg: error.php
     */
    public function setErrorFile($path)
    {
        $this->_errorFile = trim($path, '/');
    }
    
    /**
     * (Optional) Set a custom path to the error file
     * @param string $path Use the file name of your controller, eg: index.php
     */
    public function setDefaultFile($path)
    {
        $this->_defaultFile = trim($path, '/');
    }
    
    /**
     * Fetches the $_GET from 'url'
     */
    private function _getUrl()
    {
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $this->_url = explode('/', $url);
    }
    
    /**
     * This loads if there is no GET parameter passed
     */
    private function _loadDefaultController()
    {
        require $this->_controllerPath . $this->_defaultFile;
        $this->_controller = new Index();
        $this->_controller->index();		
    }
    
    /**
     * Load an existing controller if there IS a GET parameter passed
     * 
     * @return boolean|string
     */
    private function _loadExistingController()
    {
        $file = $this->_controllerPath . $this->_url[0] . '.php';
        
		
		if(in_array($this->_url[0], $this -> _defaultControllers)) {
			require $file;	
			$this->_controller = new $this->_url[0];
			$this->_callControllerMethod();
		} else {
			try {
				require $this->_controllerPath . 'catchall.php';
				$this->_controller = new CatchAll();
				$this->_controller -> view($this->_url[0]);	
			} catch(Exception $e) {
				$this->_error($e, "No Blog/Event Page");
			}
		}
    }
    
    /**
     * If a method is passed in the GET url paremter
     * 
     *  http://localhost/controller/method/(param)/(param)/(param)
     *  url[0] = Controller
     *  url[1] = Method
     *  url[2] = Param
     *  url[3] = Param
     *  url[4] = Param
     */
    private function _callControllerMethod()
    {
        $length = count($this->_url);
        // Make sure the method we are calling exists
        
        if($this->_url[0] == "inventory" || 
           $this->_url[0] == "specials" || 
           $this->_url[0] == "newsletters" || 
           $this->_url[0] == "rewards" || 
           $this->_url[0] == "contact" || 
           $this->_url[0] == "ajax" || 
           $this->_url[0] == "blog" || 
           $this->_url[0] == "shop" ||
		   $this->_url[0] == "cart" ||
		   $this->_url[0] == "preview" || 
		   $this->_url[0] == "textclub" ||
		   $this->_url[0] == "service" ||
		   $this->_url[0] == "parts" ||
		   $this->_url[0] == "apparel" ||
		   $this->_url[0] == "donation" ||
		   $this->_url[0] == "sitemap" ||
		   $this->_url[0] == "chat" ||
		   $this->_url[0] == "privacy") {
		   	if ($length > 1) {
	            if (!method_exists($this->_controller, $this->_url[1])) {
	                $this->_showError();
	            }
	        }	
        }
	

        try {
            // Determine what to load
            switch ($length) {
            	case 6:
                    //Controller->Method(Param1, Param2, Param3, Param4)
                    if($this->_url[0] != 'photos') {
                    	$this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3], $this->_url[4], $this->_url[5]);	
					} else {
                    	$this->_showError();
                    }
                    
                    break;
                case 5:
                    //Controller->Method(Param1, Param2, Param3)
                    if($this->_url[0] != 'photos') {
                    	$this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3], $this->_url[4]);	
                    } else {
                    	$this->_showError();
                    }
                    
                    break;

                case 4:
					switch($this->_url[0]) {
						case "photos":
							//if(method_exists($this->_controller, $this->_controller-> album($this->_url[1], $this->_url[2], $this->_url[3]))) {
								$this->_controller-> album($this->_url[1], $this->_url[2], $this->_url[3]);
							//} else {
							//	$this->_showError();
							//}
							break;
						default:
							//Controller->Method(Param1)	
							$this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3]);	
							break;
					}
                    //Controller->Method(Param1, Param2, Param3)
                    
                    break;

                case 3:
					switch($this->_url[0]) {
						case "events":
							$this->_controller-> view($this->_url[2]);	
							break;	
						case "photos":
							if(!isset($this->_url[3])) {
								$this->_showError();
							}
							break;
						case "brand":
							if (!method_exists($this->_controller, $this->_url[2])) {
								$this ->_controller-> pagecontent($this->_url[1], $this->_url[2]); 
							}
							break;
						default:
							
							//Controller->Method(Param1)	
							$this->_controller->{$this->_url[1]}($this->_url[2]);	
							break;
					}
                   		
                    break;

                case 2:
					switch($this->_url[0]) {
						case "photos":
							if (!method_exists($this->_controller, $this->_url[1])) {
								$this ->_controller-> year($this->_url[1]); 
							}
							break;	
						case "brand":
							if (!method_exists($this->_controller, $this->_url[1])) {
								$this ->_controller-> view($this->_url[1]); 
							}
							break;
						default:
							$this->_controller->{$this->_url[1]}();
							break;
					}
						
                    break;

                default:
					$this->_controller->index();	
					
                    break;
            }
        } catch (PDOException $e) { // Right now it is only catching SQL errors.
            $this->_error($e, "PDO EXCEPTION");
        } catch (Exception $e){
            // You can choose what to do here for other exceptions.... right now I am doing nothing. Just leaving this here to show how it works
            $this->_error($e, "Error Page");
        }
		
    }
    
    /**
     * Display an error page if nothing exists
     * 
     * @return boolean
     */
    private function _error($e, $errorType) {
        	//print_r($e->getMessage());
        $TrackError = new EmailServerError();
        $TrackError -> message = 'BootStrap error: ' . $e->getMessage();
        $TrackError -> type = $errorType;
        $TrackError -> SendMessage();
        $this->_showError();
    }

    /**
     * Display an error page if nothing exists
     *
     * @return boolean
     */
    private function _showError() {
        require $this->_controllerPath . $this->_errorFile;
        $this->_controller = new PageError();
        $this->_controller->index();
        exit;
    }

}