<?php

class Controller {
	 
	const ERRORTITLE = "404 Not Found";
	const DILLONBROTHERS = " | Dillon Brothers";
	 
    public function __construct() {
        $this -> view = new View();
		$this -> json = new JSONparse();
		$this -> email = new Email();
		$this -> time = new Time();
		$this -> redirect = new Redirect();
    }
	
    /**
     * 
     * @param string $name Name of the model
     * @param string $path Location of the models
     */
    public function loadModel($name, $modelPath = 'models/') {
        
        $path = $modelPath . $name.'_model.php';
        
        if (file_exists($path)) {
            require $modelPath .$name.'_model.php';
            
            $modelName = $name . '_Model';
            $this->model = new $modelName();
        }        
    }
	
}