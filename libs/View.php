<?php 

class View {
	
	
	public function __construct() {
		$this-> recordedTime = new Time();
		$this -> form = new Form();
		$this -> visible = New Permissions();
		$this -> security = new Security();
	}
	
	public function render($name, $noInclude = false) {

		if ($noInclude == true) {
			require 'view/' . $name . '.php';
		} else {
			require 'view/header.php';
			require 'view/' . $name . '.php';
			require 'view/footer.php';
		}
		
	}
	
	public function TwitterMetaTags($data = array()) {
			
			
		$tags = NULL;
		$tags .="<meta name='twitter:card' content='summary' /> ";
		$tags .="<meta name='twitter:site' content='@dillonbrothers' />"; 
		$tags .="<meta name='twitter:creator' content='@dillonbrothers' />";
		
		foreach($data as $key => $value) {
			if($key == "Title") {
				if(!empty($value)) {
					$tags .="<meta name='twitter:title' content='" . $value . "' />"; 	
				} else {
					$tags .="<meta name='twitter:title' content='Dillon Brothers' />"; 
				}
			}
						
			if($key == "Description") {
				if(!empty($value)) {
					$tags .="<meta name='twitter:description' content='" . str_replace("<br />", "", $value) . "' />"; 	
				} else {
					$tags .="<meta name='twitter:description' content='Dillon Brothers' />"; 
				}
				 
			}
			
			if($key == "URL") {
				if(!empty($value)) {
					$tags .="<meta name='twitter:url' content='" . $value . "' />"; 	
				} else {
					$tags .="<meta name='twitter:url' content='http://www.dillon-brothers.com/' />"; 
				}						
			}			

			if($key == "ImageURL") {
				if(!empty($value)) {
					$tags .="<meta name='twitter:image:src' content='" . $value . "' />"; 	
				} else {
					$tags .="<meta name='twitter:image:src' content='http://www.dillon-brothers.com/images/dillon-brothers-motorsports-15.png' />"; 
				}					 
			}				
		}		
		return $tags;
		
	}
	
	public function SchemeMetaTags($data = array()) {
		$tags = NULL;
		
		foreach($data as $key => $value) {
			if($key == "Name") {
				if(!empty($value)) {
					$tags .="<meta itemprop='name' content='" . $value . "' />"; 	
				} else {
					$tags .="<meta itemprop='name' content='Dillon Brothers' />"; 
				}
			}
			
			if($key == "Description") {
				if(!empty($value)) {
					$tags .="<meta itemprop='description' content='" . str_replace("<br />", "", $value) . "' />";  	
				} else {
					$tags .="<meta itemprop='description' content='Dillon Brothers' />"; 
				}
			}
			
			if($key == "ImageURL") {
				if(!empty($value)) {
					$tags .="<meta itemprop='image' content='" . $value . "' />";  	
				} else {
					$tags .="<meta itemprop='image' content='http://www.dillon-brothers.com/images/dillon-brothers-motorsports-15.png' />"; 
				}
				 
			}				
		}		
		return $tags;
	}
	
	public function OpenGraphMetaTags($data = array()) {
		$tags = NULL;
		
		foreach($data as $key => $value) {
			if($key == "Title") {
				if(!empty($value)) {
					$tags .="<meta property='og:title' content='" . $value . "' />";	
				} else {
					$tags .="<meta property='og:title' content='Dillon Brothers' />";
				}
			}	
			
			if($key == "Type") {
				if(!empty($value)) {
					$tags .="<meta property='og:type' content='" . $value . "' />";	
				} else {
					$tags .="<meta property='og:type' content='article' />";
				}
			}	
			
			if($key == "URL") {
				if(!empty($value)) {
					$tags .="<meta property='og:url' content='" . $value . "' />";	
				} else {
					$tags .="<meta property='og:url' content='http://www.dillon-brothers.com/' />";
				}
			}				

			if($key == "Image") {
				if(!empty($value)) {
					$tags .="<meta property='og:image' content='" . $value . "' />";	
				} else {
					$tags .="<meta property='og:image' content='http://www.dillon-brothers.com/images/dillon-brothers-motorsports-15.png' />";
				}
			}				

			if($key == "Description") {
				if(!empty($value)) {
					$tags .="<meta property='og:description' content='" . str_replace("<br />", "", $value) . "' />";	
				} else {
					$tags .="<meta property='og:description' content='Dillon Brothers' />";
				}
			}				
					

			if($key == "ArticlePublishTime") {
				if(!empty($value)) {
					$tags .="<meta property='article:published_time' content='" . $value . "' />";	
				}
			}	

			if($key == "ArticleModifiedTime") {
				if(!empty($value)) {
					$tags .="<meta property='article:modified_time' content='" . $value . "' />";	
				}
			}				
			
			
			if($key == "ArticleSection") {
				if(!empty($value)) {
					$tags .="<meta property='article:section' content='" . $value . "' />";	
				} else {
					$tags .="<meta property='article:section' content='Page' />";
				}
			}				

			if($key == "ArticleTag") {
				if(!empty($value)) {
					$tags .="<meta property='article:tag' content='" . $value . "' />";	
				} else {
					$tags .="<meta property='article:tag' content='Dillon Brothers' />";
				}
			}							
		}
		$tags .="<meta property='og:site_name' content='Dillon Brothers' />";	
		$tags .="<meta property='fb:admins' content='172839399568221' />";
	
		return $tags;
				
	}
	
	public function tooltip($msg) {
		return "<a href='javascript:void(0)' data-toggle='tooltip' data-placement='right' title='" . $msg . "' ><img src='" . PATH . "public/images/ToolTipHelp.png' style='margin: -5px 5px 5px 5px;' /></a>";
	}
	
	public function MotorSportInventory($condition) {
		switch($condition) {
			case "New":
				if(LIVE_SITE == true) {
					return PATH . 'inventory?q=&hPP=30&idx=MainUnitInventory&p=0&dFR%5BConditions%5D%5B0%5D=New&dFR%5BStoreName%5D%5B0%5D=Dillon%20Brothers%20MotorSports&is_v=1';
				} else {
					return PATH . 'inventory?q=&hPP=30&idx=MainUnitInventoryTest&p=0&dFR%5BConditions%5D%5B0%5D=New&dFR%5BStoreName%5D%5B0%5D=Dillon%20MotorSports&is_v=1';
				}	
			break;
			case "Used":
				if(LIVE_SITE == true) {
					return PATH . 'inventory?q=&hPP=30&idx=MainUnitInventory&p=0&dFR%5BConditions%5D%5B0%5D=Used&dFR%5BStoreName%5D%5B0%5D=Dillon%20Brothers%20MotorSports&is_v=1';
				} else {
					return PATH . 'inventory?q=&hPP=30&idx=MainUnitInventoryTest&p=0&dFR%5BConditions%5D%5B0%5D=Used&dFR%5BStoreName%5D%5B0%5D=Dillon%20MotorSports&is_v=1';
				}	
			break;
		}
		
	}

	public function HarleyInventory($condition) {
		switch($condition) {
			case "New":
				return 'http://www.dillonharley.com/search-new--xNewInventory#page=xNewInventory&d=on&t=new';	
				break;
			case "Used":
				if(LIVE_SITE == true) {
					return PATH . 'inventory?q=&hPP=30&idx=MainUnitInventory&p=0&dFR%5BConditions%5D%5B0%5D=Used&dFR%5BStoreName%5D%5B0%5D=Dillon%20Brothers%20Harley-Davidson&is_v=1';
				} else {
					return PATH . 'inventory?q=&hPP=30&idx=MainUnitInventoryTest&p=0&dFR%5BStoreName%5D%5B0%5D=Dillon%20Harley&is_v=1';
				}	
			break;
		}
	}
	
	public function IndianInventory($condition) {
		switch($condition) {
			case "New":
				if(LIVE_SITE == true) {
					return PATH . 'inventory?q=&hPP=30&idx=MainUnitInventory&p=0&dFR%5BConditions%5D%5B0%5D=New&dFR%5BStoreName%5D%5B0%5D=Dillon%20Brothers%20Indian&is_v=1';
				} else {
					return PATH . 'inventory?q=&hPP=30&idx=MainUnitInventoryTest&p=0&dFR%5BConditions%5D%5B0%5D=New&dFR%5BStoreName%5D%5B0%5D=Dillon%20Indian&is_v=1';
				}	
			break;
			case "Used":
				if(LIVE_SITE == true) {
					return PATH . 'inventory?q=&hPP=30&idx=MainUnitInventory&p=0&dFR%5BConditions%5D%5B0%5D=Used&dFR%5BStoreName%5D%5B0%5D=Dillon%20Brothers%20Indian&is_v=1';
				} else {
					return PATH . 'inventory?q=&hPP=30&idx=MainUnitInventoryTest&p=0&dFR%5BConditions%5D%5B0%5D=Used&dFR%5BStoreName%5D%5B0%5D=Dillon%20Indian&is_v=1';
				}	
			break;
		}
		
	}
	
	public function states() {
		return array('AL'=>'Alabama',
					 'AK'=>'Alaska', 
					 'AR'=>'Arkansas',
					 'AZ'=>'Arizona',
					 'CA'=>'California',
					 'CO'=>'Colorado',
					 'CT'=>'Connecticut',
					 'DE'=>'Delaware', 
					 'DC'=>'District of Columbia',
					 'FL'=>'Florida', 
					 'GA'=>'Georgia',
					 'HI'=>'Hawaii',
					 'IA'=>'Iowa',  
					 'ID'=>'Idaho',
					 'IL'=>'Illinois',
					 'IN'=>'Indiana', 
					 'KS'=>'Kansas', 
					 'KY'=>'Kentucky',
					 'LA'=>'Louisiana',
					 'ME'=>'Maine', 
					 'MD'=>'Maryland', 
					 'MA'=>'Massachusetts', 
					 'MI'=>'Michigan', 
					 'MN'=>'Minnesota', 
					 'MO'=>'Missouri',
					 'MS'=>'Mississippi', 
					 'MT'=>'Montana',
					 'NE'=>'Nebraska', 
					 'NM'=>'New Mexico', 
					 'NV'=>'Nevada',
					 'NH'=>'New Hampshire', 
					 'NJ'=>'New Jersey', 
					 'NY'=>'New York', 
					 'NC'=>'North Carolina', 
					 'ND'=>'North Dakota',
					 'OH'=>'Ohio', 
					 'OK'=>'Oklahoma', 
					 'OR'=>'Oregon', 
					 'PA'=>'Pennsylvania',
					 'RI'=>'Rhode Island', 
					 'SC'=>'South Carolina',
					 'SD'=>'South Dakota',
					 'TX'=>'Texas', 
					 'TN'=>'Tennessee',
					 'UT'=>'Utah',  
					 'VT'=>'Vermont', 
					 'VA'=>'Virginia',
					 'WA'=>'Washington', 
					 'WV'=>'West Virginia',
					 'WI'=>'Wisconsin',
					 'WY'=>'Wyoming');
	}
	
	public function months() {
		return array('01' => '01 - January',
					 '02' => '02 - February',
					 '03' => '03 - March',
					 '04' => '04 - April',
					 '05' => '05 - May',
					 '06' => '06 - June',
					 '07' => '07 - July',
					 '08' => '08 - August',
					 '09' => '09 - September',
					 '10' => '10 - October',
					 '11' => '11 - November',
					 '12' => '12 - December');
	}

	public function year() {
			
		$currentYear = date("Y");
	
		return array($currentYear => $currentYear,
					 $currentYear + 1 => $currentYear + 1,
					 $currentYear + 2 => $currentYear + 2,
					 $currentYear + 3 => $currentYear + 3,
					 $currentYear + 4 => $currentYear + 4,
					 $currentYear + 5 => $currentYear + 5,
					 $currentYear + 6 => $currentYear + 6,
					 $currentYear + 7 => $currentYear + 7,
					 $currentYear + 8 => $currentYear + 8,
					 $currentYear + 9 => $currentYear + 9,
					 $currentYear + 10 => $currentYear + 10,
					 $currentYear + 11 => $currentYear + 11,
					 $currentYear + 12 => $currentYear + 12,
					 $currentYear + 13 => $currentYear + 13,
					 $currentYear + 14 => $currentYear + 14,
					 $currentYear + 15 => $currentYear + 15,
					 $currentYear + 16 => $currentYear + 16,
					 $currentYear + 17 => $currentYear + 17,
					 $currentYear + 18 => $currentYear + 18,
					 $currentYear + 19 => $currentYear + 19,
					 $currentYear + 20 => $currentYear + 20);
	}
	
	public function PhoneFormat($phone) {
		$areaCode = substr($phone, 0, 3);
		$RestOfPhone1 = substr($phone, 3, 3);
		$RestOfPhone2 = substr($phone, 6, 7); 
		return "(" .$areaCode . ") ". $RestOfPhone1 . " - " . $RestOfPhone2;
	}
	
	
	public function MotorsportStructuredData($type = NULL, $priceRange = NULL) {
		
		if($type != NULL) {
			$typeSchema = $type;
		} else {
			$typeSchema = "LocalBusiness";
		}
		
		if($priceRange != NULL) {
			$priceSchema = $priceRange;
		} else {
			$priceSchema = '00.00';
		}
		
		return '{
					"@type": "' . $typeSchema . '",
					"@context": "http://schema.org",
					"name": "Dillon Brothers MotorSports",
					"address": {
						"@type": "PostalAddress",
						"streetAddress": "3848 N HWS Cleveland Blvd",
						"addressLocality": "Omaha",
						"addressRegion": "Nebraska",
						"postalCode" : "68116",
						"telephone" : "(402) 556-3333",
						"faxNumber" : "(402) 556-1796"
					},
					"geo": {
						"@type": "GeoCircle",
						"geoMidpoint": {
							"@type": "GeoCoordinates",
							"latitude": "41.293783",
							"longitude": "-96.187869"
						},
						"geoRadius": "100"
					},
					"openingHours" : [
					  	"Monday 9:00-19:00",
					    "Tuesday 9:00-19:00",
					    "Wednesday 9:00-17:30",
					    "Thursday 9:00-19:00",
					    "Friday 9:00-17:30",
					    "Saturday 9:00-16:30"
   					],
   					"email": "sales@dillonharley.com",	
					"image" : "' . PATH . 'public/images/DillonBrothers.png",
					"url": "' . PATH . '",
					"priceRange" : "' . $priceSchema . '"
				}';
	}

	public function HarleyStructuredData($location, $type = NULL, $priceRange = NULL) {
		$data = '';
		
		if($type != NULL) {
			$typeSchema = $type;
		} else {
			$typeSchema = "LocalBusiness";
		}
		
		if($priceRange != NULL) {
			$priceSchema = $priceRange;
		} else {
			$priceSchema = '00.00';
		}
		
		switch($location) {
			case "omaha":
			
			$data = '{
						"@type": "' . $typeSchema . '",
						"@context": "http://schema.org",
						"name": "Dillon Brothers Harley-Davidson Omaha",
						"address": {
							"@type": "PostalAddress",
							"streetAddress": "3838 N HWS Cleveland Blvd",
							"addressLocality": "Omaha",
							"addressRegion": "Nebraska",
							"postalCode" : "68116",
							"telephone" : "(402) 289-5556",
							"faxNumber" : "(402) 289-1931"
						},
						"geo": {
							"@type": "GeoCircle",
							"geoMidpoint": {
								"@type": "GeoCoordinates",
								"latitude": "41.292938",
								"longitude": "-96.1883625"
							},
							"geoRadius": "100"
						},
						"openingHours": [
							"Monday 9:00-19:00",
							"Tuesday 9:00-19:00",
							"Wednesday 9:00-17:30",
							"Thursday 9:00-19:00",
							"Friday 9:00-17:30",
							"Saturday 9:00-16:30"
					  	],
	   					"email": "sales@dillonharley.com",	
						"image" : "' . PATH . 'public/images/DillonBrothers.png",
						"url": "' . PATH . '",
						"priceRange" : "' . $priceSchema . '"
					}';
			
				break;
			case "fremont":
				$data = '{
						"@type": "' . $typeSchema . '",
						"@context": "http://schema.org",
						"name": "Dillon Brothers Harley-Davidson Fremont",
						"address": {
							"@type": "PostalAddress",
							"streetAddress": "2440 East 23rd St",
							"addressLocality": "Fremont",
							"addressRegion": "Nebraska",
							"postalCode" : "68025",
							"telephone" : "(402) 721-2007",
							"faxNumber" : "(402) 721-2441"
						},
						"geo": {
							"@type": "GeoCircle",
							"geoMidpoint": {
								"@type": "GeoCoordinates",
								"latitude": "41.4519858",
								"longitude": "-96.4654377"
							},
							"geoRadius": "100"
						},
						"openingHours": [
							"Tuesday 9:00-18:00",
							"Wednesday 9:00-17:30",
							"Thursday 9:00-18:00",
							"Friday 9:00-17:30",
							"Saturday 9:00-16:30"
					  	],
	   					"email": "sales@dillonharley.com",	
						"image" : "' . PATH . 'public/images/DillonBrothers.png",
						"url": "' . PATH . '",
						"priceRange" : "' . $priceSchema . '"
					}';
			
				break;
		}	

		return $data;
	}

	public function IndianStructuredData($type = NULL, $priceRange = NULL) {
		if($type != NULL) {
			$typeSchema = $type;
		} else {
			$typeSchema = "LocalBusiness";
		}
		
		if($priceRange != NULL) {
			$priceSchema = $priceRange;
		} else {
			$priceSchema = '00.00';
		}
		
		return '{
					"@type": "' . $typeSchema . '",
					"@context": "http://schema.org",
					"name": "Dillon Brothers MotorSports",
					"address": {
						"@type": "PostalAddress",
						"streetAddress": "3840 N 174th Ave.",
						"addressLocality": "Omaha",
						"addressRegion": "Nebraska",
						"postalCode" : "68116",
						"telephone" : "(402) 505-4233"
					},
					"geo": {
						"@type": "GeoCircle",
						"geoMidpoint": {
							"@type": "GeoCoordinates",
							"latitude": "41.29352",
							"longitude": "-96.1892845"
						},
						"geoRadius": "100"
					},
					"openingHours" : [
					  	"Monday 9:00-19:00",
					    "Tuesday 9:00-19:00",
					    "Wednesday 9:00-17:30",
					    "Thursday 9:00-19:00",
					    "Friday 9:00-17:30",
					    "Saturday 9:00-16:30"
				   ],
   					"email": "sales@dillonharley.com",	
					"image" : "' . PATH . 'public/images/DillonBrothers.png",
					"url": "' . PATH . '",
					"priceRange" : "' . $priceSchema . '"
				}';
	}
	
	
}