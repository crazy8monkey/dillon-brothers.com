<?php

Class Pagination {
	private $mysqlCount;
	private $limit;
	private $targetpage;
	
	public $pagination;
	
	public $MobilePagination;
	
	public function __construct() {}
	
	
	
	public function paginationLinksAjax($setCount, $setLimit, $setTargetPage, $pageVariable, $status = false) {		
		$adjacents = 2;
		$page = isset($pageVariable) ? max((int)$pageVariable, 1) : 1;
		
		$statusString = "";
		$paginationLink = "";
		$this -> targetpage = $setTargetPage;

		if($status) {
			$statusString = "&status=" . $status;	
		}

		if ($page == 0)
			$page = 1;
		//previous page is page - 1
		$prev = $page - 1;
		$leftArrow = "<i class='fa fa-caret-left' aria-hidden='true'></i>";
		$rightArrow = "<i class='fa fa-caret-right' aria-hidden='true'></i>";
		//next page is page + 1
		$next = $page + 1;
		$lastpage = ceil($setCount / $setLimit);
		$this->pagination = "";
		if ($lastpage > 1) {
			$this->pagination .= "<div class='paginationObject'>";
			//previous button
			if ($page > 1) {
				$this->pagination .= $this -> TogglePaginationLinksAjax($setTargetPage, $leftArrow, $prev, true, $status, "pagination-left");
			} else {
				$this->pagination .= $this -> TogglePaginationLinksAjax($setTargetPage, $leftArrow, $prev, false, $status, "pagination-left");
			}
			//pages
			if ($lastpage < 7 + ($adjacents * 2))//not enough pages to bother breaking it up
			{
				for ($counter = 1; $counter <= $lastpage; $counter++) {
					if ($counter == $page) {
						$this->pagination .= $this -> paginationLinkAjax($setTargetPage, $counter, false, false, true);
					} else {
						$this->pagination .= $this -> paginationLinkAjax($setTargetPage, $counter, $status, true);						
					}
						
				}
			} elseif ($lastpage > 5 + ($adjacents * 2))//enough pages to hide some
			{
				//close to beginning; only hide later pages
				if ($page < 1 + ($adjacents * 2)) {
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
						if ($counter == $page) {
							$this->pagination .= $this -> paginationLinkAjax($setTargetPage, $counter, false, false, true);
						} else {
							$this->pagination .= $this -> paginationLinkAjax($setTargetPage, $counter, $status, true);
						}
							
					}
					
					$this->pagination .= $this -> paginationLinkAjax($setTargetPage, $counter, $status, true);					
					$this->pagination .= $this -> paginationLinkAjax($setTargetPage, "...", false, false);
					$this->pagination .= $this -> paginationLinkAjax($setTargetPage, $lastpage, $status, true);
					
				}
				//in middle; hide some front and some back
				elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
					$this->pagination .= $this -> paginationLinkAjax($setTargetPage, 1, $status, true);	
					$this->pagination .= $this -> paginationLinkAjax($setTargetPage, 2, $status, true);
					$this->pagination .= $this -> paginationLinkAjax($setTargetPage, "...", false, false);
					
					//$this->pagination .= "...";
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
						if ($counter == $page) {
							$this->pagination .= $this -> paginationLinkAjax($setTargetPage, $counter, false, false, true);	
						} else {
							$this->pagination .= $this -> paginationLinkAjax($setTargetPage, $counter, $status, true);
						}
							
					}
					$this->pagination .= $this -> paginationLinkAjax($setTargetPage, "...", false, false);
					$this->pagination .= $this -> paginationLinkAjax($setTargetPage, $counter, $status, true);
					$this->pagination .= $this -> paginationLinkAjax($setTargetPage, $lastpage, $status, true);					
				}
				//close to end; only hide early pages
				else {
					$this->pagination .= $this -> paginationLinkAjax($setTargetPage, 1, $status, true);
					$this->pagination .= $this -> paginationLinkAjax($setTargetPage, 2, $status, true);
					$this->pagination .= $this -> paginationLinkAjax($setTargetPage, "...", false, false);

					for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
						if ($counter == $page) {
							$this->pagination .= $this -> paginationLinkAjax($setTargetPage, $counter, false, false, true);	
						} else {
							$this->pagination .= $this -> paginationLinkAjax($setTargetPage, $counter, $status, true);
						}
							
					}
				}
			}
			//next button
			if ($page < $counter - 1) {
				$this->pagination .= $this -> TogglePaginationLinksAjax($setTargetPage, $rightArrow, $next, true, $status, "pagination-right");
			} else {
				$this->pagination .= $this -> TogglePaginationLinksAjax($setTargetPage, $rightArrow, $next, false, $status, "pagination-right");
			}
			
			$this->pagination .= "<div style='clear:both'></div></div>";
		}

		return $this->pagination;
	    
	}


	public function paginationLinks($setCount, $setLimit, $setTargetPage, $pageVariable, $status = false) {		
		$adjacents = 2;
		$page = isset($pageVariable) ? max((int)$pageVariable, 1) : 1;
		
		$statusString = "";
		$paginationLink = "";

		if($status) {
			$statusString = "&status=" . $status;	
		}

		if ($page == 0)
			$page = 1;
		//previous page is page - 1
		$prev = $page - 1;
		$leftArrow = "<i class='fa fa-caret-left' aria-hidden='true'></i>";
		$rightArrow = "<i class='fa fa-caret-right' aria-hidden='true'></i>";
		//next page is page + 1
		$next = $page + 1;
		$lastpage = ceil($setCount / $setLimit);
		$this->pagination = "";
		if ($lastpage > 1) {
			$this->pagination .= "<div class='paginationObject'>";
			//previous button
			if ($page > 1) {
				$this->pagination .= $this -> TogglePaginationLinks($setTargetPage, $leftArrow, $prev, true, $status, "pagination-left");
			} else {
				$this->pagination .= $this -> TogglePaginationLinks($setTargetPage, $leftArrow, $prev, false, $status, "pagination-left");
			}
			//pages
			if ($lastpage < 7 + ($adjacents * 2))//not enough pages to bother breaking it up
			{
				for ($counter = 1; $counter <= $lastpage; $counter++) {
					if ($counter == $page) {
						$this->pagination .= $this -> paginationLink($setTargetPage, $counter, false, false, true);
					} else {
						$this->pagination .= $this -> paginationLink($setTargetPage, $counter, $status, true);						
					}
						
				}
			} elseif ($lastpage > 5 + ($adjacents * 2))//enough pages to hide some
			{
				//close to beginning; only hide later pages
				if ($page < 1 + ($adjacents * 2)) {
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
						if ($counter == $page) {
							$this->pagination .= $this -> paginationLink($setTargetPage, $counter, false, false, true);
						} else {
							$this->pagination .= $this -> paginationLink($setTargetPage, $counter, $status, true);
						}
							
					}
					
					$this->pagination .= $this -> paginationLink($setTargetPage, $counter, $status, true);					
					$this->pagination .= $this -> paginationLink($setTargetPage, "...", false, false);
					$this->pagination .= $this -> paginationLink($setTargetPage, $lastpage, $status, true);
					
				}
				//in middle; hide some front and some back
				elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
					$this->pagination .= $this -> paginationLink($setTargetPage, 1, $status, true);	
					$this->pagination .= $this -> paginationLink($setTargetPage, 2, $status, true);
					$this->pagination .= $this -> paginationLink($setTargetPage, "...", false, false);
					
					//$this->pagination .= "...";
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
						if ($counter == $page) {
							$this->pagination .= $this -> paginationLink($setTargetPage, $counter, false, false, true);	
						} else {
							$this->pagination .= $this -> paginationLink($setTargetPage, $counter, $status, true);
						}
							
					}
					$this->pagination .= $this -> paginationLink($setTargetPage, "...", false, false);
					$this->pagination .= $this -> paginationLink($setTargetPage, $counter, $status, true);
					$this->pagination .= $this -> paginationLink($setTargetPage, $lastpage, $status, true);					
				}
				//close to end; only hide early pages
				else {
					$this->pagination .= $this -> paginationLink($setTargetPage, 1, $status, true);
					$this->pagination .= $this -> paginationLink($setTargetPage, 2, $status, true);
					$this->pagination .= $this -> paginationLink($setTargetPage, "...", false, false);

					for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
						if ($counter == $page) {
							$this->pagination .= $this -> paginationLink($setTargetPage, $counter, false, false, true);	
						} else {
							$this->pagination .= $this -> paginationLink($setTargetPage, $counter, $status, true);
						}
							
					}
				}
			}
			//next button
			if ($page < $counter - 1) {
				$this->pagination .= $this -> TogglePaginationLinks($setTargetPage, $rightArrow, $next, true, $status, "pagination-right");
			} else {
				$this->pagination .= $this -> TogglePaginationLinks($setTargetPage, $rightArrow, $next, false, $status, "pagination-right");
			}
			
			$this->pagination .= "<div style='clear:both'></div></div>";
		}

		return $this->pagination;
	    
	}

	
	//ajax
	private function TogglePaginationLinksAjax($targetPage, $leftRightArrow, $leftRightCounter, $isLink = false, $status = false, $className) {
		
		$leftRight = "";
		
		if($isLink == true) {
			$leftRight .= "<a href='javascript:void(0);' onclick='". $targetPage . "(" . $leftRightCounter . ")'><div class='" . $className . "'>". $leftRightArrow . "</div></a>";
		} else {
			$leftRight .= "<div class='". $className . " linkInactive'>". $leftRightArrow . "</div>";
		}
		return $leftRight;
	}
	private function paginationLinkAjax($targetPage, $counter, $status = false, $isLink = false, $selected = false) {
		$paginationLink = "";
		
		$currentUrl = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		
		$urlArray = explode('/', $currentUrl);
		$selectedOptionString = '';
		if(end($urlArray) == $counter) {
			$selectedOptionString .= ' selected ';	
		}
		
		$this -> MobilePagination .='<option' . $selectedOptionString. ' value="' . $counter .'">Page ' . $counter .'</option>';
		if($isLink == true) {
			$paginationLink .= "<a href='javascript:void(0);' onclick='" .$targetPage. "(" . $counter .")'><div class='pagination-number'>". $counter. "</div></a>";	
		} else {
			if($selected == true) {
				$paginationLink .= "<div class='pagination-number pagination-number-selected'>" . $counter ."</div>";	
			} else {
				$paginationLink .= "<div class='pagination-number pagination-number-ellipse'>" . $counter ."</div>";
			}
			
		}
		
		
		return $paginationLink;
	}
	
	//normal
	private function TogglePaginationLinks($targetPage, $leftRightArrow, $leftRightCounter, $isLink = false, $status = false, $className) {
		$leftRight = "";
		
		if($isLink == true) {
			$url = $targetPage . "/" . $leftRightCounter;
			
			$leftRight .= "<a href='" . $url. "'><div class='" . $className . "'>". $leftRightArrow . "</div></a>";
		} else {
			$leftRight .= "<div class='". $className . " linkInactive'>". $leftRightArrow . "</div>";
		}
		return $leftRight;
	}
	private function paginationLink($targetPage, $counter, $status = false, $isLink = false, $selected = false) {
		$paginationLink = "";
		$currentUrl = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		
		$urlArray = explode('/', $currentUrl);
		$selectedOptionString = '';
		if(end($urlArray) == $counter) {
			$selectedOptionString .= ' selected ';	
		}
		
		$url = 	$targetPage  . "/" . $counter;
		
		$this -> MobilePagination .='<option' . $selectedOptionString. ' value="' . $url .'">Page ' . $counter .'</option>';
		if($isLink == true) {	
			$paginationLink .= "<a href='" . $url  . "'><div class='pagination-number'>". $counter. "</div></a>";	
		} else {
			if($selected == true) {
				$paginationLink .= "<div class='pagination-number pagination-number-selected'>" . $counter ."</div>";	
			} else {
				$paginationLink .= "<div class='pagination-number pagination-number-ellipse'>" . $counter ."</div>";
			}
			
		}
		
		
		return $paginationLink;
	}
	
	public function MobilePagination() {
		return "<div class='mobilePagination'><select onchange='if (this.value) window.location.href=this.value'>" . $this -> MobilePagination . "</select></div>";
	}

	public function MobilePaginationAjax() {
		return "<div class='mobilePagination'><select onchange='" . $this -> targetpage . "(this.value)'>" . $this -> MobilePagination . "</select></div>";
	}
	
	public function getPaginationLinks() {
		return $this-> pagination;	
	}
}