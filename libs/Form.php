<?php
Class Form {
	
	
	
	public function ValiationMessage($idName, $loadingID, $messagesID) {
		return "<div class='flashMesssages' id='" .$idName ."'>
					<div class='loadingIndicator' id='" . $loadingID . "'><img src='" . PATH . "public/images/ajax-loader.gif' /></div>
					<div class='messagesText' id='" . $messagesID . "'></div>
				</div>";
				
				
	}
	
	public function Input($type, $label, $name, $value = NULL, $onchange = NULL) {
		$valueString = NULL;	
		$onChangeString = NULL;
		if(isset($value)) {
			$valueString = "value='" . $value. "'";
		}	
			
		if(isset($onchange)) {
			$onChangeString = "onkeyup='". $onchange . "'";
		}	
			
		return "<div class='input'>
					<div class='inputLabel'>". $label . "</div>
					<input id='". $name. "' type='". $type. "' name='". $name. "'" . $valueString . $onChangeString. " />
				</div>";
			
	}
	
	public function Submit($value, $className) {
		return "<div class='input'>
					<input type='submit' value='" . $value. "' class='". $className . "' />
				</div>";
	}

}
?>