<?php

//http://www.pontikis.net/tip/?id=18

class Time {
	
	private $nebraskTime;
	
	public function __construct() {
		date_default_timezone_set('UTC');		
	}
	
	public function NebraskaTime() {
		$this -> nebraskTime = new DateTime("now", new DateTimeZone('America/Chicago'));		
		return $this -> nebraskTime -> getTimestamp() + $this -> nebraskTime -> getOffset();
	}
	
	
	public function StartOfWeek() {return $this -> formateDateByDigits(strtotime( "monday this week". "America/Chicago" ));}	
	public function EndOfWeek() {return $this -> formateDateByDigits(strtotime( "saturday this week". "America/Chicago"));}
	
	public function StartOfWeekSQL() {return $this -> formateDateByMYSQLDate(strtotime( "monday this week". "America/Chicago" ));}	
	public function EndOfWeekSQL() {return $this -> formateDateByMYSQLDate(strtotime( "saturday this week". "America/Chicago" ));}	
	
	
	public function StartOfYearSQL() {return $this -> formateDateByMYSQLDate(strtotime('first day of January '.date('Y') ));}	
	public function EndOfYearSQL() {return $this -> formateDateByMYSQLDate(strtotime( 'last day of December '.date('Y') ));}

	
	public function formatDate($value) {
		$date = DateTime::createFromFormat('Y-m-d', $value);
		$formatedDate = $date->format('F j, Y');
		return $formatedDate;
	}
	
	public function formatShortDate($value) {
		$date = DateTime::createFromFormat('Y-m-d', $value);
		$formatedDate = $date->format('M j, Y');
		return $formatedDate;
	}
	
	public function ShortDateNoYear($value) {
		$date = DateTime::createFromFormat('Y-m-d', $value);
		$formatedDate = $date->format('M j');
		return $formatedDate;
	}
	
	
	public function SaveUTCDayStamp() {
		$date = DateTime::createFromFormat('Y-m-d', $value);
		$date -> setTimezone(new DateTimeZone('America/Chicago'));
		$formatedDate = $date->format('Y-m-d');
		return $formatedDate;
	}
	
	
	public function SaveUTCTimeStamp() {
		$date = DateTime::createFromFormat('H:i:s', $value);
		$date -> setTimezone(new DateTimeZone('America/Chicago'));
		$formatedDate = $date->format('g:i a');
		return $formatedDate;
	}
	
	public function FormatExpectedDate($value) {
		$timeFormated = explode('/',$value);
		$yyyy_mm_dd = $timeFormated[2] . '-' . $timeFormated[0] . '-' . $timeFormated[1];
		return $yyyy_mm_dd;
	}
	
	
	public function formatTime($value) {
		$date = DateTime::createFromFormat('H:i', $value);
		$formatedDate = $date->format('g:i a');
		return $formatedDate;	
	}
	
	public function formatMinuteSeconts($value) {
		$date = DateTime::createFromFormat('H:i:s', $value);
		$formatedDate = $date->format('g:i a');
		return $formatedDate;	
	}
	
	public function formateDateByDigits($time) {
		return date("m/d/y", $time);
	}
	
	public function formateDateByMYSQLDate($time) {
		return date("Y-m-d", $time);
	}
	
	public function getCurrentWeek() {
		return $this -> StartOfWeek() . " - " . $this -> EndOfWeek();
	}
	
	
}

?>