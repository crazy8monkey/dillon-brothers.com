<?php

Class Email {
			
	public $to;
	public $subject;
	private $msg;
	private $header;
	public $Bcc;
	
	public function __construct() {
		$this -> header = 'From: Dillon Brothers <DillonBrothers@donotreply.com>'. "\r\n";
		$this -> header .= 'MIME-Version: 1.0' . "\r\n";
		$this -> header .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
		if(isset($this -> Bcc)) {
			$this -> header .= 'Bcc: ' . $this -> Bcc . ', netadmin@siddillon.com, adam.schmidt@siddillon.com' . "\r\n";	
		} else {
			$this -> header .= 'Bcc: netadmin@siddillon.com, adam.schmidt@siddillon.com' . "\r\n";
		}
			
	}
	
	private function emailLogo() {
		return "<img src='" . PATH . "public/images/DillonBrothers.png' />";
	}

	
	public function SendToFriend($content) {
      switch( $content['store-id']) {
			case 2:
				$background = "c30000";
				break;
			case 3:
				$background = "ff6c00";
				break;
			case 4:
				$background = "850029";
				break;
		}
     
      
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f6f7fb; margin:0px; padding:0px 0px 30px 0px'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='572' align='center'>
									<tr><td style='font-size:10px; font-family:arial; padding:10px 0px; text-align:center; background:#f6f7fb'><img src=" . PHOTO_URL . 'Stores/'. $content['company-logo'] . " width='180' /></td></tr>
									<tr>
										<td style='padding:20px 0px; background:#" . $background . ";'>
											<table cellpadding='0' cellspacing='0' width='100%'>
												<tr>
													<td style='font-size: 20px; color:white; text-align:center; font-family: arial;'>" . $content['referred-person'] . " has recommended you<br />a vehicle at <strong>" . $content['store-name'] . "</strong></td>	
												</tr>";
												if(!empty($content['personal-message'])) {
													$this -> msg .= "<tr><td style='font-size:16px; color:white; font-family: arial; padding:10px 15px 0px 15px;'>" . $content['personal-message'] . "</td></tr>";
												}
													
												
											$this -> msg .= "</table>
										</td>
									</tr>
									<tr><td style='color:#656565; text-align:center; padding:20px 0px; font-family: arial; font-size: 20px; font-weight:bold'>Check out this " . $content['vehicle-name'] . "</td></tr>
									<tr><td style='color:#1b9000; text-align:center; padding:0px 0px 20px 0px; font-family: arial; font-size: 29px; font-weight:bold'>" . $content['MSRP'] . "</td></tr>
									<tr>
										<td style='text-align:center;'>
											<img src='" . $content['MainImage'] . "' width='370' style='border:1px solid #cacaca' />
										</td>
									</tr>
									<tr>
										<td style='padding:20px 0px' width='300'>
											<a href='" . $content['bikeURL'] . "'>
												<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='300' align='center'>
													<tr>
														<td style='text-align:center; font-family: arial; font-size: 16px; border:1px solid #006abb; padding:13px; border-radius:50px;'>
															View Vehicle
														</td>
													</tr>
												</table>
											</a>
										</td>
									</tr>
									<tr>
										<td style='border-top:1px solid #f4f4f4'>
											<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%' align='center'>
												<tr>
													<td style='color:#656565; font-weight:bold; font-family:arial; font-size:20px; padding:10px;'>
														Vehicle Information
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td style='padding:10px;'>
											<table cellpadding='0' cellspacing='0' width='100%'>
												<tr>
													<td valign='top' style='padding:0px 10px 0px 0px'>
														<img src='" . PATH . "public/images/emailVehicleIndicator.png' height='30' />
													</td>
													<td valign='top'>
														<table cellpadding='0' cellspacing='0'>
															<tr>
																<td style='padding:0px 10px 0px 0px; font-weight:bold; font-family:arial; font-size:12px;'>Model #:</td>
																<td style='font-family:arial; font-size:12px;'>" . $content['ModelNumber'] . "</td>
															</tr>
															<tr>
																<td style='padding:0px 10px 0px 0px; font-weight:bold; font-family:arial; font-size:12px;'>Category:</td>
																<td style='font-family:arial; font-size:12px;'>" . $content['category'] . "</td>
															</tr>
															<tr>
																<td style='padding:0px 10px 0px 0px; font-weight:bold; font-family:arial; font-size:12px;'>VIN:</td>
																<td style='font-family:arial; font-size:12px;'>" . $content['VIN'] . "</td>
															</tr>
															<tr>
																<td style='padding:0px 10px 0px 0px; font-weight:bold; font-family:arial; font-size:12px;'>Mileage:</td>
																<td style='font-family:arial; font-size:12px;'>" . $content['Mileage'] . "</td>
															</tr>
															<tr>
																<td style='padding:0px 10px 0px 0px; font-weight:bold; font-family:arial; font-size:12px;'>Stock #:</td>
																<td style='font-family:arial; font-size:12px;'>" . $content['Stock'] . "</td>
															</tr>
															<tr>
																<td style='padding:0px 10px 0px 0px; font-weight:bold; font-family:arial; font-size:12px;'>Color:</td>
																<td style='font-family:arial; font-size:12px;'>" . $content['Color'] . "</td>
															</tr>
														</table>								
													</td>
													<td valign='top' align='right'>
														<table cellpadding='0' cellspacing='0'>
															<tr>
																<td style='padding:0px 10px 10px 0px'><img src='" . PATH . "public/images/LocationIcon.png' height='20' /></td>
																<td style='font-family:arial; padding:0px 0px 10px 0px; font-size:12px;'>" . $content['StoreLocation'] . "</td>		
															</tr>
															<tr>
																<td style='padding:0px'><img src='" . PATH . "public/images/LocationContact.png' height='20' /></td>
																<td style='font-family:arial; padding:0px; font-size:12px;'>" . $content['StoreContactInfo'] . "</td>		
															</tr>
														</table>
													</td>
												</tr>
											</table>
						
										</td>
									</tr>
									<tr>
										<td style='font-size:10px; font-family:arial; padding:10px 0px; text-align:center; background:#f6f7fb'>" .date('Y') . " &copy; Dillon Brothers. All Rights Reserved</td>
									</tr>
								</table>
							</body>
						</html>";
		$this -> SendEmail();
	}
	
	public function Lead($type, $content = array()) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f6f7fb; margin:0px; padding:0px 0px 30px 0px'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='572' align='center'>
									<tr><td style='font-size:10px;  padding:10px 0px; text-align:center; background:#f6f7fb'><img src=" . $content['logo'] . " width='180' /></td></tr>";
									
				
			
		
		switch($type) {
			case "onlineoffer":
			$this -> msg .= "<tr><td style='padding:15px 0px 15px 10px; font-size:20px; font-family:arial;'><strong>Online Offer Lead</strong></td></tr><tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px; width:130px'><strong>Full Name:</strong> </td>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 5px 0px'>" . $content['fullname'] . "</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px; width:130px'><strong>Email:</strong> </td>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 5px 0px'>" . $content['useremail'] . "</td>
										</tr>
									</table>
								</td>
							</tr><tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px'><strong>Comments:</strong> </td>
										</tr>
										<tr>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 10px 10px'>" . $content['comments'] . "</td>
										</tr>
									</table>
								</td>
							</tr>";
				break;
			case "tradeinvalue":
					$this -> msg .= "<tr><td style='padding:15px 0px 15px 10px; font-size:20px; font-family:arial;'><strong>Trade In Value Lead</strong></td></tr><tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px; width:130px'><strong>Full Name:</strong> </td>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 5px 0px'>" . $content['fullname'] . "</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px; width:130px'><strong>Contact Info:</strong> </td>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 5px 0px'>" . $content['contactInfo'] . "</td>
										</tr>
									</table>
								</td>
							</tr><tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px; width:130px'><strong>Trade In Vehicle:</strong> </td>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 5px 0px'>" . $content['TradeInVehicle'] . "</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px'><strong>Added Accessories:</strong> </td>
										</tr>
										<tr>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 10px 10px'>" . $content['AddedAccessoriesText'] . "</td>
										</tr>
									</table>
								</td>
							</tr><tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px'><strong>Trade In Comments:</strong> </td>
										</tr>
										<tr>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 10px 10px'>" . $content['TradeInComments'] . "</td>
										</tr>
									</table>
								</td>
							</tr>";
				break;
			case "testride":
				$this -> msg .= "<tr><td style='padding:15px 0px 15px 10px; font-size:20px; font-family:arial;'><strong>Test Ride Lead</strong></td></tr><tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px; width:130px'><strong>Full Name:</strong> </td>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 5px 0px'>" . $content['fullname'] . "</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px; width:130px'><strong>Contact Info:</strong></td>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 5px 0px'>" . $content['contactInfo'] . "</td>
										</tr>
									</table>
								</td>
							</tr><tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px; width:130px'><strong>Date Of Ride:</strong> </td>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 10px 0px'>" . $content['date-of-ride'] . "</td>
										</tr>
									</table>
								</td>
							</tr>";
				break;	
				
				
				case "contactus":
					$this -> msg .= "<tr><td style='padding:15px 0px 15px 10px; font-size:20px; font-family:arial;'><strong>Contact Us Lead</strong></td></tr><tr><tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px; width:130px'><strong>Contact Info:</strong> </td>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 5px 0px'>" . $content['contactInfo'] . "</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px'><strong>Comments / Concerns:</strong> </td>
										</tr>
										<tr>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 10px 10px'>" . $content['CommentsConcerns'] . "</td>
										</tr>
									</table>
								</td>
							</tr>";
				
				
					break;
		}
		
		$this -> msg .= "<tr><td style='padding:15px 0px 15px 10px; font-size:20px; border-top:1px solid #e0e0e0;font-family:arial;'><strong>Vehicle Information</strong></td></tr><tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px; width:130px'><strong>Vehicle Name:</strong> </td>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 5px 0px'>" . $content['VehicleName'] . "</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px; width:130px'><strong>Category:</strong> </td>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 5px 0px'>" . $content['category'] . "</td>
										</tr>
									</table>
								</td>
							</tr><tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px; width:130px'><strong>MSRP:</strong> </td>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 5px 0px'>" . $content['MSRP'] . "</td>
										</tr>
									</table>
								</td>
							</tr><tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px; width:130px'><strong>VIN:</strong> </td>
										<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 5px 0px'>" . $content['VIN'] . "</td>
										</tr>
									</table>
								</td>
							</tr><tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px; width:130px'><strong>Color:</strong> </td>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 5px 0px'>" . $content['Color'] . "</td>
										</tr>
									</table>
								</td>
							</tr><tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px; width:130px'><strong>Stock:</strong> </td>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 5px 0px'>" . $content['Stock'] . "</td>
										</tr>
									</table>
								</td>
							</tr><tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px; width:130px'><strong>Mileage:</strong> </td>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 5px 0px'>" . $content['Mileage'] . "</td>
										</tr>
									</table>
								</td>
							</tr><tr>
								<td style='padding:0px 0px 5px 0px'>
									<table cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='color:#8e8e8e; font-size:16px; font-family:arial; padding:0px 0px 5px 10px; width:130px;' valign='top'><strong>Address:</strong> </td>
											<td style='color:black; font-size:16px; font-family:arial; padding:0px 0px 5px 0px' valign='top'>" . $content['Address'] . "</td>
										</tr>
									</table>
								</td>
							</tr>";
		
		
		
	
		
		$this -> msg .= "</table>
							</body>
						</html>";
		
		$this -> SendEmail();
	}
	
	public function EcommerceCreateCredentials($url) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#white; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='400' align='center'>
									<tr>
										<td style='padding: 0px 0px 20px 0px; text-align:center'>" . $this -> emailLogo() . "</td>
									</tr>
									<tr>
										<td style='padding:10px 0px 20px 0px; font-family:arial; font-size:16px;'>
											Thank you for creating account <br />
											with our ecommerce store!
										</td>
									</tr>
									<tr>
										<td style='font-family:arial; color:white; background: #1e5fa8; text-align:center; padding:10px;'>
											<a href='"  .$url . "' style='text-decoration:underline; color:white;'>Click here to create your credentials</a>
										</td>
									</tr>
								</table>
							</body>
						</html>";
		$this -> SendEmail();					
	}
	
	
	public function EcommerceReceipt($items = array(), $address = array(), $cartTotal, $shipping, $taxes, $grandTotal) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#white; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr>
										<td style='padding: 0px 0px 20px 0px; text-align:center;'>" . $this -> emailLogo() . "</td>
									</tr>
									<tr>
										<td style='padding:10px 0px 20px 0px; font-family:arial; font-size:16px;'>
											Thank you for your purchase!
										</td>
									</tr>
									<tr>
										<td style='color:#c8c8c8; font-family:arial; font-size:24px; padding:0px 0px 10px 0px'>Your Shipping Option</td>
									</tr>
									<tr>
										<td>
											<table cellpadding='0' cellspacing='0' width='500' style='border: 1px solid #f3f3f3; padding: 10px;'>
												<tr>";
												if(count($address) > 0) {
													$this -> msg .= "<td style='vertical-align: top; font-family:arial;'><strong>Ship to address</strong></td>
																	<td style='vertical-align: top; font-family:arial;'>" . $address['street'] . "<br />" . $address['city'] . ", " . $address['state'] . " " . $address['zip']. "</td>";	
												} else {
													$this -> msg .= "<td style='vertical-align: top; font-family:arial;'><strong>Pick it up at the store</strong></td>";	
												}
								$this -> msg .= "</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td style='color:#c8c8c8; font-family:arial; font-size:24px; padding:15px 0px 10px 0px'>Your Cart</td>
									</tr>
									<tr>
										<td>
											<table cellpadding='0' cellspacing='0' width='500'>";
											
												foreach($items as $itemSingle) {
													$purchaseDetails = '';	
													
													if($itemSingle['product-size'] != 0) {
														$purchaseDetails .= ' | ' . $itemSingle['product-size'];
													}
													
													if($itemSingle['product-color'] != 0) {
														$purchaseDetails .= ' | ' . $itemSingle['product-color'];
													}
													
													$this -> msg .= "<tr>
																		<td style='border-bottom: 1px solid #cacaca; padding:10px 0px 10px 0px'>
																			<div style='font-family:arial; font-weight:bold; font-size:16px;'>" .$itemSingle['product-name'] . "</div>
																			<div style='font-family:arial; margin:5px 0px 0px 0px'><span style='color:#1b9000'>" . $itemSingle['product-price'] . "</span> | " . $itemSingle['product-qty'] . " Qty" . $purchaseDetails . "</div>
																		</td>
																		<td style='font-family:arial; text-align:right; font-size:24px; font-weight:bold; color:#1b9000; border-bottom: 1px solid #cacaca; padding:10px 0px 10px 0px'>$"
																			. ($itemSingle['product-price'] * $itemSingle['product-qty']) .
																		"</td>
																	</tr>";
												}
							$this -> msg .= "</table>	
										</td>
									</tr>
									<tr>
										<td style='padding:10px 0px 10px 0px' align='right'>
											<table>
												<tr>
													<td style='color:#c8c8c8; font-family:arial; vertical-align:bottom; padding:0px 0px 5px 0px'>Cart Total:</td>
													<td style='text-align:right; font-size:20px; font-family:arial; padding:0px 0px 5px 10px'>$" . $cartTotal. "</td>
												</tr>
												<tr>
													<td style='color:#c8c8c8; font-family:arial; vertical-align:bottom; padding:0px 0px 5px 0px'>Shipping:</td>
													<td style='text-align:right; font-size:20px; font-family:arial; padding:0px 0px 5px 10px'>$" . $shipping . "</td>
												</tr>
												<tr>
													<td style='color:#c8c8c8; font-family:arial; vertical-align:bottom; padding:0px 0px 5px 0px'>Taxes:</td>
													<td style='text-align:right; font-size:20px; font-family:arial; padding:0px 0px 5px 10px'>$" . $taxes. "</td>
												</tr>
												<tr>
													<td style='color:#c8c8c8; font-family:arial; vertical-align:bottom; padding:0px 0px 0px 0px'>Grand Total:</td>
													<td style='text-align:right; font-size:20px; font-family:arial; padding:0px 0px 0px 10px'>$" . $grandTotal. "</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td style='font-family:arial; border-top: 1px solid #cacaca;text-align:center; padding:10px; font-size:12px;'>Copyright &copy; " . date('Y') . " Dillon Brothers. All Rights Reserved.</td>
									</tr>
								</table>
							</body>
						</html>";
						
		$this -> SendEmail();					
	}
	
	public function ServiceEmail($content = array()) {
					
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f6f7fb; margin:0px; padding:0px 0px 30px 0px'>	
								<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='572' align='center'>
									<tr>
										<td style='font-size:10px; font-family:arial; padding:10px 0px; text-align:center; background:#f6f7fb'>" . $this -> emailLogo() . "</td>
									</tr>
									<tr>
										<td style='color:#656565; padding:10px; font-family: arial; font-size: 20px; font-weight:bold'>
											New Service Department Request
										</td>
									</tr>
									<tr>
										<td>
											<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='572' align='center'>
												<tr>
													<td style='padding:10px; font-size:16px; font-family:arial; '>
														<div><strong>Store:</strong></div>" . $content['StoreName'] . "</td>
												</tr>	
												<tr>
													<td style='border-top:1px solid #f4f4f4;'>
														<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%' align='center'>
															<tr>
																<td style='color:#656565; font-weight:bold; font-family:arial; font-size:20px; padding:10px;'>
																	Contact Information
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='padding:10px;'>
														<table cellpadding='0' cellspacing='0' width='572' align='center'>
															<tr>
																<td width='50%' valign='top' style='font-size:16px; font-family:arial;'>
																	<div>" . $content['FullName'] . "</div>
																	<table cellpadding='0' cellspacing='0'>
																		<tr>
																			<td style='padding:0px 10px 0px 0px; font-weight:bold; font-family:arial; font-size:16px;'>Home Phone:</td>
																			<td style='font-family:arial; font-size:16px;'>" . $content['HomePhone'] . "</td>
																		</tr>
																		<tr>
																			<td style='padding:0px 10px 0px 0px; font-weight:bold; font-family:arial; font-size:16px;'>Cell Phone:</td>
																			<td style='font-family:arial; font-size:16px;'>" . $content['CellPhone'] . "</td>
																		</tr>
																	</table>
																</td>
																<td width='50%' valign='top' style='font-size:16px; font-family:arial;'>
																	<div><strong>Address:</strong></div>
																	<div>" . $content['Address'] . "</div>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='border-top:1px solid #f4f4f4;'>
														<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%' align='center'>
															<tr>
																<td style='color:#656565; font-weight:bold; font-family:arial; font-size:20px; padding:10px;'>
																	Vehicle Information
																</td>
															</tr>
														</table>
													</td>
												</tr>	
												<tr>
													<td style='padding:10px;'>
														<table cellpadding='0' cellspacing='0' width='572' align='center'>
															<tr>
																<td width='50%' valign='top' style='font-size:16px; font-family:arial;'>
																	<table cellpadding='0' cellspacing='0' width='100%'>
																		<tr>
																			<td style='padding:0px 10px 10px 0px; font-family:arial; font-size:16px;'>
																				<table cellpadding='0' cellspacing='0' width='100%' align='center'>
																					<tr>
																						<td width='50%' valign='top' style='font-size:16px; font-family:arial;'><strong>Year</strong><br />" . $content['VehicleYear'] . "</td></td>
																						<td width='50%' valign='top' style='font-size:16px; font-family:arial;'><strong>Make</strong><br />" . $content['VehicleMake'] . "</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td style='padding:0px 10px 0px 0px; font-family:arial; font-size:16px;'><strong>Model</strong><br />" . $content['VehicleModel'] . "</td>
																		</tr>
																	</table>
																</td>
																<td width='50%' valign='top' style='font-size:16px; font-family:arial;'>
																	<table cellpadding='0' cellspacing='0'>
																		<tr>
																			<td style='padding:0px 10px 10px 0px; font-family:arial; font-size:16px;'><strong>Miles</strong><br />" . number_format($content['VehicleMiles'], 0) . "</td>
																		</tr>
																		<tr>
																			<td style='padding:0px 10px 0px 0px; font-family:arial; font-size:16px;'><strong>VIN Number</strong><br />" . $content['VehicleVIN'] . "</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='border-top:1px solid #f4f4f4;'>
														<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%' align='center'>
															<tr>
																<td style='color:#656565; font-weight:bold; font-family:arial; font-size:20px; padding:10px;'>
																	Service Details/History
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='padding:10px;'>
														<table cellpadding='0' cellspacing='0'>
															<tr>
																<td style='padding:0px 10px 0px 0px; font-family:arial; font-size:16px;'><strong>Previous Serviced</strong><br />" . ($content['PastServiced'] == 1 ? 'Yes': 'No') ."</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='padding:0px 10px 10px 10px;'>
														<table cellpadding='0' cellspacing='0' width='100%'>
															<tr>";
															
															if($content['PastServiced'] == 1) {
																$this -> msg .="<td width='50%' valign='top' style='font-size:16px; font-family:arial;'>
																					<table cellpadding='0' cellspacing='0' width='100%'>
																						<tr>
																							<td style='padding:0px 10px 10px 0px; font-family:arial; font-size:16px;'><strong>Last Service Done</strong><br />" . $content['PastServicedDate'] . "</td>
																							
																							
																						</tr>
																						<tr>
																							<td style='padding:0px 10px 0px 0px; font-family:arial; font-size:16px;'><strong>Work Previously Done</strong><br />" . $content['PastWorkDone'] . "</td>
																						</tr>
																					</table>
																				</td>";			
															}
													
												$this -> msg .="<td width='50%' valign='top' style='font-size:16px; font-family:arial;'>
																	<table cellpadding='0' cellspacing='0' width='100%'>
																		<tr>
																			<td style='padding:0px 10px 10px 0px; font-family:arial; font-size:16px;'><strong>New Appoitment Date</strong><br />" . $content['NewAppointmentDate'] . "</td>
																		</tr>
																		<tr>
																			<td style='padding:0px 10px 0px 0px; font-family:arial; font-size:16px;'><strong>New Work Requested</strong><br />" . $content['NewWorkDone'] . "</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td style='font-family:arial; text-align:center; padding:10px; font-size:12px;'>Copyright &copy; " . date('Y') . " Dillon Brothers. All Rights Reserved.</td>
									</tr>
								</table>
							</body>
						</html>";		
			
		
	
		$this -> SendEmail();		
	}
	
	public function PartsEmail($content = array()) {
					
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f6f7fb; margin:0px; padding:0px 0px 30px 0px'>	
								<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='572' align='center'>
									<tr>
										<td style='font-size:10px; font-family:arial; padding:10px 0px; text-align:center; background:#f6f7fb'>" . $this -> emailLogo() . "</td>
									</tr>
									<tr>
										<td style='color:#656565; padding:10px; font-family: arial; font-size: 20px; font-weight:bold'>
											New Parts Department Request
										</td>
									</tr>
									<tr>
										<td>
											<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='572' align='center'>
												<tr>
													<td style='padding:10px; font-size:16px; font-family:arial; '>
														<div><strong>Store:</strong></div>" . $content['StoreName'] . "</td>
												</tr>	
												<tr>
													<td style='border-top:1px solid #f4f4f4;'>
														<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%' align='center'>
															<tr>
																<td style='color:#656565; font-weight:bold; font-family:arial; font-size:20px; padding:10px;'>
																	Contact Information
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='padding:10px;'>
														<table cellpadding='0' cellspacing='0' width='572' align='center'>
															<tr>
																<td width='50%' valign='top' style='font-size:16px; font-family:arial;'>
																	<div>" . $content['FullName'] . "</div>
																	<table cellpadding='0' cellspacing='0'>
																		<tr>
																			<td style='padding:0px 10px 0px 0px; font-weight:bold; font-family:arial; font-size:16px;'>Home Phone:</td>
																			<td style='font-family:arial; font-size:16px;'>" . $content['HomePhone'] . "</td>
																		</tr>
																		<tr>
																			<td style='padding:0px 10px 0px 0px; font-weight:bold; font-family:arial; font-size:16px;'>Cell Phone:</td>
																			<td style='font-family:arial; font-size:16px;'>" . $content['CellPhone'] . "</td>
																		</tr>
																	</table>
																</td>
																<td width='50%' valign='top' style='font-size:16px; font-family:arial;'>
																	<div><strong>Address:</strong></div>
																	<div>" . $content['Address'] . "</div>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='border-top:1px solid #f4f4f4;'>
														<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%' align='center'>
															<tr>
																<td style='color:#656565; font-weight:bold; font-family:arial; font-size:20px; padding:10px;'>
																	Vehicle Information
																</td>
															</tr>
														</table>
													</td>
												</tr>	
												<tr>
													<td style='padding:10px;'>
														<table cellpadding='0' cellspacing='0' width='572' align='center'>
															<tr>
																<td width='50%' valign='top' style='font-size:16px; font-family:arial;'>
																	<table cellpadding='0' cellspacing='0' width='100%'>
																		<tr>
																			<td style='padding:0px 10px 10px 0px; font-family:arial; font-size:16px;'>
																				<table cellpadding='0' cellspacing='0' width='100%' align='center'>
																					<tr>
																						<td width='50%' valign='top' style='font-size:16px; font-family:arial;'><strong>Year</strong><br />" . $content['VehicleYear'] . "</td></td>
																						<td width='50%' valign='top' style='font-size:16px; font-family:arial;'><strong>Make</strong><br />" . $content['VehicleMake'] . "</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td style='padding:0px 10px 0px 0px; font-family:arial; font-size:16px;'><strong>Model</strong><br />" . $content['VehicleModel'] . "</td>
																		</tr>
																	</table>
																</td>
																<td width='50%' valign='top' style='font-size:16px; font-family:arial;'>
																	<table cellpadding='0' cellspacing='0'>
																		<tr>
																			<td style='padding:0px 10px 10px 0px; font-family:arial; font-size:16px;'><strong>Miles</strong><br />" . number_format($content['VehicleMiles'], 0) . "</td>
																		</tr>
																		<tr>
																			<td style='padding:0px 10px 0px 0px; font-family:arial; font-size:16px;'><strong>VIN Number</strong><br />" . $content['VehicleVIN'] . "</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='border-top:1px solid #f4f4f4;'>
														<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%' align='center'>
															<tr>
																<td style='color:#656565; font-weight:bold; font-family:arial; font-size:20px; padding:10px;'>
																	Parts Request Information
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='padding:0px 10px 10px 10px;'>
														<table cellpadding='0' cellspacing='0' width='100%'>
															<tr>
																<td width='50%' valign='top' style='font-size:16px; font-family:arial;'>
																	<table cellpadding='0' cellspacing='0' width='100%'>
																		<tr>
																			<td style='padding:0px 10px 10px 0px; font-family:arial; font-size:16px;'><strong>Parts Requested</strong><br />" . $content['PartsRequested'] . "</td>				
																		</tr>
																	</table>
																</td>
																<td width='50%' valign='top' style='font-size:16px; font-family:arial;'>
																	<table cellpadding='0' cellspacing='0' width='100%'>
																		<tr>
																			<td style='padding:0px 10px 10px 0px; font-family:arial; font-size:16px;'><strong>Part Number(s) Requested</strong><br />" . $content['PartNumberRequested'] . "</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td style='font-family:arial; text-align:center; padding:10px; font-size:12px;'>Copyright &copy; " . date('Y') . " Dillon Brothers. All Rights Reserved.</td>
									</tr>
								</table>
							</body>
						</html>";		
			
		
	
		$this -> SendEmail();		
	}
	
	public function DonationRequest($content = array()) {
					
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f6f7fb; margin:0px; padding:0px 0px 30px 0px'>	
								<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='572' align='center'>
									<tr>
										<td style='font-size:10px; font-family:arial; padding:10px 0px; text-align:center; background:#f6f7fb'>" . $this -> emailLogo() . "</td>
									</tr>
									<tr>
										<td style='color:#656565; padding:10px; font-family: arial; font-size: 20px; font-weight:bold'>
											New Donation Request
										</td>
									</tr>
									<tr>
										<td>
											<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='572' align='center'>
												<tr>
													<td style='border-top:1px solid #f4f4f4;'>
														<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%' align='center'>
															<tr>
																<td style='color:#656565; font-weight:bold; font-family:arial; font-size:20px; padding:10px;'>
																	Contact Information
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='padding:10px; font-size:16px; font-family:arial;'>
														<div style='font-family:arial;'><strong>Dillon Brothers Beneficiary:</strong></div>" . $content['DillonBrothersBeneficiary'] . "
													</td>
												</tr>
												<tr>
													<td style='padding:10px;'>
														<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%'>
															<tr>
																<td style='font-size:16px; font-family:arial; width:50%'>
																	<strong>Event Contact Person</strong><br />" . $content['EventContactPerson'] . "
																</td>
																<td style='font-size:16px; font-family:arial; width:50%'>
																	<strong>Event Contact Phone</strong><br />" . $content['EventContactPhone'] . "
																</td>
															</tr>
														</table>
													</td>
												</tr>	
												<tr>
													<td style='border-top:1px solid #f4f4f4;'>
														<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%' align='center'>
															<tr>
																<td style='color:#656565; font-weight:bold; font-family:arial; font-size:20px; padding:10px;'>
																	Fundraising Event Information
																</td>
															</tr>
														</table>
													</td>
												</tr>	
												<tr>
													<td style='padding:10px;'>
														<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%'>
															<tr>
																<td width='33%' valign='top' style='font-size:16px; font-family:arial;'>
																	<strong>Name Of Event</strong><br />" . $content['NameOfEvent'] . "
																</td>
																<td width='33%' valign='top' style='font-size:16px; font-family:arial;'>
																	<strong>Type Of Event</strong><br />" . $content['TypeOfEvent'] . "
																</td>
																<td width='33%' valign='top' style='font-size:16px; font-family:arial;'>
																	<strong>Tax ID Number</strong><br />" . $content['TaxIDNumber'] . "
																</td>
															</tr>
															<tr>
																<td width='33%' valign='top' style='font-size:16px; font-family:arial; padding:10px 0px 0px 0px'>
																	<strong>Event Benefiting</strong><br />" . $content['EventBenefiting'] . "
																</td>
																<td width='33%' valign='top' style='font-size:16px; font-family:arial; padding:10px 0px 0px 0px'>
																	<strong>Date of Event</strong><br />" . $content['DateOfEvent'] . "
																</td>
																<td width='33%' valign='top' style='font-size:16px; font-family:arial; padding:10px 0px 0px 0px'>
																	<strong>Anticipated Attendance</strong><br />" . $content['AnticipatedAttendance'] . "
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='border-top:1px solid #f4f4f4;'>
														<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%'>
															<tr>
																<td style='color:#656565; font-weight:bold; font-family:arial; font-size:20px; padding:10px;'>
																	Additional information
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='padding:0px 10px 10px 10px; font-size:16px; font-family:arial;'>
														<strong>Comments</strong><br />" . $content['AdditionalComments'] . "
													</td>
												</tr>";
												
												if(isset($content['Attachments'])) {
													$this -> msg .= "<tr><td style='padding:0px 10px 0px 10px; font-size:16px; font-family:arial;'><strong>Attachement</strong><br />
																			<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%'>";
													foreach($content['Attachments'] as $fileSingle) {
														$this -> msg .= "<tr>
																			<td style='width:40px; padding:0px 10px 0px 0px'>
																				<a href='" . PHOTO_URL . "DonationSubmissions/" . $content['LastedEmailID'] . "/" . $fileSingle . "'>
																					<div style='border: 1px dashed #337ab7; padding: 9px; margin-bottom:10px; width:30px;'>
																						<img src='" . PATH . "public/images/FileSingle.png' />
																					</div>
																				</a>
																			</td>
																			<td><a href='" . PHOTO_URL . "DonationSubmissions/" . $content['LastedEmailID'] . "/" . $fileSingle . "'>Click to see file</a></td>
																		</tr>";	
													}
													$this -> msg .= "</table></td></tr>";
												}
							$this -> msg .= "</table>
										</td>
									</tr>
									 <tr>
										<td style='font-family:arial; text-align:center; padding:10px; font-size:12px;'>Copyright &copy; " . date('Y') . " Dillon Brothers. All Rights Reserved.</td>
									</tr>
								</table>
							</body>
						</html>";		
			
		
	
		$this -> SendEmail();		
	}
	
	public function ApparelRequest($content = array()) {
					
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f6f7fb; margin:0px; padding:0px 0px 30px 0px'>	
								<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='572' align='center'>
									<tr>
										<td style='font-size:10px; font-family:arial; padding:10px 0px; text-align:center; background:#f6f7fb'>" . $this -> emailLogo() . "</td>
									</tr>
									<tr>
										<td style='color:#656565; padding:10px; font-family: arial; font-size: 20px; font-weight:bold'>
											New Apparel Department Request
										</td>
									</tr>
									<tr>
										<td>
											<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='572' align='center'>
												<tr>
													<td style='padding:10px; font-size:16px; font-family:arial; '>
														<div><strong>Store:</strong></div>" . $content['StoreName'] . "</td>
												</tr>	
												<tr>
													<td style='border-top:1px solid #f4f4f4;'>
														<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%' align='center'>
															<tr>
																<td style='color:#656565; font-weight:bold; font-family:arial; font-size:20px; padding:10px;'>
																	Contact Information
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='padding:10px;'>
														<table cellpadding='0' cellspacing='0' width='572' align='center'>
															<tr>
																<td width='50%' valign='top' style='font-size:16px; font-family:arial;'>
																	<div>" . $content['FullName'] . "</div>
																	<table cellpadding='0' cellspacing='0'>
																		<tr>
																			<td style='padding:0px 10px 0px 0px; font-weight:bold; font-family:arial; font-size:16px;'>Home Phone:</td>
																			<td style='font-family:arial; font-size:16px;'>" . $content['HomePhone'] . "</td>
																		</tr>
																		<tr>
																			<td style='padding:0px 10px 0px 0px; font-weight:bold; font-family:arial; font-size:16px;'>Cell Phone:</td>
																			<td style='font-family:arial; font-size:16px;'>" . $content['CellPhone']. "</td>
																		</tr>
																	</table>
																</td>
																<td width='50%' valign='top' style='font-size:16px; font-family:arial;'>
																	<div><strong>Address:</strong></div>
																	<div>" . $content['Address'] . "</div>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='border-top:1px solid #f4f4f4;'>
														<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%' align='center'>
															<tr>
																<td style='color:#656565; font-weight:bold; font-family:arial; font-size:20px; padding:10px;'>
																	Addition Information
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='padding:0px 10px 10px 10px; font-size:16px; font-family:arial;'>
														<strong>Comments</strong><br />" . $content['Comments'] . "
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td style='font-family:arial; text-align:center; padding:10px; font-size:12px;'>Copyright &copy; " . date('Y') . " Dillon Brothers. All Rights Reserved.</td>
									</tr>
								</table>
							</body>
						</html>";		
			
		
	
		$this -> SendEmail();		
	}	
		
	
		
	private function SendEmail() {
		mail($this -> to, $this -> subject . EMAIL_SUBJECT, $this -> msg, $this -> header);
	}
	
	public static function verifyEmailAddress($emailInput) {
		return preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$emailInput);
	}
}