<div id="SignUpPage">
	<div class="container">
		<div class="row">
			<div class="col-md-12" style="margin:15px 0px 20px 0px;">
				<a href="<?php echo PATH ?>shop">Shopping Center</a><span style="margin:0px 5px;"><i class="fa fa-caret-right" aria-hidden="true"></i></span>Your Account: Update your Billing	
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h1>Your Account</h1>
			</div>
		</div>
		<div class='row'>
			<div class="col-md-12">
				<div class="accountTabs">
					<a href='<?php echo PATH ?>shop/account'>
						<div class="tabSingle">
							Your Information
						</div>
					</a>
					<div class="tabSingle">
						<div class='selectedIndicator'></div>
						Update Credit / Billing Info.
					</div>
					<div style='clear:both'></div>
				</div>
			</div>
		</div>
		<div id="BillingForm">
			<div id="BillingUpdate" style='display:none;'>
				<div class="alert alert-success" style='margin-top:15px;'>
					Your Billing / Credit Card Information has been updated
				</div>
				
			</div>
			
			<form method="post" action="<?php echo PATH ?>ajax/account/updatebillinginfo" id="AccountBillingUpdate">
				<div class="row">
					<div class="col-md-12">
						<h2>Would you like to update your Credit Card / Billing Information?</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2" style='text-align:center'>
						<img src='<?php echo PATH ?>public/images/secureIcon.png' />
					</div>
					<div class="col-md-10">
						Dillon Brothers takes your privacy seriously. We do not and will not save any credit card information. This information is stored securely on a world renowned third party credit card processors servers.	
					</div>
				</div>
					
				<div class="row">
					<div class="col-md-12">
						<div class="inputItem">
							<div class="inputLabel">Address</div>
							<input type='text' class='billingAddress' />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="inputItem">
							<div class="inputLabel">City</div>
							<input type='text' class='billingCity' />
						</div>
					</div>
					<div class="col-md-4">
						<div class="inputItem">
							<div class="inputLabel">State</div>
							<select class='billingState'>
								<option value='0'>Please Select</option>
								<?php foreach($this -> states() as $key => $stateSingle): ?>
									<option value='<?php echo $key?>'><?php echo $stateSingle ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="inputItem">
							<div class="inputLabel">Zip</div>
							<input type='text' class='billingZip' />
						</div>
					</div>
				</div>
				<div class="row" style='margin-top:25px;'>
					<div class="col-md-6">
						<div class="inputItem">
							<div class="inputLabel">Card Holder Name</div>
							<input type='text' class="CardHolderName" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="inputItem">
							<div class="inputLabel">Credit Card Number</div>
							<input type='text' class="CreditCardNumber" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="inputItem">
							<div class="inputLabel">Expiration Month</div>
							<select class='ExpirationMonth'>
								<option value='0'>Please Select</option>
								<?php foreach($this -> months() as $key => $monthSingle): ?>
									<option value='<?php echo $key?>'><?php echo $monthSingle ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="inputItem">
							<div class="inputLabel">Expiration Year</div>
							<select class='ExpirationYear'>
								<option value='0'>Please Select</option>
								<?php foreach($this -> year() as $key => $yearSingle): ?>
									<option value='<?php echo $key?>'><?php echo $yearSingle ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="inputItem">
							<div class="inputLabel">CVC</div>
							<input type='text' class='CVCNumber' />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<input type='submit' value='Create Account' class="redButton" />
					</div>
				</div>
			</form>	
		</div>

		
	</div>
</div>	


