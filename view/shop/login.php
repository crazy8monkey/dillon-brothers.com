<div id="CreateCredentialsForm">
	<div class="container" style='margin-top:15px;'>
		<div class="row">
			<div class="col-md-12">
				<div class="FormContainer">
					<div class="row">
						<div class="col-md-12" style="margin:0px 0px 20px 0px;">
							<a href="<?php echo PATH ?>shop">Shopping Center</a><span style="margin:0px 5px;"><i class="fa fa-caret-right" aria-hidden="true"></i></span>Login
						</div>
					</div>
					<div class="header">
						Login into your account
					</div>
					<div id="LoginErrorMessages" style='display:none;'>
						<div class="alert alert-danger errorMessageTextPlacement">
						</div>
					</div>
					<form action="<?php echo PATH ?>ajax/ecommerceloginaccount" id="ShoppingCenterLoginForm">
						<div class="inputItem">
							<div id="inputID1">
								<div class="errorMessage"></div>
								<div class="inputLabel">Email</div>
								<input type="text" name="emailLogin" onchange='Globals.removeValidationMessage(1);' />
							</div>
						</div>
						<div class="inputItem">
							<div id="inputID2">
								<div class="errorMessage"></div>
								<div class="inputLabel">Password</div>
								<input type="password" name="passwordLogin" onchange='Globals.removeValidationMessage(2);' />
							</div>
						</div>
						<div class="submitButton">
							<input type='submit' value="Submit" class='blueButton' />
						</div>
					</form>
				</div>
			</div>
		</div>	
	</div>
</div>


