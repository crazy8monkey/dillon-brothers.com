<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html amp lang="en"> <!--<![endif]-->
	<head>
	
		<title>Create your Credentials | Dillon Brothers</title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
		<meta name="robots" content="noindex">
		<meta http-equiv="Cache-control" content="public">
		<meta charset="utf-8">

		<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		
		<link rel="stylesheet" href="<?php echo PATH ?>public/css/style.css" type="text/css" />
		<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
		<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
		
		<link rel="stylesheet" href="<?php echo PATH ?>public/css/ShopController.css" />
							
		
		<script type="text/javascript" src="<?php echo PATH ?>public/js/Globals.js"></script>
		<script type="text/javascript" src="<?php echo PATH ?>public/js/ShopController.js"></script>
		
		<script src="https://use.typekit.net/kpv4owm.js"></script>
		<script type='text/javascript'>
			$(document).ready(function() {
				ShopController.NewCredentialsForm();
			})
		</script>
		
	</head>
<body>
<div id="CreateCredentialsForm">
	<div class="container" style='margin-top:15px;'>
		<div class="row">
			<div class="col-md-12">
				<div class="FormContainer">
					<?php if (isset($_GET['token']) && isset($_GET['u'])) : ?>
						<?php if($_GET['u'] == $this -> AccountSingle -> GetID() && $_GET['token'] == $this -> AccountSingle -> SecurityToken): ?>
						<div class="logo">
							<img src="<?php echo PATH ?>public/images/DillonBrothers.png" />
						</div>
						<div id="SavedForm">
							Your Credentials has been saved<br />You can now login/access your account.<br />
							Thank you for being a Dillon Brothers Customer!
						</div>
						<div class="header">
							Create your Credentials
						</div>
						<form action="<?php echo PATH ?>ajax/newcredentials" id="NewCredentiaislForm">
							<input type='hidden' name="u" value='<?php echo Hash::mc_encrypt($_GET['u'], ENCRYPTION_KEY) ?>' />
							<div class="inputItem">
								<div id="inputID1">
									<div class="errorMessage"></div>
									<div class="inputLabel">Username</div>
									<input type="text" name="newUsername" onchange='Globals.removeValidationMessage(1);' />
								</div>
							</div>
							<div class="inputItem">
								<div id="inputID2">
									<div class="errorMessage"></div>
									<div class="inputLabel">Password</div>
									<input type="password" name="newPassword" onchange='Globals.removeValidationMessage(2);' />
								</div>
							</div>
							<div class="submitButton">
								<input type='submit' value="Submit" class='blueButton' />
							</div>
						</form>
						
						<?php else: ?>
							This page has been expired
						<?php endif; ?>
					<?php else: ?>
						This page has been expired
					<?php endif; ?>			
				</div>
				
			</div>
		
		</div>	
	</div>
</div>


</body>
</html>

