<div id="EcommerceStoreContainer" style="margin-bottom: 50px;">
	<div class="TopEcommercePage">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 ContentHeader">
					<h1>SHOPPING CENTER</h1>
				</div>
				<div class="col-md-6">
					<div class="ShoppingAccountLinks">
						<?php Session::init(); ?>
						<?php if(isset($_SESSION['EcommerceUserAccount'])): ?>
							<a href='<?php echo PATH ?>shop/logout'>Logout</a> / <a href='<?php echo PATH ?>shop/account'>Your Account</a>
						<?php else: ?>
							<a href='<?php echo PATH ?>shop/login'>
								<div class="whiteButton">
									Login	
								</div>
							</a>
							<a href='<?php echo PATH ?>shop/signup'>
								<div class="whiteButton">
									Signup
								</div>
							</a>	
						<?php endif; ?>
						
					</div>
					
				</div>
			</div>	
		</div>	
	</div>
	
	
	<div class="container-fluid" style='padding-bottom:90px;'>
		
		<?php if(count($this -> shoppingCategories) > 0): ?>		
			<div class="row">
				<div class="col-md-2 CategoryLinks">
					<div class="desktopCategoryList">
						<a href="<?php echo PATH ?>shop">
							<div class="categorySingleLink">
								All
							</div>
						</a>
						<?php foreach($this -> shoppingCategories as $category): ?>
							<a href="<?php echo PATH ?>shop/category/<?php echo $category['settingCategorySEOName'] ?>">
								<div class="categorySingleLink">
									<?php echo $category['settingCategoryName'] ?>
								</div>	
							</a>
							<script type="application/ld+json">
							{
								"@type": "SiteNavigationElement",
								"@context": "http://schema.org",
								"name": "Dillon Brothers Shop Category <?php echo $category['settingCategoryName'] ?>",
								"url": "<?php echo PATH ?>shop/category/<?php echo $category['settingCategorySEOName'] ?>"
							}				
							</script>
						<?php endforeach; ?>	
					</div>
					
					<?php 
	
					$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
					
					
					function matchesCategory($site, $strings) {
				    	if(strpos($site, $strings) !== false) 
				     	    {
				     	        return true;
				     	    }
				    	
				    	return false;
					} 
					
					?>
					<div class="mobileCategoryDropDown">
						<select onchange="if (this.value) window.location.href=this.value">
							<option value='<?php echo PATH ?>shop'>All Products</option>
							<?php foreach($this -> shoppingCategories as $category): ?>
								<option <?php echo matchesCategory($url, $category['settingCategorySEOName']) ? 'selected' : '' ?> value='<?php echo PATH ?>shop/category/<?php echo $category['settingCategorySEOName'] ?>'><?php echo $category['settingCategoryName'] ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					
				</div>
				<div class="col-md-10">
					<?php foreach(array_chunk($this -> shoppingProducts, 4, true) as $productsThree) : ?>
						<div class="row">
							<?php foreach($productsThree as $productSingle) : ?>
								<?php 
									
									switch($productSingle['productRelatedStore']) {
										case 2;
											$priceCss = 'MotorSportPrice';
											break;
										case 3;
											$priceCss = 'HarleyPrice';
											break;
										case 4;
											$priceCss = 'IndianPrice';
											break;
									}	
									
									$productID = $productSingle['productID'];	
									$filteredReviews = array_filter($this -> reviews, function ($var) use($productID) {
											 return $var['reviewItemID'] == $productID; 
										}
									);
								?>
								
								<?php 
									$largeImage = PHOTO_URL . 'ecommerce/' . $productSingle['productFolderName'] . '/'. $productSingle['PhotoName'] . '-l.'. $productSingle['PhotoExt'];
								?>
								<div class="productSingleElement">
									<a href="<?php echo PATH ?>shop/product/<?php echo $productSingle['productSEOName'] ?>">
										<div class="productItemSingle">
											<div class="productImage" style='background:url(<?php echo $largeImage ?>) no-repeat center'></div>
											<div class="priceSingle <?php echo $priceCss ?>">
												$<?php echo number_format($productSingle['productPrice'], 2) ?>		
											</div>
											<div class="productName">
												<?php echo $productSingle['productName'] ?>
											</div>
										</div>	
									</a>
									
								</div>
								<script type="application/ld+json">
								{
									"@type": "Product",
									"@context": "http://schema.org",
									"name": "<?php echo $productSingle['productName'] ?>",
								    "category": "<?php echo $productSingle['settingCategoryName'] ?>",
									"url": "<?php echo PATH ?>shop/product/<?php echo $productSingle['productSEOName'] ?>",
								    "sku": "<?php echo $productSingle['SKUNumber'] ?>",
								    "description":"<?php echo str_replace("<br>", ' ', $productSingle['productDescription']) ?>",
								    "image": "<?php echo $largeImage ?>",
								    "offers": {
								    	"@type": "Offer",
								    	"availability": "http://schema.org/InStock",
								    	"price": "<?php echo number_format($productSingle['productPrice'], 2) ?>",
								        "priceCurrency": "USD"
								    }
								    <?php if(count($filteredReviews) > 0): ?>
								    ,
								    "aggregateRating": {
								    	 "@type": "AggregateRating",
										 "ratingValue": "<?php echo $productSingle['AverageRating'] ?>",
										 "reviewCount": "<?php echo count($filteredReviews) ?>"
								    },
								    "review": [
								   <?php  $c = 0; $reviewLength = count($filteredReviews); ?>
								    	<?php foreach($filteredReviews as $key => $reviewSingle): ?>
								    		{
										      "@type": "Review",
										      "author": "<?php echo $reviewSingle['reviewedByFirstName'] ?> <?php echo substr($reviewSingle['reviewedByLastName'], 0, 1) ?>",
										      "datePublished": "<?php echo $reviewSingle['DateEntered'] ?>",
										      "description": "<?php echo str_replace("<br />", '', $reviewSingle['reviewText']) ?>",
										      "reviewRating": {
										        "@type": "Rating",
										        "bestRating": "5",
										        "ratingValue": "<?php echo $reviewSingle['rating'] ?>",
										        "worstRating": "1"
										      }
										    }<?php if($c != $reviewLength - 1) :?>,<?php endif; ?>
													<?php $c++ ?>	
								   		<?php endforeach; ?>	
								    ]
								    
								    
								    
								    <?php endif; ?>
								    
								}			
								</script>
									
							<?php endforeach; ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php else: ?>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						There are no products for sale
					</div>	
				</div>
			</div>
		<?php endif; ?>
		
	</div>
</div>
	
	
	



