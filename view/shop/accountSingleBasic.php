<div id="SignUpPage">
	<div class="container">
		<div class="row">
			<div class="col-md-12" style="margin:15px 0px 20px 0px;">
				<a href="<?php echo PATH ?>shop">Shopping Center</a><span style="margin:0px 5px;"><i class="fa fa-caret-right" aria-hidden="true"></i></span>Your Account: Information	
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h1>Your Account</h1>
			</div>
		</div>
		<div class='row'>
			<div class="col-md-12">
				<div class="accountTabs">
					<div class="tabSingle">
						<div class='selectedIndicator'></div> 
						Your Information
					</div>
					<a href='<?php echo PATH ?>shop/account/billing'>
						<div class="tabSingle">
							Update Credit / Billing Info.
						</div>	
					</a>
					
					<div style='clear:both'></div>
				</div>
			</div>
		</div>
		<div id="SavedBasicInfo" style='display:none;'>
			<div class="alert alert-success" style='margin-top:15px;'>
				Your Account info has been saved	
			</div>
		</div>
		
		<div id="SignUpForm">		
			
			
			<div class="row">
				<div class="col-md-12">
					<h2>Your Information</h2>
				</div>
			</div>
		
			<form method="post" action="<?php echo PATH ?>ajax/account/updatesingleinfo" id="AccountFormInfo">
				<div class="row">
					<div class="col-md-4">
						<div id="inputID1">
							<div class='errorMessage'></div>
							<div class="inputItem">
								<div class="inputLabel">First Name</div>
								<input type='text' name="accountFirstName" onchange='Globals.removeValidationMessage(1);' value='<?php echo $this -> AccountSingle -> FirstName?>' />
							</div>	
						</div>
					</div>
					<div class="col-md-4">
						<div id="inputID2">
							<div class='errorMessage'></div>
							<div class="inputItem">
								<div class="inputLabel">Last Name</div>
								<input type='text' name="accountLastName" onchange='Globals.removeValidationMessage(2);' value='<?php echo $this -> AccountSingle -> LastName ?>' />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div id="inputID6">
							<div class='errorMessage'></div>
							<div class="inputItem">
								<div class="inputLabel">Phone Number</div>
								<input type='text' name="accountPhoneNumber" class="accountPhoneNumber" onchange='Globals.removeValidationMessage(6);' value='<?php echo $this -> AccountSingle -> Phone ?>' />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<h2>Credentials</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div id="inputID3">
							<div class='errorMessage'></div>
							<div class="inputItem">
								<div class="inputLabel">Email</div>
								<input type='text' name="accountEmail" onchange='Globals.removeValidationMessage(3);' value='<?php echo $this -> AccountSingle -> Email ?>' />
							</div>
						</div>
						
					</div>
					<div class="col-md-6">
						<div id="inputID5">
							<div class='errorMessage'></div>
								<div class="inputItem">
								<div class="inputLabel">Password</div>
								<input type='password' name="accountPassword" onchange='Globals.removeValidationMessage(5);' />
							</div>
						</div>
						
					</div>
				</div>	
				<div class="row">
					<div class="col-md-12">
						<input type='submit' value='Update Your Info.' class="redButton" />
					</div>
				</div>
			</form>
		</div>
		<div id="CongratsText" style='display:none'>
			<div class="row">
				<div class="col-md-12">
					Thank you for signing up an account to our eccommerce store!
				</div>
			</div>
		</div>
	</div>
</div>	


