<link rel="stylesheet" href="<?php echo PATH ?>public/css/photoswipe.css" type="text/css" />
<link rel="stylesheet" href="<?php echo PATH ?>public/css/default-skin.css" type="text/css" />


<div id="ProductSinglePage" style="margin-bottom: 50px;">
	<div class="container">
		<div class="row">
			<div class="col-md-12" style="margin:20px 0px;">
				<a href="<?php echo PATH ?>shop">Shopping Center</a><span style="margin:0px 5px;"><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo PATH ?>shop/category/<?php echo $this -> productSingle -> ProductCategoryLink ?>"><?php echo $this -> productSingle -> ProductCategory ?></a><span style="margin:0px 5px;"><i class="fa fa-caret-right" aria-hidden="true"></i></span><?php echo $this -> productSingle -> productName ?>	
				
				
				
			</div>
		</div>
	</div>
	<div class="ProductSingleWrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="productSingleHeader">
						<h1><?php echo $this -> productSingle -> productName ?></h1>		
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-7">
					<div class="my-gallery" style='display: inline-block; width: 100%;'>
					<?php 
						$mainImage = PHOTO_URL . 'ecommerce/' . $this -> productSingle -> FolderName . '/' . $this -> productSingle -> Photos['0']['PhotoName'] . '-l.' . $this -> productSingle -> Photos['0']['PhotoExt']; 
						list($imageWidth, $imageHeight) = getimagesize($mainImage);
					?>
					<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
						<a href="<?php echo $mainImage ?>" itemprop="contentUrl" data-size="<?php echo $imageWidth . 'x' . $imageHeight?>">
							<div class="productPhotoMain">
								<div class="imageObj" style='background:url(<?php echo $mainImage ?>) no-repeat center'>
									<img src="<?php echo $mainImage ?>" alt="<?php echo $this -> productSingle -> Photos['0']['AltTag']; ?>" title="<?php echo $this -> productSingle -> Photos['0']['TitleTag']; ?>" />	
								</div>
								
								<div class="DillonBrothersLogo"></div>
								<?php if(!empty($this -> inventorySingle -> OverlayText)): ?>
									<div class="OverlayText">
										<?php echo $this -> inventorySingle -> OverlayText ?>
									</div>
								<?php endif; ?>				
							</div>
						</a>
					</figure>
					
					<?php foreach($this -> productSingle -> Photos as $key => $photo): ?>
						<?php if($key != 0): ?>
							
							<?php 
								$largeImage = PHOTO_URL . 'ecommerce/' . $this -> productSingle -> FolderName . '/' . $photo['PhotoName'] . '-l.' . $photo['PhotoExt'];
								$smallImage = PHOTO_URL . 'ecommerce/' . $this -> productSingle -> FolderName . '/' . $photo['PhotoName'] . '-s.' . $photo['PhotoExt'];
								list($imageWidth, $imageHeight) = getimagesize($largeImage);						
							?>
							<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
								<a href="<?php echo $largeImage ?>" itemprop="contentUrl"  data-size="<?php echo $imageWidth . 'x' . $imageHeight?>">
									<div class="OtherProductPhotos">
										<img style='position:relative; z-index:1;' src="<?php echo $smallImage ?>" alt="<?php echo $photo['AltTag']; ?>" title="<?php echo $photo['TitleTag']; ?>" />
									</div>
								</a>
							</figure>
						<?php endif; ?>
					<?php endforeach; ?>
						
					
					</div>	
				</div>
				<div class="col-md-5">
					<div class="priceContainer">
						<?php if($this -> productSingle -> MSRPPrice != 0.00 && !empty($this -> productSingle -> Price)): ?>					
							<div><strike>MSRP: $<?php echo number_format($this -> productSingle -> MSRPPrice, 2);  ?></strike></div>
							<div>Our selling price:</div>
							<div><h2 class="pricing" style='margin: 0px;'>$<?php echo number_format($this -> productSingle -> Price, 2);  ?></h2></div>
						<?php else: ?>
							<?php if($this -> productSingle -> MSRPPrice != 0.00): ?>					
								<div><h2 class="pricing" style='margin: 0px;'>$<?php echo number_format($this -> productSingle -> MSRPPrice, 2);  ?></h2></div>
							<?php elseif(!empty($this -> productSingle -> Price)): ?>
								<div><h2 class="pricing" style='margin: 0px;'>$<?php echo number_format($this -> productSingle -> Price, 2);  ?></h2></div>
							<?php endif; ?>
						<?php endif; ?>
						
						<?php if(!empty($this -> productSingle -> SKUNumber)): ?>
							<strong>#<?php echo $this -> productSingle -> SKUNumber ?></strong>
						<?php endif; ?>
						
					</div>
					<div class="productDescription">
						<?php echo $this -> productSingle -> Description ?>
					</div>
					<?php if(count($this -> productSingle -> Sizes) > 0): ?>						
						<div style='margin:20px 0px;'>
							<div><strong>Avaiable Options</strong></div>
							<table>
								<?php foreach($this -> productSingle -> Sizes as $sizeSingle): ?>
									<tr>
										<td style='padding-right:5px;'><?php echo $sizeSingle['settingSizeAbbr'] ?></td>
										<td style='width:150px'>| <?php echo $sizeSingle['settingSizeText'] ?></td>
										<td>#<?php echo $sizeSingle['PartNumber'] ?></td>
									</tr>				
								<?php endforeach; ?>	
								
							</table>
						</div>
					
					<?php endif; ?>
					
					<div class="row">
						<div class="col-md-12">
							<form method="post" id="SendToCartForm" action="<?php echo PATH ?>ajax/addtocart">
								<input type='hidden' name='productID' value="<?php echo Hash::mc_encrypt($this -> productSingle -> GetID(), ENCRYPTION_KEY) ?>" />
								<?php if(count($this -> productSingle -> Colors) > 0): ?>
									<div class="dropDownOption">
										<div id="ColorSelectedError"><div class="errorMessage"></div></div>
										<select name='ColorSelect'>
											<option value='0'>Please Select Color</option>
											<?php foreach($this -> productSingle -> Colors as $colorSingle): ?>
												<option value='<?php echo $colorSingle['relatedColorID'] ?>'><?php echo $colorSingle['settingColorText'] ?></option>
											<?php endforeach; ?>	
										</select>	
									</div>
								<?php endif; ?>
								
								<?php if(count($this -> productSingle -> Sizes) > 0): ?>						
									<div class="dropDownOption">
										<div id="SizeSelectedError"><div class="errorMessage"></div></div>
										<select name='SizeSelect'>
											<option value='0'>Please Select Size</option>
											<?php foreach($this -> productSingle -> Sizes as $sizeSingle): ?>
												<option value='<?php echo $sizeSingle['relatedSizeID'] ?>'><?php echo $sizeSingle['settingSizeAbbr'] ?> | <?php echo $sizeSingle['settingSizeText'] ?> - #<?php echo $sizeSingle['PartNumber'] ?></option>
											<?php endforeach; ?>	
										</select>
									</div>
								<?php endif; ?>
								
								<?php 
								$facebookShareURL = 'https://www.facebook.com/sharer/sharer.php?u=' . urlencode($this -> productSingle -> GetURL()) . 
											  '&amp;title=' . urlencode($this -> productSingle -> productName  . ': Dillon Brothers') . 
											  '&amp;description=' . urlencode($this -> productSingle -> Description). '&amp;' . 
											  '&amp;picture=' . $mainImage . '&amp;src=sdkpreparse'; 
											  
								$twitterShareURL = "http://twitter.com/intent/tweet?status=" . urlencode($this -> productSingle -> productName . '. ' . $this -> productSingle -> GetURL()) . " %20%23dillonbrothers";		
							
								$pinUrl = "https://www.pinterest.com/pin/create/bookmarklet/?url=" . urlencode($this -> productSingle -> GetURL()) . "&media=" . $mainImage . "&h=" . $imageHeight . "&w=" . $imageWidth . "&description=" .urlencode($this -> productSingle -> productName . " | Dillon Brothers Harley-Davidson");
								
								?>
								
								<div style="margin-bottom:10px; margin-top:10px;">
										
									<a onclick="window.open('<?php echo $facebookShareURL ?>','sharer','height=500,width=500');" href="javascript:void(0);" style="text-decoration:none;">
										<div class='facebookShare'><img src="<?php echo PATH ?>public/images/facebookIcon.png" data-toggle="tooltip" data-placement="bottom" title="Share on Facebook" /></div>
									</a>
									
									<a onclick="window.open('<?php echo $twitterShareURL ?>','sharer','height=300, width=500');" href="javascript:void(0);" style="text-decoration:none;">
										<div class="twitterShare"><img src="<?php echo PATH ?>public/images/twitter.png" data-toggle="tooltip" data-placement="bottom" title="Share on Twitter"></div>
									</a>
									
									<a onclick="window.open('<?php echo $pinUrl ?>','sharer','height=300, width=500');" href="javascript:void(0);" style="text-decoration:none;">
										<div class="pinButton"><img src="<?php echo PATH ?>public/images/pinterestIcon.png" data-toggle="tooltip" data-placement="bottom" title="Pin on Pinterest"></div>
									</a>
									
								</div>
								<!--	
									<a onclick="window.open('http://dev.dillon-brothers.com/inventory/emailfriend/jya3awe00ha152397','sharer','height=500, width=800');" href="javascript:void(0);" style="text-decoration:none;">
										<div class="pinButton"><img src="http://dev.dillon-brothers.com/public/images/emailToFriendIcon.png" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Email to a Friend"></div>
									</a>
					
									<div style="clear:both"></div>
								</div>-->
								
								<button class="AddToCartButton redButton">ADD TO CART</button>
								
							</form>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" id="AddedToCartMessage" style='display:none;'>
							<div class="cartAddedMesseage">
								Product has been added to your cart!<br />
								Click the cart icon to see your cart
							</div>	
							
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container">
	
		<div class="row">
			<div class="col-md-12">
				<div style='margin-top:20px;'>
					<div style='margin-top:10px'>
						<a href="javascript:void(0);" onclick='ShopController.OpenAddReviewPopup()'>
							<div class="AddReviewButton">
								<i class="fa fa-pencil" aria-hidden="true"></i>
							</div>	
						</a>
						<h2 class='reviewsHeader'>Reviews</h2>		
						<div style='clear:both'></div>
					</div>
				</div>
			</div>
		</div>
		<?php if(count($this -> productSingle ->  ProductReviews) > 0): ?>
			<?php foreach($this -> productSingle ->  ProductReviews as $reviewSingle): ?>
				<div class="row">
					<div class="col-md-12">
						<div class="ReviewConversationBubble">
							<div class="starRating">
							<?php switch($reviewSingle['rating']): 
								case 5.0: ?>
									<div class="starFull"></div>
									<div class="starFull"></div>
									<div class="starFull"></div>
									<div class="starFull"></div>
									<div class="starFull"></div>
								<?php break; ?>
								<?php case 4.5: ?>
									<div class="starEmpty"></div>
									<div class="starHalfFull"></div>
									<div class="starFull"></div>
									<div class="starFull"></div>
									<div class="starFull"></div>
									<div class="starFull"></div>
								<?php break; ?>
								<?php case 4.0: ?>
									<div class="starEmpty"></div>
									<div class="starFull"></div>
									<div class="starFull"></div>
									<div class="starFull"></div>
									<div class="starFull"></div>
								<?php break; ?>
								<?php case 3.5: ?>
									<div class="starEmpty"></div>
									<div class="starEmpty"></div>
									<div class="starHalfFull"></div>
									<div class="starFull"></div>
									<div class="starFull"></div>
									<div class="starFull"></div>
								<?php break; ?>
								<?php case 3.0: ?>
									<div class="starEmpty"></div>
									<div class="starEmpty"></div>
									<div class="starFull"></div>
									<div class="starFull"></div>
									<div class="starFull"></div>
								<?php break; ?>
								<?php case 2.5: ?>
									<div class="starEmpty"></div>
									<div class="starEmpty"></div>
									<div class="starEmpty"></div>
									<div class="starHalfFull"></div>
									<div class="starFull"></div>
									<div class="starFull"></div>
								<?php break; ?>
								<?php case 2.0: ?>
									<div class="starEmpty"></div>
									<div class="starEmpty"></div>
									<div class="starEmpty"></div>
									<div class="starFull"></div>
									<div class="starFull"></div>
								<?php break; ?>
								<?php case 1.5: ?>
									<div class="starEmpty"></div>
									<div class="starEmpty"></div>
									<div class="starEmpty"></div>
									<div class="starEmpty"></div>
									<div class="starHalfFull"></div>
									<div class="starFull"></div>
								<?php break; ?>
								<?php case 1.0: ?>
									<div class="starEmpty"></div>
									<div class="starEmpty"></div>
									<div class="starEmpty"></div>
									<div class="starEmpty"></div>
									<div class="starFull"></div>
								<?php break; ?>
								<?php case 0.5: ?>
									<div class="starEmpty"></div>
									<div class="starEmpty"></div>
									<div class="starEmpty"></div>
									<div class="starEmpty"></div>
									<div class="starEmpty"></div>
									<div class="starHalfFull"></div>
								<?php break; ?>
							<?php endswitch; ?>
						</div>
						<div style='clear:both'>
							<?php echo $reviewSingle['reviewText'] ?>
						</div>	
							
						<div class="conversationCarot"></div>
					</div>
					<div class='reviewedByName'>
						<?php 
							$submittedTime = explode('T', $reviewSingle['DateEntered']);
						?>
						<?php echo $reviewSingle['reviewedByFirstName'] ?> <?php echo substr($reviewSingle['reviewedByLastName'], 0, 1) ?> | <?php echo $this -> recordedTime -> formatDate($submittedTime[0]) ?> / <?php echo $this -> recordedTime -> formatMinuteSeconts($submittedTime[1]); ?>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
			
		<?php else: ?>
			<div class="row">
				<div class="col-md-12">
					There are no reviews entered into this product. Maybe you will be the first!
				</div>
			</div>
		<?php endif; ?>
		<div id="AddReviewForm">
			<div class="addreviewOverlay"></div>
			<div class='AddReviewForm'>
				<div class='addReviewHeader'>
					Add Review To Product
					<a href='javascript:void(0);' onclick='ShopController.CloseNewReviewForm()'>
						<i class="fa fa-times" aria-hidden="true"></i>	
					</a>
				</div>
				<div id="AddReviewFormContent">
					<form method='post' id="NewReviewForm" action="<?php echo PATH ?>ajax/productreview">
						<input type='hidden' name='ecommerceProductID' value='<?php echo Hash::mc_encrypt($this -> productSingle -> GetID(), ENCRYPTION_KEY)?>' />
						<div class="formContent">
							<div class="row">
								<div class="col-md-12">
									<div class="input" >
										<div class="inputLabel">Rating</div>
										<fieldset class="rating" id="RadingError">
										    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
										    <input type="radio" id="star4half" name="rating" value="4.5" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
										    <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
										    <input type="radio" id="star3half" name="rating" value="3.5" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
										    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
										    <input type="radio" id="star2half" name="rating" value="2.5" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
										    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
										    <input type="radio" id="star1half" name="rating" value="1.5" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
										    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
										    <input type="radio" id="starhalf" name="rating" value="0.5" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
										</fieldset>
										<div style='clear:both'></div>
									</div>
								</div>
							</div>
							<?php if(!isset($_SESSION['EcommerceUserAccountLoggedIn'])): ?>
							
							<div class="row">
								<div class="col-md-6">
									<div class="input">
										<div class="inputLabel">First Name</div>
										<input type='text' id="FirstNameError" name='reviewFirstName' />
									</div>
								</div>
								<div class="col-md-6">
									<div class="input">
										<div class="inputLabel">Last Name</div>
										<input type='text' name='reviewLastName' id="LastNameError" />
									</div>
								</div>
							</div>
							<?php endif; ?>
							<div class="row">
								<div class="col-md-12">
									<div class="input">
										<div class="inputLabel">Your Thoughts</div>
										<textarea name='reviewText' class='reviewText' id="ReviewTextError"></textarea>	
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="input">
										<div class="errorMessage"></div>
										<div class="inputLabel">Are you human? <span class="required">*</span></div>
										<div class="row">
											<div class="col-md-6">
												<img id="CaptaImage" src="<?php echo PATH ?>view/captcha.php" />
												<input type="text" name="reviewCapta" style='margin-top:10px' id="CaptaCheckError" />
											</div>
											<div class="col-md-6">
												<div class='errorMessage'></div>
											</div>
										</div>

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<input type='submit' value='Save Review' class='SaveReviewButton' />
								</div>
							</div>
						</div>	
					</form>
				</div>
				<div id="ReviewContentAdded" style='display:none;'>
					Your Review has been entered, thank you for your input!
				</div>
			</div>
		</div>
	</div>
	<script type="application/ld+json">
		{
	 		"@context": "http://schema.org",
	  		"@type": "BreadcrumbList",
	  		"itemListElement": [
	  			{
	    			"@type": "ListItem",
	    			"position": 1,
	    			"item": {
	     				"name": "Dillon Brothers Shopping Center"
	    			}
	  			},{
	    			"@type": "ListItem",
	    			"position": 2,
	    			"item": {
	     				"name": "Dillon Brothers Shopping Center Category <?php echo $this -> productSingle -> ProductCategory ?>"
	    			}
	  			},{
		    		"@type": "ListItem",
		    		"position": 3,
		    		"item": {
		     			"@id": "<?php echo $this -> productSingle -> GetURL(); ?>",
		      			"name": "<?php echo $this -> productSingle -> productName ?>"
		    	  	}
		    	}
		    ]
		}
	</script>
	
	
	<script type="application/ld+json">
		{
			"@type": "Product",
			"@context": "http://schema.org",
			"name": "<?php echo $this -> productSingle -> productName ?>",
			"category": "<?php echo $this -> productSingle -> ProductCategory ?>",
			"url": "<?php echo $this -> productSingle -> GetURL(); ?>",
			<?php if(count($this -> productSingle ->  ProductReviews) > 0): ?>
				"aggregateRating": {
					"@type": "AggregateRating",
					"ratingValue": "<?php echo $this -> productSingle -> AverageRating ?>",
					"reviewCount": "<?php echo count($this -> productSingle ->  ProductReviews) ?>"
				},
				"review": [
				<?php  $c = 0; $reviewLength = count($this -> productSingle ->  ProductReviews); ?>
				<?php foreach($this -> productSingle ->  ProductReviews as $key => $reviewSingle): ?>
					{
						"@type": "Review",
						"author": "<?php echo $reviewSingle['reviewedByFirstName'] ?> <?php echo substr($reviewSingle['reviewedByLastName'], 0, 1) ?>",
						"datePublished": "<?php echo $reviewSingle['DateEntered'] ?>",
						"description": "<?php echo str_replace("<br />", '', $reviewSingle['reviewText']) ?>",
						"reviewRating": {
							"@type": "Rating",
							"bestRating": "5",
							"ratingValue": "<?php echo $reviewSingle['rating'] ?>",
							"worstRating": "1"
						}
					}<?php if($c != $reviewLength - 1) :?>,<?php endif; ?>
					<?php $c++ ?>	
				<?php endforeach; ?>	
			],
			<?php endif; ?>
			"description":"<?php echo str_replace("<br>", ' ', $this -> productSingle -> Description) ?>",
			"image": "<?php echo $largeImage ?>",
			<?php if(!empty($this -> productSingle -> SKUNumber)): ?>
				"sku": "<?php echo $this -> productSingle -> SKUNumber ?>",
				"offers": {
					"@type": "Offer",
					"availability": "http://schema.org/InStock",
					"price": "<?php echo number_format($this -> productSingle -> Price, 2); ?>",
					"priceCurrency": "USD"
				},
			<?php else: ?>
				"offers": [
					<?php  $offer = 0; $offerLength = count($this -> productSingle -> Sizes); ?>
					<?php foreach($this -> productSingle -> Sizes as $sizeSingle): ?>
						{
							"@type": "Offer",
							"availability": "http://schema.org/InStock",
							"price": "<?php echo number_format($this -> productSingle -> Price, 2); ?>",
							"priceCurrency": "USD",
							"sku": "#<?php echo $sizeSingle['PartNumber'] ?>"	
						}<?php if($offer != $offerLength - 1) :?>,<?php endif; ?>
					<?php $offer++ ?>				
					<?php endforeach; ?>
				
				]
			<?php endif; ?>
		}			
	</script>
	
	
</div>


<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe. 
         It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. 
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>


<script src="<?php echo PATH ?>public/js/photoswipe.min.js" type="text/javascript" type="text/javascript"></script>	
<script src="<?php echo PATH ?>public/js/photoswipe-ui-default.min.js" type="text/javascript" type="text/javascript"></script>

<script>
var initPhotoSwipeFromDOM = function(gallerySelector) {

    // parse slide data (url, title, size ...) from DOM elements 
    // (children of gallerySelector)
    var parseThumbnailElements = function(el) {
        var thumbElements = el.childNodes,
            numNodes = thumbElements.length,
            items = [],
            figureEl,
            linkEl,
            size,
            item;

        for(var i = 0; i < numNodes; i++) {

            figureEl = thumbElements[i]; // <figure> element

            // include only element nodes 
            if(figureEl.nodeType !== 1) {
                continue;
            }

            linkEl = figureEl.children[0]; // <a> element

            size = linkEl.getAttribute('data-size').split('x');

            // create slide object
            item = {
                src: linkEl.getAttribute('href'),
                w: parseInt(size[0], 10),
                h: parseInt(size[1], 10)
            };



            if(figureEl.children.length > 1) {
                // <figcaption> content
                item.title = figureEl.children[1].innerHTML; 
            }

            if(linkEl.children.length > 0) {
                // <img> thumbnail element, retrieving thumbnail url
                item.msrc = linkEl.children[0].getAttribute('src');
            } 

            item.el = figureEl; // save link to element for getThumbBoundsFn
            items.push(item);
        }

        return items;
    };

    // find nearest parent element
    var closest = function closest(el, fn) {
        return el && ( fn(el) ? el : closest(el.parentNode, fn) );
    };

    // triggers when user clicks on thumbnail
    var onThumbnailsClick = function(e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        var eTarget = e.target || e.srcElement;

        // find root element of slide
        var clickedListItem = closest(eTarget, function(el) {
            return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
        });

        if(!clickedListItem) {
            return;
        }

        // find index of clicked item by looping through all child nodes
        // alternatively, you may define index via data- attribute
        var clickedGallery = clickedListItem.parentNode,
            childNodes = clickedListItem.parentNode.childNodes,
            numChildNodes = childNodes.length,
            nodeIndex = 0,
            index;

        for (var i = 0; i < numChildNodes; i++) {
            if(childNodes[i].nodeType !== 1) { 
                continue; 
            }

            if(childNodes[i] === clickedListItem) {
                index = nodeIndex;
                break;
            }
            nodeIndex++;
        }



        if(index >= 0) {
            // open PhotoSwipe if valid index found
            openPhotoSwipe( index, clickedGallery );
        }
        return false;
    };

    // parse picture index and gallery index from URL (#&pid=1&gid=2)
    var photoswipeParseHash = function() {
        var hash = window.location.hash.substring(1),
        params = {};

        if(hash.length < 5) {
            return params;
        }

        var vars = hash.split('&');
        for (var i = 0; i < vars.length; i++) {
            if(!vars[i]) {
                continue;
            }
            var pair = vars[i].split('=');  
            if(pair.length < 2) {
                continue;
            }           
            params[pair[0]] = pair[1];
        }

        if(params.gid) {
            params.gid = parseInt(params.gid, 10);
        }

        return params;
    };

    var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
            gallery,
            options,
            items;

        items = parseThumbnailElements(galleryElement);

        // define options (if needed)
        options = {

            // define gallery index (for URL)
            galleryUID: galleryElement.getAttribute('data-pswp-uid'),

            getThumbBoundsFn: function(index) {
                // See Options -> getThumbBoundsFn section of documentation for more info
                var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                    rect = thumbnail.getBoundingClientRect(); 

                return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
            }

        };

        // PhotoSwipe opened from URL
        if(fromURL) {
            if(options.galleryPIDs) {
                // parse real index when custom PIDs are used 
                // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                for(var j = 0; j < items.length; j++) {
                    if(items[j].pid == index) {
                        options.index = j;
                        break;
                    }
                }
            } else {
                // in URL indexes start from 1
                options.index = parseInt(index, 10) - 1;
            }
        } else {
            options.index = parseInt(index, 10);
        }

        // exit if index not found
        if( isNaN(options.index) ) {
            return;
        }

        if(disableAnimation) {
            options.showAnimationDuration = 0;
        }

        // Pass data to PhotoSwipe and initialize it
        gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    };

    // loop through all gallery elements and bind events
    var galleryElements = document.querySelectorAll( gallerySelector );

    for(var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute('data-pswp-uid', i+1);
        galleryElements[i].onclick = onThumbnailsClick;
    }

    // Parse URL and open gallery if it contains #&pid=3&gid=1
    var hashData = photoswipeParseHash();
    if(hashData.pid && hashData.gid) {
        openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
    }
};

// execute above function
initPhotoSwipeFromDOM('.my-gallery');
  </script>


