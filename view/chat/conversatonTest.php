<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html amp lang="en"> <!--<![endif]-->
	<head>
	
		<title>Start Chatting with our Employees | Dillon Brothers</title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
		<meta name="robots" content="noindex">
		<meta http-equiv="Cache-control" content="public">
		<meta charset="utf-8">

		<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		
		
		<link rel="stylesheet" href="<?php echo PATH ?>public/css/ChatController.css" />
		
		
		<script src="//media.twiliocdn.com/taskrouter/js/v1.9.4/taskrouter.min.js" type="text/javascript" ></script>
		<script src="https://media.twiliocdn.com/sdk/js/chat/v1.0/twilio-chat.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js" type="text/javascript" ></script>
									
		<script type="text/javascript" src="<?php echo PATH ?>public/js/Globals.js"></script>
		<script type="text/javascript" src="<?php echo PATH ?>public/js/ChatController.js"></script>
		
		<script type='text/javascript'>
			$(document).ready(function() {
				ChatController.initializeConversation();
			});
		</script>
		
					
				
		
	</head>
<body>
<div class="container" style='margin-top:10px;'>
	<div class="row">
		<div class="col-md-12">
			<div id="messagesLogged"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<textarea id="UserNewMessage" style='width:100%; height:200px;'></textarea>
		</div>
	</div>
	
</div>

</body>
</html>