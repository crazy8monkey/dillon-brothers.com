<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html amp lang="en"> <!--<![endif]-->
	<head>
	
		<title>Start Chatting with our Employees | Dillon Brothers</title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
		<meta name="robots" content="noindex">
		<meta http-equiv="Cache-control" content="public">
		<meta charset="utf-8">

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		
		
		<link rel="stylesheet" href="<?php echo PATH ?>public/css/ChatController.css" />
		<link rel="stylesheet" href="<?php echo PATH ?>public/css/style.css" />	
      
		<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>							
		<script type="text/javascript" src="<?php echo PATH ?>public/js/Globals.js"></script>
		<script type="text/javascript" src="<?php echo PATH ?>public/js/maskedInput.js"></script>
		<script type="text/javascript" src="<?php echo PATH ?>public/js/ChatController.js"></script>
		
		<script src="https://use.typekit.net/kpv4owm.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
      
		<script>
			$(document).ready(function() {
				ChatController.initializeStartForm();
			})
		</script>
					
				
		
	</head>
<body>
<div class="ChatBoxHeader">
	<div class='imgContainer'>
		<img src='<?php echo PHOTO_URL ?>Stores/<?php echo $this -> chatchannel -> StoreImage?>' />	
	</div>
</div>	
	
<div class="container" style='margin-top:10px;'>
	<div class="row">
		<div class="col-md-12" style='margin-bottom:20px;'>
			<h1>Please type in your information</h1>
		</div>
	</div>
	<form method='post' action="<?php echo PATH ?>ajax/startchat" id="StartChatForm">
		<input type='hidden' name='worker' value='<?php echo $_GET['id'] ?>' />
		<input type='hidden' name='channel' value='<?php echo $_GET['channel'] ?>' />
		<div class="row">
			<div class="col-xs-6">
				<div id="inputID1">
					<div class="errorMessage"></div>
					<div class="inputLine">
						<div class="inputLabel">Your First Name: <span class='requiredIndicator'>*</span></div>
						<input type="text" name="yourFirstName" onchange="Globals.removeValidationMessage(1)" />
					</div>
				</div>					
			</div>
			<div class="col-xs-6">
				<div id="inputID2">
					<div class="errorMessage"></div>
					<div class="inputLine">
						<div class="inputLabel">Your Last Name: <span class='requiredIndicator'>*</span></div>
						<input type="text" name="yourLastName" onchange="Globals.removeValidationMessage(2)" />
					</div>
				</div>					
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<div id="inputID3">
					<div class="errorMessage"></div>
					<div class="inputLine">
						<div class="inputLabel">Your Phone Number: <span class='requiredIndicator'>*</span></div>
						<input type="text" name="yourPhoneNumber" class="yourPhoneNumber" onchange="Globals.removeValidationMessage(3)" />
					</div>
				</div>					
			</div>
			<div class="col-xs-6">
				<div id="inputID4">
					<div class="errorMessage"></div>
					<div class="inputLine">
						<div class="inputLabel">Your Email: <span class='requiredIndicator'>*</span></div>
						<input type="text" name="yourEmail" onchange="Globals.removeValidationMessage(4)" />
					</div>
				</div>					
			</div>
		</div>	
		<div class="row">
			<div class="col-md-12">
				<input type='submit' value='Start Chat' class="blueButton startChatButton" />
			</div>
		</div>
	</form>
	
</div>

</body>
</html>