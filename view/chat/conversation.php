<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html amp lang="en"> <!--<![endif]-->
	<head>
	
		<title>Start Chatting with our Employees | Dillon Brothers</title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
		<meta name="robots" content="noindex">
		<meta http-equiv="Cache-control" content="public">
		<meta charset="utf-8">

		<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		
		<link rel="stylesheet" href="<?php echo PATH ?>public/css/style.css" />
		<link rel="stylesheet" href="<?php echo PATH ?>public/css/ChatController.css" />
		
		<script src="//media.twiliocdn.com/taskrouter/js/v1.9.4/taskrouter.min.js" type="text/javascript" ></script>
		<script src="https://media.twiliocdn.com/sdk/js/chat/v1.0/twilio-chat.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js" type="text/javascript" ></script>
									
		<script type="text/javascript" src="<?php echo PATH ?>public/js/Globals.js"></script>
		<script type="text/javascript" src="<?php echo PATH ?>public/js/ChatController.js"></script>
		
      	<script src="https://use.typekit.net/kpv4owm.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
      
		<script type='text/javascript'>
			$(document).ready(function() {
				ChatController.initializeConversation('<?php echo $this -> taskDetails -> CustomerName?>',
													  '<?php echo $this -> taskDetails -> ChannelFriendlyName?>',
													  '<?php echo $this -> taskDetails -> ChannelUniqueName?>',
													  '<?php echo count($this -> taskDetails -> CurrentMessages) ?>',
													  '<?php echo $this -> taskDetails -> GetSalesmanFullName()?>');
			});
		</script>
		
	</head>
<body>
<div class="ChatBoxHeader">
	<div class='imgContainer'>
		<div style='float:left'>
			<img src='<?php echo PHOTO_URL ?>Stores/<?php echo $this -> taskDetails -> StoreImage?>' />		
		</div>
		<div style='float:right'>
			<?php 
				if($this -> taskDetails -> UserProfile != NULL) {
					$userProfileImg = PHOTO_URL . 'Accounts/' . $this -> taskDetails -> UserProfile;	
				} else {
					$userProfileImg = PHOTO_URL . 'Accounts/no-picture.png';	
				}
			
				
			?>
			<div class="salesmanProfilePic" style='background:url(<?php echo $userProfileImg ?>) no-repeat center;'></div>
			
		</div>
		<div style='clear:both'></div>
	</div>
</div>	
<?php 

	switch($this -> taskDetails -> StoreID) {
		case 2:
			$storeClass = 'motorsportchat';
			break;
		case 3:
			$storeClass = 'harleychat';
			break;
		case 4:
			$storeClass = 'indianchat';
			break;	
	}


?>

<div id="ChatMessageContainer" class='<?php echo $storeClass ?>'>
	<div id="messagesLogged">
		<?php foreach($this -> taskDetails -> CurrentMessages as $msgSingle): ?>
			<?php 
				$attributes = json_decode($msgSingle -> attributes, true);
												//print_r($msgSingle);
				if($attributes['fromCustomer'] == 1) {
					$cssClass = "msgBubbleCustomer";
				} else {
					$cssClass = "msgBubbleDillonSalesMan";
					
				}									
			?>
			<div style="clear:both; margin-bottom:10px;">
				<div class="<?php echo $cssClass ?>">
					<?php echo $msgSingle -> body ?>
				</div> 
				<div style="clear:both"></div>
			</div>
			
			
		<?php endforeach; ?>
	</div>	
</div>


<div class="newMessageTextBoxContainer">
	<textarea id="UserNewMessage"></textarea>
	<button class="blueButton">Send Message</button>
</div>

</body>
</html>