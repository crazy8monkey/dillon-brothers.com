<?php 
	function base64url_encode($plainText) {
		return rtrim(strtr(base64_encode($plainText), '+/', '-_'), '='); ;	
		//return base64_encode($plainText);
	} 
?>

<div class="GrayBackground">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="ContentHeader">
					<h1>DILLON BROTHERS BLOG</h1>
				</div>
			</div>
		</div>
	</div>	
</div>
<div class="container" style='margin-top:20px;'>
	<div class="row">
		<div class="col-md-9">
			
			<div class="row">
				<div class="col-md-12">
					<?php if(isset($this -> categoryView)) : ?>
						<div class="CategorizedHeaders">
							<div>Category</div>
							<h2><?php echo strtoupper($this -> categoryName) ?></h2>
						</div>
					<?php endif; ?>
					<?php if(isset($this -> AuthorName)) : ?>
						<div class="CategorizedHeaders">
							<div>Written By</div>
							<h2><?php echo strtoupper($this -> AuthorName) ?></h2>	
						</div>
						
					<?php endif; ?>	
				</div>
			</div>
			<?php if(count($this -> blogposts['blogposts']) > 0): ?>
				<?php foreach($this -> blogposts['blogposts'] as $post): ?>
					<?php 
					  if($post['OptionalBlogSEOUrl'] == NULL) {
					  	$url = PATH . 'blog/post/' . $post['blogPostSEOurl'];
					  } else {
					  	$url = PATH . $post['OptionalBlogSEOUrl'];
					  }
					
					?>
					<div class="row">
						<div class="col-md-12">
							<div class="postSingle">
								<div class="postContent">
									<div class="row">
										<div class="col-md-4">
											<div class="imageObject">
												<?php 
													$mainImage = PHOTO_URL . $post['ParentDirectory'] . '/blog/' . $post['albumFolderName'] . '/' . $post['photoName'] . '-m.'. $post['ext'];
													list($imageWidth, $imageHeight) = getimagesize($mainImage);
													
													if($post['CommentCount'] == 1) {
														$commentText = $post['CommentCount'] . ' Comment';
													} else {
														$commentText = $post['CommentCount'] . ' Comments';
													}
													
												?>
												<a href="<?php echo $url ?>">
													<img src='<?php echo $mainImage?>' width='100%' alt='<?php echo $post['altText'] ?>' title='<?php echo $post['title'] ?>' />	
												</a>
											</div>
										</div>
										<div class="col-md-8">
											<div class="blogName">
												<?php echo $post['blogPostName']; ?>	
											</div>
											<div class="detailSection" style='margin-bottom: 5px;'>
												<i class="fa fa-calendar-o"  aria-hidden="true"></i>Published on <?php echo $this-> recordedTime -> formatShortDate($post['publishedDate']) ?>
											</div>
											<div class="detailSection">
												<i class="fa fa-comment" aria-hidden="true"></i><?php echo $commentText; ?>
											</div>
											
										</div>
									</div>
									<div class="postDetails">
									
										<div style='width:100%'>
											<a href="<?php echo $url ?>">
												<div class="blueButton" style='float:left;'>
													Read More	
												</div> 
											</a>
											<?php if(!isset($this -> AuthorName)): ?>
												<?php 
													if($post['showUserNameOnPublish'] == 0) {
														$writtenBy = "<a href='" . PATH . "blog/author/dillon-brothers'>Dillon Brothers</a>";	
													} else {
														$writtenBy = "<a href='" . PATH . "blog/author/" . base64url_encode($post['userID'])  . "'>". $post['firstName'] . ' ' . substr($post['lastName'], 0, 1) . '</a>';	
													}
												?>
												<div class="detailSection" style='margin-top: 9px; float: left; margin-left: 15px;'>
													<i class="fa fa-pencil" aria-hidden="true"></i>Written By <?php echo $writtenBy?>
												</div>		
													
											<?php endif; ?>
											<div style='clear:both'></div>
										</div>
										
								</div>
									
								</div>
								
								
								
								
								
								
								
								<div class='readMore'></div>	
								
								<?php if(!isset($this -> categoryView)) : ?>
									<?php 
									$categories = array();
									$linkedCategoryNames = explode(",", $post['BlogCategoryNames']);
									$linkedCategoryURLS = explode(",", $post['BlogCategorySEOUrl']);
									
									foreach($linkedCategoryNames as $key => $category) {
										array_push($categories, array('siteURL' => $linkedCategoryURLS[$key],
																	  'categoryName' => $linkedCategoryNames[$key]));
									}
									
									
									?>
									<div class="categoryLinks">
										<?php foreach($categories as $categorySingle) :?>
											<a href='<?php echo PATH ?>blog/category/<?php echo $categorySingle['siteURL'] ?>'><div class="link"><?php echo $categorySingle['categoryName'] ?></div></a>
										<?php endforeach; ?>
										<div style="clear:both"></div>
									</div>
									
								<?php endif; ?>		
								
								<script type="application/ld+json">
									{
										"@context":"http://schema.org",
										"@type":"BlogPosting",
									    "headline":"<?php echo $post['blogPostName'] ?>",
									    "articleBody": "<?php echo $post['blogPostPreview'] ?>",
									    "author": {
								        	"@type":"Person",
								        	"name": "<?php echo $post['firstName'] . ' ' . substr($post['lastName'], 0, 1)?>"
								       },
								       "datePublished":"<?php echo $post['publishedDateFull'] ?>",
									   "image" : {
								         	"@type":"ImageObject",
								         	"url" : "<?php echo $mainImage ?>",
								         	"height": "<?php echo $imageWidth ?>px",
								            "width": "<?php echo $imageHeight ?>px"
								       },
								       "commentCount": "<?php echo $post['CommentCount'] ?>",
		     						   "articleSection": "<?php echo $post['BlogCategoryNames'] ?>",
		     						   "publisher" : { 
									     	"@type":"Organization",
									     	"name": "Dillon Brothers",
									        "logo": {
								            	"@type":"ImageObject",
								                "url": "<?php echo PATH ?>/public/images/DillonBrothers.png"
								       		}	        
									   },
									   "dateModified":"<?php echo $post['dateModifiedDate'] ?>",
		     						   "mainEntityOfPage" : "<?php echo PATH ?><?php echo $post['blogPostSEOurl'] ?>"
		     						   
		     						   <?php 
		     						   
			     						   	$blogComments = array(); 
		  		
											if($post['CommentFullNames'] != NULL) {
												$commentNames = explode(",", $post['CommentFullNames']);
												$commentPublished = explode(",", $post['CommentDatePublishes']);
												$commentNotes = explode(",", $post['CommentNotesTotal']);
												
												foreach($commentNames as $key => $commentSingle) {
													array_push($blogComments, array("FullName" => $commentNames[$key],
																					"Published" => $commentPublished[$key],
																					"CommentContent" => $commentNotes[$key]));
												}
												
											}	
								  		
								  		?>
								  		
								  		<?php if(count($blogComments) > 0): ?>
										 ,"Comment": [
										 	<?php  $c = 0; $commentLength = count($blogComments); ?>
										 	<?php foreach ($blogComments as $comment): ?>
										 		{
										 			"@type": "Comment",
										            "creator":  {
										            	"@type": "Person",
										                "name": "<?php echo $comment['FullName'] ?>" 
										            },
										            "datePublished": "<?php echo $comment['Published'] ?>",
										            "description" : "<?php echo $comment['CommentContent'] ?>"
										 		}<?php if($c != $commentLength - 1) :?>,<?php endif; ?>
														<?php $c++ ?>		
										 	<?php endforeach; ?>
										 ]
										 <?php endif; ?>
		     						   
									}
								</script>
								
							</div>
	
						</div>
					</div>
				<?php endforeach; ?>
				<div class="row">
					<div class="col-md-12" style='margin-bottom:20px;'>
						<?php echo $this -> blogposts['pagination'] ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" style='margin-bottom:20px;'>
						<?php echo $this -> blogposts['mobile-pagination'] ?>
					</div>
				</div>
				
			<?php else: ?>
				<div class='row'>
					<div class="col-md-12">
						<?php if(isset($this -> categoryView)) : ?>
							There are no blog posts in this category
						<?php else: ?>
							There are no blog posts entered
						<?php endif; ?>	
					</div>				
				</div>
			<?php endif; ?>
			
		</div>
		<div class="col-md-3">
			<div class="CategoryLinkSection">
				<div style='font-size: 20px; margin-bottom: 10px; font-weight:bold;'>Categories</div>	
				<div class="desktopCategory">
					<?php 
						$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
						$options = '';
					?>
					<a href="<?php echo PATH ?>blog"><div class='categoryLink'>All</div></a>	
					<?php foreach($this -> categories as $category): ?>
						<a href="<?php echo PATH ?>blog/category/<?php echo $category['blogCategorySEOUrl'] ?>"><div class='categoryLink'><?php echo $category['blogCategoryName'] ?></div></a>
						<script type="application/ld+json">
						{
							"@context":"http://schema.org",
							"@type": "SiteNavigationElement",
							"name": "<?php echo $category['blogCategoryName'] ?>: Dillon Brothers Blog Category",
							"url" : "<?php echo PATH ?>blog/category/<?php echo $category['blogCategorySEOUrl'] ?>"	
						}
						</script>
						<?php 
							$selected = '';
							if(strpos($url, $category['blogCategorySEOUrl']) !== false) {
								$selected .= ' selected ';	
							}
							$options .='<option ' . $selected . ' value="' . PATH .'blog/category/' . $category['blogCategorySEOUrl'] .'">' . $category['blogCategoryName']. '</option>';
						?>
					<?php endforeach; ?>
					<a href="<?php echo PATH ?>blog/category/uncategorized"><div class='categoryLink'>Uncategorized</div></a>
				</div>
				<div class="mobileCategoryLinks">
					<select onchange="if (this.value) window.location.href=this.value">
						<option <?php echo strpos($url,'category') === false ? 'selected' : '' ?> value='<?php echo PATH ?>blog'>All</option>
						<?php echo $options ?>
						<option <?php echo strpos($url,'uncategorized') !== false ? 'selected' : '' ?> value='<?php echo PATH ?>blog/category/uncategorized'>Uncategorized</option>
					</select>
				</div>
				
			</div>
			
		</div>
	</div>
</div>






			



<script type="application/ld+json">
[{
	"@context":"http://schema.org",
	"@type": "SiteNavigationElement",
	"name": "All: Dillon Brothers Blog Category"
},{
	"@context":"http://schema.org",
	"@type": "SiteNavigationElement",
	"name": "Uncategorized: Dillon Brothers Blog Category",
	"url" : "<?php echo PATH ?>blog/category/uncategorized"
}
]
</script>


