<style type="text/css">
	body {background:#f3f3f3}
</style>

<?php 

$blogPostImage = $this -> postSingle -> GetBlogImage();
list($imageWidth, $imageHeight) = getimagesize($blogPostImage);
?>


<div id="BlogPreviewImage" style='background:url(<?php echo $blogPostImage?>) no-repeat center'>
	<div class="BlogDetails">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="content">
						<div class="text">
							<?php echo $this -> postSingle -> blogPostName ?>	
						</div>
						<div class="detailContent">
							<div class="detailContainer">
								<i class="fa fa-pencil icon" data-toggle="tooltip" data-placement="bottom" title="" aria-hidden="true" data-original-title="Written By"></i><span><?php echo $this -> postSingle -> CreatedByFullName() ?></span>
							</div>
							<div class="detailContainer">
								<i class="fa fa-calendar-o icon" data-toggle="tooltip" data-placement="bottom" title="" aria-hidden="true" data-original-title="Published Date" style='margin-left: 2px;'></i><span><?php echo $this-> recordedTime -> formatShortDate($this -> postSingle -> publishedDate) ?></span>
							</div>
						</div>
					</div>
			
					
				</div>
			</div>
		</div>
		
	</div>
	
</div>
<div class="BlogPostContent">
	<div class="container">
		<div class="row">
			<div class="col-md-12" style="margin:15px 0px 20px 0px;">
				<a href="<?php echo PATH ?>blog">Blog</a><span style="margin:0px 5px;"><i class="fa fa-caret-right" aria-hidden="true"></i></span><?php echo $this -> postSingle -> blogPostName ?>		
			</div>
		</div>
				
		<div class="row">
			<div class="col-md-12">
				<?php echo $this -> postSingle -> blogPostContent ?><br /><br />
				#dillonbrothers #omaha <?php echo $this -> postSingle -> KeyWords ?>
			</div>			
		</div>
		<div class="row">
			<div class="col-md-12" style="margin: 10px 0px 25px 0px;">
				<?php
				
				$facebookShareURL = 'https://www.facebook.com/sharer/sharer.php?u=' . urlencode(PATH . 'blog/' . $this -> postSingle -> blogPostSEOUrl) . 
										  '&amp;title=' . urlencode($this -> postSingle -> blogPostName . ': Dillon Brothers') . 
										  '&amp;description=' . urlencode($this -> postSingle -> BlogPostPreview). '&amp;' . 
										  '&amp;picture=' . $blogPostImage . '&amp;src=sdkpreparse'; 
										  
				$twitterShareURL = "http://twitter.com/intent/tweet?status=" . urlencode($this -> postSingle -> blogPostName . '. ' . PATH . 'blog/' . $this -> postSingle -> blogPostSEOUrl) . " %20%23dillonbrothers"					  
				?>
				
				<a onclick="window.open('<?php echo $facebookShareURL ?>','sharer','height=500,width=500');" href="javascript:void(0);" style="text-decoration:none;">
					<div class='facebookShare'><img src="<?php echo PATH ?>public/images/facebookIcon.png" data-toggle="tooltip" data-placement="bottom" title="Share on Facebook" /></div>
				</a>
				
				<a onclick="window.open('<?php echo $twitterShareURL ?>','sharer','height=300, width=500');" href="javascript:void(0);" style="text-decoration:none;">
					<div class="twitterShare"><img src="<?php echo PATH ?>public/images/twitter.png" data-toggle="tooltip" data-placement="bottom" title="Share on Twitter"></div>
				</a>
				<div style="clear:both"></div>
			</div>		
				
		</div>
		
	</div>
</div>
<div class="container">
	
	
	<div class="row">
		<div class="col-md-12">
			<div class="commentsHeader">Comments</div>
			<?php if(count($this -> comments) > 0): ?>
				<?php foreach($this -> comments as $comment): ?>
					<div class="row">
						<div class="col-md-12">
							<div class="CommentSingleElement">
								<div class="commentIndicator">
									"<?php echo $comment['commentNotesSection'] ?>"
									<div class="carot"></div>
								</div>
								<div class="commentByDetails">
									<?php echo $comment['commentFullName']; ?> | <?php echo $this-> recordedTime -> formatShortDate($comment['commentEnteredDate']) . ' ' . $this-> recordedTime -> formatMinuteSeconts($comment['commentEnteredTime']) ?>									
								</div>								
							</div>

						</div>
					</div>
					
					<script type="application/ld+json">
					{
						"@context":"http://schema.org",
						"@type": "UserComments",
						"creator": "<?php echo $comment['commentFullName']; ?>",
				        "commentTime": "<?php echo $comment['commentDatePublished']; ?>",
				        "commentText": "<?php echo $comment['commentNotesSection']; ?>" 		
					}
					</script>
					
				<?php endforeach; ?>
			<?php endif; ?>
			<div class="row">
				<div class="col-md-5">
					<div id="CommentForm">
						<form method="post" action="<?php echo PATH ?>ajax/submitComment" id="BlogCommentForm">
							<input type="hidden" name="post" value="<?php echo Hash::mc_encrypt($this -> postSingle -> blogPostID, ENCRYPTION_KEY)?>" />
							<div class="row">
								<div class="col-md-12">
									<div class="input">
										<div class="inputLabel">Full Name</div>
										<input type="text" name="commentFullName" />								
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="inputID1">	
										<div class="input">
											<div class="errorMessage"></div>
											<div class="inputLabel">Your Comment <span class="required">*</span></div>
											<textarea name="commentContent"></textarea>	
										</div>
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="inputID2">	
										<div class="input">
											<div class="errorMessage"></div>
											<div class="inputLabel">Are you human? <span class="required">*</span></div>
											<img src="<?php echo PATH ?>view/captcha.php" />
											<input type="text" name="commentCapta" style='margin-top:10px' />
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style="margin-bottom:30px;">
									<input type="submit" value="Submit Comment" class="subscribeButton">
								</div>
							</div>
						</form>
					</div>
					<div id="SuccessMessage" style="display:none;"></div>
				</div>

			</div>
		</div>
	</div>
	
	
</div>


<script type="application/ld+json">
[{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "name": "Dillon Brothers Blog"
    }
  },{
    "@type": "ListItem",
    "position": 2,
    "item": {
      "@id": "<?php echo PATH ?>blog/<?php echo $this -> postSingle -> blogPostSEOUrl ?>",
      "name": "<?php echo $this -> postSingle -> blogPostSEOUrl ?>"
    }
  }]
},
{
    "@context": "http://schema.org",
    "@type": "Blog",
    "blogPost": {
    	 "@type": "BlogPosting",
         "headline" : "<?php echo $this -> postSingle -> blogPostName ?>",
    	 "name": "<?php echo $this -> postSingle -> blogPostName ?>",
    	 "description": "<?php echo $this -> postSingle -> BlogPostPreview ?>#dillonbrothers #omaha <?php echo $this -> postSingle -> KeyWords ?>",
         "publisher": {
              "@type": "Organization",
              "name": "Dillon Brothers",
              "logo": {
	            	"@type":"ImageObject",
	                "url": "<?php echo PATH ?>public/images/DillonBrothers.png"
	            }	
          },
          "image": {
          	"@type":"ImageObject",
          	"url":"<?php echo $blogPostImage; ?>",
          	"height": "<?php echo $imageHeight; ?>",
          	"width": "<?php echo $imageWidth; ?>"
          },
          "author": "<?php echo $this -> postSingle -> CreatedByFullName() ?>",
          "datePublished": "<?php echo $this -> postSingle -> blogPublishedDateFull?>",
          "dateModified": "<?php echo $this -> postSingle -> modifiedDateFull?>",
          "commentCount": "0",
          "mainEntityOfPage": "<?php echo PATH ?>blog/<?php echo $this -> postSingle -> blogPostSEOUrl ?>"
    }
    
    
}
]

</script>



