<div id="RewardsImage">
	<img src="<?php echo PATH ?>public/images/vib-rewards-banner.jpg" />
</div>



<div class="container">	
	<div class="row">
		<div class="col-md-12">
			<a href="javascript:void(0);" onclick="RewardsController.OpenLoginForm();" style='text-decoration:none;'>	
				<div class="LoginButton">
					Login to see your VIB Balance!
				</div>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 ContentHeader">
			<h1>VIB REWARDS</h1>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="PageSection">
				<h2><strong style="color:#c30000">V.I.B Rewards Points are redeemable at all our locations!</strong></h1>	
				Welcome to the VIB Rewards Program! This program is our way of thanking our customers for choosing Dillon Brothers as their motorsports headquarters. 
				This program is offered to all customers at no cost and is subject to the program terms and conditions.
			</div>
			
			<div class="PageSection">
				<div class="secondarHeader">
					How It Works
				</div>
				You will receive 1 point for every $1.00 you spend on parts, general merchandise, Motorclothes® and service work at Dillon Brothers Harley-Davidson, Dillon Brothers MotorSports or 
				Dillon Brothers Indian Motorcycle. Your first Rewards Certificate is available at 100 points. You may choose to accumulate points over the course of the program period. Once you 
				decide to redeem a Rewards Certificate, the points will automatically subtract from your total. <br />(***See items excluded from rewards in terms and conditions***).<br />
				<ul>
					<li>Motorcycle purchases also qualify for the Rewards Program. You will also receive 500 points for every new or used vehicle purchased at Dillon Brothers.</li>
					<li>The VIB Rewards Program is reset on March 1st of each year. It is important to redeem all reward certificates before March 1st as all reward point values 
				are set to "0" and accumulation of points begins again.</li>
					<li>VIB Rewards Punch Cards are no longer be valid and have no value.</li>
				</ul>
				
			</div>
			
			<div class="PageSection">
				<div class="secondarHeader">
					Dillon Brothers HOG Members and Active Military
				</div>
				Current members of our HOG Chapter and Active Military have a rewards point multiplier of 1.5. For every dollar spent, these customers will receive 1.5 points. ($100 spent = 150 points). 
				On promotion days that give a higher reward rate (Double Rewards) this multiplier will not be in effect. Vehicle purchases are also exempt from the multiplier.
			</div>
			
			<div class="PageSection">
				<div class="secondarHeader">
					How To Track Your Points
				</div>
				Points are accumulated through purchases and special promotions and will be posted for purchases in 24-48 hours and special promotions in 48-72 hours. 
				Bonus points may also be accumulated by attending certain events throughout the year. Log in to your VIB Rewards portal via our website to track your rewards points
			</div>
			
			<div class="PageSection">
				<div class="secondarHeader">
					How To Use Your Points
				</div>
				You can redeem your points by printing a reward certificate from your VIB Rewards portal accessed from our website. Your membership number and access code are 
				on the back of your card right under the barcode (membership is on top in the format SDXXXX). Points will be available for use on your next purchase after a rewards level is achieved.<br /><br />
				
				<strong>Points Earned</strong> = <strong>Certificate value</strong>
				<ul>
					<li>100 = $5.00</li>
					<li>300 = $15.00</li>
					<li>500 = $25.00</li>
					<li>1000 = $50.00</li>
					<li>1500 = $75.00</li>
					<li>2000 = $100.00</li>
					<li>5000 = $250.00</li>
					<li>10000 = $500.00</li>
					<li>Not Used = $0</li>					
				</ul>
				
			</div>
			
			<div class="PageSection" style='border-bottom:none;'>
				<div class="secondarHeader">
					Bonus Points
				</div>
				Customers may earn bonus points for special promotions. Bonus points can be earned by attending certain events and presenting your Rewards Card. Dillon Brothers may feature 
				bonus point shopping days at its discretion. In addition, bonus points may be available on purchases of certain items or categories. All bonus point promotions will be communicated via electronic media (email, text club, website).
			</div>
		</div>
	</div>
</div>
<div class="Disclaimer">
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				<div style='margin-bottom:10px;'><strong>Terms & Conditions</strong></div>
				Points are automatically credited to your Rewards account with each purchase at Dillon Brothers Harley-Davidson, MotorSports and Indian Motorcycle. Rewards are available 24-48 hours after purchase. 
				For every bonus event your card must be presented for credit and will be available 48-72 hours from the next business day.<br /><br />
				
				Points earned with the Dillon Brothers Rewards Program expire on March 1st of every calendar year. Points will not rollover to the following year. Reward cards can only be used 
				by the original customer who joined the program or a member of their immediate household. Picture ID will be required and only the Dillon Brothers Harley-Davidson VIB Rewards account holder may redeem Reward Certificates.
				
				<ul style='padding-left:15px; padding-top:10px;'>
					<li>Dillon Brothers VIB Rewards Certificates must be redeemed within 30 days of the print date and ID will be required.</li>
					<li>Points are awarded on discounted or clearance items in general merchandise, motorclothes and parts.</li>
					<li>The following items are excluded from earning VIB Rewards Points: sales tax, online or off-site purchases, gift cards, H-D Authorized Rentals, and Riding Academy Tuition.</li>
					<li>Points are not awarded for purchasing Pre-Paid Maintenance (PPM) plans, Extended Service Plans (ESP), extended warranties, insurance products, or any other items included in a vehicle deal.</li>
					<li>Points are not awarded for the following types of service work: collision and wreck repairs, warranty work and extended service plan work.</li>
					<li>Employees of Dillon Brothers Harley-Davidson are not eligible for the program. Anyone with a store discount for any reason will not be eligible to participate.</li>
					<li>Rewards points cannot be used towards the purchase of a motorcycle.
						Points and rewards have no cash value and rewards cannot be redeemed and placed on account.</li>
					<li>All points are given at the discretion of Dillon Brothers. Points can be changed, removed or added at any time and hold no cash value.</li>
					<li>Any or all discrepancies are decided by the Dillon Brothers management and the decision is final.</li>
					<li>Dillon Brothers reserves the right to discontinue VIB Rewards Program privileges and/or void all or a portion of a member's point balance if points have been issued, received, or redeemed 
						through human or computer error, fraud or theft, through illegal means, or in any manner inconsistent with the intent of the program. Dillon Brothers also reserves the right to modify or cancel the VIB Rewards Program.</li>
				</ul>
				
			</div>
		</div>
	</div>	
</div>


<div class="MoreThanRewardsLoginAccount">
	<a href="javascript:void(0);" onclick="RewardsController.CloseLoginForm();">
			<div class="closeLogin" data-toggle="tooltip" data-placement="right" title="Close">
				<i class="fa fa-times" aria-hidden="true"></i>
			</div>
		</a>
	<div class="LoginContent">
		<iframe id="iframeMain" src="http://Portal.MoreThanRewards.com/Login.aspx?SID=aaf36e15-88ec-49d8-9e4a-f2c27156c21a" style='border:none;'></iframe>
	</div>
</div>

<script type="application/ld+json">
{
	"@type": "ProgramMembership",
	"@context": "http://schema.org",
    "url": "http://dev.dillon-brothers.com/rewards",
    "image": "<?php echo PATH ?>public/images/vib-rewards-banner.jpg",
    "programName": "Dillon Brothers VIB (Very Important Biker) Rewards Program",
    "description": "Earn Rewards Points for all Dillon Brothers locations with our Very Important Biker Customer Rewards Program.",
    "hostingOrganization": {
    	"@type": "Organization",
		"@context": "http://schema.org",
		"location": <?php echo $this -> MotorSportSchemaLocation(); ?>
    }
}
</script>


