<!-- Schema.org markup for Google+ --> 
<?php 
if(isset($this -> SchemaMetaData)) {
	echo $this -> SchemeMetaTags($this -> SchemaMetaData);	
}

?>

<!-- Twitter Card data --> 
<?php 
	if(isset($this -> TwitterMetaData)) {
		echo $this -> TwitterMetaTags($this -> TwitterMetaData);		
	}
?> 

<!-- Open Graph data --> 
<?php 
	if(isset($this -> OpenGraphMetaData)) {
		echo $this -> OpenGraphMetaTags($this -> OpenGraphMetaData);	
	}
?>

<?php if(isset($this->Canonical)): ?>
	<!-- Google Canonical Link Markup --> 
	<link rel="canonical" href="<?php echo $this->Canonical; ?>" />
<?php endif; ?>



<!-- Google Authorship and Publisher Markup --> 
<link rel="publisher" href="https://plus.google.com/103831261532249725840/" />
<link rel="author" href="https://plus.google.com/103831261532249725840/" />
<meta name="language" content="english">