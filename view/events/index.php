<div class="container" style="padding-top:25px; padding-bottom:0px;">
	<div class="row">
		<div class="col-md-12 ContentHeader">
			<h1>DILLON BROTHERS EVENTS</h1>
		</div>
	</div>
</div>
<div class="grayBackground" style='padding:10px; 0px;'>
	<div class="container">
		<div class="row">
			<?php if($this -> showPDFLink == "show"): ?>
				<div class="col-md-9">
					Dillon Brothers Motorsports, Dillon Brothers Harley-Davidson, and Dillon Brothers Indian Motorcycles all love to be involved 
with what's happening. We always have a more than full event schedule. From bike nights, poker runs, to swap meets and 
concerts there's always some sort of motorcycle related fun happening.
				</div>
				<div class="col-md-3">
					<div style='font-size:24px; text-align:center'><?php echo date('Y') ?> Events PDF</div>
					<a href="<?php echo PHOTO_URL ?>EventPDFs/<?php echo $this -> pdfName ?>" target="_blank">
						<div class="donloadPDFLink">	
							<img src="<?php echo PATH ?>public/images/DownloadPDF.png" />
						</div>
					</a>			
				</div>
			<?php else: ?>	
				<div class="col-md-12">
					Dillon Brothers Motorsports, Dillon Brothers Harley-Davidson, and Dillon Brothers Indian Motorcycles all love to be involved 
	with what's happening. We always have a more than full event schedule. From bike nights, poker runs, to swap meets and 
	concerts there's always some sort of motorcycle related fun happening.
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12 SubHeader">
			<h2>UPCOMING EVENTS</h1>
		</div>
	</div>
	<?php if(count($this -> LatestEvents) > 0) : ?>
		<?php foreach(array_chunk($this -> LatestEvents, 3, true) as $eventArray) : ?>			
			<div class="row">
				<?php $prevSuperEventID = null; ?>
				<?php foreach($eventArray as $event) : ?>	
					
					
					<div class="col-md-4">
						<?php 
							$hyperLink = NULL;
							$EventUrlName = NULL;
							
							if($event['SubEvent'] == 1) {	
								$hyperLink = PATH . $event['SuperEventSEOUrl'] . '/#' . $event['seoUrl'];
								$EventUrlName = $event['SuperEventName'] . ': ' . $event['eventName'];
							} else {
								$hyperLink = PATH .  $event['seoUrl'];
								$EventUrlName = $event['eventName'];
							}
									
									
						?>
						<a href="<?php echo $hyperLink ?>">
							<div class="eventSingle">
								<?php
									$mainImage = PHOTO_URL . $event['ParentDirectory'] . '/events/' . $event['albumFolderName'] . '/'. $event['photoName'] . '-m.' . $event['ext'];
								?>
								
								<div class="imageContainer" style='background:url(<?php echo $mainImage ?>) no-repeat center'>
									<img src="<?php echo $mainImage ?>" alt="<?php echo $event['altText'] ?>" title="<?php echo $event['title'] ?>" style='width:100%' />		
									
								</div>
								<div class="eventName">
									<strong><?php echo $EventUrlName ?></strong>
								</div>
								<div class='DateOfEvent'>
									<?php echo $this -> recordedTime -> formatDate($event['startDate']) ?> / <?php echo $this -> recordedTime -> formatTime($event['startTime']) ?> - <?php echo $this -> recordedTime -> formatTime($event['endTime']) ?>
								</div>
								<div class="buttonContianer">
									<div class='blueButton' style='float:left'>
										More Info
									</div>
									<div style='clear:both'></div>
								</div>
							</div>
						</a>			
						
					</div>
					<?php $SuperEventStructuredData = json_decode($event['SuperEventStructuredData'], true) ?>
					<?php $structuredData = json_decode($event['StructuredDataInfo'], true) ?>
					<script type="application/ld+json">
					{
						"name": "<?php echo ($event['SubEvent'] == 1 ? $event['SuperEventName'] . ': ' . $event['eventName']: $event['eventName']) ?>",
						"startDate": "<?php echo $event['startDate'] . 'T' . $event['startTime'] ?>:00",
						"@context": "http://schema.org",
						"@type": "Event",
						"description" : "<?php echo $event['GoogleEventDescription'] ?>",
						"image": "<?php echo $mainImage ?>",
						"location": {
							"@type" : "Place",
							"name": "Dillon Brothers",
							"address": {
								"name" : "Dillon Brothers Omaha",
						 		"streetaddress" : "<?php echo $event['eventAddress'] ?>",
						 		"addressLocality" : "<?php echo $event['eventCity'] ?>",
						 		"addressRegion" : "Nebraska",
						 		"postalCode" : "<?php echo $event['eventZip'] ?>"
							},
							"geo": {
					          "@type": "GeoCircle",
					          "geoMidpoint": {
					            "@type": "GeoCoordinates",
					            "latitude": "<?php echo $structuredData['Latitude'] ?>",
					            "longitude": "<?php echo $structuredData['Longitude'] ?>"
					          },
					          "geoRadius": "100"
					        }		
						},
						"url": "<?php echo $hyperLink ?>",
						"performer": {
							"@type": "Thing", 
							"name": "<?php echo $structuredData['Performer'] ?>"
						},
						"offers": {
							"@type": "Offer",
							"price": "<?php echo $structuredData['EventPrice'] ?>",
							"priceCurrency": "USD",
							"url" : "<?php echo ($event['SubEvent'] == 1 ? PATH . $event['SuperEventSEOUrl'] . '/#' . $event['seoUrl'] : PATH . $event['seoUrl']) ?>",
							"availability" : "NA",
							"availabilityStarts" : "<?php echo $event['startDate'] . 'T' . $event['startTime'] ?>:00",
							"inventoryLevel" : "NA",
							"validFrom": "<?php echo $event['startDate'] . 'T' . $event['startTime'] ?>:00"
						},	
						"endDate": "<?php echo $event['endDate'] . 'T' . $event['endTime'] ?>:00",	  
						"url": "<?php echo ($event['SubEvent'] == 1 ? PATH . $event['SuperEventSEOUrl'] . '/#' . $event['seoUrl'] : PATH . $event['seoUrl']) ?>",
						"eventStatus" : "NA"
					}
					</script>
					
					<?php $prevSuperEventID = $event['SuperEventID']; ?>
				<?php endforeach; ?>
			</div>
				
		<?php endforeach; ?>	
		
	<?php else: ?>
		<div class="row">
			<div class="col-md-12">
				<div class="emptyEvents">There are no new current events at this time</div>
			</div>
		</div>
	<?php endif; ?>	
</div>
<div style='background:#424a5d' id="EventsCalendar">
	<div class="container">	
		<div class="row" style='margin-top: 20px;'>
			<div class="col-md-12">
				<div id="calendar"></div>
			</div>
		</div>
		<br />
	</div>	
</div>





