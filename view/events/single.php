<link rel="stylesheet" href="<?php echo PATH ?>public/css/photoswipe.css" type="text/css" />
<link rel="stylesheet" href="<?php echo PATH ?>public/css/default-skin.css" type="text/css" />

<?php 
	function in_array_r($needle, $haystack, $strict = false) {
	    foreach ($haystack as $item) {
	        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
	            return true;
	        }
	    }
	
	    return false;
	} 
?>


<div class="MainPhotoEvent">
	<div class="EventDetailWrapper">
		<img src="<?php echo $this -> EventSingle -> GetMainEventPhoto() ?>" />
	</div>
</div>	

<div class="container EventDetailWrapper">
	<div class="row">
		<div class="col-md-12" style="margin:15px 0px 0px 0px;">
			<a href="<?php echo PATH ?>events">Dillon Brothers Events</a><span style="margin:0px 5px;"><i class="fa fa-caret-right" aria-hidden="true"></i></span><?php echo $this -> MainEventTitle ?><span id="SubEvent"></span>		
		</div>
	</div>
</div>

<?php if(isset($this -> IsSubEvent)) :?>
		<?php 
			$SuperEventStructuredData = json_decode($this -> EventSingle -> StructuredData, true);
			$superEventID = PATH . $this -> EventSingle -> SeoURL . '/#' . $SuperEventStructuredData['SuperEventIDText']; 
		?>
		<script type="application/ld+json">
			{
				"name" : "<?php echo $this -> EventSingle -> eventName ?>",
				"startDate" : "<?php echo $this -> EventSingle -> startDate ?>T<?php echo $this -> EventSingle -> startTime ?>:00",
				"endDate" : "<?php echo $this -> EventSingle -> endDate ?>T<?php echo $this -> EventSingle -> endTime ?>:00",
				"@context" : "http://schema.org",
				"@type" : "socialEvent",
				"@id" : "<?php echo $superEventID?>",
				"description" : "<?php echo $this -> EventSingle -> SuperEventDescription ?>",
				"url" : "<?php echo PATH . $this -> EventSingle -> SeoURL ?>",
				"image" : "<?php echo PHOTO_URL . 'SuperEvents/' . $this -> EventSingle -> eventImage; ?>",
				"performer": {
					"@type": "Thing", 
					"name": "<?php echo $SuperEventStructuredData['Performer'] ?>"
				},
				"location": {
					"@type" : "Place",
					"name": "Dillon Brothers",
					"address": {
						"name" : "Dillon Brothers",
						"streetaddress" : "<?php echo $this -> EventSingle -> address?>",
						"addressLocality" : "<?php echo $this -> EventSingle -> city?>",
						"addressRegion" : "Nebraska",
						"postalCode" : "<?php echo $this -> EventSingle -> zip?>"
					},
					"geo": {
						"@type": "GeoCircle",
						"geoMidpoint": {
							"@type": "GeoCoordinates",
						    "latitude": "<?php echo $SuperEventStructuredData['Latitude'] ?>",
						    "longitude": "<?php echo $SuperEventStructuredData['Longitude'] ?>"
						},
						"geoRadius": "100"
					}		
				},
				"offers" : {
					"@type" : "Offer",
					"price": "<?php echo $SuperEventStructuredData['EventPrice'] ?>",
					"priceCurrency" : "USD",
					"url" : "<?php echo PATH . $this -> EventSingle -> SeoURL ?>",
					"availability" : "NA",
					"availabilityStarts" : "<?php echo $this -> EventSingle -> startDate ?>T<?php echo $this -> EventSingle -> startTime ?>:00",
					"inventoryLevel" : "NA",
					"validFrom": "<?php echo $this -> EventSingle -> startDate ?>T<?php echo $this -> EventSingle -> startTime ?>:00"
				},
				"eventStatus" : "NA"
			}
		</script>
	
	
	<div class="container EventDetailWrapper">
		<div class="row">
			<div class="col-md-12">	
				<div><h1>About <?php echo $this -> EventSingle -> eventName ?></h1></div>
				<div><?php echo $this -> EventSingle -> SuperEventDescription ?></div>			
			</div>
		</div>
	</div>
	<div class="grayBackground" style='padding: 10px 0px; margin-top:10px;'>
		<div class="container EventDetailWrapper">
			<div class="row">
				<div class="col-md-12">
					<div>
						<h4 class='CurrentSuperEventDates'>Current <?php echo date("Y") ?> Dates</h2>	
					</div>
					<div style='display:inline-block;'>	
					<?php $count = count($this -> SubEvents); ?>
					<?php foreach($this -> SubEvents as $key => $subEvent): ?>
						<div class="subDates">
							<?php echo $this -> recordedTime -> ShortDateNoYear($subEvent['startDate']) ?>
						</div>
						<?php if($key != $count - 1): ?>
							<div class="separator"></div>	
						<?php endif; ?>
					<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if(count($this -> SubEvents) > 0) :?>
		<?php foreach($this -> SubEvents as $subEvent): ?>
			<div id="<?php echo $subEvent['seoUrl']?>" style='display:none;'>
				<div class="SubEventBackground">
					<div class="container EventDetailWrapper">
						<div class="row">
							<div class="col-md-12">
								<div class="EventMainImage">
									<?php $mainImage = PHOTO_URL . $subEvent['ParentDirectory'] . '/events/' . $subEvent['albumFolderName'] . '/' . $subEvent['photoName'] . '-l.' . $subEvent['ext']; ?>
									<img src='<?php echo $mainImage ?>' alt="<?php echo $this -> EventSingle -> eventName ?> <?php echo $subEvent['altText'] ?>" title="<?php echo $this -> EventSingle -> eventName ?> <?php echo $subEvent['title'] ?>" width='100%' />
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="container EventDetailWrapper">
					<div class="row">
						<div class="col-md-12" style=''>
							<h2><?php echo $subEvent['eventName'] ?></h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style='margin-bottom:10px;'>
							<?php 
								$startDate = new DateTime($subEvent['startDate']);
								$endDate = new DateTime($subEvent['endDate']);
								if($startDate == $endDate) {
									echo $this -> recordedTime -> formatDate($subEvent['startDate']) . ' / ' . $this -> recordedTime -> formatTime($subEvent['startTime']) . ' - ' . $this -> recordedTime -> formatTime($subEvent['endTime']); 
								} else {
									echo $this -> recordedTime -> formatDate($subEvent['startDate']) . ' / ' .  $this -> recordedTime -> formatTime($subEvent['startTime']) . ' - ' . $this -> recordedTime -> formatDate($subEvent['endDate']) . ' / ' . $this -> recordedTime -> formatTime($subEvent['endTime']);
								}
								
							?>							
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style='margin-bottom:10px;'>
							<?php echo $subEvent['eventAddress']  . '<br />' . $subEvent['eventCity'] . ', ' . $subEvent['eventState'] . ' ' . $subEvent['eventZip'] ?>	
						</div>
					</div>
				</div>
				<div class="grayBackground EventHtmlContent">
					<div class="container EventDetailWrapper">
						<?php 
						$subEventJSON = json_decode(file_get_contents(JSON_EVENT_URL . $subEvent['eventHTMLJSON']), true); 
						usort($subEventJSON, function($a, $b) {
							return $a['Order'] - $b['Order'];
						});
						
						?>
						<?php foreach($subEventJSON as $row) : ?>
							<div class="row">
								<?php if(isset($row['Columns'])): ?>
									<?php usort($row['Columns'], function($a, $b) {
										return $a['Order'] - $b['Order'];
									}); ?>
									<?php foreach($row['Columns'] as $column) : ?>
										<div class="col-md-<?php echo $column['ClassID'] ?>">
											<div class="ColumnContent">
												<?php if(isset($column['Content'])) : ?>
													<?php foreach($column['Content'] as $content) : ?>
														
														<?php if($content['ContentType'] == "Header1") : ?>
															<h1><?php echo $content["Value"] ?></h1>							
														<?php endif; ?>
														
														<?php if($content['ContentType'] == "Header2") : ?>
														 	<h2><?php echo $content["Value"] ?></h2>
														<?php endif; ?>
														 							
														<?php if($content['ContentType'] == "Header3") : ?>
															<h3><?php echo $content["Value"] ?></h3>
														<?php endif; ?>
														
														<?php if($content['ContentType'] == "Text") : ?>
															<p><?php echo $content["Value"] ?></p>
														<?php endif; ?>
														
														<?php if($content['ContentType'] == "PhotoAlbum") : ?>
															<?php 
																$key = array_search($content["Value"], array_column($this -> albumsSelected, 'albumsSelected'));
																switch($this -> albumsSelected[$key]['AlbumType']) {
																	//events	
																	case 1:
																		$path = "/events/";	
																		break;
																	//blog
																	case 2:
																		$path = "/blog/";	
																		break;
																	case 3:
																		//misc
																		$path = "/misc/";
																		break;																		
																}
														
																$albumIDEncrypt = Hash::mc_encrypt($content["Value"], ENCRYPTION_KEY);
																
																$onclick="EventsController.AlbumView('" . $albumIDEncrypt . "')";
															?>
										
															<a href="javascript:void(0)" onclick="<?php echo $onclick ?>">
																<?php if($this -> albumsSelected[$key]['albumThumbNail'] != NULL): ?>
																	<div class="albumThumb" style='background:url(<?php echo PHOTO_URL . $this -> albumsSelected[$key]['ParentDirectory'] . $path . $this -> albumsSelected[$key]['albumFolderName'] . '/' . $this -> albumsSelected[$key]['photoName'] . '-m.' . $this -> albumsSelected[$key]['ext']?>)'>
																		<img src='<?php echo PHOTO_URL . $this -> albumsSelected[$key]['ParentDirectory'] . $path . $this -> albumsSelected[$key]['albumFolderName'] . '/' . $this -> albumsSelected[$key]['photoName'] . '-l.' . $this -> albumsSelected[$key]['ext']?>' alt='<?php echo $this -> albumsSelected[$key]['altText']?>' title='<?php echo $this -> albumsSelected[$key]['title']?>' />
																	</div>
																<?php else: ?>
																	<div class='NoThumbNail'>
																		<img src="<?php echo PATH ?>public/images/PhotoAlbumIcon.png" />	
																	</div>
																<?php endif; ?>
																<div class="viewAlbumText">
																	View Album
																</div>
																
															</a>
						
															
														<?php endif; ?>
														
														<?php if($content['ContentType'] == "Photo") : ?>
															<?php if (in_array_r($content["Value"], $this -> photosSelected)) : ?>
															
																<?php 
																	$key = array_search($content["Value"], array_column($this -> photosSelected, 'photosSelected')); 
																	switch($this -> photosSelected[$key]['AlbumType']) {
																		//events	
																		case 1:
																			$path = "/events/";	
																			break;
																		//blog
																		case 2:
																			$path = "/blog/";	
																			break;
																		case 3:
																		//misc
																		$path = "/misc/";
																			break;																		
																	}
																	 
																
																?>
																<?php 
																	$largeImage = PHOTO_URL . $this -> photosSelected[$key]['ParentDirectory'] . $path. '/' . $this -> photosSelected[$key]['albumFolderName'] . '/' . $this -> photosSelected[$key]['photoName'] . '-l.' . $this -> photosSelected[$key]['ext']; 
																
																	list($imageWidth, $imageHeight) = getimagesize($largeImage);
																?>
																<div id="SingleImage<?php echo $this -> photosSelected[$key]['photoID'] ?>">
																	<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
																		<a href="<?php echo $largeImage ?>" itemprop="contentUrl" data-size="<?php echo $imageWidth . 'x' . $imageHeight?>">
																			<div class="imageBorder">
																				<img src="<?php echo PHOTO_URL . $this -> photosSelected[$key]['ParentDirectory'] . $path .  $this -> photosSelected[$key]['albumFolderName'] . '/' . $this -> photosSelected[$key]['photoName'] . '-m.' . $this -> photosSelected[$key]['ext']; ?>" width='100%' />		
																			</div>
																			
																		</a>
																	</figure>
																</div>
																
															    
															    
															<?php endif; ?>
														<?php endif; ?>
														
													<?php endforeach; ?>
												<?php endif; ?>
											</div>
											
										</div>
									<?php endforeach; ?>
								<?php endif; ?>		
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				
				<?php $structuredData = json_decode($subEvent['StructuredDataInfo'], true) ?>
				<script type="application/ld+json">
					{
						"name": "<?php echo $subEvent['eventName'] ?>",
						"startDate": "<?php echo $subEvent['startDate'] . 'T' . $subEvent['startTime'] ?>:00",
						"@context": "http://schema.org",
						"@type": "SocialEvent",
						"@id" : "<?php echo PATH . $this -> EventSingle -> SeoURL ?>/#<?php echo $subEvent['seoUrl']?>",
						"description" : "<?php echo str_replace("<br />", "", $subEvent['GoogleEventDescription']) ?>",
						"image": "<?php echo $mainImage ?>",
						"superEvent": { "@id": "<?php echo $superEventID ?>" },
							
						"location": {
							"@type" : "Place",
							"name": "Dillon Brothers",
							"address": {
								"name" : "Dillon Brothers Omaha",
						 		"streetaddress" : "<?php echo $subEvent['eventAddress'] ?>",
						 		"addressLocality" : "<?php echo $subEvent['eventCity'] ?>",
						 		"addressRegion" : "Nebraska",
						 		"postalCode" : "<?php echo $subEvent['eventZip'] ?>"
							},
							"geo": {
					          "@type": "GeoCircle",
					          "geoMidpoint": {
					            "@type": "GeoCoordinates",
					            "latitude": "<?php echo $structuredData['Latitude'] ?>",
					            "longitude": "<?php echo $structuredData['Longitude'] ?>"
					          },
					          "geoRadius": "100"
					        }		
						},
						"performer": {
							"@type": "Thing", 
							"name": "<?php echo $structuredData['Performer'] ?>"
						},
						"offers": {
							"@type": "Offer",
							"price": "<?php echo $structuredData['EventPrice'] ?>",
							"priceCurrency": "USD",
							"url" : "<?php echo PATH . $this -> EventSingle -> SeoURL ?>/#<?php echo $subEvent['seoUrl']?>",
							"availability" : "NA",
							"availabilityStarts" : "<?php echo $subEvent['startDate'] . 'T' . $subEvent['startTime'] ?>:00",
							"inventoryLevel" : "NA",
							"validFrom": "<?php echo $subEvent['startDate'] . 'T' . $subEvent['startTime'] ?>:00"
						},	
						"endDate": "<?php echo $subEvent['endDate'] . 'T' . $subEvent['endTime'] ?>:00",	  
						"url": "<?php echo PATH . $this -> EventSingle -> SeoURL ?>/#<?php echo $subEvent['seoUrl']?>",
						"eventStatus" : "NA"
					}
				</script>
				
				
			</div>
			
		<?php endforeach; ?>
	<?php endif; ?>
	
<?php else: ?>
	<?php 
		$EventStructuredData = json_decode($this -> EventSingle -> StructuredData, true);
	?>
	<script type="application/ld+json">
		{
			"name" : "<?php echo $this -> EventSingle -> eventName ?>",
			"startDate" : "<?php echo $this -> EventSingle -> startDate ?>T<?php echo $this -> EventSingle -> startTime ?>:00",
			"endDate" : "<?php echo $this -> EventSingle -> endDate ?>T<?php echo $this -> EventSingle -> endTime ?>:00",
			"@context" : "http://schema.org",
			"@type" : "socialEvent",
			"description" : "<?php echo $this -> EventSingle -> GoogleEventDescription ?>",
			"url" : "<?php echo PATH . $this -> EventSingle -> SeoURL ?>",
			"image" : "<?php echo $this -> EventSingle -> GetMainEventPhoto(); ?>",
			"performer": {
				"@type": "Thing", 
				"name": "<?php echo $EventStructuredData['Performer'] ?>"
			},
			"location": {
				"@type" : "Place",
				"name": "Dillon Brothers",
				"address": {
					"name" : "Dillon Brothers",
					"streetaddress" : "<?php echo $this -> EventSingle -> address?>",
					"addressLocality" : "<?php echo $this -> EventSingle -> city?>",
					"addressRegion" : "Nebraska",
					"postalCode" : "<?php echo $this -> EventSingle -> zip?>"
				},
				"geo": {
					"@type": "GeoCircle",
					"geoMidpoint": {
						"@type": "GeoCoordinates",
					    "latitude": "<?php echo $EventStructuredData['Latitude'] ?>",
					    "longitude": "<?php echo $EventStructuredData['Longitude'] ?>"
					},
					"geoRadius": "100"
				}		
			},
			"offers" : {
				"@type" : "Offer",
				"price": "<?php echo $EventStructuredData['EventPrice'] ?>",
				"priceCurrency" : "USD",
				"url" : "<?php echo PATH . $this -> EventSingle -> SeoURL ?>",
				"availability" : "NA",
				"availabilityStarts" : "<?php echo $this -> EventSingle -> startDate ?>T<?php echo $this -> EventSingle -> startTime ?>:00",
				"inventoryLevel" : "NA"
			},
			"eventStatus" : "NA",
			"validFrom": "<?php echo $this -> EventSingle -> startDate ?>T<?php echo $this -> EventSingle -> startTime ?>:00"
		}
	</script>
	
	<div class="container EventDetailWrapper">
		<div class="row">
			<div class="col-md-12" style=''>
				<div><h1><?php echo $this -> EventSingle -> eventName ?></h1></div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this -> EventSingle -> GetScheduledTime() ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style="margin-top:10px;">
				<div>
					<?php echo $this -> EventSingle -> GetEventLocation() ?>	
				</div>
			</div>
		</div>
	</div>	
	<div class="EventHtmlContent" style='margin-top:10px;'>
		<style type='text/css'>
			<?php echo $this -> pageLayout['gjs-css'] ?>
		</style>
		<?php echo $this -> pageLayout['gjs-html'] ?>
	</div>
	
<?php endif; ?>





<div class="container EventDetailWrapper">
		
	<?php if(count($this -> relatedAlbums) > 0) :?>
		<div style='margin-bottom:30px;'>
			
		<div class="row">
			<div class="col-md-12">
				<div style='margin-bottom:20px;'>
					<h2 style='margin-top:15px;'>View Albums from Previous <?php echo $this -> EventSingle -> eventName ?>s</h2>	
				</div>
			</div>
		</div>
		<?php foreach(array_chunk($this -> relatedAlbums, 4, true) as $setAlbums) : ?>
			<div class="row">		
				<?php foreach($setAlbums as $album): ?>
					<?php 
						//$albumIDEncrypt = urlencode(Hash::mc_encrypt($album['albumID'], ENCRYPTION_KEY));
						$albumIDEncrypt = Hash::mc_encrypt($album['albumID'], ENCRYPTION_KEY);
						//$albumIDEncrypt = $album['albumID'];
						$onclick="EventsController.AlbumView('" . $albumIDEncrypt . "')";
					?>
					<div class="col-md-3">
						<a href="javascript:void(0)" onclick="<?php echo $onclick ?>">
							<div class="relatedAlbumSingle">
								<?php if($album['photoName'] != NULL): ?>
									<?php			
										switch($album['AlbumType']) {
											//events	
											case 1:
												$path = "/events/";	
												break;
											//blog
											case 2:
												$path = "/blog/";	
												break;
											case 3:
												//misc
												$path = "/misc/";
												break;																		
										}					
									?>
									<div class='relatedPhotoAlbum' style='background:url(<?php echo PHOTO_URL . $album['ParentDirectory'] . $path. '/' . $album['albumFolderName'] . '/' . $album['photoName'] . '-m.' . $album['ext']; ?>)  no-repeat center'>
										<img src='<?php echo PHOTO_URL . $album['ParentDirectory'] . $path. '/' . $album['albumFolderName'] . '/' . $album['photoName'] . '-m.' . $album['ext']; ?>' alt='<?php echo $album['altText']; ?>' title='<?php echo $album['title']; ?>' style='opacity: 0;' />
									</div>						
								<?php else: ?>
									<div class='relatedPhotoAlbumNoThumb' style='background:#f7f7f7 url(<?php echo PATH ?>public/images/PhotoAlbumIcon.png)  no-repeat center'></div>
								<?php endif; ?>
								<div class="relatedAlbumtext"><?php echo $album['albumName'] ?> / <?php echo $album['ParentDirectory'] ?></div>
							</div>
						</a>										
					</div>		
				<?php endforeach; ?>	
			</div>
		<?php endforeach; ?>
		</div>
	<?php endif; ?>
	
</div>


<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe. 
         It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. 
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>

<?php if(count($this -> photosSelected) > 0): ?>

<?php 
	$photoIDElements = NULL;
	foreach($this -> photosSelected as $photoSingle) {
		$photoIDElements .='#SingleImage' . $photoSingle['photoID'] . ',';
	}	

?>

<script>
var initPhotoSwipeFromDOM = function(gallerySelector) {

    // parse slide data (url, title, size ...) from DOM elements 
    // (children of gallerySelector)
    var parseThumbnailElements = function(el) {
        var thumbElements = el.childNodes,
            numNodes = thumbElements.length,
            items = [],
            figureEl,
            linkEl,
            size,
            item;

        for(var i = 0; i < numNodes; i++) {

            figureEl = thumbElements[i]; // <figure> element

            // include only element nodes 
            if(figureEl.nodeType !== 1) {
                continue;
            }

            linkEl = figureEl.children[0]; // <a> element

            size = linkEl.getAttribute('data-size').split('x');

            // create slide object
            item = {
                src: linkEl.getAttribute('href'),
                w: parseInt(size[0], 10),
                h: parseInt(size[1], 10)
            };



            if(figureEl.children.length > 1) {
                // <figcaption> content
                item.title = figureEl.children[1].innerHTML; 
            }

            if(linkEl.children.length > 0) {
                // <img> thumbnail element, retrieving thumbnail url
                item.msrc = linkEl.children[0].getAttribute('src');
            } 

            item.el = figureEl; // save link to element for getThumbBoundsFn
            items.push(item);
        }

        return items;
    };

    // find nearest parent element
    var closest = function closest(el, fn) {
        return el && ( fn(el) ? el : closest(el.parentNode, fn) );
    };

    // triggers when user clicks on thumbnail
    var onThumbnailsClick = function(e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        var eTarget = e.target || e.srcElement;

        // find root element of slide
        var clickedListItem = closest(eTarget, function(el) {
            return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
        });

        if(!clickedListItem) {
            return;
        }

        // find index of clicked item by looping through all child nodes
        // alternatively, you may define index via data- attribute
        var clickedGallery = clickedListItem.parentNode,
            childNodes = clickedListItem.parentNode.childNodes,
            numChildNodes = childNodes.length,
            nodeIndex = 0,
            index;

        for (var i = 0; i < numChildNodes; i++) {
            if(childNodes[i].nodeType !== 1) { 
                continue; 
            }

            if(childNodes[i] === clickedListItem) {
                index = nodeIndex;
                break;
            }
            nodeIndex++;
        }



        if(index >= 0) {
            // open PhotoSwipe if valid index found
            openPhotoSwipe( index, clickedGallery );
        }
        return false;
    };

    // parse picture index and gallery index from URL (#&pid=1&gid=2)
    var photoswipeParseHash = function() {
        var hash = window.location.hash.substring(1),
        params = {};

        if(hash.length < 5) {
            return params;
        }

        var vars = hash.split('&');
        for (var i = 0; i < vars.length; i++) {
            if(!vars[i]) {
                continue;
            }
            var pair = vars[i].split('=');  
            if(pair.length < 2) {
                continue;
            }           
            params[pair[0]] = pair[1];
        }

        if(params.gid) {
            params.gid = parseInt(params.gid, 10);
        }

        return params;
    };

    var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
            gallery,
            options,
            items;

        items = parseThumbnailElements(galleryElement);

        // define options (if needed)
        options = {

            // define gallery index (for URL)
            galleryUID: galleryElement.getAttribute('data-pswp-uid'),

            getThumbBoundsFn: function(index) {
                // See Options -> getThumbBoundsFn section of documentation for more info
                var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                    rect = thumbnail.getBoundingClientRect(); 

                return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
            }

        };

        // PhotoSwipe opened from URL
        if(fromURL) {
            if(options.galleryPIDs) {
                // parse real index when custom PIDs are used 
                // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                for(var j = 0; j < items.length; j++) {
                    if(items[j].pid == index) {
                        options.index = j;
                        break;
                    }
                }
            } else {
                // in URL indexes start from 1
                options.index = parseInt(index, 10) - 1;
            }
        } else {
            options.index = parseInt(index, 10);
        }

        // exit if index not found
        if( isNaN(options.index) ) {
            return;
        }

        if(disableAnimation) {
            options.showAnimationDuration = 0;
        }

        // Pass data to PhotoSwipe and initialize it
        gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    };

    // loop through all gallery elements and bind events
    var galleryElements = document.querySelectorAll( gallerySelector );

    for(var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute('data-pswp-uid', i+1);
        galleryElements[i].onclick = onThumbnailsClick;
    }

    // Parse URL and open gallery if it contains #&pid=3&gid=1
    var hashData = photoswipeParseHash();
    if(hashData.pid && hashData.gid) {
        openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
    }
};

// execute above function
initPhotoSwipeFromDOM('<?php echo rtrim($photoIDElements, ',') ?>,#AlbumViewSlideShow');
  </script>
<?php endif; ?>
