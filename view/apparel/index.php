<div id="TopApparelPage">
	<div class="HalfElementSellingPoint EntryPageText">
		<div class="ContentHeader">
			<h1>WELCOME TO OUR APPAREL DEPARTMENT</h1>
		</div>
		<div>
			Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. 
				Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur 
				ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla 
				consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In 
				enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis 
				pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate 
				eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem 
				ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. 
				Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper 
				ultricies nisi. 
		</div>
	</div>
	<div class="HalfElementSellingPoint" style='position:relative'>
		<div class="apparelImageElement">
			
		</div>
	</div>
	<div style='clear:both'></div>
</div>

<div class="container">
	<div class="row">
		
	</div>
</div>

<div class="grayBackground" style='padding: 30px 0px; margin-bottom:20px;'>
	<div class="container">
		<div class="row">
			<div class="col-md-12" style='font-size:20px; text-align: center;'>
				blah blah blah something about our shopping cart blah blah blah something about our shopping cart blah blah blah
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style='font-size:20px; text-align: center; margin-top:20px;'>
				<a href='<?php echo PATH ?>shop'>
					<div class="shopNowButton">
						Shop Now
					</div>
				</a>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12" style='margin-bottom:20px;'>
			<div>
				
			</div>
			<div>
				
			</div>
		</div>
	</div>
	<form method='post' action="<?php echo PATH ?>ajax/appareldeptform" id="ApparelDeptForm">
		<div class="row">
			<div class="col-md-12" style='margin-bottom:20px;'>
				<h2>Contact our Apparel Department</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="inputID1">
					<div class="errorMessage"></div>
				</div>
			</div>
		</div>
		<div class="row" style='margin-bottom:10px;'>
			
			<div class="col-md-4">
				<div class='radioButton'><input type='radio' name='apparelStore[]' value='3:Omaha' onchange="Globals.removeValidationMessage(1)" /> Harley-Davidson Omaha</div>
				<div class='radioButton'><input type='radio' name='apparelStore[]' value='3:Fremont' onchange="Globals.removeValidationMessage(1)" /> Harley-Davidson Fremont</div>
			</div>
			<div class="col-md-4">
				<div class='radioButton'><input type='radio' name='apparelStore[]' value='2:Omaha' onchange="Globals.removeValidationMessage(1)" /> Motorsports</div>
				<div class='radioButton'><input type='radio' name='apparelStore[]' value='4:Omaha' onchange="Globals.removeValidationMessage(1)" /> Indian Motorcycle</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="formHeader">
					<div class="text">
						Contact Information 	
					</div>
					<div class="divider"></div>
					<div style='clear:both'></div>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-6">
						<div id="inputID2">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">First Name <span class='requiredIndicator'>*</span></div>
								<input type="text" name="apparelFirstName" onchange="Globals.removeValidationMessage(2)" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div id="inputID3">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">Last Name <span class='requiredIndicator'>*</span></div>
								<input type="text" name="apparelLastName" onchange="Globals.removeValidationMessage(3)" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div id="inputID4">
							<div class="errorMessage"></div>												
							<div class="inputLine">
								<div class="inputLabel">Home Phone <span class='requiredIndicator'>*</span></div>
								<input type="text" name="apparelHomePhone" class="partsHomePhone" onchange="Globals.removeValidationMessage(4)" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="inputLine">
							<div class="inputLabel">Cell Phone</div>
							<input type="text" name="apparelCellPhone" class="partsCellPhone" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div id="inputID12">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">Email Address</div>
								<input type="text" name="apparelEmailAddress" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div class="inputLine">
							<div class="inputLabel">Street Address</div>
							<input type="text" name="apparelStreetAddress" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8">
						<div class="inputLine">
							<div class="inputLabel">City</div>
							<input type="text" name="apparelCity" />
						</div>
					</div>
					<div class="col-md-2">
						<div class="inputLine">
							<div class="inputLabel">State</div>
							<select name="apparelState">
								<option value=''></option>
								<?php foreach($this -> states() as $key => $stateSingle): ?>
									<option value='<?php echo $key ?>'><?php echo $key ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="inputLine">
							<div class="inputLabel">Zip</div>
							<input type="text" name="apparelZip" maxlength="5" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="formHeader">
					<div class="text">
						Additional Comments
					</div>
					<div class="divider"></div>
					<div style='clear:both'></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="inputLine">
					<div class="inputLabel">if this applies, please type in some notes/etc. about what you are requesting</div>
					<textarea name="apparelAdditionalComments"></textarea>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div id="inputID13">
					<div class="errorMessage"></div>					
					<div class="inputLine">
						<div class="inputLabel">Are You Human?: <span class='requiredIndicator'>*</span></div>
						<img src="<?php echo PATH ?>view/captcha.php" style='margin-bottom:10px' />
						<input type="text" name="capta"  onchange="Globals.removeValidationMessage(13)" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<input type='submit' value='Send Request' class="blueButton" />
			</div>
		</div>
	</form>
</div>
<div class="grayBackground" style='padding: 15px 0px; margin:20px 0px 0px 0px;'>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				Dillon Brothers is committed to your privacy.
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style='margin-top:10px;'>
				<a href='<?php echo PATH ?>privacy'>View our Privacy Statement</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style='font-size:12px; margin-top:10px;'>
				
	
			</div>
		</div>
	</div>
</div>

