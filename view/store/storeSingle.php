<?php 
	$showMotorSportBrands = false;
	$showHarley = false;
	$showIndian = false;
	$storeSEOurl = NULL;
	switch($this -> storeType) {
		case "motor-sports":
			$storeSEOurl = "MotorSports";
			$showMotorSportBrands = true;
			break;
		case "harley":
			$storeSEOurl = "Harley";
			$showHarley = true;
			break;
		case "indian":
			$storeSEOurl = "Indian";
			$showIndian = true;
			break;
		
	}
	
	?>
<?php if($showMotorSportBrands == true) : ?>
	<div id="SlideShow" style="background: url(<?php echo PATH ?>public/images/DillonMotor.jpg) no-repeat center; background-size: cover;">
		<?php echo $this -> storeName; ?>
	</div>
<?php endif; ?>
<?php if($showHarley == true) : ?>
	<div id="SlideShow" style="background: url(<?php echo PATH ?>public/images/DillonHarley.jpg) no-repeat center; background-size: cover;">
		<?php echo $this -> storeName; ?>
	</div>
<?php endif; ?>
<?php if($showIndian == true) : ?>
	<div id="SlideShow" style="background: url(<?php echo PATH ?>public/images/DillonIndian.jpg) no-repeat center; background-size: cover;">
		<?php echo $this -> storeName; ?>
	</div>
<?php endif; ?>
<div class="container">	
	
	<?php if($showMotorSportBrands == true) : ?>
		<div class="row DillonMotorSportBrands">
			<div class="col-md-6">
				<div class="row">
					<div class="col-xs-4" style="text-align:center;">
						<img src="<?php echo PATH ?>public/images/DillonHonda.png" style='max-width:100%' />
					</div>
					<div class="col-xs-4" style="text-align:center;">
						<img src="<?php echo PATH ?>public/images/DillonKTM.png" style='max-width:100%' />
					</div>
					<div class="col-xs-4" style="text-align:center;">
						<img src="<?php echo PATH ?>public/images/DillonSuzuki.png" style='max-width:100%' />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-xs-4" style="text-align:center;">
						<img src="<?php echo PATH ?>public/images/DillonTriumph.png" style='max-width:100%' />
					</div>
					<div class="col-xs-4" style="text-align:center;">
						<img src="<?php echo PATH ?>public/images/DillonYamaha.png" style='max-width:100%' />
					</div>
					<div class="col-xs-4" style="text-align:center;">
						<img src="<?php echo PATH ?>public/images/DillonKawasaki.png" style='max-width:100%' />
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php if($showHarley == true) : ?>
		<div class="row DillonHarleyBrands">
			<div class="col-md-12" style="text-align:center;">
				<img src="<?php echo PATH ?>public/images/HarleyDavidson.png" />
			</div>
		</div>
	<?php endif; ?>
	<?php if($showIndian == true) : ?>
		<div class="row DillonIndianBrands">
			<div class="col-md-12" style="text-align:center; padding-top: 15px;">
				<img src="<?php echo PATH ?>public/images/Indian.png" />
			</div>
		</div>
	<?php endif; ?>	
	<div class="row">
		<div class="col-md-12 ContentHeader">
			<span style="font-weight:100">CURRENT</span> SPECIALS
		</div>
	</div>
	
	<?php if(count($this -> storeSpecials) > 0) : ?>
	
	<?php foreach(array_chunk($this -> storeSpecials, 3, true) as $setSpecials) : ?>
				
		<div class="row" style='margin-bottom:10px;'>
			<?php foreach($setSpecials as $special) : ?>
				<div class="col-md-4">
					<a href="<?php echo PATH ?>store/special/<?php echo $special['specialSEOurl'] ?>/<?php echo $storeSEOurl ?>">
						<div class="SpecialImage" style="background:url(<?php echo SPECIALS_URL . $special['FolderName'] . '/' . $special['SpecialMainImage'] ?>) no-repeat center">
							<img src="" width='100%' />							
							<div class="specialTitle">
								<?php echo $special['SpecialTitle'] ?>
							</div>
						</div>
						
						<div style="text-align:center">
							View Special
						</div>
						
					</a>	
				</div>
				
				
			<?php endforeach; ?>
		</div> 
					
	<?php endforeach; ?>
	<?php else: ?>
		<div class="row">
			<div class="col-md-12">
				There are no specials at this time
			</div>
		</div>
	<?php endif; ?>				
	
	
</div>
<div style="background:url(<?php echo PATH ?>public/images/InsideStore.JPG) no-repeat; background-size: cover; padding: 20px 0px 40px 0px; margin-top: 20px;">
	<div class="employeeHeader">Our Employees</div>
	<div class="container">	
		<div class="row" id="DillonMotorSports">
			<div class="col-md-12">
				<?php foreach($this -> Employeelist as $Employee) : ?>
					<div class="EmployeeSingle">
						<div class="image" style='background:url(<?php echo PHOTO_URL . 'employees/' . $Employee['Photo'] ?>) no-repeat center'></div>
						<div class="Name">
							<?php echo $Employee['EmployeeFirstName'] . ' ' . $Employee['EmployeeLastName'] ?>
						</div>	
						<div class="title">
							<?php echo $Employee['Title'] ?>
						</div>
						<?php if(!empty($Employee['Email'])) : ?>								
							<div class="email">
								<a href="mailto:<?php echo $Employee['Email'] ?>">
									<img src="<?php echo PATH ?>public/images/EmployeeEmail.png" />	
								</a>
							</div>
						<?php endif; ?>			
					</div>

				
				<?php endforeach; ?>
			</div>
		</div>
		<div class="buttonContainer">
			<div class="element">
				<?php if($showMotorSportBrands == true) : ?>
					<a href="http://www.dilloncycles.com/meet-the-dealership--staff" target="_blank">
						<div class="redButton">
							View More Staff	
						</div>
					</a>
				<?php endif; ?>
				<?php if($showHarley == true) : ?>
					<a href="http://www.dillonharley.com/" target="_blank">
						<div class="orangeButton">
							View More Staff	
						</div>
					</a>
				<?php endif; ?>
				<?php if($showIndian == true) : ?>
					<a href="http://www.dillonbrothersindian.com/Meet-Our-Helpful--Staff" target="_blank">
						<div class="maroonButton">
							View More Staff	
						</div>
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<div class="container" style="padding: 15px;">	
	<?php 
	if($showMotorSportBrands == true) {
		require "view/MotorSportHours.php";
	}
	
	if($showHarley == true) {
		require "view/HarleyHours.php";
	}
	
	if($showIndian == true) {
		require "view/IndianHours.php";
	}
	
	?>
</div><br /><br />
<script type="application/ld+json">
[
	<?php if($showMotorSportBrands == true) : ?>
		{
			"@context": "http://schema.org",
			"@type": "LocalBusiness",
			"address": {
			    "@type": "PostalAddress",
			    "name": "Dillon Brothers MotorSports",
			    "streetAddress": "3848 N HWS Cleveland Blvd",
			    "addressLocality": "Omaha",
			    "addressRegion": "Nebraska",
			    "postalCode" : "68116",
			    "telephone" : "(402) 556-3333",
			    "faxNumber" : "(402) 556-1796"
			},
			"openingHours" : [
			  	"Monday 9:00-19:00",
			    "Tuesday 9:00-19:00",
			    "Wednesday 9:00-17:30",
			    "Thursday 9:00-19:00",
			    "Friday 9:00-17:30",
			    "Saturday 9:00-16:30"
			],
			  "name" : "Dillon Brothers MotorSports",
			  "image" : "<?php echo PATH ?>public/images/DillonBrothers.png",
			  "url": "<?php echo PATH ?>store/name/motor-sports",
			  "priceRange" : "00.00"
		},
		{
			"@context": "http://schema.org",
			"@type": "Brand",
			"name" : "Honda Powersports",
			"description" : "Honda motorcycles, scooters, ATVs, and side-by-sides",
			"url": "http://powersports.honda.com/"
		},{
			"@context": "http://schema.org",
			"@type": "Brand",
			"name" : "Honda Power Equipment",
			"description" : " Honda Power Equipment: Honda Generators, Lawn Mowers, Snow blowers, Tillers",
			"url" : "http://powerequipment.honda.com/"
		},{
			"@context": "http://schema.org",
			"@type": "Brand",
			"name" : "KTM",
			"description" : "Adventure, Purity, Performance, Extreme - KTM is READY TO RACE",
			"url" : "http://www.ktm.com/us/"
		},{
			"@context": "http://schema.org",
			"@type": "Brand",
			"name" : "Suzuki Cycles",
			"description" : "Suzuki manufactures legendary motorcycles such as the GSX-R, championship winning RM-Z motocross bikes, agile scooters, and revolutionary ATVs.",
			"url" : "http://www.suzukicycles.com/"
		},{
			"@context": "http://schema.org",
			"@type": "Brand",
			"name" : "Triump Motorcyles",
			"description" : "The official Triumph site. Motorcycles that deliver the complete riding experience. View our range, find a dealer and test ride a Triumph icon today.",
			"url" : "http://www.triumphmotorcycles.com/"
		},{
			"@context": "http://schema.org",
			"@type": "Brand",
			"name" : "Yamaha Motor Company",
			"description" : "Motorcycles, ATVs, Outboard Motors, Snowmobiles, Sport Boats, WaveRunners, Motorcycle, Race Kart Engines, Generators, powersport industry leader Yamaha Motor Corporation USA.",
			"url" : "http://www.triumphmotorcycles.com/"
		},{
			"@context": "http://schema.org",
			"@type": "Brand",
			"name" : "Kawasaki",
			"description" : "Official website of Kawasaki Motors Corp., U.S.A., distributor of powersports vehicles including motorcycles, ATVs, Side x Sides and personal watercraft.",
			"url" : "https://www.kawasaki.com/"
		}
	<?php endif; ?>
	<?php if($showHarley == true) : ?>
	{
	  "@context": "http://schema.org",
	  "@type": "LocalBusiness",
	  "address": {
	    "@type": "PostalAddress",
	    "name": "Dillon Brothers Harley-Davidson Omaha",
	    "streetAddress": "3838 N HWS Cleveland Blvd",
	    "addressLocality": "Omaha",
	    "addressRegion": "Nebraska",
	    "postalCode" : "68116",
	    "telephone" : "(402) 289-5556",
	    "faxNumber" : "(402) 289-1931"
	  },
	  "openingHours": [
			"Monday 9:00-19:00",
			"Tuesday 9:00-19:00",
			"Wednesday 9:00-17:30",
			"Thursday 9:00-19:00",
			"Friday 9:00-17:30",
			"Saturday 9:00-16:30"
	  	],
	  "name" : "Dillon Brothers Harley-Davidson-Omaha",
	  "image" : "<?php echo PATH ?>public/images/DillonBrothers.png",
	  "url": "<?php echo PATH ?>store/name/harley",
	  "priceRange" : "00.00"
	},
	{
	  "@context": "http://schema.org",
	  "@type": "LocalBusiness",
	  "address": {
	    "@type": "PostalAddress",
	    "name": "Dillon Brothers Harley-Davidson Fremont",
		"streetAddress": "2440 East 23rd St",
		"addressLocality": "Fremont",
		"addressRegion": "Nebraska",
		"postalCode" : "68025",
		"telephone" : "(402) 721-2007",
		"faxNumber" : "(402) 721-2441"
	  },
	  "openingHours": [
		"Tuesday 9:00-18:00",
		"Wednesday 9:00-17:30",
		"Thursday 9:00-18:00",
		"Friday 9:00-17:30",
		"Saturday 9:00-16:30"
	  	],
	  "name" : "Dillon Brothers Harley-Davidson Fremont",
	  "image" : "<?php echo PATH ?>public/images/DillonBrothers.png",
	  "url": "<?php echo PATH ?>store/name/harley",
	  "priceRange" : "00.00"
	},
	{
		"@context": "http://schema.org",
		"@type": "Brand",
		"name" : "Harley-Davidson",
		"description" : "Harley-Davidson Motorcyles",
		"url" : "http://www.harley-davidson.com/"
	}
	<?php endif; ?>
	<?php if($showIndian == true) : ?>
		{
		  "@context": "http://schema.org",
		  "@type": "LocalBusiness",
		  "address": {
		    "@type": "PostalAddress",
		    "name": "Dillon Brothers Indian",
		    "streetAddress": "3840 N 174th Ave.",
		    "addressLocality": "Omaha",
		    "addressRegion": "Nebraska",
		    "postalCode" : "68116",
		    "telephone" : "(402) 505-4233"
		  },
		  "openingHours" : [
			  	"Monday 9:00-19:00",
			    "Tuesday 9:00-19:00",
			    "Wednesday 9:00-17:30",
			    "Thursday 9:00-19:00",
			    "Friday 9:00-17:30",
			    "Saturday 9:00-16:30"
		   ],
		  "name" : "Dillon Brothers Indian",
		  "image" : "<?php echo PATH ?>public/images/DillonBrothers.png",
		  "url": "<?php echo PATH ?>",
		  "priceRange" : "00.00"
		},
		{
			"@context": "http://schema.org",
			"@type": "Brand",
			"name" : "Indian Motorcyle",
			"description" : "Founded in 1901, Indian Motorcycle is America&#39;s first motorcycle company.  Modern bikes are designed to reflect Indian&#39;s traditional styling &amp; engineered to be powerful works of art.",
			"url" : "http://www.indianmotorcycle.com/"
		},
		{
			"@context": "http://schema.org",
			"@type": "Brand",
			"name" : "Polaris",
			"description" : "Polaris Slingshot, 3-Wheel Motorcycles, Trikes, reverse trike, 3 wheel motorcycle, 3-wheeled motorcycle, auto-cycle, autocycle, trike, roadster, 2 seat car, 3 wheel car",
			"url" : "http://www.polaris.com/"
		}
	<?php endif; ?>
	<?php if(count($this -> storeSpecials) > 0): ?>
		<?php foreach($this -> storeSpecials as $specials) : ?>
		<?php 
			$specialIncentives = array(); 
		
			$categoryArray = explode(",",$special['RelatedIncentiveCategories']);
			$nameArray = explode(",",$special['RelatedIncentiveName']); 
			foreach($categoryArray as $key => $item)  {
				array_push($specialIncentives, array("Category" => $categoryArray[$key],
													 "Name" => $nameArray[$key]));
			}
		?>
		
			
			,{
				"@context": "http://schema.org",
		  		"@type": "Event",
		  		"url" : "<?php echo PATH ?>store/special/<?php echo $specials['specialSEOurl'] ?>",
		  		"name" : "<?php echo $specials['SpecialTitle'] ?>",
		  		"startDate": "<?php echo $specials['SpecialStartDate'] ?>T9:00:00Z",
				"endDate": "<?php echo $specials['SpecialEndDate'] ?>T17:00:00Z",
				"description" : "<?php echo $specials['SpecialDescription'] ?>",
				"performer" : "N/A",
				"image" : "<?php echo SPECIALS_URL . $specials['SpecialMainImage'] ?>",
				<?php if($showMotorSportBrands == true) : ?>
					"location": {
						"@type": "Place",
					    "name": "Dillon Brothers MotorSports",
					    "address": {
					      "@type": "PostalAddress",
						  "streetAddress": "3848 N HWS Cleveland Blvd",
						  "addressLocality": "Omaha",
						  "addressRegion": "Nebraska",
						  "postalCode" : "68116",
						  "telephone" : "(402) 556-3333",
						  "faxNumber" : "(402) 556-1796"
					    }
					},
				<?php endif; ?>
				<?php if($showHarley == true) : ?>
					"location": [{
						"@type": "Place",
					    "address": {
					      "@type": "PostalAddress",
						  "name": "Dillon Brothers Harley-Davidson Omaha",
						  "streetAddress": "3838 N HWS Cleveland Blvd",
						  "addressLocality": "Omaha",
						  "addressRegion": "Nebraska",
						  "postalCode" : "68116",
						  "telephone" : "(402) 289-5556",
						  "faxNumber" : "(402) 289-1931"
					    }
					},{
						 "@type": "PostalAddress",
					    "name": "Dillon Brothers Harley-Davidson Fremont",
						"streetAddress": "2440 East 23rd St",
						"addressLocality": "Fremont",
						"addressRegion": "Nebraska",
						"postalCode" : "68025",
						"telephone" : "(402) 721-2007",
						"faxNumber" : "(402) 721-2441"
					}],
				<?php endif; ?>
				"offers": [
					<?php 
						$i = 0;
						$len = count($specialIncentives);
					?>
					<?php foreach($specialIncentives as $incentiveSingle) : ?>
						{
							"@type": "Offer",
				            "category": "<?php echo $incentiveSingle['Category'] ?>",
				            "name" : "<?php echo $incentiveSingle['Name'] ?>",
				            "priceCurrency" : "USD",
				            "url": "<?php echo PATH ?>store/special/<?php echo $specials['specialSEOurl'] ?>"
						}<?php if($i != $len - 1) :?>,<?php endif; ?>
						<?php $i++ ?>
					
					
					<?php endforeach; ?>
				]
			}
		<?php endforeach; ?>
	<?php endif; ?>
]
</script>


