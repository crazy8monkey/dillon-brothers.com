<?php 
	$showMotorSport = false;
	$showHarley = false;
	$showIndian = false;
	$color = NULL;
	$ParentLink = NULL;
	
	switch($this -> SpecialSingle -> StoreID) {
		case 2:
			$showMotorSport = true;
			$color = "red";
			$ParentLink = PATH . "store/name/motor-sports";
			break;
		case 3:
			$color = "#ff6c00";
			$ParentLink = PATH . "store/name/harley";
			$showHarley = true;
			break;
		case 4:
			$color = "#850029";
			$ParentLink = PATH . "store/name/indian";
			$showIndian = true;
			break;
		
	}
	
?>
<?php if($showMotorSport == true) : ?>
	<div id="SlideShow" style="background: url(<?php echo PATH ?>public/images/DillonMotor.jpg) no-repeat center; background-size: cover;">
		MOTOR SPORTS SPECIALS
	</div>
<?php endif; ?>
<?php if($showHarley == true) : ?>
	<div id="SlideShow" style="background: url(<?php echo PATH ?>public/images/DillonHarley.jpg) no-repeat center; background-size: cover;">
		HARLEY SPECIALS
	</div>
<?php endif; ?>
<?php if($showIndian == true) : ?>
	<div id="SlideShow" style="background: url(<?php echo PATH ?>public/images/DillonIndian.jpg) no-repeat center; background-size: cover;">
		INDIAN SPECIALS
	</div>
<?php endif; ?>
<div class="container" style="margin-bottom: 80px;">	
	
	<div class="row">
		<div class="col-md-12" style='margin:15px 0px 20px 0px;'>
			<a href="<?php echo $ParentLink ?>">
				<?php echo $this -> SpecialSingle -> StoreName; ?>	
			</a>
			<span style="margin:0px 5px 0px 2px;"><i class="fa fa-caret-right" aria-hidden="true"></i></span><?php echo $this -> SpecialSingle -> SpecialTitle; ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="MainSpecialImage" style="background:url(<?php echo SPECIALS_URL . $this -> SpecialSingle -> folderName . '/' . $this -> SpecialSingle -> SpecialImage; ?>) no-repeat">
				<div class="MainSpecialTitle">
					<?php echo $this -> SpecialSingle -> SpecialTitle; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" style='margin-bottom:15px;'>
			<?php echo $this -> SpecialSingle -> Description; ?>
		</div>
		<div style="border-bottom: 1px solid #f7f7f7; clear: both; margin: 0px 15px 15px 15px  ;"></div>
	</div>
	<?php $prevCategory = NULL;?>
	<?php foreach ($this -> SpecialsIncentivesList as $incentive): ?>
		<?php if($this -> SpecialSingle -> VehicleSpecialBrand != NULL) :?>
			<?php 
				$showDateHeader = false;
				if($prevCategory != $incentive['VehicleCategory']) {
	            	$showDateHeader = true;
	            }
			$prevCategory = $incentive['VehicleCategory'];
			?>
			<?php if ($showDateHeader == true) : ?>
				<div class="categoryHeader" style="color:<?php echo $color ?>">
					<?php echo $incentive['VehicleCategory'] ?>	
				</div>
			<?php endif; ?>
			<div class="VehicleSingle">
				<div class="title">
					<div class="text">
						<a href="javascript:void(0);" onclick="StoreController.OpenSpecialLine($(this).parent().parent().parent())">
							<div class="expandIndicator">+</div><span style='color:black'><?php echo $incentive['VehicleName'] ?> | <?php echo $incentive['SpecialTypeValue'] ?> <?php echo $incentive['SpecialType'] ?></span>
						</a>
					</div>
					<div class="divider"></div>
					<div style="clear:both"></div>
				</div>
				<div class="content">
					<div class="Description">
						<?php echo $incentive['Description'] ?>
					</div>
					<div class="disclaimer">
						<strong style="font-size: 11px;">Disclaimer</strong><br />
						<?php echo $incentive['Disclaimer'] ?>
					</div>	
				</div>
				
			</div>
		<?php endif; ?>
		
		<?php if($this -> SpecialSingle -> PartsSpecialBrand != NULL) :?>
			<div class="VehicleSingle">
				<div class="title">
					<div class="text">
						<a href="javascript:void(0);" onclick="StoreController.OpenSpecialLine($(this).parent().parent().parent())">
							<div class="expandIndicator">+</div><span style='color:black'><?php echo $incentive['partAccessoryName'] ?> | <?php echo $incentive['partAccessoryTypeValue'] ?> <?php echo $incentive['partAccessoryType'] ?></span>
						</a>
					</div>
					<div class="divider"></div>
					<div style="clear:both"></div>
				</div>
				<div class="content">
					<div class="Description">
						<?php echo $incentive['partDescription'] ?>
					</div>
					<div class="disclaimer">
						<strong style="font-size: 11px;">Disclaimer</strong><br />
						<?php echo $incentive['partDisclaimer'] ?>
					</div>	
				</div>
				
			</div>
		<?php endif; ?>
		
		
		<?php if($this -> SpecialSingle -> IsServiceSpecial != NULL) :?>
			<div class="VehicleSingle">
				<div class="title">
					<div class="text">
						<a href="javascript:void(0);" onclick="StoreController.OpenSpecialLine($(this).parent().parent().parent())">
							<div class="expandIndicator">+</div><span style='color:black'><?php echo $incentive['serviceIncentiveName'] ?> | <?php echo $incentive['serviceIncentiveTypeValue'] ?> <?php echo $incentive['serviceIncentiveType'] ?></span>
						</a>
					</div>
					<div class="divider"></div>
					<div style="clear:both"></div>
				</div>
				<div class="content">
					<div class="Description">
						<?php echo $incentive['serviceIncentiveDescription'] ?>
					</div>
					<div class="disclaimer">
						<strong style="font-size: 11px;">Disclaimer</strong><br />
						<?php echo $incentive['serviceIncentiveDisclaimer'] ?>
					</div>	
				</div>
				
			</div>
		<?php endif; ?>	
		
	<?php endforeach; ?>
	<?php if(!empty($this -> SpecialSingle -> SpecialDescriptionFooter)): ?>
		<div class="row">
			<div class="col-md-12">
				<div style="border-top: 1px solid #f7f7f7; clear: both; margin: 60px auto 30px auto; width: 100px;"></div>
				<div style="color:#979797; font-size:24px; margin:0px; font-family: 'acumin-pro-semi-condensed';">
					Other Information
				</div>
				<?php echo $this -> SpecialSingle -> SpecialDescriptionFooter ?>
			</div>
		</div>
	<?php endif; ?>
	
	<?php if($this -> SpecialSingle -> VehicleSpecialBrand != NULL) :?>
		<div class="row">
			<div style="border-top: 1px solid #f7f7f7; clear: both; margin: 30px auto; width: 100px;"></div>
			<div class="col-md-12">
				<div style="color:#979797; font-size:24px; margin:0px; font-family: 'acumin-pro-semi-condensed'">
					About <?php echo $this -> SpecialSingle -> BrandName ?>
				</div>
			</div>
		</div>
		<div class="row">
			
			<div class="col-md-2">
				<img src="<?php echo BRANDS_URL . $this -> SpecialSingle -> BrandImage ?>" width='100%' />
			</div>
			<div class="col-md-8">
				<?php echo $this -> SpecialSingle -> OEMBrandDescription ?>
			</div>
		</div>
	<?php endif; ?>
	
	<?php if($this -> SpecialSingle -> PartsSpecialBrand != NULL) :?>
		<div class="row">
			<div style="border-top: 1px solid #f7f7f7; clear: both; margin: 30px auto; width: 100px;"></div>
			<div class="col-md-12">
				<div style="color:#979797; font-size:24px; margin:0px; font-family: 'acumin-pro-semi-condensed'">
					About <?php echo $this -> SpecialSingle -> PartBrandName ?>
				</div>
			</div>
		</div>
		<div class="row">
			
			<div class="col-md-2">
				<img src="<?php echo PARTS_BRANDS_URL . $this -> SpecialSingle -> PartBrandImage ?>" width='100%' />
			</div>
			<div class="col-md-8">
				<?php echo $this -> SpecialSingle -> PartBrandDescription ?>
			</div>
		</div>
	<?php endif; ?>
	
	
</div>





<script type="application/ld+json">
[{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "<?php echo $ParentLink ?>",
      "name": "<?php echo $this -> SpecialSingle -> StoreName; ?>"
    }
  },{
    "@type": "ListItem",
    "position": 2,
    "item": {
      "@id": "<?php echo PATH ?>store/special/<?php echo $this -> SpecialSingle -> seoUrl; ?>",
      "name": "<?php echo $this -> SpecialSingle -> SpecialTitle; ?>"
    }
  }]
},
{
	"@context": "http://schema.org",
	"@type": "Event",
	"url" : "<?php echo PATH ?>",
	"name" : "<?php echo $this -> SpecialSingle -> SpecialTitle; ?>",
	"startDate": "<?php echo $this -> SpecialSingle -> startDate; ?>T9:00:00Z",
	"endDate": "<?php echo $this -> SpecialSingle -> endDate; ?>T17:00:00Z",
	"description" : "<?php echo $this -> SpecialSingle -> Description; ?>",
	"performer" : "N/A",
	"image" : "<?php echo SPECIALS_URL . $this -> SpecialSingle -> SpecialImage ?>",
	<?php if($showMotorSport == true) : ?>
	"location": {
		"@type": "Place",
		"name": "Dillon Brothers MotorSports",
		"address": {
			"@type": "PostalAddress",
			"streetAddress": "3848 N HWS Cleveland Blvd",
			"addressLocality": "Omaha",
			"addressRegion": "Nebraska",
			"postalCode" : "68116",
			"telephone" : "(402) 556-3333",
			"faxNumber" : "(402) 556-1796"
		}
	},
	<?php endif; ?>
	<?php if($showHarley == true) : ?>
	"location": [{
		"@type": "Place",
		"name": "Dillon Brothers Harley-Davidson Omaha",
		"address": {	
			
			"streetAddress": "3838 N HWS Cleveland Blvd",
			"addressLocality": "Omaha",
			"addressRegion": "Nebraska",
			"postalCode" : "68116",
			"telephone" : "(402) 289-5556",
			"faxNumber" : "(402) 289-1931"
		}
	}, {
		"@type": "Place",
		"name": "Dillon Brothers Harley-Davidson Fremont",
		"address": {	
			"streetAddress": "2440 East 23rd St",
			"addressLocality": "Fremont",
			"addressRegion": "Nebraska",
			"postalCode" : "68025",
			"telephone" : "(402) 721-2007",
			"faxNumber" : "(402) 721-2441"
		}
	}],
	<?php endif; ?>
	<?php if($showIndian == true) : ?>
	"location": {
		"@type": "Place",
		"name": "Dillon Brothers Indian",
		"address": {
			"@type": "PostalAddress",
			"streetAddress": "3840 N 174th Ave.",
			"addressLocality": "Omaha",
			"addressRegion": "Nebraska",
			"postalCode" : "68116",
			"telephone" : "(402) 505-4233"
		}
	},	
	<?php endif; ?>
	"offers": [
	<?php  $i = 0; $len = count($this -> SpecialsIncentivesList); ?>
	<?php foreach ($this -> SpecialsIncentivesList as $incentive): ?>
		{
			"@type": "Offer",
		<?php if($this -> SpecialSingle -> VehicleSpecialBrand != NULL) :?>
			"category": "<?php echo $incentive['VehicleCategory'] ?>",
			"name" : "<?php echo $incentive['VehicleName'] ?> | <?php echo $incentive['SpecialTypeValue'] ?> <?php echo $incentive['SpecialType'] ?>",
		<?php endif; ?>
		<?php if($this -> SpecialSingle -> PartsSpecialBrand != NULL) :?>
			"category": "Parts and Appareal",
			"name" : "<?php echo $incentive['partAccessoryName'] ?> | <?php echo $incentive['partAccessoryTypeValue'] ?> <?php echo $incentive['partAccessoryType'] ?>",
		<?php endif; ?>
		<?php if($this -> SpecialSingle -> IsServiceSpecial != NULL) :?>
			"category": "Store Special",
			"name" : "<?php echo $incentive['serviceIncentiveName'] ?> | <?php echo $incentive['serviceIncentiveTypeValue'] ?> <?php echo $incentive['serviceIncentiveType'] ?>",
		<?php endif; ?>
			"priceCurrency" : "USD",
			"url": "<?php echo PATH ?>store/special/<?php echo $this -> SpecialSingle -> seoUrl; ?>/<?php echo $this -> StoreLinkName ?>"
		}<?php if($i != $len - 1) :?>,<?php endif; ?>
		<?php $i++ ?>			
	<?php endforeach; ?>
	]
}
<?php if($showMotorSport == true) : ?>
,{
	"@context": "http://schema.org",
	"@type": "LocalBusiness",
	"address": {
		"@type": "PostalAddress",
		"name": "Dillon Brothers Motor Sports",
		"streetAddress": "3848 N HWS Cleveland Blvd",
		"addressLocality": "Omaha",
		"addressRegion": "Nebraska",
		"postalCode" : "68116",
		"telephone" : "(402) 556-3333",
		"faxNumber" : "(402) 556-1796"
	},
	"openingHours" : [
		"Monday 9:00-19:00",
		"Tuesday 9:00-19:00",
		"Wednesday 9:00-17:30",
		"Thursday 9:00-19:00",
		"Friday 9:00-17:30",
		"Saturday 9:00-16:30"
	],
	"name" : "Dillon Brothers Motor Sports",
	"image" : "<?php echo PATH ?>public/images/DillonBrothers.png",
	"url": "<?php echo PATH ?>store/name/motor-sports",
	"priceRange" : "00.00"
}
<?php endif; ?>
<?php if($showHarley == true) : ?>
,{
  "@context": "http://schema.org",
  "@type": "LocalBusiness",
  "address": {
    "@type": "PostalAddress",
    "name": "Dillon Brothers Harley-Davidson Omaha",
    "streetAddress": "3838 N HWS Cleveland Blvd",
    "addressLocality": "Omaha",
    "addressRegion": "Nebraska",
    "postalCode" : "68116",
    "telephone" : "(402) 289-5556",
    "faxNumber" : "(402) 289-1931"
  },
  "openingHours": [
		"Monday 9:00-19:00",
		"Tuesday 9:00-19:00",
		"Wednesday 9:00-17:30",
		"Thursday 9:00-19:00",
		"Friday 9:00-17:30",
		"Saturday 9:00-16:30"
  	],
  "email": "sales@dillonharley.com",	
  "name" : "Dillon Brothers Harley-Davidson Omaha",
  "image" : "<?php echo PATH ?>public/images/DillonBrothers.png",
  "url": "<?php echo PATH ?>",
  "priceRange" : "00.00"
},
{
  "@context": "http://schema.org",
  "@type": "LocalBusiness",
  "address": {
    "@type": "PostalAddress",
    "name": "Dillon Brothers Harley-Davidson Fremont",
    "streetAddress": "2440 East 23rd St",
    "addressLocality": "Fremont",
    "addressRegion": "Nebraska",
    "postalCode" : "68025",
    "telephone" : "(402) 721-2007",
    "faxNumber" : "(402) 721-2441"
  },
  "openingHours": [
	"Tuesday 9:00-18:00",
	"Wednesday 9:00-17:30",
	"Thursday 9:00-18:00",
	"Friday 9:00-17:30",
	"Saturday 9:00-16:30"
  ],
  "email": "sales@dillonharley.com",
  "name" : "Dillon Brothers Harley-Davidson Fremont",
  "image" : "<?php echo PATH ?>public/images/DillonBrothers.png",
  "url": "<?php echo PATH ?>",
  "priceRange" : "00.00"
}
<?php endif; ?>
<?php if($showIndian == true) : ?>
,{
  "@context": "http://schema.org",
  "@type": "LocalBusiness",
  "address": {
    "@type": "PostalAddress",
    "name": "Dillon Brothers Indian",
    "streetAddress": "3840 N 174th Ave.",
    "addressLocality": "Omaha",
    "addressRegion": "Nebraska",
    "postalCode" : "68116",
    "telephone" : "(402) 505-4233"
  },
  "openingHours" : [
	  	"Monday 9:00-19:00",
	    "Tuesday 9:00-19:00",
	    "Wednesday 9:00-17:30",
	    "Thursday 9:00-19:00",
	    "Friday 9:00-17:30",
	    "Saturday 9:00-16:30"
   ],
  "email": "sales@dillonbrothersindian.com",
  "name" : "Dillon Brothers Indian",
  "image" : "<?php echo PATH ?>public/images/DillonBrothers.png",
  "url": "<?php echo PATH ?>",
  "priceRange" : "00.00"
}
<?php endif; ?>
<?php if($this -> SpecialSingle -> BrandName == "Honda") : ?>
,{
	"@context": "http://schema.org",
	"@type": "Brand",
	"name" : "Honda Powersports",
	"description" : "Honda motorcycles, scooters, ATVs, and side-by-sides",
	"url": "http://powersports.honda.com/"
},{
	"@context": "http://schema.org",
	"@type": "Brand",
	"name" : "Honda Power Equipment",
	"description" : " Honda Power Equipment: Honda Generators, Lawn Mowers, Snow blowers, Tillers",
	"url" : "http://powerequipment.honda.com/"
}
<?php endif; ?>

]
</script>


