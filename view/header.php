
<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
	<head>
		
		
		<title><?php echo isset($this->title) ? $this->title : 'Dillon Brothers'; ?></title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
		<meta name="robots" content="noindex">
		<meta http-equiv="Cache-control" content="public">
      	<meta http-equiv="Expires" content="30" />
		<meta charset="utf-8">
		
		<?php require 'view/MetaDataContent.php'; ?>
		
		
		<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		
		<link rel="stylesheet" href="<?php echo PATH ?>public/css/style.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo PATH ?>public/css/slidePanel.css">
		<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
		<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
		
		<script src="https://media.twiliocdn.com/sdk/js/common/v0.1/twilio-common.min.js"></script>
		<script src="https://media.twiliocdn.com/sdk/js/chat/v1.0/twilio-chat.min.js"></script>

		
		<?php if(isset($this->css))  :?>
			<?php foreach ($this -> css as $css) :	?>
				<link rel="stylesheet" href="<?php echo $css; ?>" />
			<?php endforeach; ?>
		<?php endif; ?>
		
		
		<script type="text/javascript" src="<?php echo PATH . 'public/js/Globals.js' ?>"></script>
		
		<script src="https://use.typekit.net/kpv4owm.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
		
		
		
		<?php if(isset($this->js)) : ?>
			<?php foreach ($this -> js as $js) : ?>
				<script type="text/javascript" src="<?php echo $js; ?>"></script>
			<?php endforeach; ?>	
		<?php endif; ?>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
		
		<script type="text/javascript"> 
			
		
			document.createElement("header");
			document.createElement("footer");
			


			$(document).ready(function(){
				Globals.StartWebsite();
				
				
				$(window).scroll(function() {
					if($(window).scrollTop()) { //abuse 0 == false :)
					  $("#ScrollUpToPage").fadeIn();
					  console.log('you are at the top of page');
					} else {
						$("#ScrollUpToPage").fadeOut();
					}
				});
				
				
				<?php if(isset($this->startJsFunction)) : ?>
					<?php foreach ($this -> startJsFunction as $startJsFunction) : ?>	
						<?php echo $startJsFunction; ?>	
					<?php endforeach ?>
				<?php endif; ?>
				
				var $toggleButton = $('.toggle-button'),
			    	$menuWrap = $('.DropDownLinks'),
			    	$sidebarArrow = $('.sidebar-menu-arrow');
			
				// Hamburger button
			
				$toggleButton.on('click', function() {
					$(this).toggleClass('button-open');
					//$menuWrap.slideToggle();
					$('.MainMenuColor').toggleClass('mainMenuFixed');
					$("#MainNavigationElement").fadeToggle(function() {
						$('body').toggleClass('bodyNoScroll');
						
					});
				});
			
				// Sidebar navigation arrows
			
				$sidebarArrow.click(function() {
					$("#MainNavigationElement").toggle();
					//$(this).next().slideToggle(300);
				});
				
				$(document).click(function (e) {
		            if ($(e.target).closest('.Quicklinks, .MoreLink').length === 0) {
		            	$(".Quicklinks").hide();
		        	}
		        });
				
			});
			
		
			
		</script>
		
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		  ga('create', '<?php echo GOOGLE_ANALYTICS_ID ?>', 'auto');
		  ga('send', 'pageview');
		
		</script>
		
	</head>
<body <?php echo isset($this->bodyClassName) ? "class='". $this->bodyClassName ."'" : ''; ?>>

<header>
	<div class="MainMenuColor">
		<div class="container-fluid">
			<div class="col-xs-6 Logo">
				<div class="toggle-button">
		        	<div class="menu-bar menu-bar-top"></div>
		        	<div class="menu-bar menu-bar-bottom"></div>
		        	<img src="<?php echo PATH ?>public/images/DillonBrothers.png" />		
		        </div>
			
			</div>
			<div class="col-xs-6 RightMenu" style='text-align:right'>
				<?php Session::init(); ?>
				<!--<div class="item" style='border-left:1px solid white;'>
					<a href="javascript:void(0)" onclick='Globals.OpenInventoryQuickLinks()' class='MoreLink'>
						<div class='wrapperContainer'>
							<div class="arrowRight"></div>
						</div>			
					</a>
				</div>-->
				
				<div class="item CartCountElement">
					<a href="<?php echo PATH ?>cart">
						<img src="<?php echo PATH ?>public/images/CartIcon.png"  />	
						<span id="CartCount">
							
							<?php if(isset($_SESSION['CurrentCartObject'])): ?>
								<?php echo $_SESSION['CurrentCartObject']['results']['count'] ?>
							<?php else: ?>
								0
							<?php endif; ?>
						</span>
					</a>
				</div>
				<?php if(isset($_SESSION['EcommerceUserAccount'])): ?>
					<div class="item YourAccountLink" id="DesktopAccountLink">
						<a href='<?php echo PATH ?>shop/account'>Your Account</a>
					</div>
				<?php endif; ?>	
			</div>
		</div>		
	</div>
	<div id="MainNavigationElement" style='display:none;'>
			<div style="overflow: scroll; position: fixed; bottom: 0px; left: 0px; right: 0px; top: 0px; padding-bottom: 80px;">
				<div class="container">
					<div class="row">
						<div class="col-md-12" style='margin-bottom:10px;'>
							<div style='font-size:24px;'><strong>Main Navigation</strong></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-xs-6">
									<a href="<?php echo PATH ?>">
										<div class="blueNavigationLink">
											Home
										</div>
									</a>
								</div>
								<div class="col-xs-6">
									<a href="<?php echo PATH ?>brand">
										<div class="blueNavigationLink">
											Brands We Carry
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-xs-6">
									<a href="<?php echo PATH ?>inventory">
										<div class="blueNavigationLink">
											Inventory
										</div>
									</a>
								</div>
								<div class="col-xs-6">
									<a href="<?php echo PATH ?>apparel">
										<div class="blueNavigationLink">
											Apparel
										</div>
									</a>
								</div>
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-xs-6">
									<a href="<?php echo PATH ?>service">
										<div class="blueNavigationLink">
											Service
										</div>
									</a>
								</div>
								<div class="col-xs-6">
									<a href="<?php echo PATH ?>parts">
										<div class="blueNavigationLink">
											Parts
										</div>
									</a>
								</div>
								
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-xs-6">
									<a href="<?php echo PATH ?>shop">
										<div class="blueNavigationLink">
											Shop
										</div>
									</a>
								</div>
								<div class="col-xs-6">
									<a href="<?php echo PATH ?>specials">
										<div class="blueNavigationLink">
											Specials
										</div>
									</a>
								</div>
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-xs-6">
									<a href="<?php echo PATH ?>blog">
										<div class="blueNavigationLink">
											Blog
										</div>
									</a>
								</div>
								<div class="col-xs-6">
									<a href="<?php echo PATH ?>events">
										<div class="blueNavigationLink">
											Events
										</div>
									</a>
								</div>
								
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-xs-6">
									<a href="<?php echo PATH ?>photos">
										<div class="blueNavigationLink">
											Photos
										</div>
									</a>
								</div>
								<div class="col-xs-6">
									<a href="<?php echo PATH ?>newsletters">
										<div class="blueNavigationLink">
											Newsletter
										</div>
									</a>	
								</div>
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-xs-6">
									<a href="<?php echo PATH ?>rewards">
										<div class="blueNavigationLink">
											Rewards
										</div>
									</a>
								</div>
								<div class="col-xs-6">
									<a href="<?php echo PATH ?>contact">
										<div class="blueNavigationLink">
											Contact
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-xs-6">
									<a href="http://www.siddillon.com/" target="_blank">
										<div class="blueNavigationLink">
											Sid Dillon
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style='margin:50px 0px 10px 0px; '>
							<div style='font-size:24px;'><strong>Quick links</strong></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div style="text-align:center;">
								<div>Dillon-Brothers</div>
								<div style='font-size:20px;'>Harley-Davidson</div>
							</div>
							<div class='quickLinkButtonContainer'>
								<a href="<?php echo $this -> HarleyInventory('New') ?>" target="_blank">
									<div class="buttonSingle HarleyButton">
										New Inventory
									</div>
								</a>
								<a href="<?php echo $this -> HarleyInventory('Used') ?>">
									<div class="buttonSingle HarleyButton">
										Used Inventory
									</div>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div style="text-align:center;">
								<div>Dillon-Brothers</div>
								<div style='font-size:20px;'>Motorsports</div>
							</div>
							<div class='quickLinkButtonContainer'>
								<a href="<?php echo $this -> MotorSportInventory('New') ?>">
									<div class="buttonSingle redButton">
										New Inventory
									</div>
								</a>
								<a href="<?php echo $this -> MotorSportInventory('Used') ?>">
									<div class="buttonSingle redButton">
										Used Inventory
									</div>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div style="text-align:center;">
								<div>Dillon-Brothers</div>
								<div style='font-size:20px;'>Indian Motorcycles</div>
							</div>
							<div class='quickLinkButtonContainer'>
								<a href="<?php echo $this -> IndianInventory('New') ?>">
									<div class="buttonSingle IndianButton">
										New Inventory
									</div>
								</a>
								<a href="<?php echo $this -> IndianInventory('Used') ?>">
									<div class="buttonSingle IndianButton">
										Used Inventory
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="ExtraLinkBottom">
					<div class="container" style='margin-top:0px;'>
						<div class="row">
							<div class="col-md-12">
								<div style='float:left; margin-right:20px;'>
									<a href='http://www.siddillon.com/all-vehicles/' target='_blank'><img src='<?php echo PATH ?>public/images/SidDillonLogoBlack.png' /></a><span class='bottomText'>Need more wheels?</span>	
								</div>
								<div style='float:left'>
									<a onclick="window.open('<?php echo PATH ?>textclub','sharer','height=800,width=800');" href="javascript:void(0);" style="text-decoration:none; color:black">
										<img src='<?php echo PATH ?>public/images/TextClub.png' />
									</a>
									<span class='bottomText'>Join our Text Club</span>	
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			
		</div>
	
	<!--<div class="DropDownLinks">
		
		<div class="Quicklinks" style='display:none;'>
			<div style='padding:0px 0px 10px 0px;'>
				<a href="javascript:void(0)" onclick='Globals.CloseInventoryQuickLinks()'>
					<i class="fa fa-times" style='color:#c9c9c9' aria-hidden="true"></i>
				</a>
			</div>
			<div style='border-bottom: 1px solid #a5a5a5; padding-bottom:5px; margin-bottom: 10px;'>
				<div class="container" style='width:100%'>
					<div class="row">
						
						
					</div>	
				</div>
			</div>
			<div style='width: 100%; display: table;'>
				<div class='NeedMoreWheelsText'>
					
				</div>
				<div style='display: table-cell; text-align: right;'>
					
				</div>
			</div>
			<div style='border-top: 1px solid #a5a5a5; padding-bottom: 5px; margin-top: 10px; padding-top: 10px; font-size: 20px;'>
				<a onclick="window.open('<?php echo PATH ?>textclub','sharer','height=800,width=800');" href="javascript:void(0);" style="text-decoration:none; color:black">Join our Text Club</a>
			</div>
			<?php if(isset($_SESSION['EcommerceUserAccount'])): ?>
				<div style='border-top: 1px solid #a5a5a5; padding-bottom: 5px; margin-top: 10px; padding-top: 10px; font-size: 20px;' id="MobileAccountLink">
					<a href='<?php echo PATH ?>shop/account' style="text-decoration:none; color:black">Your Account</a>
				</div>
			<?php endif; ?>	
			
		</div>-->
		
</header>



<div class="headerPush"></div>

<script type="application/ld+json">
[{
	"@type": "SiteNavigationElement",
	"@context": "http://schema.org",
	"name": "Dillon Brothers Home",
	"url": "<?php echo PATH ?>"
},
{
	"@type": "SiteNavigationElement",
	"@context": "http://schema.org",
	"name": "Dillon Brothers Inventory",
	"url": "<?php echo PATH ?>inentory"
},
{
	"@type": "SiteNavigationElement",
	"@context": "http://schema.org",
	"name": "Dillon Brothers Shop",
	"url": "<?php echo PATH ?>shop"
},
{
	"@type": "SiteNavigationElement",
	"@context": "http://schema.org",
	"name": "Dillon Brothers Specials",
	"url": "<?php echo PATH ?>specials"
},
{
	"@type": "SiteNavigationElement",
	"@context": "http://schema.org",
	"name": "Dillon Brothers Blog",
	"url": "<?php echo PATH ?>blog"	
},
{
	"@type": "SiteNavigationElement",
	"@context": "http://schema.org",
	"name": "Dillon Brothers Events",
	"url": "<?php echo PATH ?>events"	
},
{
	"@type": "SiteNavigationElement",
	"@context": "http://schema.org",
	"name": "Dillon Brothers Photos",
	"url": "<?php echo PATH ?>photos"	
},
{
	"@type": "SiteNavigationElement",
	"@context": "http://schema.org",
	"name": "Dillon Brothers Newsletters",
	"url": "<?php echo PATH ?>newsletters"	
},
{
	"@type": "SiteNavigationElement",
	"@context": "http://schema.org",
	"name": "Dillon Brothers VIB Rewards Program",
	"url": "<?php echo PATH ?>rewards"
},
{
	"@type": "SiteNavigationElement",
	"@context": "http://schema.org",
	"name": "Dillon Brothers Contact",
	"url": "<?php echo PATH ?>contact"
},
{
	"@type": "SiteNavigationElement",
	"@context": "http://schema.org",
	"name": "Sid Dillon",
	"url": "http://www.siddillon.com"	
}]				
</script>

<div class="vcard" style='display:none;'>
	<span class="fn n">Dillon Brothers MotorSports</span>
	<div class="org">
		<span class="organization-name">Dillon Brothers MotorSports</span>
	</div>
	<a class="email" href="mailto:customerservice@siddillon.com">customerservice@dillon-brothers.com</a>	<div class="adr">
		<div class="street-address">3848 N HWS Cleveland Blvd.</div>
		<span class="locality"></span>Omaha<span class="region">Nebraska</span>, 
		<span class="postal-code">68116</span>
	 	<span class="country-name">United States</span>
	 </div>
	<div class="tel">(402) 556-3333</div>
	<div class='pricerange'>$$$$</div>
	<div class='photo'><?php echo PATH ?>public/images/DillonMotor.jpg</div>
	<div class='logo'><?php echo PHOTO_URL ?>Stores/MotorSportLogo.png</div>
	<div class='url'>http://www.dilloncycles.com/</div>
	<div class='geo'>
    	<div class='latitude'>41.293818</div>
    	<div class='longitude'>-96.1884585</div>
    </div>
</div>
<div class="vcard" style='display:none;'>
	<span class="fn n">Dillon Brothers Harley-Davidson Omaha</span>
	<div class="org">
		<span class="organization-name">Dillon Brothers Harley-Davidson Omaha</span>
	</div>
	<a class="email" href="mailto:customerservice@siddillon.com">customerservice@dillon-brothers.com</a>	<div class="adr">
		<div class="street-address">3838 N HWS Cleveland Blvd.</div>
		<span class="locality"></span>Omaha<span class="region">Nebraska</span>, 
		<span class="postal-code">68116</span>
	 	<span class="country-name">United States</span>
	 </div>
	<div class="tel">(402) 289-5556</div>
	<div class='pricerange'>$$$$</div>
	<div class='photo'><?php echo PATH ?>public/images/DillonHarley.jpg</div>
	<div class='logo'><?php echo PHOTO_URL ?>Stores/HarleyLogo.png</div>
	<div class='url'>http://www.dillonharley.com/</div>
	<div class='geo'>
    	<div class='latitude'>41.292938</div>
    	<div class='longitude'>-96.1883625</div>
    </div>
</div>
<div class="vcard" style='display:none;'>
	<span class="fn n">Dillon Brothers Harley-Davidson Fremont</span>
	<div class="org">
		<span class="organization-name">Dillon Brothers Harley-Davidson Fremont</span>
	</div>
	<a class="email" href="mailto:customerservice@siddillon.com">customerservice@dillon-brothers.com</a>	<div class="adr">
		<div class="street-address">2440 East 23rd Street</div>
		<span class="locality"></span>Fremont<span class="region">Nebraska</span>, 
		<span class="postal-code">68025</span>
	 	<span class="country-name">United States</span>
	 </div>
	<div class="tel">(402) 721-2441</div>
	<div class='pricerange'>$$$$</div>
	<div class='photo'><?php echo PATH ?>public/images/DillonHarley.jpg</div>
	<div class='logo'><?php echo PHOTO_URL ?>Stores/HarleyLogo.png</div>
	<div class='url'>http://www.dillonharley.com/</div>
	<div class='geo'>
    	<div class='latitude'>41.4519858</div>
    	<div class='longitude'>-96.4654377</div>
    </div>

</div>

<div class="vcard" style='display:none;'>
	<span class="fn n">Dillon Brothers Indian Motorcycle</span>
	<div class="org">
		<span class="organization-name">Dillon Brothers Indian Motorcycle</span>
	</div>
	<a class="email" href="mailto:customerservice@siddillon.com">customerservice@dillon-brothers.com</a>	<div class="adr">
		<div class="street-address">3840 N 174th Ave</div>
		<span class="locality"></span>Omaha<span class="region">Nebraska</span>, 
		<span class="postal-code">68116</span>
	 	<span class="country-name">United States</span>
	 </div>
	<div class="tel">(402) 505-4233</div>
	<div class='pricerange'>$$$$</div>
	<div class='photo'><?php echo PATH ?>public/images/DillonIndian.jpg</div>
	<div class='logo'><?php echo PHOTO_URL ?>Stores/IndianLogo.png</div>
	<div class='url'>http://www.dillonbrothersindian.com/</div>
	<div class='geo'>
    	<div class='latitude'>41.29352</div>
    	<div class='longitude'>-96.1892845</div>
    </div>
</div>


