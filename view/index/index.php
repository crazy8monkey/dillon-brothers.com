<div class="container" style="padding-top:25px;">
	<div class="row">
		<div class="col-md-12 ContentHeader">
			<h1><span style='font-weight:normal;'>WELCOME TO</span> DILLON BROTHERS MOTORSPORTS</h1>
		</div>
	</div>
	<div style='margin-bottom:10px'>
		<div class="row">
			<div class="col-md-6">
				Whether your ripping around corners, taking a long ride on an open mountain pass, riding the waves or working the land; Dillon Brothers has the vehicle to fit your needs!
				Dillon Brothers is a multi-line motorcycle, ATV, watercraft and side by side dealer. We carry Harley-Davidson, Indian, Honda, Yamaha, Kawasaki, Suzuki, KTM and Triumph vehicles.<br /><br />
			</div>
			<div class="col-md-6">
				Here at Dillon Brothers, we want all our customers to have the best experience possible. Because we are enthusiasts, we ride motorcycles, we go off roading, we customize our 
				bikes, and we wear the gear. Above all, we love the feeling of belonging to the motorcycling family, and we want to share this experience with you.<br /><br />
			</div>
		</div>	
	</div>
	
</div>
<div class='StoreQuickLinks'>
	<div class="storeDetailSingle">
		<div class="storeImage" id="DillonMotor">
			<div class="websiteName">
				<a href='http://www.dilloncycles.com/' target="_blank">www.dilloncycles.com</a>
			</div>
			<div class="name">
				Dillon Brothers
				<div class="branchName">
					MotorSports
				</div>
				<div class="buttons">
					<a href="<?php echo $this -> MotorSportInventory('New') ?>"><div class='buttonSingle redButton'>New Inventory</div></a>
					<a href="<?php echo $this -> MotorSportInventory('Used') ?>"><div class='buttonSingle redButton'>Used Inventory</div></a>
				</div>
			</div>
			<div class="overlayStore"></div>
		</div>
		<div class="locationContact MotorSportContactSection">
			<strong>3848 N HWS Cleveland Blvd.<br>
			Omaha, NE 68116</strong><br><br>
			<table>
				<tr>
					<td class="phoneLabel">Phone:</td>
					<td>
						<div class="showDesktop">
							(402) 556-3333	
						</div>
						<div class="hideForMobilePhone">
							<a href='tel:4025563333'>(402) 556-3333</a>
						</div>
					</td>
				</tr>
				<tr>
					<td class="phoneLabel hideForMobileTr">Fax:</td>
					<td class="hideForMobileTr">(402) 556-1796</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="storeDetailSingle">
		<div class="storeImage" id="DillonHarley">
			<div class="websiteName">
				<a href='http://www.dillonharley.com/' target="_blank">www.dillonharley.com</a>
			</div>
			<div class="name">
				Dillon Brothers
				<div class="branchName">
					Harley-Davidson
				</div>
				<div class="buttons">
					<a href='<?php echo $this -> HarleyInventory('New') ?>' target='_blank'><div class='buttonSingle HarleyButton'>New Inventory</div></a>
					<a href="<?php echo $this -> HarleyInventory('Used') ?>"><div class='buttonSingle HarleyButton'>Used Inventory</div></a>
				</div>
			</div>
			<div class="overlayStore"></div>
		</div>
		<div class="locationContact HarleyContactSection">
			<div class="row" style='margin-left:0px; margin-right:0px;'>
				<div class="col-md-6" style='padding-left:0px; padding-right:0px;'>
					<strong>3838 N HWS Cleveland Blvd<br>
					Omaha, NE 68116</strong><br><br>
					<table>
						<tr>
							<td class="phoneLabel">Phone:</td>
							<td>
								<div class="showDesktop">
									(402) 289-5556
								</div>
								<div class="hideForMobilePhone">
									<a href='tel:4022895556'>(402) 289-5556</a>
								</div>	
							</td>
						</tr>
						<tr>
							<td class="phoneLabel hideForMobileTr">Fax:</td>
							<td class="hideForMobileTr">(402) 289-1931</td>
						</tr>
					</table><br />
				</div>
				<div class="col-md-6" style='padding-left:0px; padding-right:0px;'>
					<strong>2440 East 23rd Street<br>
					Fremont, NE 68025</strong><br><br>
					<table>
						<tr>
							<td class="phoneLabel">Phone:</td>
							<td>
								<div class="showDesktop">
									(402) 721-2007
								</div>
								<div class="hideForMobilePhone">
									<a href='tel:4027212007'>(402) 721-2007</a>
								</div>		
							</td>
						</tr>
						<tr>
							<td class="phoneLabel hideForMobileTr">Fax:</td>
							<td class="hideForMobileTr">(402) 721-2441</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="storeDetailSingle">
		<div class="storeImage" id="DillonIndian">
			<div class="websiteName">
				<a href='http://www.dillonbrothersindian.com/' target="_blank">www.dillonbrothersindian.com</a>
			</div>
			<div class="name">
				Dillon Brothers
				<div class="branchName">
					Indian Motorcycle
				</div>
				<div class="buttons">
					<a href="<?php echo $this -> IndianInventory('New') ?>"><div class='buttonSingle IndianButton'>New Inventory</div></a>
					<a href="<?php echo $this -> IndianInventory('Used') ?>"><div class='buttonSingle IndianButton'>Used Inventory</div></a>
				</div>
			</div>
			<div class="overlayStore"></div>
		</div>
		<div class="locationContact IndianContactSection">
			<strong>3840 N 174th Ave<br>
				Omaha, NE 68116</strong><br><br>
				<table>
					<tbody><tr>
						<td class="phoneLabel">Phone:</td>
						<td>
							<div class="showDesktop">
								(402) 505-4233
							</div>
							<div class="hideForMobilePhone">
								<a href='tel:4025054233'>(402) 505-4233</a>
							</div>	
						</td>
					</tr>
				</tbody></table>
		</div>
	</div>
	<div style='clear:both'></div>
</div>

<?php if(count($this -> featuredInventory) > 0): ?>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="FeaturedInventorySection" style='display:none'>
					<h1 class='HomePage'>FEATURED <strong>INVENTORY</strong></h1>
					<div class='featuredInventory'>
						<?php foreach($this -> featuredInventory as $inventorySingle): ?>
							<div class='featuredInventoryItem'>
								<a href="<?php echo PATH . 'inventory/' . $inventorySingle['InventoryURLString']; ?>">
									<?php if($inventorySingle['InventoryMainPhoto'] != NULL): ?>
										<?php 
											$bikeMainPhoto = PHOTO_URL . 'inventory/' . $inventorySingle['InventoryMainPhoto'];
										?>
										
										
										<div class="image" style='background:url(<?php echo $bikeMainPhoto ?>) no-repeat center'>
										
										</div>
									<?php else: ?>
										<div class="image" style='background:#eee; position: relative;'>
											<div class='NoPhoto'></div>
										</div>
									<?php endif; ?>
									
									<div class="bikeName">
										<?php echo $inventorySingle['InventoryBikeName'] ?>
									</div>
								</a>
							</div>
						<?php endforeach; ?>
						
					</div>	
				</div>
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div style='text-align:center; margin: 20px 0px 30px 0px;'>
					<a href='<?php echo PATH ?>inventory'>
						<div class="redButton viewAllInventory">
							View Full Inventory	
						</div>
						
					</a>	
				</div>
				
			</div>
		</div>
	</div>
<?php endif; ?>
<div class="featuredSpecialsEvents">
	<?php 
	if(count($this -> featuredevent) > 0) {
		$halfClass = "featureHalf";
	} else {
		$halfClass = "featureFull";
	}?>
	
	<?php if(count($this -> featuredSpecial) > 0): ?>	
		<?php 
			if($this -> featuredSpecial[0]['StoreNames'] != NULL) {
				switch(true) {
					case stripos($this -> featuredSpecial[0]['StoreNames'], "MotorSports"):
						$url = PATH . 'specials/motorsport/' . $this -> featuredSpecial['specialSEOurl'];
						break;
					case stripos($this -> featuredSpecial[0]['StoreNames'], "Harley"):
						$url = PATH . 'specials/harley/' . $this -> featuredSpecial['specialSEOurl'];
						break;
					case stripos($this -> featuredSpecial[0]['StoreNames'], "Indian"):
						$url = PATH . 'specials/indian/' . $this -> featuredSpecial['specialSEOurl'];
						break;
				}
			} else {
				switch($this -> featuredSpecial[0]['specialType']) {
					case 2:
						$url = PATH . 'specials/apparelspecial/' . $this -> featuredSpecial[0]['specialSEOurl'];
						break;		
					case 3:
						$url = PATH . 'specials/partsspecial/' . $this -> featuredSpecial[0]['specialSEOurl'];
						break;		
					case 4:
						$url = PATH . 'specials/servicespecial/' . $this -> featuredSpecial[0]['specialSEOurl'];
						break;	
					case 5:
						$url = PATH . 'specials/storespecial/' . $this -> featuredSpecial[0]['specialSEOurl'];
						break;
				}
			}
		
		?>
		<div class="specialFeatures <?php echo $halfClass ?>" style='position:relative;'>
			<div class="Text">
				<div style='text-align:center; font-size:36px; color: #979797;'>
					Specials
				</div>
				<div>
					<div style="text-align:center; margin: 20px 0px;">
						<a href="<?php echo $url ?>">
							<div class="redButton viewAllInventory">
								View Special
							</div>
						</a>	
					</div>
				</div>
				<div class="caretRight"></div>
				<div class="caretBottom"></div>
			</div>
			<div class="image" style='background:url(<?php echo PHOTO_URL . 'specials/'. $this -> featuredSpecial[0]['FolderName'] . '/' . $this -> featuredSpecial[0]['SpecialMainImage'] ?>) no-repeat center;'></div>
			<div style='clear:both'></div>
		</div>
	<?php endif; ?>	
	
	<?php if(count($this -> featuredevent) > 0): ?>
		<?php 
		
			if($this -> featuredevent[0]['SubEvent'] == 1) {	
				$hyperLink = PATH . $this -> featuredevent[0]['SuperEventSEOUrl'] . '/#' . $this -> featuredevent[0]['seoUrl'];
			} else {
				$hyperLink = PATH .  $this -> featuredevent[0]['seoUrl'];
			} 
			
		?>
		
		<div class="specialFeatures <?php echo $halfClass ?>" style='position:relative;'>
			<div class="Text">
				<div style='text-align:center; font-size:36px; color: #979797;'>
					Events
				</div>
				<div>
					<div style="text-align:center; margin: 20px 0px;">
						<a href="<?php echo $hyperLink ?>">
							<div class="redButton viewAllInventory">
								View Event
							</div>
							
						</a>	
					</div>
				</div>
				<div class="caretRight"></div>
				<div class="caretBottom"></div>
			</div>
			<div class="image" style='background:url(<?php echo PHOTO_URL . $this -> featuredevent[0]['ParentDirectory'] . '/events/' . $this -> featuredevent[0]['albumFolderName'] . '/'. $this -> featuredevent[0]['photoName'] . '-m.' . $this -> featuredevent[0]['ext'] ?>) no-repeat center;'></div>
			<div style='clear:both'></div>
		</div>
	<?php endif; ?>
	
	
	
	
	<div style='clear:both'></div>
</div>


<script type="application/ld+json">
[{
  "@context": "http://schema.org",
  "@type": "LocalBusiness",
  "address": {
    "@type": "PostalAddress",
    "name": "Dillon Brothers MotorSports",
    "streetAddress": "3848 N HWS Cleveland Blvd",
    "addressLocality": "Omaha",
    "addressRegion": "Nebraska",
    "postalCode" : "68116",
    "telephone" : "(402) 556-3333",
    "faxNumber" : "(402) 556-1796"
  },
  "openingHours" : [
	  	"Monday 9:00-19:00",
	    "Tuesday 9:00-19:00",
	    "Wednesday 9:00-17:30",
	    "Thursday 9:00-19:00",
	    "Friday 9:00-17:30",
	    "Saturday 9:00-16:30"
   ],
  "name" : "Dillon Brothers MotorSports",
  "image" : "<?php echo PATH ?>public/images/DillonBrothers.png",
  "url": "<?php echo PATH ?>",
  "priceRange" : "00.00"
},
{
  "@context": "http://schema.org",
  "@type": "LocalBusiness",
  "address": {
    "@type": "PostalAddress",
    "name": "Dillon Brothers Harley-Davidson Omaha",
    "streetAddress": "3838 N HWS Cleveland Blvd",
    "addressLocality": "Omaha",
    "addressRegion": "Nebraska",
    "postalCode" : "68116",
    "telephone" : "(402) 289-5556",
    "faxNumber" : "(402) 289-1931"
  },
  "openingHours": [
		"Monday 9:00-19:00",
		"Tuesday 9:00-19:00",
		"Wednesday 9:00-17:30",
		"Thursday 9:00-19:00",
		"Friday 9:00-17:30",
		"Saturday 9:00-16:30"
  	],
  "name" : "Dillon Brothers Harley-Davidson Omaha",
  "image" : "<?php echo PATH ?>public/images/DillonBrothers.png",
  "url": "<?php echo PATH ?>",
  "priceRange" : "00.00"
},
{
  "@context": "http://schema.org",
  "@type": "LocalBusiness",
  "address": {
    "@type": "PostalAddress",
    "name": "Dillon Brothers Harley-Davidson Fremont",
    "streetAddress": "2440 East 23rd St",
    "addressLocality": "Fremont",
    "addressRegion": "Nebraska",
    "postalCode" : "68025",
    "telephone" : "(402) 721-2007",
    "faxNumber" : "(402) 721-2441"
  },
  "openingHours": [
	"Tuesday 9:00-18:00",
	"Wednesday 9:00-17:30",
	"Thursday 9:00-18:00",
	"Friday 9:00-17:30",
	"Saturday 9:00-16:30"
  	],
  "name" : "Dillon Brothers Harley-Davidson Fremont",
  "image" : "<?php echo PATH ?>public/images/DillonBrothers.png",
  "url": "<?php echo PATH ?>",
  "priceRange" : "00.00"
},
{
  "@context": "http://schema.org",
  "@type": "LocalBusiness",
  "address": {
    "@type": "PostalAddress",
    "name": "Dillon Brothers Indian",
    "streetAddress": "3840 N 174th Ave.",
    "addressLocality": "Omaha",
    "addressRegion": "Nebraska",
    "postalCode" : "68116",
    "telephone" : "(402) 505-4233"
  },
  "openingHours" : [
	  	"Monday 9:00-19:00",
	    "Tuesday 9:00-19:00",
	    "Wednesday 9:00-17:30",
	    "Thursday 9:00-19:00",
	    "Friday 9:00-17:30",
	    "Saturday 9:00-16:30"
   ],
  "name" : "Dillon Brothers Indian",
  "image" : "<?php echo PATH ?>public/images/DillonBrothers.png",
  "url": "<?php echo PATH ?>",
  "priceRange" : "00.00"
}]
</script>
