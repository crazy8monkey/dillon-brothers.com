<?php 
	function LabelOrder($a, $b) {
		$t1 = $a['Order'];
		$t2 = $b['Order'];
		return $t1 - $t2;
	} 
?>
<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html xml:lang="en"> <!--<![endif]-->
<head>
	<title><?php echo $this -> title ?></title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
	
	<?php require 'view/MetaDataContent.php'; ?>
		
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />	
	<link rel="stylesheet" href="<?php echo PATH ?>public/css/style.css" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
	
	<link rel="stylesheet" href="<?php echo PATH ?>public/css/InventoryController.css" type="text/css" />
		
		
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>	
	<script src="<?php echo PATH ?>public/js/Globals.js" type="text/javascript" type="text/javascript"></script>
	
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="<?php echo PATH ?>public/js/InventoryController.js"></script>
		
	<script src="https://use.typekit.net/kpv4owm.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<script type="text/javascript">
		$(document).ready(function(){
			InventoryController.SendToFriend()
		});
		
		function CloseWindow() {
			window.close();
		}
		
	</script>	
		

</head>
<body>

<div id="ShoppingToolPopupForm">
	<?php 
		switch($this -> CurrentInventory -> StoreID) {
			case 2:
				$class = "MotorSport";
				break;
			case 3:
				$class = "Harley";
				break;
			case 4:
				$class = "Indian";
				break;
		}
	?>
	
	<div class="headerContent <?php echo $class ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Estimate Your Payments</h1>
				</div>
			</div>
		</div>	
	</div>
	<div class="container">
		
		<!--<div class="row">
			<div class="col-md-12" style='margin-bottom:20px;'>
				<div>Selling Price</div>
				<div class="loanAmount">$<?php echo number_format($this -> CurrentInventory -> MSRP, 0);  ?></div>
			</div>
		</div> -->
		<form name="loandata">
			<div class="row">
				<div class="col-md-12">
					<div class="input">
						<div class="inputLabel">Selling Price <span style='font-weight:normal'>(In Numbers)</span></div>
						<input type="text" name="principal" size="12" value='<?php echo $this -> CurrentInventory -> MSRP?>' onchange="calculate();">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div class="input">
						<div class="inputLabel">APR</div>
						<select name="interest" onchange="calculate();">
							<option value='0.25'>0.25%</option>
							<option value='0.5'>0.5%</option>
							<option value='0.75'>0.75%</option>
							<option value='1.0'>1.0%</option>
							<option value='1.25'>1.25%</option>
							<option value='1.5'>1.5%</option>
							<option value='1.75'>1.75%</option>
							<option value='2.0'>2.0%</option>
							<option value='2.25'>2.25%</option>
							<option value='2.5'>2.5%</option>
							<option value='2.75'>2.75%</option>
							<option value='3.0'>3.0%</option>
							<option value='3.25'>3.25%</option>
							<option value='3.5'>3.5%</option>
							<option value='3.75'>3.75%</option>
							<option value='4.0'>4.0%</option>
							<option value='4.25'>4.25%</option>
							<option value='4.5' selected>4.5%</option>
							<option value='4.75'>4.75%</option>
							<option value='5.0'>5.0%</option>
							<option value='5.25'>5.25%</option>
							<option value='5.5'>5.5%</option>
							<option value='5.75'>5.75%</option>
							<option value='6.0'>6.0%</option>
							<option value='6.25'>6.25%</option>
							<option value='6.5'>6.5%</option>
							<option value='6.75'>6.75%</option>
							<option value='7.0'>7.0%</option>
							<option value='7.25'>7.25%</option>
							<option value='7.5'>7.5%</option>
							<option value='7.75'>7.75%</option>
							<option value='8.0'>8.0%</option>
							<option value='8.25'>8.25%</option>
							<option value='8.5'>8.5%</option>
							<option value='8.75'>8.75%</option>
							<option value='9.0'>9.0%</option>
							<option value='9.25'>9.25%</option>
							<option value='9.5'>9.5%</option>
							<option value='9.75'>9.75%</option>
							<option value='10.0'>10.0%</option>
							<option value='10.25'>10.25%</option>
							<option value='10.5'>10.5%</option>
							<option value='10.75'>10.75%</option>
							<option value='11.0'>11.0%</option>
						</select>
					</div>
					
				</div>
				<div class="col-xs-6">
					<div class="input">
						<div class="inputLabel">Payment Period</div>
						<select name="years" onchange="calculate();">
							<option value='1'>12 Months</option>
							<option value='2'>24 Months</option>
							<option value='3'>36 Months</option>
							<option value='4'>48 Months</option>
							<option value='5' selected>60 Months</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<input type="button" value="Calculate Payments" class="blueButton" onclick="calculate();">			
				</div>
			</div>
			<div id="TotalPaymentInfo" style='display:none;'>
				<div class="row">
					<div class="col-md-12">
						<h2>Payment Information</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="loanNumber">
							<div><strong>Your Monthly Payment</strong></div>
							<div class='finalPaymentNumber'><div id="MonthyPayment"></div></div>	
						</div>
						<div class="loanNumber">
							<div><strong>Your Payment Total</strong></div>
							<div class='finalPaymentNumber'><div id="TotalPaymentLoan"></div></div>
						</div>
						<div class="loanNumber">
							<div><strong>Total Interest</strong></div>
							<div class='finalPaymentNumber'><div id="TotalInterest"></div></div>
						</div>
						<div style='clear:both'></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="disclaimer">
							* This information does not include Sales Tax and fees
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12" style='margin-top:10px;'>
					<a href="javascript:void(0);" onclick='CloseWindow()'>
						<div class="closePopup">
							Close Popup
						</div>	
					</a>			
				</div>
			</div>
			
</form>

<!--
  This is the JavaScript program that makes the example work. Note that
  this script defines the calculate() function called by the event
  handlers in the form.  The function refers to values in the form
  fields using the names defined in the HTML code above.
-->
<script language="JavaScript">
	function MoneyFormat(NumberValue) {
		return '$' + NumberValue.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	}


function calculate() {
    // Get the user's input from the form. Assume it is all valid.
    // Convert interest from a percentage to a decimal, and convert from
    // an annual rate to a monthly rate. Convert payment period in years
    // to the number of monthly payments.
    var principal = document.loandata.principal.value;
    var interest = document.loandata.interest.value / 100 / 12;
    var payments = document.loandata.years.value * 12;

    // Now compute the monthly payment figure, using esoteric math.
    var x = Math.pow(1 + interest, payments);
    var monthly = (principal*x*interest)/(x-1);

    // Check that the result is a finite number. If so, display the results
    if (!isNaN(monthly) && 
        (monthly != Number.POSITIVE_INFINITY) &&
        (monthly != Number.NEGATIVE_INFINITY)) {
		
		$("#MonthyPayment").html(MoneyFormat(round(monthly)));
		$("#TotalPaymentLoan").html(MoneyFormat(round(monthly * payments)));
		$("#TotalInterest").html(MoneyFormat(round((monthly * payments) - principal)));
		
        $("#TotalPaymentInfo").show();
    }
    // Otherwise, the user's input was probably invalid, so don't
    // display anything.
    else {
        document.loandata.payment.value = "";
        document.loandata.total.value = "";
        document.loandata.totalinterest.value = "";
    }
}

// This simple method rounds a number to two decimal places.
function round(x) {
  return Math.round(x*100)/100;
}
</script>
	</div>
</div>





</body>	
</html>
