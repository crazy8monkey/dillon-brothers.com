<div id="InventoryList">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2 FilterInventory">
				
				<div style="border-bottom:1px solid #cecece;">
				<form method="post" id="FilterInventory">
					
					<input type="hidden" name="RowNumber" id="RowNumber" value="2" />
					<?php if(count($this -> condition) > 0): ?>
						<div class="filterSection">
							<a href="javascript:void(0);" onclick="InventoryController.OpenFilterList(this)">
								<div class="sectionHeader">Condition</div>	
							</a>
							<div class="filterList" id="ConditionFilterHtml">
								<?php foreach($this -> condition as $conditionSingle): ?>
									<div class="line">
										<input type='checkbox' class='ConditionCheck' name="Condition[]" value="<?php echo $conditionSingle['Conditions'] ?>" /> <?php echo ($conditionSingle['Conditions'] == 0 ? "New": "Used") ?>	
									</div>
									
								<?php endforeach; ?>							
							</div>
	
						</div>
					<?php endif; ?>	
					
					<?php if(count($this -> years) > 0): ?>					
						<div class="filterSection">
							<a href="javascript:void(0);" onclick="InventoryController.OpenFilterList(this)">
								<div class="sectionHeader">Years</div>
							</a>
							<div class="filterList" id="YearsFilterHtml">
								<?php foreach($this -> years as $year): ?>
									<div class="line">
										<input type='checkbox' name="Year[]" value="<?php echo $year['Year'] ?>" /> <?php echo $year['Year'] ?>	
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					<?php endif; ?>	

					<?php if(count($this -> category) > 0): ?>					
						<div class="filterSection">
							<a href="javascript:void(0);" onclick="InventoryController.OpenFilterList(this)">
								<div class="sectionHeader">Category</div>
							</a>
							<div class="filterList" id="CategoryFilterHtml">
								<?php foreach($this -> category as $categorySingle): ?>
									<div class="line">
										<input type='checkbox' name="category[]" value="<?php echo $categorySingle['Category'] ?>" /> <?php echo $categorySingle['inventoryCategoryName'] ?>	
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					<?php endif; ?>	
					
				
					<?php if(count($this -> stores) > 0): ?>
						<div class="filterSection">
							<a href="javascript:void(0);" onclick="InventoryController.OpenFilterList(this)">
								<div class="sectionHeader">Stores</div>
							</a>
							<div class="filterList" id="StoresFilterHTML">
								<?php foreach($this -> stores as $storeSingle): ?>
									<div class="line">
										<input type='checkbox' name="store[]" value="<?php echo $storeSingle['storeID'] ?>" /> <?php echo $storeSingle['StoreName'] ?>
									</div>
								<?php endforeach; ?>
							</div>
							
						</div>
					<?php endif; ?>
					
					<?php if(count($this -> manufactureFilter) > 0): ?>				
						<div class="filterSection">
							<a href="javascript:void(0);" onclick="InventoryController.OpenFilterList(this)">
								<div class="sectionHeader">Manufactures</div>
							</a>
							<div class="filterList" id="ManufactureFilterHTML">
								<?php foreach($this -> manufactureFilter as $manufactureSingle): ?>
									<div class="line">
										<input type='checkbox' name='manufacture[]' value="<?php echo $manufactureSingle['Manufacturer'] ?>" /> <?php echo $manufactureSingle['Manufacturer'] ?>
									</div>
								<?php endforeach; ?>					
							</div>
							
						</div>
					<?php endif; ?>
					
					<?php if(count($this -> colors) > 0): ?>				
						<div class="filterSection">
							<a href="javascript:void(0);" onclick="InventoryController.OpenFilterList(this)">
								<div class="sectionHeader">Colors</div>
							</a>
							<div class="filterList" id="ColorFilterHTML">
								<?php foreach($this -> colors as $colorSingle): ?>
									<?php if($colorSingle['FilterColor'] != NULL): ?>
										<div class="line">
											<input type='checkbox' name="color[]" value="<?php echo $colorSingle['FilterColor'] ?>" /> <?php echo $colorSingle['FilterColor'] ?>
										</div>
									<?php endif; ?>		
								<?php endforeach; ?>		
							</div>			
							
							
						</div>
					<?php endif; ?>
					
					
				</form>
				
				
				
				
				
				
				
				</div>
				
			</div>
			<div class="col-md-10" style='margin-bottom: 90px;'>
				<div class="FiterResultsSection">
					<div style="float:left">
						<div class="resultNumber"><strong data-bind='text: inventoryList().length'  id="count"></strong> results</div>	
					</div>
					<div style="float:right">
						<div class="toggleViews">
							<a href="javascript:void(0)" onclick="InventoryController.toggleList(2)">							
								<div class="TwoByTwo SelectedList">
									<div class='line spacing'>
										<div class="toggleBox"></div>
										<div class="toggleBox" style='margin-left:3px;'></div>	
										<div style='clear:both'></div>
									</div>
									<div>
										<div class="toggleBox"></div>
										<div class="toggleBox" style='margin-left:3px;'></div>
										<div style='clear:both'></div>
									</div>
								</div>
							</a>
							<a href="javascript:void(0)" onclick="InventoryController.toggleList(3)">							
								<div class="ThreeByThree">
									<div class='spacing'>
										<div class="toggleBox"></div>
										<div class="toggleBox" style='margin-left:3px;'></div>	
										<div class="toggleBox" style='margin-left:3px;'></div>	
										<div style='clear:both'></div>
									</div>
									<div class='line spacing'>
										<div class="toggleBox"></div>
										<div class="toggleBox" style='margin-left:3px;'></div>	
										<div class="toggleBox" style='margin-left:3px;'></div>	
										<div style='clear:both'></div>
									</div>
									<div>
										<div class="toggleBox"></div>
										<div class="toggleBox" style='margin-left:3px;'></div>
										<div class="toggleBox" style='margin-left:3px;'></div>
										<div style='clear:both'></div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div style='clear:both'></div>
				</div>
				<div id="CurrentInventoryList" data-bind="foreach: inventoryList">
					<div class="inventorySingleObject HalfItem">
						<a data-bind='attr: {href: url}'>
							<div class="inventoryTitle" style="color:black;" data-bind='text: MotorcyleName'>
							</div>
						</a>
						<div class="row" style="margin-left: 0px; margin-right: 0px;">
							<a data-bind='attr: {href: url}'>
								<div class="col-md-4" style="padding-left: 0px; padding-right: 0px;" data-bind='visible: MainImage != false'>
									<img data-bind='attr: {src: MainImage}' width='100%' />
								</div>
								<div class="col-md-4" style="padding-left: 0px; padding-right: 0px; background:#eee; position:relative;" data-bind='visible: MainImage == false'>
									<div class="NoPicture"></div>
								</div>
								<div class="col-md-8">
									<div class="row" style="margin-top: 10px;">
										<div class="col-md-12">
											<div class="CarDetailInfo">
												<table>
													<tr><td style="padding-right:15px;"><strong>Condition:</strong></td></td>
													<tr><td style="padding-right:15px;"><strong>Stock:</strong></td></td>
													<tr><td style="padding-right:15px;"><strong>Stock:</strong></td></td>
												</table>
											</div>
										</div>
										
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
	
	
	



