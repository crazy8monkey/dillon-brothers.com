<?php 
	function LabelOrder($a, $b) {
		$t1 = $a['Order'];
		$t2 = $b['Order'];
		return $t1 - $t2;
	} 
?>
<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html xml:lang="en"> <!--<![endif]-->
<head>
	<title>Stock: <?php echo $this -> inventoryPrint -> stockNumber ?> | <?php echo str_replace("<br />", " | ", $this -> inventoryPrint -> GetStoreAddress())?> | <?php echo $this -> inventoryPrint -> GetStorePhoneNumber() ?></title>
	
	<?php require 'view/MetaDataContent.php'; ?>
				
	<!-- Google Authorship and Publisher Markup --> 
	<link rel="publisher" href="https://plus.google.com/103831261532249725840/" />
	<link rel="author" href="https://plus.google.com/103831261532249725840/" />
		
		
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />	
	<link rel="stylesheet" href="<?php echo PATH ?>public/css/style.css" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
	
	<link rel="stylesheet" href="<?php echo PATH ?>public/css/InventoryController.css" type="text/css" />
		
		
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>	
	<script src="<?php echo PATH ?>public/js/Globals.js" type="text/javascript" type="text/javascript"></script>
	
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
	<script src="https://use.typekit.net/kpv4owm.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip({html: true});
		});
		
		function CloseWindow() {
			window.close();
		}
		
	</script>	
		

</head>
<body>
<a href="javascript:void()" onclick="javascript:print();">
	<div class="PrintButton" data-toggle="tooltip" data-placement="bottom" title="Print">
		<img src="<?php echo PATH ?>public/images/PrintButton.png" />
	</div>		
</a>

<div class="PrintWrapper">
	<div class="logoHeader">
		<div style="float:left">
			<img src='<?php echo PHOTO_URL ?>Stores/<?php echo $this -> inventoryPrint -> StoreImage ?>' />		
		</div>
		<div style="float:right">
			<div style='margin:10px 0px; font-size:14px;'>
				<div style="float:left; margin-right:10px;">
					<i class="fa fa-phone" aria-hidden="true"></i>	
				</div>
				<?php echo $this -> inventoryPrint -> GetStorePhoneNumber() ?>	
				<div style="clear:both"></div>
			</div>
			<div style='clear:both; font-size:14px;'>
				<div style="float:left; margin-right:10px;">
					<i class="fa fa-envelope-o" aria-hidden="true"></i>
				</div>
				<?php echo $this -> inventoryPrint -> GetStoreEmail() ?>
				<div style="clear:both"></div>
			</div>
			
		</div>
		<div style="clear:both"></div>
		
	</div>
				
		
	<div class="printHeader">
		<h1><?php echo $this -> inventoryPrint -> GetCondition();  ?> <?php echo $this -> inventoryPrint -> GetBikeName() ?></h1>
	</div>
	<div class="Content">
		<div style="clear:both; margin-bottom: 15px;">
			<div class="MainImage">
				<img src='<?php echo $this -> inventoryPrint -> GetMainPhoto() ?>' />
			</div>
			<div class="details">
				<div style="margin-bottom:10px;">
					<h2>Selling Price</h2>
					<div class="price">$<?php echo number_format($this -> inventoryPrint -> MSRP, 0);  ?></div>				
				</div>
	
				<div style='margin-bottom:10px;'>
						<table class="briefDescription">
							<?php if(!empty($this -> inventoryPrint -> FriendlyModelName)): ?>
								<tr>
									<td class='MainLabel'>Model Number:</td>
									<td><?php echo $this -> inventoryPrint -> ModelName; ?></td>
								</tr>
							<?php endif; ?>
							<tr>
								<td class='MainLabel'>Category:</td>
								<td><?php echo $this -> inventoryPrint -> Category; ?></td>
							</tr>
							<?php if($this -> inventoryPrint -> IsDummyInventoryFlag == 0): ?>
								<tr>
									<td class='MainLabel'>VIN:</td>
									<td><?php echo $this -> inventoryPrint -> VIN; ?></td>
								</tr>
							<?php endif; ?>
							<tr>
								<td class='MainLabel'>Mileage:</td>
								<td><?php echo number_format($this -> inventoryPrint -> Mileage, 0); ?></td>
							</tr>
							<?php if($this -> inventoryPrint -> IsDummyInventoryFlag == 0): ?>
								<tr>
									<td class='MainLabel'>Stock Number:</td>
									<td><?php echo $this -> inventoryPrint -> stockNumber; ?></td>
								</tr>
							<?php endif; ?>
							<tr>
								<td class='MainLabel'>Color:</td>
								<td><?php echo $this -> inventoryPrint -> Color; ?></td>
							</tr>
							<tr>
								<td class='MainLabel'>Location:</td>
								<td><?php echo $this -> inventoryPrint -> GetStoreAddress(); ?></td>
							</tr>
						</table>
					</div>
				
				
			</div>	
			<div style="clear:both;"></div>		
		</div>
		
		<div style="clear:both">
			<div class="SectionHeader">
				<h2>Information on this Vehicle</h2>
			</div>
			
			<div style="clear:both; margin-bottom:10px;">
				<div style="float:left; margin-top:10px; width:490px;font-size: 14px;">
					<?php if(!empty($this -> inventoryPrint -> InventoryDescription)) {
						echo $this -> inventoryPrint -> InventoryDescription;
					} else {
						echo "There is no description of this bike";
					}
					
					?>		
				</div>
				
					
					
				<div style="float:right; width:300px; margin-top:10px;">
					<?php switch($this -> inventoryPrint -> storeLocation): 
					case 'A':?>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1498.8659734479236!2d-96.18890734164641!3d41.29293799481838!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x879392baaaaaaaab%3A0x26919175d25cc9d5!2sDillon+Brothers+Harley-Davidson+Omaha!5e0!3m2!1sen!2sus!4v1487620581226" width="300" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
					<?php break; ?>
					<?php case 'B': ?>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2990.41064370398!2d-96.46753028389769!3d41.45200807925821!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x879160e668b24bdd%3A0xd842147447e22932!2sDillon+Brothers+Harley-Davidson+Fremont!5e0!3m2!1sen!2sus!4v1487620660538" width="300" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
					<?php break; ?>
					<?php case 'C': ?>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1498.8460622400476!2d-96.18758678111641!3d41.293804576053056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8793ee364b427cc7%3A0x170b72a9865a3c45!2sDillon+Brothers+MotorSports!5e0!3m2!1sen!2sus!4v1487619271223" width="300" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
					<?php break; ?>
					<?php case 'D': ?>	
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2997.7052395285264!2d-96.19092278395199!3d41.29351917927287!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8793ee366f2e8091%3A0x5472b7a7dc221602!2sDillon+Brothers+Indian+Motorcycle+%F0%9F%8F%8D!5e0!3m2!1sen!2sus!4v1487620679494" width="300" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
					<?php break; ?>
					<?php endswitch; ?>
				</div>
				<div style="clear:both;"></div>	
			</div>
			
		</div>
		<div style="clear:both">
			<div class="SectionHeader">
				<h2>Specifications</h2>
			</div>
			<div style="clear:both; margin-top:10px;">
			<?php 
				$specDecoded = json_decode($this -> inventoryPrint -> GetInventorySpecs(), true); 
				$schemaArray = array();			
			?>
						
			<?php 
				$specLayout = array();
				foreach($this -> specgroups as $spec) {
					$labelCount = 0;
					$groupSpecID = $spec['specGroupID'];
										
					$filteredSpecs = array_filter($this -> speclabels, function ($var) use($groupSpecID) {
						 return $var['relatedSpecGroupID'] == $groupSpecID; 
						}
					);	
								
					$finalSpecs = array();
					foreach($filteredSpecs as $specLabelCheck) {
						if(array_search($specLabelCheck['specLabelID'], array_column($specDecoded, 'LabelID')) !== FALSE) {
							$key = array_search($specLabelCheck['specLabelID'], array_column($specDecoded, 'LabelID'));
							if(!empty($specDecoded[$key]['Content'])) {
								$labelCount++;							   
											
								array_push($finalSpecs, array("Text" => $specLabelCheck['labelText'],
															  "Content" => $specDecoded[$key]['Content']));	
													
								array_push($schemaArray, array("Text" => $specLabelCheck['labelText'],
															   "Content" => $specDecoded[$key]['Content']));
							}
										
										
						}
					}

									
					array_push($specLayout, array("GroupText" => $spec['specGroupText'],
												  "SpecGroupCount" => $labelCount,
												  "SpecLabels" => $finalSpecs));	
				}
			?>
			<?php foreach($specLayout as $specDetails): ?>
				<?php if($specDetails['SpecGroupCount'] > 0): ?>
					<div style="margin-bottom:20px;">
						<div class="specGroupHeader"><?php echo strtoupper($specDetails['GroupText']) ?></div>
						<?php foreach($specDetails['SpecLabels'] as $specSingle) : ?>
							<div class="row" style="margin: 0px;">
								<div class="col-xs-2" style='padding-left:0px;'>
									<div class='printSpecLabel'>
										<?php echo $specSingle['Text'] ?>:
									</div>
								</div>
								<div class="col-xs-10">
									<?php echo $specSingle['Content'] ?>
								</div>
								<div style="clear: both; border-bottom:1px solid #cecece; padding-bottom:5px; margin-bottom:5px;"></div>													
							</div>	
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
			</div>
			
			
		</div>

	</div>
	
	<a href="javascript:void(0);" onclick='CloseWindow()'>
		<div class="closePopup" style='margin-top:10px;'>
			Close Popup
		</div>	
	</a>
	
</div>

<?php  $i = 0; $len = count($schemaArray); ?>
<script type="application/ld+json">
[

{
  "@context": "http://schema.org",
  "@type": "Motorcycle",
  "model": "<?php echo $this -> inventoryPrint -> ModelName; ?>",
  "modelDate": "<?php echo $this -> inventoryPrint -> year ?>",
  "name": "<?php echo $this -> inventoryPrint -> GetCondition();  ?> <?php echo $this -> inventoryPrint -> GetBikeName() ?>",
  "productID": "<?php echo $this -> inventoryPrint -> VIN; ?>",
  "mileageFromOdometer": "<?php echo $this -> inventoryPrint -> Mileage ?>",
  "sku": "<?php echo $this -> inventoryPrint -> stockNumber; ?>",
  <?php if(!empty($this -> inventoryPrint -> InventoryDescription)): ?>
	"description": "<?php echo $this -> inventoryPrint -> InventoryDescription; ?>",						
  <?php endif; ?>
  
  
  
  <?php if(isset($mainImage)): ?>
  	"image": "<?php echo $mainImage ?>",
  <?php else: ?>  
    "image": "<?php echo PATH ?>public/images/NoPhotoPlacement.png",
  <?php endif; ?>  
  
  "additionalProperty": [
  	<?php foreach($schemaArray as $spec): ?>
    {  "@type": "PropertyValue",


      "name": "<?php echo $spec['Text'] ?>",
      "value": "<?php echo $spec['Content'] ?>"
    }<?php if($i != $len - 1) :?>,<?php endif; ?>
		<?php $i++ ?>	  
  <?php endforeach; ?>
  ],
  
  
  
  
  "color": "<?php echo $this -> inventoryPrint -> Color; ?>",
  "category": "<?php echo $this -> inventoryPrint -> Category; ?>",
  "brand": {
  	"@type": "Thing",
    "name": "<?php echo $this -> inventoryPrint -> manufactureText; ?>"
  },
  "offers": {
  "@type": "Offer",
    "priceCurrency": "USD",
    "price": "<?php echo $this -> inventoryPrint -> MSRP;  ?>",
    <?php if($this -> inventoryPrint -> Conditions == 0): ?>
   		"itemCondition": "http://schema.org/NewCondition",
   	<?php else: ?>
   		"itemCondition": "http://schema.org/UsedCondition",
   	<?php endif; ?>

   	
   	
    "availability": "http://schema.org/InStock",
    "seller": {
      "@type": "MotorcycleDealer",
      <?php switch($this -> inventoryPrint -> storeLocation): 
		case 'A':?>
		"name" : "Dillon Brothers Harley-Davidson Omaha",
		"address": {
		 	"name" : "Dillon Brothers Harley-Davidson Omaha",
		 	"streetaddress" : "3838 N HWS Cleveland Blvd",
		 	"addressLocality" : "Omaha",
		 	"addressRegion" : "Nebraska",
		 	"postalCode" : "68116"
		},
		"openingHours": [
			"Monday 9:00-19:00",
			"Tuesday 9:00-19:00",
			"Wednesday 9:00-17:30",
			"Thursday 9:00-19:00",
			"Friday 9:00-17:30",
			"Saturday 9:00-16:30"
	  	],
		"email": "sales@dillonharley.com",	
	  	"priceRange" : "<?php echo $this -> inventoryPrint -> GetPriceRange() ?>",
		"image" : "<?php echo PATH ?>public/images/HarleyDavidson.png",
		"geo": {
          "@type": "GeoCircle",
          "geoMidpoint": {
            "@type": "GeoCoordinates",
            "latitude": "41.292940",
            "longitude": "-96.187751"
          },
          "geoRadius": "100"
        }
		<?php break; ?>
		<?php case 'B': ?>
		"name" : "Dillon Brothers Harley-Davidson Fremont",
		"address": {
			"name" : "Dillon Brothers Harley-Davidson Fremont",
		 	"streetaddress" : "2440 East 23rd Street Fremont",
		 	"addressLocality" : "Fremont",
		 	"addressRegion" : "Nebraska",
		 	"postalCode" : "68025"
		},
		"telephone": "(402) 556-3333",
		"faxNumber" : "(402) 556-1796",
		"openingHours": [
			"Tuesday 9:00-18:00",
			"Wednesday 9:00-17:30",
			"Thursday 9:00-18:00",
			"Friday 9:00-17:30",
			"Saturday 9:00-16:30"
		  ],
	  	"email": "sales@dillonharley.com",
	  	"priceRange" : "<?php echo $this -> inventoryPrint -> GetPriceRange() ?>",
	  	"image" : "<?php echo PATH ?>public/images/HarleyDavidson.png",
	  	"geo": {
          "@type": "GeoCircle",
          "geoMidpoint": {
            "@type": "GeoCoordinates",
            "latitude": "41.452004",
            "longitude": "-96.465387"
          },
          "geoRadius": "100"
        }
		
		<?php break; ?>
		<?php case 'C': ?>
		
		"name" : "Dillon Brothers MotorSports",
		"address": {
			"name" : "Dillon Brothers MotorSports",
		 	"streetaddress" : "3848 N HWS Cleveland Blvd",
		 	"addressLocality" : "Omaha",
		 	"addressRegion" : "Nebraska",
		 	"postalCode" : "68116"
		},
		"telephone": "(402) 556-3333",
		"faxNumber" : "(402) 556-1796",
		"openingHours" : [
		  	"Monday 9:00-19:00",
		    "Tuesday 9:00-19:00",
		    "Wednesday 9:00-17:30",
		    "Thursday 9:00-19:00",
		    "Friday 9:00-17:30",
		    "Saturday 9:00-16:30"
	   ],
	  "email": "sales@dillon-brothers.com",
	  "priceRange" : "<?php echo $this -> inventoryPrint -> GetPriceRange() ?>",
	  "image" : "<?php echo PATH ?>public/images/MotorSportLogo.png",
	  "geo": {
          "@type": "GeoCircle",
          "geoMidpoint": {
            "@type": "GeoCoordinates",
            "latitude": "41.293783",
            "longitude": "-96.187869"
          },
          "geoRadius": "100"
        }
		
		<?php break; ?>
		<?php case 'D': ?>	
		"name" : "Dillon Brothers Indian",
		"address": {
		 	"name" : "Dillon Brothers Indian",
		 	"streetaddress" : "3840 N 174th Ave.",
		 	"addressLocality" : "Omaha",
		 	"addressRegion" : "Nebraska",
			"postalCode" : "68116"
		},
		"openingHours" : [
		  	"Monday 9:00-19:00",
		    "Tuesday 9:00-19:00",
		    "Wednesday 9:00-17:30",
		    "Thursday 9:00-19:00",
		    "Friday 9:00-17:30",
		    "Saturday 9:00-16:30"
	   ],
	   "email": "sales@dillonbrothersindian.com",
	   "priceRange" : "<?php echo $this -> inventoryPrint -> GetPriceRange() ?>",
	  "image" : "<?php echo PATH ?>public/images/Indian.png",
	  "geo": {
          "@type": "GeoCircle",
          "geoMidpoint": {
            "@type": "GeoCoordinates",
            "latitude": "41.293559",
            "longitude": "-96.189009"
          },
          "geoRadius": "100"
        }
		<?php break; ?>
		<?php endswitch; ?>
    }
  }
}

]
</script>



</body>	
</html>
