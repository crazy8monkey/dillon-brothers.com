<link rel="stylesheet" href="<?php echo PATH ?>public/css/photoswipe.css" type="text/css" />
<link rel="stylesheet" href="<?php echo PATH ?>public/css/default-skin.css" type="text/css" />

<?php 
	function LabelOrder($a, $b) {
		$t1 = $a['Order'];
		$t2 = $b['Order'];
		return $t1 - $t2;
	} 
	
?>

<div id="InventoryList">
	<div class="container">
		<div class="row">
			<div class="col-md-12" style="margin:0px 0px 20px 0px;">
				<a href="<?php echo PATH ?>inventory">Inventory</a><span style="margin:0px 5px;"><i class="fa fa-caret-right" aria-hidden="true"></i></span><?php echo $this -> inventorySingle -> GetBikeName() ?>	
			</div>
		</div>
	</div>
	<div class="MainInventoryRow">
		<div class="container">
			<div class="row">
				<div class="col-md-12 inventorySingleHeader">
					<h1><?php echo $this -> inventorySingle -> GetCondition();  ?> <?php echo $this -> inventorySingle -> GetBikeName() ?></h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<div class="my-gallery" <?php if(count($this -> inventoryPhotos) > 0): ?>style='display: inline-block; width: 100%;' <?php endif; ?>	>
						
					
					<?php if(count($this -> inventoryPhotos) > 0): ?>
						<?php 
							$mainImage = PHOTO_URL . 'inventory/' . $this -> inventorySingle -> VIN . '/' . $this -> inventoryPhotos['0']['inventoryPhotoName'] . '-l.' . $this -> inventoryPhotos['0']['inventoryPhotoExt']; 
							list($imageWidth, $imageHeight) = getimagesize($mainImage);
						?>
						<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
							<a href="<?php echo $mainImage ?>" itemprop="contentUrl" data-size="<?php echo $imageWidth . 'x' . $imageHeight?>">
								<div class="inventoryPhotoMain">
									<?php if($this -> inventorySingle -> MarkAsSoldDate != NULL): ?>
										<div class="SoldOutText">SOLD</div>
									<?php endif; ?>
									<img style='position:relative; z-index:1;' src="<?php echo $mainImage ?>" alt="<?php echo $this -> inventoryPhotos['0']['inventoryAltTag']; ?>" title="<?php echo $this -> inventoryPhotos['0']['inventoryTitleTag']; ?>" />
									<div class="DillonBrothersLogo"></div>
									<?php if(!empty($this -> inventorySingle -> OverlayText)): ?>
										<div class="OverlayText">
											<?php echo $this -> inventorySingle -> OverlayText ?>
										</div>
									<?php endif; ?>
									
									
									
									
								</div>
							</a>
						</figure>
					<?php else: ?>
						<div class="inventoryPhotoMain">
							<?php if($this -> inventorySingle -> MarkAsSoldDate != NULL): ?>
								<div class="SoldOutText">SOLD</div>
							<?php endif; ?>
							<div class="DillonBrothersLogo"></div>
							<?php if(!empty($this -> inventorySingle -> OverlayText)): ?>
								<div class="OverlayText">
									<?php echo $this -> inventorySingle -> OverlayText ?>
								</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>		
					<?php if(count($this -> inventoryPhotos) > 0): ?>
						<?php foreach($this -> inventoryPhotos as $key => $photo): ?>
							<?php if($key != 0): ?>
							
								<?php 
									$largeImage = PHOTO_URL . 'inventory/' . $this -> inventorySingle -> VIN . '/' . $photo['inventoryPhotoName'] . '-l.' . $photo['inventoryPhotoExt'];
									$smallImage = PHOTO_URL . 'inventory/' . $this -> inventorySingle -> VIN . '/' . $photo['inventoryPhotoName'] . '-s.' . $photo['inventoryPhotoExt'];
									list($imageWidth, $imageHeight) = getimagesize($largeImage);
									
								?>
								<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
								<a href="<?php echo $largeImage ?>" itemprop="contentUrl"  data-size="<?php echo $imageWidth . 'x' . $imageHeight?>">
									<div class="OtherInventoryPhotos">
										<img style='position:relative; z-index:1;' src="<?php echo $smallImage ?>" alt="<?php echo $photo['inventoryAltTag']; ?>" title="<?php echo $photo['inventoryTitleTag']; ?>" />
									</div>
								</a>
								</figure>
							<?php endif; ?>
						<?php endforeach; ?>
						
					<?php endif; ?>	
					
					</div>	
				</div>
				<div class="col-md-4">
					<div class="priceContainer">
						<div><h2 class="BikeSpecInfo" style='margin: 0px;'>Selling Price</h2></div>
						<div class="SellingPrice"><span class="price">$<?php echo number_format($this -> inventorySingle -> MSRP, 0);  ?></span></div>
					</div>
					<div style='margin-bottom:10px;'>
						<table class="briefDescription">
							<?php if(!empty($this -> inventorySingle -> FriendlyModelName)): ?>
								<tr>
									<td class='MainLabel'>Model Number:</td>
									<td><?php echo $this -> inventorySingle -> ModelName; ?></td>
								</tr>
							<?php endif; ?>
							<tr>
								<td class='MainLabel'>Category:</td>
								<td><?php echo $this -> inventorySingle -> Category; ?></td>
							</tr>
							<?php if($this ->  inventorySingle -> IsDummyInventoryFlag == 0): ?>
								<tr>
									<td class='MainLabel'>VIN:</td>
									<td><?php echo $this -> inventorySingle -> VIN; ?></td>
								</tr>
							<?php endif; ?>
							<tr>
								<td class='MainLabel'>Mileage:</td>
								<td><?php echo number_format($this -> inventorySingle -> Mileage, 0); ?></td>
							</tr>
							<?php if($this ->  inventorySingle -> IsDummyInventoryFlag == 0): ?>
								<tr>
									<td class='MainLabel'>Stock Number:</td>
									<td><?php echo $this -> inventorySingle -> stockNumber; ?></td>
								</tr>
							<?php endif; ?>
							<tr>
								<td class='MainLabel'>Color:</td>
								<td><?php echo str_replace(",", "/",$this -> inventorySingle -> Color); ?></td>
							</tr>
							<tr>
								<td class='MainLabel'>Location:</td>
								<td><?php echo $this -> inventorySingle -> GetStoreAddress(); ?></td>
							</tr>
						</table>
					</div>
					<div style='margin-bottom:10px;'>
						<?php
					
					 
						if(isset($mainImage)) {
							$picture = $mainImage;	
						} else {
							$picture = PATH . "public/images/NoPhotoPlacement.png";
						}
						
						if(!empty($this -> inventorySingle -> InventoryDescription)) {
							$description = $this -> inventorySingle -> InventoryDescription;
						} else {
							$description = "";
						}
						
						list($pinWith, $pinHeight) = getimagesize($picture);
						
						$facebookShareURL = 'https://www.facebook.com/sharer/sharer.php?u=' . urlencode($this -> inventorySingle -> GetURL()) . 
												  '&amp;title=' . urlencode($this -> inventorySingle -> GetBikeName()  . ': Dillon Brothers') . 
												  '&amp;description=' . urlencode($description). '&amp;' . 
												  '&amp;picture=' . $picture . '&amp;src=sdkpreparse'; 
												  
						$twitterShareURL = "http://twitter.com/intent/tweet?status=" . urlencode($this -> inventorySingle -> GetBikeName() . '. ' . $this -> inventorySingle -> GetURL()) . " %20%23dillonbrothers";		
						
						$pinUrl = "https://www.pinterest.com/pin/create/bookmarklet/?url=" . urlencode($this -> inventorySingle -> GetURL()) . "&media=" . $picture . "&h=" . $pinHeight . "&w=" . $pinWith . "&description=" .urlencode($this -> inventorySingle -> GetBikeName() . " | " . $this -> inventorySingle -> GetStoreName());
						 								  
						?>
						
						<a onclick="window.open('<?php echo $facebookShareURL ?>','sharer','height=500,width=500');" href="javascript:void(0);" style="text-decoration:none;">
							<div class='facebookShare'><img src="<?php echo PATH ?>public/images/facebookIcon.png" data-toggle="tooltip" data-placement="bottom" title="Share on Facebook" /></div>
						</a>
						
						<a onclick="window.open('<?php echo $twitterShareURL ?>','sharer','height=300, width=500');" href="javascript:void(0);" style="text-decoration:none;">
							<div class="twitterShare"><img src="<?php echo PATH ?>public/images/twitter.png" data-toggle="tooltip" data-placement="bottom" title="Share on Twitter"></div>
						</a>
						
						<a onclick="window.open('<?php echo $pinUrl ?>','sharer','height=300, width=500');" href="javascript:void(0);" style="text-decoration:none;">
							<div class="pinButton"><img src="<?php echo PATH ?>public/images/pinterestIcon.png" data-toggle="tooltip" data-placement="bottom" title="Pin on Pinterest"></div>
						</a>
						<a onclick="window.open('<?php echo PATH ?>inventory/emailfriend/<?php echo strtolower($this -> inventorySingle -> VIN); ?>','sharer','height=700, width=800');" href="javascript:void(0);" style="text-decoration:none;">
							<div class="pinButton"><img src="<?php echo PATH ?>public/images/emailToFriendIcon.png" data-toggle="tooltip" data-placement="bottom" title="Email to a Friend"></div>
						</a>
						
						<div style="clear:both"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php 
	
	if($this -> inventorySingle -> StoreID == 2) {
		$colorClass = "MotorSportShoppingTools";	
	} else if($this -> inventorySingle -> StoreID == 3) {
		$colorClass = "HarleyColorShoppingTools";
	} else if($this -> inventorySingle -> StoreID == 4) {
		$colorClass = "IndianColorShoppingTools";
	}
	?>
	
	<div class="ShoppingLinksContainer <?php echo $colorClass ?>">
		<div class="container">	
			<div class="row">
				<div class="col-md-12">
					<h3>Shopping Tools</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="ShoppingTools">
						
						<div class="buttons">
							<div class="button">
								<a href="javascript:void(0);" onclick="window.open('<?php echo PATH ?>inventory/onlineoffer/<?php echo $this -> inventorySingle -> VIN; ?>','sharer','height=700, width=800')">
									<div class="icon">
										<?php switch($this -> inventorySingle -> StoreID): 
											case 2:?>
												<div id="GetQuoteMotorSport"></div>
											<?php break; ?>
											<?php case 3: ?>
												<div id="GetQuoteHarley"></div>
											<?php break; ?>
											<?php case 4: ?>
												<div id="GetQuoteIndian"></div>
											<?php break; ?>
										<?php endswitch; ?>
									</div>	
								</a>							
								<div class="Text">Online Offer</div>
							</div>
							<div class="button">
								<a href="javascript:void(0);" onclick="window.open('<?php echo PATH ?>inventory/payment/<?php echo $this -> inventorySingle -> VIN; ?>','sharer','height=700, width=800')">
									<div class="icon">
										<?php switch($this -> inventorySingle -> StoreID): 
											case 2:?>
												<div id="PaymentCalculatorMotorSport"></div>
											<?php break; ?>
											<?php case 3: ?>
												<div id="PaymentCalculatorHarley"></div>
											<?php break; ?>
											<?php case 4: ?>
												<div id="PaymentCalculatorIndian"></div>
											<?php break; ?>
										<?php endswitch; ?>
									</div>	
								</a>							
								<div class="Text">Payment Calculator</div>
							</div>
							<div class="button">
								<a href="javascript:void(0);" onclick="window.open('<?php echo PATH ?>inventory/tradevalue/<?php echo $this -> inventorySingle -> VIN; ?>','sharer','height=700, width=800')">
									<div class="icon">
										<?php switch($this -> inventorySingle -> StoreID): 
											case 2:?>
												<div id="TradeValueMotorSport"></div>
											<?php break; ?>
											<?php case 3: ?>
												<div id="TradeValueHarley"></div>
											<?php break; ?>
											<?php case 4: ?>
												<div id="TradeValueIndian"></div>
											<?php break; ?>
										<?php endswitch; ?>
										
									</div>
								</a>
								
								<div class="Text">Trade Value</div>
							</div>
							<div class="button">
								<a href="javascript:void(0);" onclick="window.open('<?php echo PATH ?>inventory/testride/<?php echo $this -> inventorySingle -> VIN; ?>','sharer','height=700, width=800')">
									<div class="icon">
										<?php switch($this -> inventorySingle -> StoreID): 
											case 2:?>
												<div id="TestRideMotorSport"></div>
											<?php break; ?>
											<?php case 3: ?>
												<div id="TestRideHarley"></div>
											<?php break; ?>
											<?php case 4: ?>
												<div id="TestRideIndian"></div>
											<?php break; ?>
										<?php endswitch; ?>
									</div>
								</a>
								<div class="Text">Schedule Test Ride</div>
							</div>
							<div class="button">
								<div class="icon">
									<?php switch($this -> inventorySingle -> StoreID): 
										case 2:?>
											<div id="FinancingMotorsport"></div>
										<?php break; ?>
										<?php case 3: ?>
											<div id="FinancingHarley"></div>
										<?php break; ?>
										<?php case 4: ?>
											<div id="FinancingIndian"></div>
										<?php break; ?>
									<?php endswitch; ?>
									
								</div>
								<div class="Text">Get Financing</div>
							</div>
							<div class="button">
								<a href="javascript:void(0);" onclick="window.open('<?php echo PATH ?>inventory/contact/<?php echo $this -> inventorySingle -> VIN; ?>','sharer','height=700, width=800')">
									<div class="icon">
										<?php switch($this -> inventorySingle -> StoreID): 
											case 2:?>
												<div id="ContactUsMotorsport"></div>
											<?php break; ?>
											<?php case 3: ?>
												<div id="ContactUsHarley"></div>
											<?php break; ?>
											<?php case 4: ?>
												<div id="ContactUsIndian"></div>
											<?php break; ?>
										<?php endswitch; ?>
										
									</div>
								</a>
								
								<div class="Text">Contact Us</div>
							</div>
							<!--<div class="button">
								<div class="icon" id="InsuranceQuote"></div>
								<div class="Text">Insurance Quote</div>
							</div>-->
							<div class="button">
								<a href="<?php echo PATH ?>inventory/print/<?php echo $this -> inventorySingle -> VIN; ?>" target="_blank">
									<div class="icon">
										<?php switch($this -> inventorySingle -> StoreID): 
											case 2:?>
												<div id="PrintMotorsport"></div>
											<?php break; ?>
											<?php case 3: ?>
												<div id="PrintHarley"></div>
											<?php break; ?>
											<?php case 4: ?>
												<div id="PrintIndian"></div>
											<?php break; ?>
										<?php endswitch; ?>
									</div>	
								</a>
								
								<div class="Text">Print</div>
							</div>
							<div style="clear:both"></div>
						</div>
					</div>
				</div>
			</div>
			
			
			
		</div>
	</div>
	
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="subTabs">
					<a href="javascript:void(0)" onclick="InventoryController.OpenBikeDescription()">
						<div class="tab" id="bikeDescriptionTab">
							<h2 class="BikeSpecInfo" style='margin:0px; line-height: 24px;'>Description</h2>
							<div class="tabSelected" style='display:block;'></div>
						</div>	
					</a>
					
					<a href="javascript:void(0)" onclick="InventoryController.OpenBikeSpecs()">
						<div class="tab" id="SpecTab">
							<h2 class="BikeSpecInfo" style='margin:0px; line-height: 24px;'>Specifications*</h2>
							<div class="tabSelected"></div>
						</div>
					</a>
					<div style="clear:both"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="SubTabContent" id="BikeDescription" style='display:block;'>
					<?php if(!empty($this -> inventorySingle -> InventoryDescription)) {
						echo $this -> inventorySingle -> InventoryDescription;
					} else {
						echo "There is no description of this bike";
					}
					
					?>	
					
					<?php if(!empty($this -> inventorySingle -> YoutubeURL)):?>
						<div class="embed-responsive embed-responsive-16by9" style='margin-top:10px;'>
							<iframe class="embed-responsive-item" frameborder="0" allowfullscreen src="https://www.youtube.com/embed/<?php echo $this -> inventorySingle -> YoutubeURL ?>"></iframe>
						</div>
					<?php endif; ?>
				</div>
				
				<div class="SubTabContent" id="BikeSpecsContent">
					<div class="specContent">
						<?php 
	
							$specDecoded = json_decode($this -> inventorySingle -> GetInventorySpecs(), true); 
							$schemaArray = array();
						
						
						?>
						
						<?php //$this -> view -> specgroups 
							$specLayout = array();
							foreach($this -> specgroups as $spec) {
								$labelCount = 0;
								$groupSpecID = $spec['specGroupID'];
										
								$filteredSpecs = array_filter($this -> speclabels, function ($var) use($groupSpecID) {
									 return $var['relatedSpecGroupID'] == $groupSpecID; 
									}
								);	
								
								$finalSpecs = array();
								foreach($filteredSpecs as $specLabelCheck) {
									if(array_search($specLabelCheck['specLabelID'], array_column($specDecoded, 'LabelID')) !== FALSE) {
										$key = array_search($specLabelCheck['specLabelID'], array_column($specDecoded, 'LabelID'));
										if(!empty($specDecoded[$key]['Content'])) {
											$labelCount++;							   
											
											array_push($finalSpecs, array("Text" => $specLabelCheck['labelText'],
																		  "Content" => $specDecoded[$key]['Content']));	
													
											array_push($schemaArray, array("Text" => $specLabelCheck['labelText'],
																		   "Content" => $specDecoded[$key]['Content']));
										}
										
										
									}
								}

									
								array_push($specLayout, array("GroupText" => $spec['specGroupText'],
															  "SpecGroupCount" => $labelCount,
															  "SpecLabels" => $finalSpecs));	
							}
						?>
								
						<?php foreach($specLayout as $specDetails): ?>
							<?php if($specDetails['SpecGroupCount'] > 0): ?>
								<div class="row">
									<div class="col-md-12" style='margin-bottom: 15px;'>	
									<div class="specGroupText"><?php echo $specDetails['GroupText'] ?></div>
									<?php foreach(array_chunk($specDetails['SpecLabels'], 2, true) as $labelThree) : ?>
										<div class="row SpecRow" style="margin: -1px 0px 0px 0px; border-bottom:1px solid #c0f5ff; border-top:1px solid #c0f5ff; overflow: hidden;">
											<?php foreach($labelThree as $labelSingle) : ?>
												<div class="col-md-6">
													<div class="row SpecRow" style='margin-top: -1px;'>
														<div class="col-xs-4" style='padding: 0px;'>
															<div class="LabelContent">
																<?php echo $labelSingle['Text'] ?>	
															</div>
															
														</div>
														<div class="col-xs-8" style='padding: 0px;'>
															<div class="SpecContent">
																<?php echo $labelSingle['Content'] ?>	
															</div>
															
														</div>
													</div>
												</div>
												
											<?php endforeach; ?>
										</div>	
									<?php endforeach; ?>
								</div>
							</div>
							<?php endif; ?>
						<?php endforeach; ?>
					</div>
				</div>
				
			</div>
			
			
			
		</div>
	
		<?php if(count($this -> RelatedInventory) > 0): ?>
		
		<div class="row" style="margin-bottom: 50px;">
				<div class="col-md-12">
					<div class="RelatedInventorySection">
						<div class="RelatedInventoryHeader">
							<h4>Related Inventory</h4>	
						</div>	
						<div class="row">
							<?php foreach($this -> RelatedInventory as $related):?>
								<div class="col-md-3">
									<?php 
										$url = NULL;
										if($related['Conditions'] == 0) {
											$url = PATH . 'inventory/new/' . $related['inventorySeoURL'];	
										} else {
											$url = PATH . 'inventory/used/' . $related['inventorySeoURL'];
										} 
									?>
									
									<a href="<?php echo $url ?>">
										<div class="relatedSingleElement">
											<?php if($related['inventoryPhotoName'] == NULL) : ?>
												<div class="imageElement" style="height: 220px;">
													<div class="DillonBrothersLogo"></div>
												</div>	
											<?php else: ?>	
												<div class="imageElement">
													<img src='<?php echo PHOTO_URL . 'inventory/' . $related['VinNumber'] . '/' . $related['inventoryPhotoName'] . '-s.' . $related['inventoryPhotoExt']; ?>' alt="<?php echo $related['inventoryAltTag']; ?>" title="<?php echo $related['inventoryTitleTag']; ?>" />
												</div>
											<?php endif; ?>
											
											<div class="text">
												<?php 
												if($related['FriendlyModelName'] != NULL) {
													echo $related['Year'] . ' ' . $related['Manufacturer'] . ' ' . $related['FriendlyModelName'];
												} else {
													echo $related['Year'] . ' ' . $related['Manufacturer'] . ' ' . $related['ModelName'];
												}
												 ?>
											</div>	
										</div>
										
									</a>
									
								
								</div>
							<?php endforeach; ?>
							
						</div>
						
					</div>
					
							
					
					
				</div>
			</div>
		<?php endif; ?>
	</div>
	
	<div class="disclaimerContent">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div style='margin-top:20px;'><strong>Disclaimer</strong></div>
					<div class="specDisclaimer">
							*While every reasonable effort is made to ensure the accuracy of this information, we are not responsible for any errors or omissions contained in this listing.
							<br />Please verify any vehicle information or details by contacting us at 402-556-3333.
						</div>
					<div style='margin-bottom:30px; font-size: 14px;'>
						Prices exclude dealer setup, taxes, title, freight and licensing and are subject to change
					</div>	
				</div>
				
			</div>
		</div>
	</div>
	
</div>


<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe. 
         It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. 
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>


<script src="<?php echo PATH ?>public/js/photoswipe.min.js" type="text/javascript" type="text/javascript"></script>	
<script src="<?php echo PATH ?>public/js/photoswipe-ui-default.min.js" type="text/javascript" type="text/javascript"></script>

<script>
var initPhotoSwipeFromDOM = function(gallerySelector) {

    // parse slide data (url, title, size ...) from DOM elements 
    // (children of gallerySelector)
    var parseThumbnailElements = function(el) {
        var thumbElements = el.childNodes,
            numNodes = thumbElements.length,
            items = [],
            figureEl,
            linkEl,
            size,
            item;

        for(var i = 0; i < numNodes; i++) {

            figureEl = thumbElements[i]; // <figure> element

            // include only element nodes 
            if(figureEl.nodeType !== 1) {
                continue;
            }

            linkEl = figureEl.children[0]; // <a> element

            size = linkEl.getAttribute('data-size').split('x');

            // create slide object
            item = {
                src: linkEl.getAttribute('href'),
                w: parseInt(size[0], 10),
                h: parseInt(size[1], 10)
            };



            if(figureEl.children.length > 1) {
                // <figcaption> content
                item.title = figureEl.children[1].innerHTML; 
            }

            if(linkEl.children.length > 0) {
                // <img> thumbnail element, retrieving thumbnail url
                item.msrc = linkEl.children[0].getAttribute('src');
            } 

            item.el = figureEl; // save link to element for getThumbBoundsFn
            items.push(item);
        }

        return items;
    };

    // find nearest parent element
    var closest = function closest(el, fn) {
        return el && ( fn(el) ? el : closest(el.parentNode, fn) );
    };

    // triggers when user clicks on thumbnail
    var onThumbnailsClick = function(e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        var eTarget = e.target || e.srcElement;

        // find root element of slide
        var clickedListItem = closest(eTarget, function(el) {
            return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
        });

        if(!clickedListItem) {
            return;
        }

        // find index of clicked item by looping through all child nodes
        // alternatively, you may define index via data- attribute
        var clickedGallery = clickedListItem.parentNode,
            childNodes = clickedListItem.parentNode.childNodes,
            numChildNodes = childNodes.length,
            nodeIndex = 0,
            index;

        for (var i = 0; i < numChildNodes; i++) {
            if(childNodes[i].nodeType !== 1) { 
                continue; 
            }

            if(childNodes[i] === clickedListItem) {
                index = nodeIndex;
                break;
            }
            nodeIndex++;
        }



        if(index >= 0) {
            // open PhotoSwipe if valid index found
            openPhotoSwipe( index, clickedGallery );
        }
        return false;
    };

    // parse picture index and gallery index from URL (#&pid=1&gid=2)
    var photoswipeParseHash = function() {
        var hash = window.location.hash.substring(1),
        params = {};

        if(hash.length < 5) {
            return params;
        }

        var vars = hash.split('&');
        for (var i = 0; i < vars.length; i++) {
            if(!vars[i]) {
                continue;
            }
            var pair = vars[i].split('=');  
            if(pair.length < 2) {
                continue;
            }           
            params[pair[0]] = pair[1];
        }

        if(params.gid) {
            params.gid = parseInt(params.gid, 10);
        }

        return params;
    };

    var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
            gallery,
            options,
            items;

        items = parseThumbnailElements(galleryElement);

        // define options (if needed)
        options = {

            // define gallery index (for URL)
            galleryUID: galleryElement.getAttribute('data-pswp-uid'),

            getThumbBoundsFn: function(index) {
                // See Options -> getThumbBoundsFn section of documentation for more info
                var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                    rect = thumbnail.getBoundingClientRect(); 

                return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
            }

        };

        // PhotoSwipe opened from URL
        if(fromURL) {
            if(options.galleryPIDs) {
                // parse real index when custom PIDs are used 
                // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                for(var j = 0; j < items.length; j++) {
                    if(items[j].pid == index) {
                        options.index = j;
                        break;
                    }
                }
            } else {
                // in URL indexes start from 1
                options.index = parseInt(index, 10) - 1;
            }
        } else {
            options.index = parseInt(index, 10);
        }

        // exit if index not found
        if( isNaN(options.index) ) {
            return;
        }

        if(disableAnimation) {
            options.showAnimationDuration = 0;
        }

        // Pass data to PhotoSwipe and initialize it
        gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    };

    // loop through all gallery elements and bind events
    var galleryElements = document.querySelectorAll( gallerySelector );

    for(var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute('data-pswp-uid', i+1);
        galleryElements[i].onclick = onThumbnailsClick;
    }

    // Parse URL and open gallery if it contains #&pid=3&gid=1
    var hashData = photoswipeParseHash();
    if(hashData.pid && hashData.gid) {
        openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
    }
};

// execute above function
initPhotoSwipeFromDOM('.my-gallery');
  </script>


<?php  $i = 0; $len = count($schemaArray); ?>
<script type="application/ld+json">
[

{
  "@context": "http://schema.org",
  "@type": "Motorcycle",
  "model": "<?php echo $this -> inventorySingle -> ModelName; ?>",
  "modelDate": "<?php echo $this -> inventorySingle -> year ?>",
  "name": "<?php echo $this -> inventorySingle -> GetCondition();  ?> <?php echo $this -> inventorySingle -> GetBikeName() ?>",
  "productID": "<?php echo $this -> inventorySingle -> VIN; ?>",
  "mileageFromOdometer": "<?php echo $this -> inventorySingle -> Mileage ?>",
  "sku": "<?php echo $this -> inventorySingle -> stockNumber; ?>",
  <?php if(!empty($this -> inventorySingle -> InventoryDescription)): ?>
	"description": "<?php echo $this -> inventorySingle -> InventoryDescription; ?>",						
  <?php endif; ?>
  
  
  
  <?php if(isset($mainImage)): ?>
  	"image": "<?php echo $mainImage ?>",
  <?php else: ?>  
    "image": "<?php echo PATH ?>public/images/NoPhotoPlacement.png",
  <?php endif; ?>  
  
  "additionalProperty": [
  	<?php foreach($schemaArray as $spec): ?>
    {  "@type": "PropertyValue",


      "name": "<?php echo $spec['Text'] ?>",
      "value": "<?php echo $spec['Content'] ?>"
    }<?php if($i != $len - 1) :?>,<?php endif; ?>
		<?php $i++ ?>	  
  <?php endforeach; ?>
  ],
  
  
  
  
  "color": "<?php echo $this -> inventorySingle -> Color; ?>",
  "category": "<?php echo $this -> inventorySingle -> Category; ?>",
  "brand": {
  	"@type": "Thing",
    "name": "<?php echo $this -> inventorySingle -> manufactureText; ?>"
  },
  
  <?php $count = count($this -> RelatedInventory); ?>
  <?php if(count($this -> RelatedInventory) > 0): ?>
 	"isRelatedTo":[
 		<?php foreach($this -> RelatedInventory as $key => $related):?>
 			<?php 
 			
 				if($related['Conditions'] == 0) {
 					$conditionText = "New";
 				} else {
 					$conditionText = "Used";
				}
 			
				if($related['FriendlyModelName'] != NULL) {
					$MotorCyleName = $related['Year'] . ' ' . $related['Manufacturer'] . ' ' . $related['FriendlyModelName'];
				} else {
					$MotorCyleName = $related['Year'] . ' ' . $related['Manufacturer'] . ' ' . $related['ModelName'];
				}
			?>
 			{
	 			"@context": "http://schema.org",
				"@type": "Motorcycle",
				"model": "<?php echo $related['ModelName'] ?>",
				"modelDate": "<?php echo $related['Year'] ?>",
				"name": "<?php echo $conditionText . ' ' . $MotorCyleName ?>",
				"productID": "<?php echo $related['VinNumber'] ?>",
				"offers": {
					"@type": "Offer",
				    "priceCurrency": "USD",
				    "price": "<?php echo $related['MSRP'] ?>",
				    "itemCondition": "<?php echo ($related['Conditions'] == 0 ? "http://schema.org/NewCondition" : "http://schema.org/UsedCondition") ?>",
					"availability": "http://schema.org/InStock"
				},
				"brand": {
				  	"@type": "Thing",
				    "name": "<?php echo $related['Manufacturer']; ?>"
				},
				"mileageFromOdometer": "<?php echo $related['Mileage'] ?>",
	  			"sku": "<?php echo $related['Stock'] ?>"
			  
			  
			}<?php if($key != $count - 1): ?>,<?php endif; ?>
 		<?php endforeach; ?>
 	],
 <?php endif; ?>
  
  "offers": {
  "@type": "Offer",
    "priceCurrency": "USD",
    "price": "<?php echo $this -> inventorySingle -> MSRP;  ?>",
    <?php if($this -> inventorySingle -> Conditions == 0): ?>
   		"itemCondition": "http://schema.org/NewCondition",
   	<?php else: ?>
   		"itemCondition": "http://schema.org/UsedCondition",
   	<?php endif; ?>

   	
   	
    "availability": "http://schema.org/InStock",
    "seller": {
      "@type": "MotorcycleDealer",
      <?php switch($this -> inventorySingle -> storeLocation): 
		case 'A':?>
		"name" : "Dillon Brothers Harley-Davidson Omaha",
		"address": {
		 	"name" : "Dillon Brothers Harley-Davidson Omaha",
		 	"streetaddress" : "3838 N HWS Cleveland Blvd",
		 	"addressLocality" : "Omaha",
		 	"addressRegion" : "Nebraska",
		 	"postalCode" : "68116"
		},
		"openingHours": [
			"Monday 9:00-19:00",
			"Tuesday 9:00-19:00",
			"Wednesday 9:00-17:30",
			"Thursday 9:00-19:00",
			"Friday 9:00-17:30",
			"Saturday 9:00-16:30"
	  	],
	  	"telephone" : "(402) 289-5556",
    	"faxNumber" : "(402) 289-1931",
		"email": "sales@dillonharley.com",	
	  	"priceRange" : "<?php echo $this -> inventorySingle -> GetPriceRange() ?>",
		"image" : "<?php echo PATH ?>public/images/HarleyDavidson.png",
		"geo": {
          "@type": "GeoCircle",
          "geoMidpoint": {
            "@type": "GeoCoordinates",
            "latitude": "41.292940",
            "longitude": "-96.187751"
          },
          "geoRadius": "100"
        }
		<?php break; ?>
		<?php case 'B': ?>
		"name" : "Dillon Brothers Harley-Davidson Fremont",
		"address": {
			"name" : "Dillon Brothers Harley-Davidson Fremont",
		 	"streetaddress" : "2440 East 23rd Street Fremont",
		 	"addressLocality" : "Fremont",
		 	"addressRegion" : "Nebraska",
		 	"postalCode" : "68025"
		},
		"telephone" : "(402) 721-2007",
    	"faxNumber" : "(402) 721-2441",
		"openingHours": [
			"Tuesday 9:00-18:00",
			"Wednesday 9:00-17:30",
			"Thursday 9:00-18:00",
			"Friday 9:00-17:30",
			"Saturday 9:00-16:30"
		  ],
	  	"email": "sales@dillonharley.com",
	  	"priceRange" : "<?php echo $this -> inventorySingle -> GetPriceRange() ?>",
	  	"image" : "<?php echo PATH ?>public/images/HarleyDavidson.png",
	  	"geo": {
          "@type": "GeoCircle",
          "geoMidpoint": {
            "@type": "GeoCoordinates",
            "latitude": "41.452004",
            "longitude": "-96.465387"
          },
          "geoRadius": "100"
        }
		
		<?php break; ?>
		<?php case 'C': ?>
		
		"name" : "Dillon Brothers MotorSports",
		"address": {
			"name" : "Dillon Brothers MotorSports",
		 	"streetaddress" : "3848 N HWS Cleveland Blvd",
		 	"addressLocality" : "Omaha",
		 	"addressRegion" : "Nebraska",
		 	"postalCode" : "68116"
		},
		"telephone": "(402) 556-3333",
		"faxNumber" : "(402) 556-1796",
		"openingHours" : [
		  	"Monday 9:00-19:00",
		    "Tuesday 9:00-19:00",
		    "Wednesday 9:00-17:30",
		    "Thursday 9:00-19:00",
		    "Friday 9:00-17:30",
		    "Saturday 9:00-16:30"
	   ],
	  "email": "sales@dillon-brothers.com",
	  "priceRange" : "<?php echo $this -> inventorySingle -> GetPriceRange() ?>",
	  "image" : "<?php echo PATH ?>public/images/MotorSportLogo.png",
	  "geo": {
          "@type": "GeoCircle",
          "geoMidpoint": {
            "@type": "GeoCoordinates",
            "latitude": "41.293783",
            "longitude": "-96.187869"
          },
          "geoRadius": "100"
        }
		
		<?php break; ?>
		<?php case 'D': ?>	
		"name" : "Dillon Brothers Indian",
		"address": {
		 	"name" : "Dillon Brothers Indian",
		 	"streetaddress" : "3840 N 174th Ave.",
		 	"addressLocality" : "Omaha",
		 	"addressRegion" : "Nebraska",
			"postalCode" : "68116"
		},
		"openingHours" : [
		  	"Monday 9:00-19:00",
		    "Tuesday 9:00-19:00",
		    "Wednesday 9:00-17:30",
		    "Thursday 9:00-19:00",
		    "Friday 9:00-17:30",
		    "Saturday 9:00-16:30"
	   ],
	   "email": "sales@dillonbrothersindian.com",
	   "priceRange" : "<?php echo $this -> inventorySingle -> GetPriceRange() ?>",
	  "image" : "<?php echo PATH ?>public/images/Indian.png",
	  "geo": {
          "@type": "GeoCircle",
          "geoMidpoint": {
            "@type": "GeoCoordinates",
            "latitude": "41.293559",
            "longitude": "-96.189009"
          },
          "geoRadius": "100"
        }
		<?php break; ?>
		<?php endswitch; ?>
    }
  }
},
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "name": "Dillon Brothers Inventory"
    }
  },{
    "@type": "ListItem",
    "position": 2,
    "item": {
      "@id": "<?php echo $this -> inventorySingle -> GetURL() ?>",
      "name": "<?php echo $this -> inventorySingle -> GetCondition();  ?> <?php echo $this -> inventorySingle -> GetBikeName() ?>"
    }
  }]
}

]
</script>
