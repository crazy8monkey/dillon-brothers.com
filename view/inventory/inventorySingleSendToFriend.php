<?php 
	function LabelOrder($a, $b) {
		$t1 = $a['Order'];
		$t2 = $b['Order'];
		return $t1 - $t2;
	} 
?>
<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html xml:lang="en"> <!--<![endif]-->
<head>
	<title><?php echo $this -> title ?></title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
	
	<?php require 'view/MetaDataContent.php'; ?>
		
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />	
	<link rel="stylesheet" href="<?php echo PATH ?>public/css/style.css" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
	
	<link rel="stylesheet" href="<?php echo PATH ?>public/css/InventoryController.css" type="text/css" />
		
		
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>	
	<script src="<?php echo PATH ?>public/js/Globals.js" type="text/javascript" type="text/javascript"></script>
	
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="<?php echo PATH ?>public/js/InventoryController.js"></script>
		
	<script src="https://use.typekit.net/kpv4owm.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<script type="text/javascript">
		$(document).ready(function(){
			InventoryController.SendToFriend()
		});
		
		function CloseWindow() {
			window.close();
		}
	</script>	
		

</head>
<body>

<div id="ShoppingToolPopupForm">
	<?php 
		switch($this -> CurrentInventory -> StoreID) {
			case 2:
				$class = "MotorSport";
				break;
			case 3:
				$class = "Harley";
				break;
			case 4:
				$class = "Indian";
				break;
		}
	?>
	<div class="headerContent <?php echo $class ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Send to a Friend</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					Your friend will receive an email containg a reference to this Vehicle. <br />
					Please send this email only to people you who would be interested in this formation.
				</div>
			</div>
		</div>	
	</div>
	<div class="container">
		
		<div class="row" id="MessageShow" style='display:none'>
			<div class="col-md-12" id="SuccessMessage" style="color: #18ad00; text-align: center; padding: 10px 0px 20px 0px;"></div>
		</div>
		<form action="<?php echo PATH ?>ajax/inventorylead/sendToFriend" method="post" id="SendToFriendForm">
			<input type="hidden" name="u" value="<?php echo Hash::mc_encrypt($this -> CurrentInventory -> VIN, ENCRYPTION_KEY) ?>" />
			<div class="row">
				<div class="col-xs-6">
					<div id="inputID1">
						<div class="errorMessage"></div>
						<div class="inputLine">
							<div class="inputLabel">Your First Name: <span class='requiredIndicator'>*</span></div>
							<input type="text" name="yourFirstName" />
						</div>	
					</div>
					
				</div>
				<div class="col-xs-6">
					<div id="inputID2">
						<div class="errorMessage"></div>
						<div class="inputLine">
							<div class="inputLabel">Your Last Name: <span class='requiredIndicator'>*</span></div>
							<input type="text" name="yourLastName" />
						</div>
					</div>
					
				</div>	
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div id="inputID3">
						<div class="errorMessage"></div>
						<div class="inputLine">
							<div class="inputLabel">Your Email: <span class='requiredIndicator'>*</span></div>
							<input type="text" name="yourEmail" />
						</div>
					</div>
					
				</div>
				<div class="col-xs-6">
					<div id="inputID4">
						<div class="errorMessage"></div>					
						<div class="inputLine">
							<div class="inputLabel">Your Friends Email: <span class='requiredIndicator'>*</span></div>
							<input type="text" name="friendsEmail" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine">
						<div class="inputLabel">Personal Message:</div>
						<textarea name="PersonalMessage"></textarea>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div id="inputID5">
						<div class="errorMessage"></div>					
						<div class="inputLine">
							<div class="inputLabel">Are You Human?: <span class='requiredIndicator'>*</span></div>
							<img src="<?php echo PATH ?>view/captcha.php" />
							<input type="text" name="capta" style='margin-top:10px' />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<input type="submit" name="Send Email" class="blueButton" />
				</div>
			</div>
		</form>
		<div class="row">
			<div class="col-md-12">
				<div class="disclaimer">
					We respect your privacy and won't share your information with any other company. Terms and conditions apply
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="javascript:void(0);" onclick='CloseWindow()'>
					<div class="closePopup">
						Close Popup
					</div>	
				</a>			
			</div>
		</div>
	</div>
</div>





</body>	
</html>
