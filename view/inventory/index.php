<div id="InventoryList">
	<div class="container-fluid" style='padding-bottom:90px;'>
			<div class="row">
				<div class="col-md-2">
					<div class="InventoryListContainer" id='DesktopFilterContent'>
						<div class="dividerContent">
							<div class="FilterInventoryOptions">
								
								<div class='filterOptionsHeader'>	
									<a href="javascript:void(0)" onclick='InventoryController.OpenMobileFilters()'>
										<div class="text">
											FILTER
										</div>
									</a>
									<div class='closeFilters'>
										<a href='javascript:void(0);' onclick='InventoryController.CloseMobileFilters()'>
											<i class="fa fa-times-circle" aria-hidden="true"></i>	
										</a>
									</div>
								</div>
								
								<div class="resultNumber" id="count"></div>	
								<div class="clearFilters">
									<a href="javascript:void(0);" onclick='InventoryController.ClearFilters()'>Clear Filters</a>
								</div>
								
								
								<!-- TEMPLATES -->
								<script type="text/html" id="inventoryTwoByTwo-template">
									<div class='inventorySingleObjectTwoByTwo'>
										<div class="inventoryItem">
											<a href="{{BikeURL}}">
			      								<div class="inventoryTitle" style="color:black;">{{BikeName}}</div>
			      							</a>
			      							<div class="row" style="margin-left: 0px; margin-right: 0px;">
												<a href="{{BikeURL}}">
													<div class="col-md-4 MainInventoryPhoto">
														<div class="inventoryPhotoSingle" style="background:url({{Image}}) no-repeat center">
															<img src="{{Image}}" alt="2013 Harley-Davidson FLHTK Electra Glide® Ultra Limited" title="{{BikeName}}">	
														</div>
														{{#MarkAsSoldDate}}
															<div style='font-size:16px;' class='SoldOutText'>SOLD</div>
														{{/MarkAsSoldDate}}
														<div class="NoPicture"></div>
													</div>
												</a>
												<div class="col-md-8">
													<div class="row">
														<div class="col-md-12">
															<div class="CarDetailInfo">
																 {{#OverlayText}}
														          <div class="OverlayTextList">{{OverlayText}}</div>
														        {{/OverlayText}}
																
																<div class="MSRPPriceList">${{Price}}</div>
																<div class="row DetailTable">
																	<div class="col-md-12">
																		<table>
																			<tr><td class='detailLabel'><strong>Condition:</strong></td><td>{{Conditions}}</td></tr>
																			{{#Stock}}
																	          <tr><td class='detailLabel' ><strong>Stock:</strong></td><td>{{Stock}}</td></tr>
																	        {{/Stock}}
																			<tr><td class='detailLabel'><strong>Color:</strong></td><td>{{CurrentColors}}</td></tr>
																			<tr><td class='detailLabel'><strong>Mileage:</strong></td><td>{{Mileage}}</td></tr>
																		</table>	
																	</div>
																</div>
																
															</div>
														</div>		
													</div>
												</div>
											</div>
						      		  		<div style="border-top: 1px solid #eaeaea;"></div>
						      		  		<div class="row">
												<div class="col-md-8">
													<a href="{{BikeURL}}">
														<div class="ViewDetailsButton {{InventoryClassName}}">
															More Info.
														</div>
													</a>
												</div>
											</div>	
										</div>
					      		  	</div>
								</script>
								
								<script type="text/html" id="inventoryThreeByThree-template">
									<div class='inventorySingleObjectThreeByThree'>
										<div class="inventoryItem">
											<a href="{{BikeURL}}">
			      								<div class="inventoryTitle" style="color:black;">{{BikeName}}</div>
			      							</a>
			      							<div class="row" style="margin-left: 0px; margin-right: 0px;">
												<a href="{{BikeURL}}">
													<div class='row'>
														<div class="col-md-12 MainInventoryPhoto">
															<div class="inventoryPhotoSingle" style="background:url({{Image}}) no-repeat center">
																<img src="{{Image}}" alt="2013 Harley-Davidson FLHTK Electra Glide® Ultra Limited" title="{{BikeName}}">	
															</div>
															{{#MarkAsSoldDate}}
														     	<div style='font-size:16px;' class='SoldOutText'>SOLD</div>
														     {{/MarkAsSoldDate}}
															<div class="NoPicture"></div>
														</div>	
													</div>
													
												</a>
												<div class='row'>
													<div class="col-md-12">
															<div class="CarDetailInfo">
																{{#OverlayText}}
														          <div class="OverlayTextList">{{OverlayText}}</div>
														        {{/OverlayText}}
																<div class="MSRPPriceList">${{Price}}</div>
																<div class="row DetailTable">
																	<div class="col-md-12">
																		<table>
																			<tr><td class='detailLabel'><strong>Condition:</strong></td><td>{{Conditions}}</td></tr>
																			{{#Stock}}
																	          <tr><td class='detailLabel' ><strong>Stock:</strong></td><td>{{Stock}}</td></tr>
																	        {{/Stock}}
																			<tr><td class='detailLabel'><strong>Color:</strong></td><td>{{CurrentColors}}</td></tr>
																			<tr><td class='detailLabel'><strong>Mileage:</strong></td><td>{{Mileage}}</td></tr>
																		</table>	
																	</div>
																</div>
																
															</div>
														</div>		
												</div>
											</div>
						      		  		<div style="border-top: 1px solid #eaeaea;"></div>
						      		  		<div class="row">
												<div class="col-md-8">
													<a href="{{BikeURL}}">
														<div class="ViewDetailsButton {{InventoryClassName}}">
															More Info.
														</div>
													</a>
												</div>
											</div>	
										</div>
										
		      							
					      		  	</div>
								</script>
								
								<div class='FilterOptionsContent'>
									<!--<form method="post" id="FilterInventory">-->
										<input type='hidden' name="hiddenQuickInventorySearch" id="hiddenQuickInventorySearch" />
										<input type="hidden" name="RowNumber" id="RowNumber" value="2" />
										<input type='hidden' name="OrderByValue" id="OrderByValue" value="YearsDesc" />
										
										<div class="filterSection">
											<a href="javascript:void(0);" onclick="InventoryController.OpenFilterList(this)">
												<div class="sectionHeader">Condition</div>	
											</a>
											<div class="filterList" id="ConditionFilterHtml"></div>
										</div>
										
										
										
										<div class="filterSection">
											<a href="javascript:void(0);" onclick="InventoryController.OpenFilterList(this)">
												<div class="sectionHeader">Years</div>
											</a>
											<div class="filterList" id="YearsFilterHtml"></div>
										</div>
										
										
										<div class="filterSection">
											<a href="javascript:void(0);" onclick="InventoryController.OpenFilterList(this)">
												<div class="sectionHeader">Category</div>
											</a>
											<div class="filterList" id="CategoryFilterHtml"></div>
										</div>
										
										<div class="filterSection">
											<a href="javascript:void(0);" onclick="InventoryController.OpenFilterList(this)">
												<div class="sectionHeader">Stores</div>
											</a>
											<div class="filterList" id="StoresFilterHTML"></div>
										</div>
										
										
										<div class="filterSection">
											<a href="javascript:void(0);" onclick="InventoryController.OpenFilterList(this)">
												<div class="sectionHeader">Manufactures</div>
											</a>
											<div class="filterList" id="ManufactureFilterHTML"></div>
										</div>
										
										
										<div class="filterSection">
											<a href="javascript:void(0);" onclick="InventoryController.OpenFilterList(this)">
												<div class="sectionHeader">Colors</div>
											</a>
											<div class="filterList" id="ColorFilterHTML"></div>					
										</div>
										
										<div class="filterSection">
											<a href="javascript:void(0);" onclick="InventoryController.OpenFilterList(this)">
												<div class="sectionHeader">Mileage</div>
											</a>
											<div class="filterList" style='padding: 10px' id="MileageFilterRanges"></div>			
												
												
										</div>
										
											
										<div class="filterSection">
											<a href="javascript:void(0);" onclick="InventoryController.OpenFilterList(this)">
												<div class="sectionHeader">Price</div>
											</a>
											<div class="filterList" style='padding: 10px' id="PriceRangeFilterHtml"></div>			
										</div>
										
															
										<div class="filterSection">
											<a href="javascript:void(0);" onclick="InventoryController.OpenFilterList(this)">
												<div class="sectionHeader">Engine Size</div>
											</a>
											<div class="filterList" style='padding: 10px' id="EngineSizeRangeFilterHtml"></div>			
										</div>
														
									<!--</form>-->
								</div>
								
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-md-10">
					<div class="InventoryListContainer">
						<div class="dividerContent">
							<div class="row">
								<div class="col-md-8">
									<div id="muisearch"></div>
								</div>
								<div class="col-md-2">
									<div id="sort-by-container"></div>
								</div>
								<div class="col-md-2">
									<div style="float:right" id="toggleViewElement">
										<div class="toggleViews">
											<a href="javascript:void(0)" onclick="InventoryController.toggleList(2)">							
												<div class="TwoByTwo SelectedList">
													<div class='line spacing'>
														<div class="toggleBox"></div>
														<div class="toggleBox" style='margin-left:3px;'></div>	
														<div style='clear:both'></div>
													</div>
													<div>
														<div class="toggleBox"></div>
														<div class="toggleBox" style='margin-left:3px;'></div>
														<div style='clear:both'></div>
													</div>
												</div>
											</a>
											<a href="javascript:void(0)" onclick="InventoryController.toggleList(3)">							
												<div class="ThreeByThree">
													<div class='spacing'>
														<div class="toggleBox"></div>
														<div class="toggleBox" style='margin-left:3px;'></div>	
														<div class="toggleBox" style='margin-left:3px;'></div>	
														<div style='clear:both'></div>
													</div>
													<div class='line spacing'>
														<div class="toggleBox"></div>
														<div class="toggleBox" style='margin-left:3px;'></div>	
														<div class="toggleBox" style='margin-left:3px;'></div>	
														<div style='clear:both'></div>
													</div>
													<div>
														<div class="toggleBox"></div>
														<div class="toggleBox" style='margin-left:3px;'></div>
														<div class="toggleBox" style='margin-left:3px;'></div>
														<div style='clear:both'></div>
													</div>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>	
							<div style='border-bottom:1px solid #eaeaea; margin: 10px -10px 20px -10px;'></div>
							<div class="row">
								<div class="col-md-12">
									<div id="PaginationTop"></div>
								</div>
							</div>
							
						</div>
						
					</div>
					<div id="CurrentInventoryListTwoByTwo" style='margin: 20px -10px; '></div>
					<div id="CurrentInventoryListThreeByThree" style='margin: 20px -10px; display:none;'></div>
					<div class="InventoryListContainer" style='clear: both;'>
						<div class="dividerContent">
							<div class='row'>
								<div class='col-md-12' style='padding-top:10px;'>
									<div id="PaginationBottom"></div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
</div>

	
	



