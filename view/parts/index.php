<div id="TopPartsPage">
	<div class="HalfSection EntryServiceDeptText">
		<div class="ContentHeader">
			<h1>WELCOME TO OUR PARTS DEPARTMENTS</h1>
		</div>
		<div style='margin-bottom:10px;'>
			Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. 
			Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur 
			ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla 
			consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
		</div>
		<div>
			<a href='http://www.partsfish.com/' target="_blank"><img src='<?php echo PATH ?>public/images/PartsFichLogoButton.png' /></a>
		</div>
		
	</div>
	<div class="HalfSection">
		<div class="PartsDeptPicture"></div>
	</div>
	<div style='clear:both'></div>
</div>

<div class="grayBackground" style='padding: 15px 0px; margin-bottom:20px;'>
	<div class="container">
		<div class="row">
			<div class="col-md-12" style='font-size: 20px; text-align: center;'>
				Visit one of our Parts Departments.
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="ServiceButtonContainer">
					<a href="http://www.dillonharley.com/we-sell-accessories-and--parts" target="_blank">
						<div class="buttonSingle HarleyButton">
							Harley-Davidson Parts Dept.
						</div>		
					</a>
					<a href="http://www.dilloncycles.com/we-sell-motorcycle-atv-and-utv--parts" target="_blank">
						<div class="buttonSingle redButton">
							Motorsport Parts Dept.
						</div>
					</a>
					<a href="http://www.dillonbrothersindian.com/we-help-you-get-any--parts" target="_blank">
						<div class="buttonSingle IndianButton">
							Indian Motorcycle Parts Dept.
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<form method='post' action="<?php echo PATH ?>ajax/partsdeptform" id="PartsDeptForm">
		<div class="row">
			<div class="col-md-12" style='margin-bottom:20px;'>
				<h2>Contact our Parts Department</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="inputID1">
					<div class="errorMessage"></div>
				</div>
			</div>
		</div>
		<div class="row" style='margin-bottom:10px;'>
			
			<div class="col-md-4">
				<div class='radioButton'><input type='radio' name='partsStore[]' value='3:Omaha' onchange="Globals.removeValidationMessage(1)" /> Harley-Davidson Omaha</div>
				<div class='radioButton'><input type='radio' name='partsStore[]' value='3:Fremont' onchange="Globals.removeValidationMessage(1)" /> Harley-Davidson Fremont</div>
			</div>
			<div class="col-md-4">
				<div class='radioButton'><input type='radio' name='partsStore[]' value='2:Omaha' onchange="Globals.removeValidationMessage(1)" /> Motorsports</div>
				<div class='radioButton'><input type='radio' name='partsStore[]' value='4:Omaha' onchange="Globals.removeValidationMessage(1)" /> Indian Motorcycle</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="formHeader">
					<div class="text">
						Contact Information 	
					</div>
					<div class="divider"></div>
					<div style='clear:both'></div>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-6">
						<div id="inputID2">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">First Name <span class='requiredIndicator'>*</span></div>
								<input type="text" name="partsFirstName" onchange="Globals.removeValidationMessage(2)" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div id="inputID3">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">Last Name <span class='requiredIndicator'>*</span></div>
								<input type="text" name="partsLastName" onchange="Globals.removeValidationMessage(3)" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div id="inputID7">
							<div class="errorMessage"></div>												
							<div class="inputLine">
								<div class="inputLabel">Home Phone <span class='requiredIndicator'>*</span></div>
								<input type="text" name="partsHomePhone" class="partsHomePhone" onchange="Globals.removeValidationMessage(7)" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="inputLine">
							<div class="inputLabel">Cell Phone</div>
							<input type="text" name="partsCellPhone" class="partsCellPhone" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div id="inputID12">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">Email Address</div>
								<input type="text" name="partsEmailAddress" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div class="inputLine">
							<div class="inputLabel">Street Address</div>
							<input type="text" name="partsStreetAddress" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8">
						<div class="inputLine">
							<div class="inputLabel">City</div>
							<input type="text" name="partsCity" />
						</div>
					</div>
					<div class="col-md-2">
						<div class="inputLine">
							<div class="inputLabel">State</div>
							<select name="partsState">
								<option value=''></option>
								<?php foreach($this -> states() as $key => $stateSingle): ?>
									<option value='<?php echo $key ?>'><?php echo $key ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="inputLine">
							<div class="inputLabel">Zip</div>
							<input type="text" name="partsZip" maxlength="5" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="formHeader">
					<div class="text">
						Vehicle Information
					</div>
					<div class="divider"></div>
					<div style='clear:both'></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-4">
						<div id="inputID4">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">Vehicle Year <span class='requiredIndicator'>*</span></div>
								<input type="text" name="partsVehicleYear" onchange="Globals.removeValidationMessage(4)" />
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div id="inputID5">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">Vehicle Make <span class='requiredIndicator'>*</span></div>
								<input type="text" name="partsVehicleMake" onchange="Globals.removeValidationMessage(5)" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div id="inputID6">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">Vehicle Model <span class='requiredIndicator'>*</span></div>
								<input type="text" name="partsVehicleModel" onchange="Globals.removeValidationMessage(6)" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div class="inputLine">
							<div class="inputLabel">Miles</div>
							<input type="text" name="partsVehicleMiles" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="inputLine">
							<div class="inputLabel">VIN Number</div>
							<input type="text" name="partsVehicleVinNumber" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="formHeader">
					<div class="text">
						Parts Request Information
					</div>
					<div class="divider"></div>
					<div style='clear:both'></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div id="inputID8">
					<div class="errorMessage"></div>						
					<div class="inputLine">
						<div class="inputLabel">What parts are needed? <span class='requiredIndicator'>*</span></div>
						<textarea name="partNeededRequest" onchange="Globals.removeValidationMessage(8);"></textarea>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div id="inputID9">
					<div class="errorMessage"></div>						
					<div class="inputLine">
						<div class="inputLabel">Do you have any part number(s)?</div>
						<textarea name="partNumberRequest" onchange="Globals.removeValidationMessage(9)"></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div id="inputID13">
					<div class="errorMessage"></div>					
					<div class="inputLine">
						<div class="inputLabel">Are You Human?: <span class='requiredIndicator'>*</span></div>
						<img src="<?php echo PATH ?>view/captcha.php" />
						<input type="text" name="capta" style='margin-top:10px' />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<input type='submit' value='Send Request' class="blueButton" />
			</div>
		</div>
	</form>
</div>
<div class="grayBackground" style='padding: 15px 0px; margin:20px 0px 0px 0px;'>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				Dillon Brothers is committed to your privacy.
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style='margin-top:10px;'>
				<a href='<?php echo PATH ?>privacy'>View our Privacy Statement</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style='font-size:12px; margin-top:10px;'>
				
	
			</div>
		</div>
	</div>
</div>

