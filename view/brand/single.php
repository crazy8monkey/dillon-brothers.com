<div class="container">	
	<div class="row">
		<div class="col-md-12" style="margin:15px 0px 20px 0px;">
			<?php if(isset($this -> ISOEMBrandPage)): ?>
				<a href="<?php echo PATH ?>brand">Brands We Carry</a><span style="margin:0px 5px;"><i class="fa fa-caret-right" aria-hidden="true"></i></span><?php echo $this -> OEMContent -> BrandName?> Vehicles
			<?php endif; ?>
			<?php if(isset($this -> ISPartApparelBrandPage)): ?>
				<a href="<?php echo PATH ?>brand">Brands We Carry</a><span style="margin:0px 5px;"><i class="fa fa-caret-right" aria-hidden="true"></i></span><?php echo $this -> PartApparelBrandContent -> BrandName; ?>
			<?php endif; ?>		
		</div>
	</div>
</div>

<?php if(isset($this -> ISOEMBrandPage)): ?>
	<?php 
		$brandName = $this -> OEMContent -> BrandName;
		$brandImage = PHOTO_URL . 'brands/' . $this -> OEMContent -> BrandImage;
		$brandDescription = $this -> OEMContent -> BrandDescription;
	?>
	<div class="container">	
		<div class="row">
			<div class="col-md-12 ContentHeader">
				<h1><?php echo $this -> OEMContent -> BrandName?> Vehicles</h1>
			</div>
		</div>	
	</div>
	<div class="grayBackground" style='padding:10px 0px; margin-bottom:20px;'>
		<div class="container">	
			<div class="row">
				<div class="col-md-4" style='text-align:center;'>
					<?php if(!empty($this -> OEMContent -> AlternateBrandImage)): ?>
						<img src='<?php echo PHOTO_URL ?>brands/<?php echo $this -> OEMContent -> AlternateBrandImage ?>' />
					<?php else: ?>
						<img src='<?php echo PHOTO_URL ?>brands/<?php echo $this -> OEMContent -> BrandImage ?>' />
					<?php endif; ?>
					
				</div>
				<div class="col-md-8">
					<?php echo $this -> OEMContent -> BrandDescription ?>
				</div>
			</div>
		</div>
	</div>	
<?php endif; ?>	

<?php if(isset($this -> ISPartApparelBrandPage)): ?>
	<?php 
		$brandName = $this -> PartApparelBrandContent -> BrandName;
		$brandImage = PHOTO_URL . 'brands/' . $this -> PartApparelBrandContent -> BrandImage;
		$brandDescription = $this -> PartApparelBrandContent -> PartDescription . ' / ' . $this -> PartApparelBrandContent -> ApparelDescription;
	?>
	<div class="container">	
		<div class="row">
			<div class="col-md-12 ContentHeader">
				<h1><?php echo $this -> PartApparelBrandContent -> BrandName?></h1>
			</div>
		</div>	
	</div>
	<div class="grayBackground" style='margin-bottom:20px;'>
		<div class="container">	
			<div class="row">
				<div class="col-md-4" style='text-align:center; padding:10px 0px;'>
					<img src='<?php echo PHOTO_URL ?>PartsBrand/<?php echo $this -> PartApparelBrandContent -> BrandImage ?>' />
				</div>
				<div class="col-md-8">
					<?php if($this -> PartApparelBrandContent -> BrandType == 1 || $this -> PartApparelBrandContent -> BrandType == 3): ?>
						<div class='partApparelDescription'>
							<?php echo $this -> PartApparelBrandContent -> PartDescription; ?>	
						</div>
					<?php endif; ?>
					<?php if($this -> PartApparelBrandContent -> BrandType == 2 || $this -> PartApparelBrandContent -> BrandType == 3): ?>
						<div class='partApparelDescription'>
							<?php echo $this -> PartApparelBrandContent -> ApparelDescription; ?>	
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>	
<?php endif; ?>	


<?php if(count($this -> BrandContent) > 0): ?>
	<div class="container">	
		<div class="row">
			<div class="col-md-12" style='margin-bottom:20px;'>
				<?php if(isset($this -> ISOEMBrandPage)): ?>
					<h2>Research more about <?php echo $this -> OEMContent -> BrandName?> Vehicles</h2>
				<?php endif; ?>	
				<?php if(isset($this -> ISPartApparelBrandPage)): ?>
					<h2>Research more about <?php echo $this -> PartApparelBrandContent -> BrandName ?></h2>
				<?php endif; ?>	
			</div>
		</div>	
		<?php foreach($this -> BrandContent as $contentSingle): ?>
			<div class="row">
				<div class="col-md-12">
					<?php if(isset($this -> ISOEMBrandPage)): ?>
						<a href='<?php echo PATH ?>brand/<?php echo $this -> OEMContent -> SEOUrl?>/<?php echo $contentSingle['contentUrl'] ?>'><?php echo $contentSingle['ContentTitle'] ?></a>
					<?php endif; ?>
					<?php if(isset($this -> ISPartApparelBrandPage)): ?>
						<a href='<?php echo PATH ?>brand/<?php echo $this -> PartApparelBrandContent -> GetSEOUrl() ?>/<?php echo $contentSingle['contentUrl'] ?>'><?php echo $contentSingle['ContentTitle'] ?></a>
					<?php endif; ?>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>

<script type="application/ld+json">
	{
		"@context":"http://schema.org",
		"@type":"Brand",
		"name": "<?php echo $brandName ?>",
		"description": "<?php echo $brandDescription ?>",
		"image": "<?php echo $brandImage ?>"
		<?php if(count($this -> BrandContent) > 0): ?>
			,
			"mainEntityOfPage": [
				<?php foreach($this -> BrandContent as $key => $contentSingle): ?>
					{"@type": "WebPage"}<?php if((count($this -> BrandContent)-1) != $key): ?>,<?php endif; ?>
				<?php endforeach; ?>
            ]
				
			
		<?php endif; ?>			   
	}
</script>
		
<?php if(isset($this -> ISOEMBrandPage)): ?>
	
<?php endif; ?>			


<!-- -->



