<div class="container">	
	<div class="row">
		<div class="col-md-12" style="margin:15px 0px 20px 0px;">
			<a href="<?php echo PATH ?>brand">Brands We Carry</a><span style="margin:0px 5px;"><i class="fa fa-caret-right" aria-hidden="true"></i></span><a href="<?php echo PATH ?>brand/<?php echo $this -> pagecontent -> GetBrandUrl(); ?>"><?php echo $this -> pagecontent -> ParentBrandName ?></a><span style="margin:0px 5px;"><i class="fa fa-caret-right" aria-hidden="true"></i></span><?php echo $this -> pagecontent -> LandingPageTitle ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 ContentHeader">
			<h1><?php echo $this -> pagecontent -> LandingPageTitle ?></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php echo $this -> pagecontent -> Content ?>
		</div>
	</div>
</div>

