<div class="container">	
	<div class="row">
		<div class="col-md-12 ContentHeader">
			<h1>BRANDS WE CARRY</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h2>Vehicle Brands</h2>
		</div>
	</div>
	<div class="row">
		<?php foreach(array_chunk($this -> OEMBrands, 6, true) as $OEMBrandThree) : ?>
			<div class="row">
				<?php foreach($OEMBrandThree as $OEMBrandSingle) : ?>
					<div class="col-md-2">
						<a href='<?php echo PATH ?>brand/<?php echo $OEMBrandSingle['brandNameSEOUrl'] ?>'>
							<div class="BrandSingleElement">
								<?php if(!empty($OEMBrandSingle['AlternateBrandImage'])): ?>
									<img src='<?php echo PHOTO_URL ?>brands/<?php echo $OEMBrandSingle['AlternateBrandImage'] ?>' />
								<?php else: ?>
									<img src='<?php echo PHOTO_URL ?>brands/<?php echo $OEMBrandSingle['BrandImage'] ?>' />
								<?php endif; ?>
								
							</div>
						</a>
					</div>
					
					<script type="application/ld+json">
						{
							"@context":"http://schema.org",
							"@type":"Brand",
							"name": "<?php echo $OEMBrandSingle['BrandName'] ?>",
							"description": "<?php echo $OEMBrandSingle['BrandDescription'] ?>",
							"image": "<?php echo PHOTO_URL ?>brands/<?php echo $OEMBrandSingle['BrandImage'] ?>"			   
						}
					</script>
				<?php endforeach; ?>
			</div>
		<?php endforeach; ?>						
		
	</div>
	<div class="row">
		<div class="col-md-12">
			<h2>Part/Apparel Brands</h2>
		</div>
	</div>
	<div class="row">
		<?php foreach(array_chunk($this -> PartApparelBrands, 6, true) as $PartApparelBrandThree) : ?>
			<div class="row">
				<?php foreach($PartApparelBrandThree as $PartApparelBrandSingle) : ?>
					<div class="col-md-2">
						<a href='<?php echo PATH ?>brand/<?php echo $PartApparelBrandSingle['brandSEOUrl'] ?>'>
							<div class="BrandSingleElement">
								<img src='<?php echo PHOTO_URL ?>PartsBrand/<?php echo $PartApparelBrandSingle['partBrandImage'] ?>' />	
							</div>
						</a>
						
					</div>
					<script type="application/ld+json">
						{
							"@context":"http://schema.org",
							"@type":"Brand",
							"name": "<?php echo $PartApparelBrandSingle['brandName'] ?>",
							"description": "<?php echo $OEMBrandSingle['partDescription'] ?>",
							"image": "<?php echo PHOTO_URL ?>PartsBrand/<?php echo $PartApparelBrandSingle['partBrandImage'] ?>"			   
						}
					</script>
					
					
				<?php endforeach; ?>
			</div>
		<?php endforeach; ?>						
		
	</div>
</div>



