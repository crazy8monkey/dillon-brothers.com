<div class="container">
	<div class="row">
		<div class="col-md-12" style="margin:20px 0px;">
			<a href="<?php echo PATH ?>contact">Contact</a><span style="margin:0px 5px;"><i class="fa fa-caret-right" aria-hidden="true"></i></span>Donation Request	
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12 ContentHeader">
			<h1>DILLON BROTHERS DONATION/SPONSORSHIP REQUEST SUBMISSION</h1>
		</div>
	</div>
	<form method='post' action="<?php echo PATH ?>ajax/donationform" id="DonationRequestForm">
		<div class="row">
			<div class="col-md-12">
				<div class="formHeader">
					<div class="text">
						Contact Information
					</div>
					<div class="divider"></div>
					<div style='clear:both'></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="inputLine">
					<div class="inputLabel">If contact person, organizer or beneficiary is a customer of Dillon Brothers Harley-Davidson or Motorsports, please list name</div>
					<input type="text" name="donationNameList" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div id="inputID1">
					<div class="errorMessage"></div>										
					<div class="inputLine">
						<div class="inputLabel">Event Contact Person <span class='requiredIndicator'>*</span></div>
						<input type="text" name="donationEventContactName" onchange="Globals.removeValidationMessage(1)" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div id="inputID2">
					<div class="errorMessage"></div>										
					<div class="inputLine">
						<div class="inputLabel">Contact Phone <span class='requiredIndicator'>*</span></div>
						<input type="text" name="donationEventContactPhone" class="donationEventContactPhone" onchange="Globals.removeValidationMessage(2)" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div id="inputID3">
					<div class="errorMessage"></div>
					<div class="inputLine">
						<div class="inputLabel">Contact Email <span class='requiredIndicator'>*</span></div>
						<input type="text" name="donationEventContactEmail" onchange="Globals.removeValidationMessage(3)" />
					</div>
				</div>										
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="formHeader">
					<div class="text">
						Fundraising Event Information
					</div>
					<div class="divider"></div>
					<div style='clear:both'></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div id="inputID4">
					<div class="errorMessage"></div>
					<div class="inputLine">
						<div class="inputLabel">Name of Event <span class='requiredIndicator'>*</span></div>
						<input type="text" name="donationEventName" onchange="Globals.removeValidationMessage(4)" />
					</div>
				</div>
				
			</div>
			<div class="col-md-4">
				<div id="inputID5">
					<div class="errorMessage"></div>				
					<div class="inputLine">
						<div class="inputLabel">Type of Event <span class='requiredIndicator'>*</span></div>
						<input type="text" name="donationEventType" onchange="Globals.removeValidationMessage(5)" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div id="inputID6">
					<div class="errorMessage"></div>	
					<div class="inputLine">
						<div class="inputLabel">is the event tax deductible with Tax ID Number? <span class='requiredIndicator'>*</span></div>
						<input type="text" name="donationTaxIDNumber" onchange="Globals.removeValidationMessage(6)" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div id="inputID7">
					<div class="errorMessage"></div>				
					<div class="inputLine">
						<div class="inputLabel">Who is event Benefiting? <span class='requiredIndicator'>*</span></div>
						<input type="text" name="donationBenefiting" onchange="Globals.removeValidationMessage(7)" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div id="inputID8">
					<div class="errorMessage"></div>
					<div class="inputLine">
						<div class="inputLabel">Date of Event <span class='requiredIndicator'>*</span></div>
						<input type="text" name="donationDateofEvent" class="donationDateofEvent" onchange="Globals.removeValidationMessage(8)" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div id="inputID9">
					<div class="errorMessage"></div>
					<div class="inputLine">
						<div class="inputLabel">Anticipated attendance of event <span class='requiredIndicator'>*</span></div>
						<input type="text" name="donationAnticipatedAttendance" onchange="Globals.removeValidationMessage(9)" />
					</div>
				</div>
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="formHeader">
					<div class="text">
						Additional Information
					</div>
					<div class="divider"></div>
					<div style='clear:both'></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="inputLine">
					<div class="inputLabel">Attachment(s)</div>
					<input type='file' name='donationAttachments[]' multiple="" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="inputLine">
					<div class="inputLabel">Additional Comments</div>
					<textarea name='donationAdditionalComments' class='additionalCommentText'></textarea>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div id="inputID10">
					<div class="errorMessage"></div>					
					<div class="inputLine">
						<div class="inputLabel">Are You Human?: <span class='requiredIndicator'>*</span></div>
						<img src="<?php echo PATH ?>view/captcha.php" />
						<input type="text" name="capta" style='margin-top:10px' onchange="Globals.removeValidationMessage(10)" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<input type='submit' value='Send Donation Request'  class='blueButton' />
			</div>
		</div>
	</form>
</div>
<div class="grayBackground" style='padding: 15px 0px; margin:20px 0px 0px 0px;'>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				Dillon Brothers is committed to your privacy.
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style='margin-top:10px;'>
				<a href='<?php echo PATH ?>privacy'>View our Privacy Statement</a>
			</div>
		</div>
	</div>
</div>

