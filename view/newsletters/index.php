	
<div class="container">	
	<div class="row">
		<div class="col-md-12 ContentHeader">
			<h1>SIGN UP FOR YOUR NEWSLETTER</h1>
		</div>
	</div>
</div>
<div class="BackgroundGrey">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				At Dillon Brothers there's always a lot going on and we like to keep everyone informed. We send out frequent updates on events and offers 
				to our Newsletter subscribers (and VIB Text members... and Facebook friends... and Twitter subscribers.. and also through our newsfeeds.. and so on) 
				so no one misses a thing. Click the images below to go through our Newsletter archive:
			</div>
			<div class="col-md-6">
						<!--
Do not modify the NAME value of any of the INPUT fields
the FORM action, or any of the hidden fields (eg. input type=hidden).
These are all required for this form to function correctly.
-->
<form method="post" action="http://emarketing.morethanrewards.com/form.php?form=281" id="frmSS281" onsubmit="return CheckForm281(this);" class="myForm">
	<div class="row">
		<div class="col-md-12">
			<div class="input">
				<div class="inputLabel">Your Email Address <span class="required">*</span></div>
				<input type="text" name="email" value="" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="input">
				<div class="inputLabel">First Name</div>
				<input type="text" name="CustomFields[2]" id="CustomFields_2_281" value="" size='50'>
			</div>
		</div>
		<div class="col-md-6">
			<div class="input">
				<div class="inputLabel">Last Name</div>
				<input type="text" name="CustomFields[3]" id="CustomFields_3_281" value="" size='50'>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="input">
				<div class="inputLabel">City</div>
				<input type="text" name="CustomFields[8]" id="CustomFields_8_281" value="" size='50'>
			</div>
		</div>
		<div class="col-md-4">
			<div class="input">
				<div class="inputLabel">State</div>
				<input type="text" name="CustomFields[9]" id="CustomFields_9_281" value="" size='50'>
			</div>
		</div>
		<div class="col-md-4">
			<div class="input">
				<div class="inputLabel">Zip Code</div>
				<input type="text" name="CustomFields[10]" id="CustomFields_10_281" value="" size='50'>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="input">
				<input type="hidden" name="format" value="h" />
				<div class="inputLabel">Enter the security code shown <span class="required">*</span></div>
				<div>
					<script type="text/javascript">
					// <![CDATA[
						if (!Application) var Application = {};
						if (!Application.Page) Application.Page = {};
						if (!Application.Page.ClientCAPTCHA) {
							Application.Page.ClientCAPTCHA = {
								sessionIDString: '',
								captchaURL: [],
								getRandomLetter: function () { return String.fromCharCode(Application.Page.ClientCAPTCHA.getRandom(65,90)); },
								getRandom: function(lowerBound, upperBound) { return Math.floor((upperBound - lowerBound + 1) * Math.random() + lowerBound); },
								getSID: function() {
									if (Application.Page.ClientCAPTCHA.sessionIDString.length <= 0) {
										var tempSessionIDString = '';
										for (var i = 0; i < 32; ++i) tempSessionIDString += Application.Page.ClientCAPTCHA.getRandomLetter();
										Application.Page.ClientCAPTCHA.sessionIDString.length = tempSessionIDString;
									}
									return Application.Page.ClientCAPTCHA.sessionIDString;
								},
								getURL: function() {
									if (Application.Page.ClientCAPTCHA.captchaURL.length <= 0) {
										var tempURL = 'http://emarketing.morethanrewards.com/admin/resources/form_designs/captcha/index.php?c=';
										
																tempURL += Application.Page.ClientCAPTCHA.getRandom(1,1000);
																		tempURL += '&ss=' + Application.Page.ClientCAPTCHA.getSID();
																	Application.Page.ClientCAPTCHA.captchaURL.push(tempURL);
														}
									return Application.Page.ClientCAPTCHA.captchaURL;
								}
							}
						}
					
						var temp = Application.Page.ClientCAPTCHA.getURL();
						for (var i = 0, j = temp.length; i < j; i++) document.write('<img src="' + temp[i] + '" alt="img' + i + '" />');
					// ]]>
					</script>
					<script type="text/javascript">
					// <![CDATA[
					
								function CheckMultiple281(frm, name) {
									for (var i=0; i < frm.length; i++)
									{
										fldObj = frm.elements[i];
										fldId = fldObj.id;
										if (fldId) {
											var fieldnamecheck=fldObj.id.indexOf(name);
											if (fieldnamecheck != -1) {
												if (fldObj.checked) {
													return true;
												}
											}
										}
									}
									return false;
								}
							function CheckForm281(f) {
								var email_re = /[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i;
								if (!email_re.test(f.email.value)) {
									alert("Please enter your email address.");
									f.email.focus();
									return false;
								}
							
									if (f.captcha.value == "") {
										alert("Please enter the security code shown");
										f.captcha.focus();
										return false;
									}
								
									return true;
								}
							
					// ]]>
					</script>
					
					
				</div>
				<input type="text" name="captcha" value="" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="margin-bottom:30px;">
			<input type="submit" value="Subscribe" class="blueButton" />
			<!--<span style="display: block; font-size: 10px; color: gray; padding-top: 5px;"><a href="http://www.morethanrewards.com" target="__blank" style="font-size:10px;color:gray;">Email marketing</a> by MoreThanRewards.com</span> -->
		</div>
	</div>
</form>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<?php $prevYear = null ?>
	<?php 
		$yearNewsletters = array(); 
		$years = array(); 
	?>
	<?php if(count($this -> newsLetters) > 0) : ?>
		<?php foreach($this -> newsLetters as $newsLetter) : ?>	
			<?php $NewYear = explode("-", $newsLetter['DateSent']);
	
				if($prevYear != $NewYear[0]) {
					//echo "<strong>Array Push</strong><br />";
					array_push($years, array("Year" => $NewYear[0]));	
				}
				$prevYear = $NewYear[0]; 
			?>
		<?php endforeach; ?>
		
		
		<?php foreach($years as $yearSingle): ?>
			<div class="row" style='margin-bottom:10px;'>
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12 ContentHeader">
							<h2><?php echo $yearSingle['Year'] ?></h1>
						</div>
					</div>	
					<div class="row">
						<div class="col-md-12">
						<?php 
							
							$currentYear = $yearSingle['Year'];
												
							$filteredYearNewsletters = array_filter($this -> newsLetters, function ($var) use($currentYear) {
								$Year = explode("-", $var['DateSent']);
								return $Year[0] == $currentYear; 
							});
						?>
						
						
								
						<?php //foreach(array_chunk($filteredYearNewsletters, 6, true) as $newsletterSix): ?>
							<?php foreach($filteredYearNewsletters as $newsletterSingle): ?>
									<div class="newsLetterSingleElement">
										<a href="<?php echo NEWSLETTER_URL. $newsletterSingle['folderName'] ?>/" target='_blank'>
											<div class="newsLetterLink">
												<img src="<?php echo NEWSLETTER_URL . $newsletterSingle['folderName'] . '/' . $newsletterSingle['ThumbNail'] ?> " />
												<div class="name">
													<?php echo $newsletterSingle['Name'] ?>
												</div>
											</div>
										</a>
									</div>
								<?php endforeach; ?>	
						<?php //endforeach; ?>
						
						</div>
					</div>
					
					
				</div>
			</div>
		<?php endforeach; ?>
		
	<?php endif; ?>
	
</div>

