<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html amp lang="en"> <!--<![endif]-->
	<head>
	
		<title>Thank You | Dillon Brothers</title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
		<meta name="robots" content="noindex">
		<meta http-equiv="Cache-control" content="public">
		<meta charset="utf-8">

		<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		
		<link rel="stylesheet" href="<?php echo PATH ?>public/css/style.css" type="text/css" />
		<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
		<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
		
		<link rel="stylesheet" href="<?php echo PATH ?>public/css/CartController.css" />
							
		
		<script type="text/javascript" src="<?php echo PATH ?>public/js/Globals.js"></script>
		
		<script src="https://use.typekit.net/kpv4owm.js"></script>
		
		
					
				
		
	</head>
<body >
<div class="container">
	<div class="col-md-12" style='text-align:center'>
		Thank you making a purhase to Dillon Brothers Ecommerce Store!
	</div>
</div>
<script type='text/javascript'>
$(document).ready(function() {
	setTimeout(function(){
		window.location.assign('<?php echo PATH ?>shop'); 
	}, 2000);	
})

</script>
</body>
</html>