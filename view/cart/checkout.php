<div id="CheckoutPage">
	<div class="container-fluid">
		<div class="col-md-8">
			<div class="whiteBoxContent">
				<div><h1>Checkout</h1></div>
				<form id="CheckoutForm" method='post' action="<?php echo PATH ?>ajax/SavePendingOrder">					
					<?php if(isset($this -> showBillingInformation)): ?>
						<div class="CheckboxOptions">
							<div class='checkboxElement'><input type='hidden' name='createAccountOption' value='0' /><input type='checkbox' name='createAccountOption' value='1' />Create Account</div>
						</div>
						<div class="row">
							<div class="col-md-12" style='margin-top:20px;'>
								<h2>Your Information</h2>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div id="shippingFirstNameError">
									<div class="errorMessage"></div>
									<div class="inputItem">
										<div class="inputLabel">First Name</div>
										<input type='text' class='shippingFirstName' name='shippingFirstName' />
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div id="shippingLastNameError">
									<div class="errorMessage"></div>
									<div class="inputItem">
										<div class="inputLabel">Last Name</div>
										<input type='text' class='shippingLastName' name='shippingLastName' />
									</div>	
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div id="shippingPhoneError">
									<div class="errorMessage"></div>
									<div class="inputItem">
										<div class="inputLabel">Phone Number</div>
										<input type='text' class='phoneNumber' name='phoneNumber' />
									</div>	
								</div>
								
							</div>
							<div class="col-md-6">
								<div id="shippingEmailError">
									<div class="errorMessage"></div>
									<div class="inputItem">
										<div class="inputLabel">Email</div>
										<input type='text' class='shippingEmail' name='shippingEmail' />
									</div>	
								</div>
								
							</div>	
						</div>
						<div class="row">
							<div class="col-md-12" style='margin-top:20px;'>
								<h2>Billing Information</h2>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="inputItem">
									<div class="inputLabel">Address</div>
									<input type='text' class='billingAddress' />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="inputItem">
									<div class="inputLabel">City</div>
									<input type='text' class='billingCity' />
								</div>
							</div>
							<div class="col-md-4">
								<div class="inputItem">
									<div class="inputLabel">State</div>
									<select class='billingState'>
										<option value='0'>Please Select</option>
										<?php foreach($this -> states() as $key => $stateSingle): ?>
											<option value='<?php echo $key?>'><?php echo $stateSingle ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="inputItem">
									<div class="inputLabel">Zip</div>
									<input type='text' class='billingZip' />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12" style='margin-top:20px;'>
								<h2>Card Info.</h2>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="inputItem">
									<div class="inputLabel">Card Holder Name</div>
									<input type='text' class='CardHolderName' />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="inputItem">
									<div class="inputLabel">Card Card Number</div>
									<input type='text' class='CreditCardNumber' />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="inputItem">
									<div class="inputLabel">Expiration Month</div>
									<select class='ExpirationMonth'>
										<option value='0'>Please Select</option>
										<?php foreach($this -> months() as $key => $monthSingle): ?>
											<option value='<?php echo $key?>'><?php echo $monthSingle ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="inputItem">
									<div class="inputLabel">Expiration Year</div>
									<select class='ExpirationYear'>
										<option value='0'>Please Select</option>
										<?php foreach($this -> year() as $key => $yearSingle): ?>
											<option value='<?php echo $key?>'><?php echo $yearSingle ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="inputItem">
									<div class="inputLabel">CVC</div>
									<input type='text' class='CVCNumber' />
								</div>
							</div>
						</div>
					
					<?php else: ?>
						<div class="row">
							<div class="col-md-12" style='margin-top:20px;'>
								<h2>Your Information being sent to checkout</h2>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div><?php echo $this -> AccountSingle -> FirstName ?> <?php echo $this -> AccountSingle -> LastName ?></div>
								<div><?php echo $this -> AccountSingle -> Email ?></div>
								<div><?php echo $this -> PhoneFormat($this -> AccountSingle -> Phone) ?></div>
							</div>
							<div class="col-md-6">
							
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div style='border-bottom:1px solid #cecece; margin-top:15px;'></div>
							</div>
						</div>
					<?php endif; ?>
					
					
					
					
					
					<div class="row">
						<div class="col-md-12">
							<div id="storePickupError">
								<div class="errorMessage"></div>
								<div class="optionButtons">
									<div style='margin-bottom:3px;'><input type='radio' name='shipOption[]' onclick='CartController.ShowShippingForm(this); CartController.UpdateShippingRates(this)' value='0' />Pick it up at store</div>
									<div><input type='radio' name='shipOption[]' onclick='CartController.ShowShippingForm(this); CartController.UpdateShippingRates(this)' value='1' />Ship to address</div>
								</div>		
							</div>
							
						</div>
					</div>
					<div id="ShippingInformationElement" style='display:none;'>
						<div class="row">
							<div class="col-md-12">
								<h2>Shipping Information</h2>
							</div>
						</div>	
						
						<div class="row">
							<div class="col-md-12">
								<div id="shippingAddressError">
									<div class="errorMessage"></div>
									<div class="inputItem">
										<div class="inputLabel">Address</div>
										<input type='text' name='shippingAddress' class='shippingAddress' />
									</div>	
								</div>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div id="shippingCityError">
									<div class="errorMessage"></div>
									<div class="inputItem">
										<div class="inputLabel">City</div>
										<input type='text' class='shippingCity' name='shippingCity' />
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="inputItem">
									<div id="shippingStateError">
										<div class="errorMessage"></div>
										<div class="inputLabel">State</div>
										<select name='shippingState' class='shippingState'>
											<option value='0'>Please Select</option>
											<?php foreach($this -> states() as $key => $stateSingle): ?>
												<option value='<?php echo $key?>'><?php echo $stateSingle ?></option>
											<?php endforeach; ?>
										</select>
									</div>
									
									
								</div>
							</div>
							<div class="col-md-4">
								<div id="shippingZipError">
									<div class="errorMessage"></div>
									<div class="inputItem">
										<div class="inputLabel">Zip</div>
										<input type='text' class='shippingZip' maxlength='5' name='shippingZip' />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style='margin-top:10px;'>
							<input type='submit' value="Submit" class='checkoutButton redButton' />
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="col-md-4">
			<div id="CheckoutCartSummary">	
				<div class="checkoutHeader">
					<a href='javascript:void(0)' onclick='CartController.OpenMobileCartSummary();'>
						Your Cart 	
					</a>
					<a href='javascript:void(0)' onclick='CartController.CloseMobileCartSummary()'><i class="fa fa-times-circle" aria-hidden="true"></i></a></div>		
				<div class='mobileCartSummary'>				
					<?php foreach($_SESSION['CurrentCartObject']['results']['cartItems'] as $key => $cartItemSingle): ?>
						<?php 
							$productKey = array_search($cartItemSingle['product_id'], array_column($this -> productsSelected, 'productID'));
							if($cartItemSingle['colorSelected'] != 0) {
								$colorSelectedKey = array_search($cartItemSingle['colorSelected'], array_column($this -> colorsSelected, 'settingColorID'));	
							}
							
							if($cartItemSingle['sizeSelected'] != 0) {
								$sizeSelectedKey = array_search($cartItemSingle['sizeSelected'], array_column($this -> sizesSelected, 'settingSizeID'));	
							}
						?>
						
						<div class="row">
							<div class="col-md-12">
								<div class="CheckoutCartSingleItem">
									<div style='display: table-row;'>
										<div class="CheckoutProductInfo">
											<div class="name"><?php echo $this -> productsSelected[$productKey]['productName'] ?></div>
											<div>
												<span class='price'>$<?php echo number_format($cartItemSingle['price'], 2) ?></span> | <?php echo $cartItemSingle['quantity'] ?> Qty
												<?php if(isset($colorSelectedKey)): ?>
													| <?php echo $this -> colorsSelected[$colorSelectedKey]['settingColorText'] ?>
												<?php endif; ?>	
												<?php if(isset($sizeSelectedKey)): ?>
													| <?php echo $this -> sizesSelected[$sizeSelectedKey]['settingSizeText'] ?>
												<?php endif; ?>	
											</div>
										</div>
										<div class="itemTotalPrice">
											$<?php echo number_format($cartItemSingle['price'] * $cartItemSingle['quantity'], 2) ?>
										</div>	
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
					<div class="row">
						<div class="col-md-12">
							<table class='cartSummaryTotals'>
								<tr>
									<td>Cart Total:</td>
									<td class='Number' id='CartTotalNumber'>$<?php echo number_format($_SESSION['CurrentCartObject']['results']['total'], 2) ?></td>
								</tr>
								<tr>
									<td>Shipping:</td>
									<td class='Number' id='ShippingRates'> -- </td>
								</tr>
								<tr>
									<td>Taxes:</td>
									<td class='Number'> -- </td>
								</tr>
								<tr>
									<td>Grand Total:</td>
									<td class='Number' id='GrandTotalRates'>$<?php echo number_format($_SESSION['CurrentCartObject']['results']['total'], 2) ?></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



