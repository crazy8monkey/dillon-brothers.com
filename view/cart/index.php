<div class="container" style='padding-bottom:90px;'>
	<div class="row">
		<div class="col-md-12 ContentHeader">
			<h1>YOUR CART</h1>
		</div>
	</div>
	
	<?php if(isset($this -> productsSelected)): ?>
		<div id="TotalCartItems">
			<?php foreach($_SESSION['CurrentCartObject']['results']['cartItems'] as $key => $cartItemSingle): ?>
				<?php 
					$productKey = array_search($cartItemSingle['product_id'], array_column($this -> productsSelected, 'productID'));
					if($cartItemSingle['colorSelected'] != 0) {
						$colorSelectedKey = array_search($cartItemSingle['colorSelected'], array_column($this -> colorsSelected, 'settingColorID'));	
					}
					
					if($cartItemSingle['sizeSelected'] != 0) {
						$sizeSelectedKey = array_search($cartItemSingle['sizeSelected'], array_column($this -> sizesSelected, 'settingSizeID'));	
					}
				?>
				
				<div class="row">
					<div class="col-md-12">
						<div class="CartItemSingle">
							<div style='display: table-row;'>
								<div class="ProductImage">
									<img src='<?php echo PHOTO_URL ?>ecommerce/<?php echo $this -> productsSelected[$productKey]['productFolderName'] ?>/<?php echo $this -> productsSelected[$productKey]['PhotoName'] ?>-s.<?php echo $this -> productsSelected[$productKey]['PhotoExt'] ?>' />
								</div>
								<div class="ProductInfo">
									<div class="name"><?php echo $this -> productsSelected[$productKey]['productName'] ?></div>
									<div>
										<span class='price'>$<?php echo number_format($cartItemSingle['price'], 2) ?></span> | <?php echo $cartItemSingle['quantity'] ?> Qty
										<?php if(isset($colorSelectedKey)): ?>
											| <?php echo $this -> colorsSelected[$colorSelectedKey]['settingColorText'] ?>
										<?php endif; ?>	
										<?php if(isset($sizeSelectedKey)): ?>
											| <?php echo $this -> sizesSelected[$sizeSelectedKey]['settingSizeText'] ?>
										<?php endif; ?>	
									</div>
								</div>
								<div class="itemTotalPrice">
									$<?php echo number_format($cartItemSingle['price'] * $cartItemSingle['quantity'], 2) ?> <a href='javascript:void(0);' onclick='CartController.RemoveCartItem("<?php echo $key ?>", this)'><div class="deleteCartItem"></div></a>
								</div>	
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
			<div class="row">
				<div class="col-md-12" style='text-align:right'>
					Your Total: <span id="CartTotal">$<?php echo number_format($_SESSION['CurrentCartObject']['results']['total'], 2); ?></span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<a href="<?php echo PATH ?>cart/checkout">
						<div class="redButton CheckOutButton">
							CHECKOUT
						</div>	
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12" style='text-align:right; margin-top: 10px; font-size: 12px;'>
					*Shipping & Taxes will be calculated at checkout
				</div>
			</div>
		</div>
		<div id="EmptyCart" style='display:none;'>
			<div class="row">
				<div class="col-md-12">
					You have no items in your cart
				</div>
			</div>
		</div>
	<?php else: ?>
		<div class='row'>
			<div class='col-md-12'>
				You have no items in your cart	
			</div>
		</div>
	<?php endif; ?>
</div>	
	



