<?php 
$taxes = number_format(0.00, 2);
$shipping = number_format(0.00, 2);


//generate taxes
if($_SESSION['NewOrder']['ship-option']['0'] == 1) {
	if($_SESSION['NewOrder']['ship-state'] == 'NE') {
		$taxes = number_format($_SESSION['CurrentCartObject']['results']['total'] * .07, 2);
	} 
} else {
	$taxes = number_format($_SESSION['CurrentCartObject']['results']['total'] * .07, 2);
}

//generate Shipping
if($_SESSION['NewOrder']['ship-option']['0'] == 1) {
	$shipping = number_format($this -> shippingRates, 2);
}

$grandTotal = number_format($_SESSION['CurrentCartObject']['results']['total'] + $taxes + $shipping, 2);

?>

<div id="CheckoutPage">
	<div class="container">
		<div class="col-md-12">
			<div class="whiteBoxContent">
				
				<form id="ConfirmPage" method='post' action="<?php echo PATH ?>ajax/SaveOrder">
					<input type='hidden' name='taxRate' value='<?php echo $taxes ?>' />
					<input type='hidden' name='shippingRate' value='<?php echo $shipping ?>' />
					<input type='hidden' name='GrandTotal' value='<?php echo $grandTotal ?>' />
					<div><h1>Confirm Your Cart</h1></div>	
					<div class="row">
						<div class="col-md-6">
							<div><h2>Your Information</h2></div>
							<div><?php echo $_SESSION['NewOrder']['user-first-name'] ?> <?php echo $_SESSION['NewOrder']['user-first-last'] ?></div>
							<div><?php echo $_SESSION['NewOrder']['user-phone'] ?></div>
							<div><?php echo $_SESSION['NewOrder']['user-email'] ?></div>
						</div>	
						<div class="col-md-6">
							<div><h2>Shipping Option</h2></div>
							<div class="row">
								<div class="col-md-6">
									<?php if($_SESSION['NewOrder']['ship-option']['0'] == 0): ?>
										<strong>Picking it up at store</strong>
									<?php else: ?>
										<div class="row">
											<div class="col-md-4">
												<strong>Shipping to Store</strong>
											</div>
											<div class="col-md-8">
												<div><?php echo $_SESSION['NewOrder']['ship-address'] ?></div>
												<div><?php echo $_SESSION['NewOrder']['ship-city'] ?>, <?php echo $_SESSION['NewOrder']['ship-state'] ?> <?php echo $_SESSION['NewOrder']['ship-zip'] ?></div>
											</div>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12" style='margin-top:10px;'>
							<input type='submit' value="Submit" class='redButton checkoutButton' onclick="CartController.Processing()" />
						</div>
					</div>
				</form>
			</div>
			<div class="whiteBoxContent" style='margin-top: 15px;'>
					<?php foreach($_SESSION['CurrentCartObject']['results']['cartItems'] as $key => $cartItemSingle): ?>
					<?php 
						$productKey = array_search($cartItemSingle['product_id'], array_column($this -> productsSelected, 'productID'));
						if($cartItemSingle['colorSelected'] != 0) {
							$colorSelectedKey = array_search($cartItemSingle['colorSelected'], array_column($this -> colorsSelected, 'settingColorID'));	
						}
						
						if($cartItemSingle['sizeSelected'] != 0) {
							$sizeSelectedKey = array_search($cartItemSingle['sizeSelected'], array_column($this -> sizesSelected, 'settingSizeID'));	
						}
					?>
					
					<div class="row">
						<div class="col-md-12">
							<div class="CheckoutCartSingleItem">
								<div style='display: table-row;'>
									<div class="CheckoutProductInfo">
										<div class="name"><?php echo $this -> productsSelected[$productKey]['productName'] ?></div>
										<div>
											<span class='price'>$<?php echo number_format($cartItemSingle['price'], 2) ?></span> | <?php echo $cartItemSingle['quantity'] ?> Qty
											<?php if(isset($colorSelectedKey)): ?>
												| <?php echo $this -> colorsSelected[$colorSelectedKey]['settingColorText'] ?>
											<?php endif; ?>	
											<?php if(isset($sizeSelectedKey)): ?>
												| <?php echo $this -> sizesSelected[$sizeSelectedKey]['settingSizeText'] ?>
											<?php endif; ?>	
										</div>
									</div>
									<div class="itemTotalPrice">
										$<?php echo number_format($cartItemSingle['price'] * $cartItemSingle['quantity'], 2) ?>
									</div>	
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
				<div class="row">
					<div class="col-md-12">
						<table class='cartSummaryTotals'>
							<tr>
								<td>Cart Total:</td>
								<td class='Number' id='CartTotalNumber'>$<?php echo number_format($_SESSION['CurrentCartObject']['results']['total'], 2) ?></td>
							</tr>
							<tr>
								<td>Shipping:</td>
								<td class='Number'>
									$<?php echo $shipping ?>
								</td>
							</tr>
							<tr>
								<td>Taxes:</td>
								<td class='Number'>
									$<?php echo $taxes ?>
								</td>
							</tr>
							<tr>
								<td>Grand Total:</td>
								<td class='Number'>$<?php echo $grandTotal ?></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<!--<div class="whiteBoxContent">
			
			</div>-->
			
			
				
			
		</div>
	</div>
</div>



