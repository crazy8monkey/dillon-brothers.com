<div class="footerPush"></div>
<footer>
	<div class="container-fluid">
		<div class="col-md-4">
			<div class="copyRight">
				Copyright &copy; <?php echo date('Y') ?> Dillon Brothers. All Rights Reserved | <a href="<?php echo PATH ?>sitemap" style='text-decoration:underline; color:white;'>Sitemap</a>
			</div>		
		</div>
		<div class="col-md-4">
			<div class="socialLinks">
				<a href="https://www.facebook.com/DillonBrothersOmaha" target="_blank">
					<img src="<?php echo PATH ?>public/images/facebookIcon.png" />
				</a>
				
				<a href="https://twitter.com/dillonbrothers" target="_blank">
					<img src="<?php echo PATH ?>public/images/twitter.png" />
				</a>
				
				<a href="https://www.youtube.com/user/dillonbrothers?feature=mhee" target="_blank">
					<img src="<?php echo PATH ?>public/images/youtubeIcon.png" />
				</a>	
			</div>
			
		</div>
		<div class="col-md-4">
			<div class="footerRightContent">
				<a href="http://learntoride.dillon-brothers.com/" target="_blank">
					<img src="<?php echo PATH ?>public/images/LearnToRide.png" />	
				</a>
				
				<a href="http://www.partsfish.com/" target="_blank">
					<img src="<?php echo PATH ?>public/images/PartsFish.png" />	
				</a>
				<a href="http://www.siddillon.com/" target="_blank">
					<img src="<?php echo PATH ?>public/images/SidDillonLogo.png" />	
				</a>
					
			</div>		
		</div>
	</div>
</footer>


<div id="SalesChatButton">
	<a href='javascript:void(0);' onclick='Globals.OpenConversationWindow()'>
		<div class="circle">
			<img src='<?php echo PATH ?>public/images/OnlineChatIndicator.png' />	
		</div>	
	</a>
		
</div>

<a href="javascript:void(0);" onclick="Globals.ScrollToTop()">
	<div id="ScrollUpToPage" style='display:none;' data-toggle="tooltip" data-placement="top" title='Scroll to top'>
		<div class="arrowUp"></div>
	</div>	
</a>

<div id="LoadingSpinnerObject">
	<img src='<?php echo PATH ?>public/images/ajax-loader.gif' />
</div>

<div id="ecommerceLoadingObject">
	<div class="OverLay"></div>
	<div class="processingText">
		<div class='proccessingHeader'>Processing</div>
		<img src='<?php echo PATH ?>public/images/ajax-loader.gif' />	
	</div>
	
</div>


<script type="application/ld+json">
[{
	"@type": "SiteNavigationElement",
	"@context": "http://schema.org",
	"name" : "Learn to Ride At Dillon-Brothers",
	"url" : "http://learntoride.dillon-brothers.com/"	
},
{
	"@type": "SiteNavigationElement",
	"@context": "http://schema.org",
	"name" : "PartsFish",
	"url" : "http://www.partsfish.com/"	
},{
	"@type": "SiteNavigationElement",
	"@context": "http://schema.org",
	"name" : "Dillon Brothers Facebook",
	"url" : "https://www.facebook.com/DillonBrothersOmaha"	
},{
	"@type": "SiteNavigationElement",
	"@context": "http://schema.org",
	"name" : "Dillon Brothers Twitter",
	"url" : "https://twitter.com/dillonbrothers"	
},{
	"@type": "SiteNavigationElement",
	"@context": "http://schema.org",
	"name": "Dillon Brothers Youtube",
	"url" : "https://www.youtube.com/user/dillonbrothers?feature=mhee"	
}]				
</script>

</body>	
</html>