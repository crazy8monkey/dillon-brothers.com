<div class="container">
	<div class="row">
		<div class="col-md-12 ContentHeader">
			<h1>OUR SPECIALS</h1>
		</div>
	</div>
	<?php if(count($this -> currentSpecials) > 0) : ?>
	
		
		<?php foreach(array_chunk($this -> currentSpecials, 2, true) as $specialArray) : ?>
			
			<div class="row">
				<?php foreach($specialArray as $special) : ?>	
					<?php 
						$url = NULL;
						$cssClass = NULL;
						$categoryText = NULL;
						$structuredDataLocation = NULL;
						if($special['StoreNames'] != NULL) {
							switch(true) {
								case stripos($special['StoreNames'], "MotorSports"):
									$url = PATH . 'specials/motorsport/' . $special['specialSEOurl'];
									$cssClass = "redButton";
									break;
								case stripos($special['StoreNames'], "Harley"):
									$url = PATH . 'specials/harley/' . $special['specialSEOurl'];
									$cssClass = "HarleyContact";
									break;
								case stripos($special['StoreNames'], "Indian"):
									$url = PATH . 'specials/indian/' . $special['specialSEOurl'];
									$cssClass = "maroonButton";
									break;
							}
						} else {
							switch($special['specialType']) {
								case 2:
									$url = PATH . 'specials/apparelspecial/' . $special['specialSEOurl'];
									break;		
								case 3:
									$url = PATH . 'specials/partsspecial/' . $special['specialSEOurl'];
									break;		
								case 4:
									$url = PATH . 'specials/servicespecial/' . $special['specialSEOurl'];
									break;	
								case 5:
									$url = PATH . 'specials/storespecial/' . $special['specialSEOurl'];
									break;
							}
						}
						
						$storeIDS = explode(',', $special['StoreIDS']);
						foreach($storeIDS as $storeSingle) {
							if($storeSingle == 2) {
								$structuredDataLocation .= $this -> MotorSportSchemaLocation() . ',';
							}
							if($storeSingle == 3) {
								$structuredDataLocation .= $this -> HarleySchemaLocation() . ',';
							}
							
							if($storeSingle == 4) {
								$structuredDataLocation .= $this -> IndianSchemaLocation() . ',';
							}
							
						}
						
						switch($special['specialType']) {
							case '1':
								$categoryText = $special['BrandName'] . ' Manufacturer Incentive';
								$imgCategory = PATH .'public/images/VehicleSpecialCategory.png';	
								break;
							case '2':
								$imgCategory = PATH .'public/images/ClothingSpecialCategory.png';
								if($special['PartsApparelID'] != 0) {
									$categoryText = $special['BrandName'] . ' Apparel Incentive';
								} else {
									$categoryText = 'Apparel Incentive';
								}
								break;
							case '3':
								$imgCategory = PATH .'public/images/PartsSpecialCategory.png';
								if($special['PartsApparelID'] != 0) {
									$categoryText = $special['brandName'] . " Parts Incentive";
								} else {
									$categoryText = "Parts Incentive";
								}
								break;
							case '4':
								$categoryText = "Service Incentive";
								$imgCategory = PATH .'public/images/ServiceSpecialCategory.png';
								break;
							case '5':
								$categoryText = "Store Incentive";
								$imgCategory = PATH .'public/images/StoreSpecialCategory.png';
								break;
						}
						
						
						$specialMainImage = PHOTO_URL . 'specials/'. $special['FolderName'] . '/' . $special['SpecialMainImage'];
						
					?>
					<div class="col-md-6">
						<div class='specialSingleElement'>
							<?php 
								switch(count($storeIDS)) {
									case 1:
										$cssBar = "FullBar";
										break;
									case 2:
										$cssBar = "HalfBar";
										break;
									case 3:
										$cssBar = "ThirdBar";
										break;
								}
							?>
							
							<div class="specialCategoryElement">
								<img src='<?php echo $imgCategory ?>' />
								<div class='text'><?php echo $categoryText ?></div>
							</div>
							<div class="StoreBar">
								<?php foreach($storeIDS as $storeSingle) : ?>
									<?php if($storeSingle == 2): ?>
										<div class="<?php echo $cssBar ?> MotorSportIndicator" data-placement="bottom" data-toggle="tooltip" title="Dillon Brothers MotorSports"></div>
									<?php endif; ?>
									<?php if($storeSingle == 3): ?>
										<div class="<?php echo $cssBar ?> HarleyIndicator" data-placement="bottom" data-toggle="tooltip" title="Dillon Brothers Harley-Davidson"></div>
									<?php endif; ?>
									<?php if($storeSingle == 4): ?>
										<div class="<?php echo $cssBar ?> IndianIndicator" data-placement="bottom" data-toggle="tooltip" title="Dillon Brothers Indian"></div>
									<?php endif; ?>
								<?php endforeach; ?>
								<div style="clear:both"></div>
							</div>
							<div class="specialsImageContainer">
								<div class="ImageElement" style='background:url(<?php echo $specialMainImage ?>) no-repeat center;'>
									<img src="<?php echo $specialMainImage ?>" alt='<?php echo $special['SpecialTitle'] ?>' title='<?php echo $special['SpecialTitle'] ?>' style='width:100%' />		
								</div>
								<div class="blackBackground">
									<div class="specialName">
										<?php echo $special['SpecialTitle'] ?>
									</div>
									<div class="expDate">
										Exp. <?php echo $this -> recordedTime -> formatDate($special['SpecialEndDate']) ?>
									</div>
								</div>
							</div>
							<div class="specialItemButtonContainer">
								<a href="<?php echo $url ?>">
									<div class="blueButton" style='float:left'>
										More Info.
									</div>
								</a>
								<?php if($special['StoreEmail'] != NULL): ?>
									<a href="mailto:<?php echo $special['StoreEmail'] ?>">
										<div class="ContactButton <?php echo $cssClass ?>">
											Contact Sales
										</div>	
									</a>
								<?php endif; ?>
								<div style='clear:both'></div>
							</div>
							
							<div class='row'>
								<div class='col-md-12'>
																				
								</div>

							</div>
							
						</div>
					</div>
					<script type="application/ld+json">
						{
						  "@context" : "http://schema.org",
						  "@type" : "saleEvent",
						  "name" : "<?php echo $specialTitle ?> <?php echo $this -> recordedTime -> formatDate($special['SpecialStartDate']) ?> through <?php echo $this -> recordedTime -> formatDate($special['SpecialEndDate']) ?>",
						  "description" : "<?php echo $special['SpecialDescription'] ?>",
  						  "url" : "<?php echo $url; ?>",
  						  "image" : "<?php echo $specialMainImage ?>",
						  "eventStatus" : "NA",
						  "startDate": "<?php echo $special['specialPublishedDate'] ?>",
						  "endDate": "<?php echo $this -> recordedTime -> formatDate($special['SpecialEndDate']) ?>T17:00:00",
						  "location" : [<?php echo rtrim($structuredDataLocation, ',') ?>],
						  "performer": "NA",
						  "offers" : [
					  	  		{
					  			"@type" : "AggregateOffer",
							    "availability": "http://schema.org/InStock",
							    "availabilityStarts": "<?php echo $special['specialPublishedDate'] ?>",
							    "priceCurrency": "USD",
							    "url" : "<?php echo $url; ?>",
							    "inventoryLevel": "Demand",
							    "price": "00",
							    "validFrom": "<?php echo $special['SpecialStartDate'] ?>T9:00:00",
							    "validThrough": "<?php echo $special['SpecialEndDate'] ?>T17:00:00",
							    "potentialAction": {
							    	"@type" : "Action",
							        "name": "<?php echo $specialTitle ?>",
							        "description": "<?php echo $special['SpecialDescription'] ?>"
							        
							    }
							   }
		    		 		]
						}
					</script>
					
					<?php if($special['specialType'] == 1): ?>
					  <script type="application/ld+json">
							{
								"@context": "http://schema.org",
								"@type": "Brand",
								"name" : "<?php echo $special['BrandName'] ?>",
								"description" : "<?php echo $special['BrandDescription'] ?>",
								"url": "<?php echo $special['OfferPageRef'] ?>",
								"logo": "<?php echo PHOTO_URL . 'brands/'  . $special['BrandImage'] ?>"
							}
					</script>
					<?php endif; ?>
					
					<?php if($special['specialType'] == 3): ?>
					  <script type="application/ld+json">
							{
								"@context": "http://schema.org",
								"@type": "Brand",
								"name" : "<?php echo $special['brandName'] ?>",
								<?php if(!empty($special['partDescription'])): ?>
								"description" : "<?php echo $special['partDescription'] ?>",
								<?php endif; ?>
								"logo": "<?php echo PHOTO_URL . 'PartsBrand/'  . $special['BrandImage'] ?>"
							}
						</script>
					<?php endif; ?>
					<?php if($special['specialType'] == 2): ?>
					  <script type="application/ld+json">
							{
								"@context": "http://schema.org",
								"@type": "Brand",
								"name" : "<?php echo $special['brandName'] ?>",
								<?php if(!empty($special['apparelDescription'])): ?>
								"description" : "<?php echo $special['apparelDescription'] ?>",
								<?php endif; ?>
								"logo": "<?php echo PHOTO_URL . 'PartsBrand/'  . $special['BrandImage'] ?>"
							}
						</script>
					<?php endif; ?>
					
				<?php endforeach; ?>
			</div>
				
		<?php endforeach; ?>	
		
	<?php else: ?>
		<div class="row">
			<div class="col-md-12">
				There are no new current specials at this time
			</div>
		</div>
	<?php endif; ?>	
	
	
</div>


