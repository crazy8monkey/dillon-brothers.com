<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html xml:lang="en"> <!--<![endif]-->
<head>
	<title><?php echo $this -> title ?></title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
	
	<?php require 'view/MetaDataContent.php'; ?>
		
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />	
	<link rel="stylesheet" href="<?php echo PATH ?>public/css/style.css" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
	
	<link rel="stylesheet" href="<?php echo PATH ?>public/css/InventoryController.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo PATH ?>public/css/SpecialsController.css" type="text/css" />
		
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>	
	<script src="<?php echo PATH ?>public/js/Globals.js" type="text/javascript" type="text/javascript"></script>
	
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="<?php echo PATH ?>public/js/SpecialsController.js"></script>
		
	<script src="https://use.typekit.net/kpv4owm.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
		
		

</head>
<body>

<div id="SpecialSingleInventoryView">
	<div class="container" style="margin-bottom: 50px;">
		
		<div class="row">
			<div class="col-md-12 inventorySingleHeader">
				<h1><?php echo $this -> inventorySingle -> GetCondition();  ?> <?php echo $this -> inventorySingle -> GetBikeName() ?></h1>
				
			</div>
		</div>
		<div class="row">
			<div class="col-xs-8">
				<div class="my-gallery" <?php if(count($this -> inventoryPhotos) > 0): ?>style='display: inline-block; width: 100%;' <?php endif; ?>	>
					
				
				<?php if(count($this -> inventoryPhotos) > 0): ?>
					<?php 
						$mainImage = PHOTO_URL . 'inventory/' . $this -> inventorySingle -> VIN . '/' . $this -> inventoryPhotos['0']['inventoryPhotoName'] . '-l.' . $this -> inventoryPhotos['0']['inventoryPhotoExt']; 
						list($imageWidth, $imageHeight) = getimagesize($mainImage);
					?>
					<div class="inventoryPhotoMain" style='min-height: 300px;'>
						<?php if($this -> inventorySingle -> MarkAsSoldDate != NULL): ?>
							<div class="SoldOutText">SOLD</div>
						<?php endif; ?>
						<img style='position:relative; z-index:1;' src="<?php echo $mainImage ?>" alt="<?php echo $this -> inventoryPhotos['0']['inventoryAltTag']; ?>" title="<?php echo $this -> inventoryPhotos['0']['inventoryTitleTag']; ?>" />
						<div class="DillonBrothersLogo"></div>
					</div>
					
				<?php else: ?>
					<div class="inventoryPhotoMain">
						<div class="DillonBrothersLogo"></div>
					</div>
				<?php endif; ?>						
				</div>	
			</div>
			<div class="col-xs-4">
				<div class="priceContainer">
					<div><h2 class="BikeSpecInfo" style='margin: 0px;'>Selling Price</h2></div>
					<div class="SellingPrice"><span class="price">$<?php echo number_format($this -> inventorySingle -> MSRP, 0);  ?></span></div>
				</div>
				<div style='margin-bottom:10px;'>
					<table class="briefDescription">
						<?php if(!empty($this -> inventorySingle -> FriendlyModelName)): ?>
							<tr>
								<td class='MainLabel'>Model Number:</td>
								<td><?php echo $this -> inventorySingle -> ModelName; ?></td>
							</tr>
						<?php endif; ?>
						<tr>
							<td class='MainLabel'>Category:</td>
							<td><?php echo $this -> inventorySingle -> Category; ?></td>
						</tr>
						<tr>
							<td class='MainLabel'>VIN:</td>
							<td><?php echo $this -> inventorySingle -> VIN; ?></td>
						</tr>
						<tr>
							<td class='MainLabel'>Mileage:</td>
							<td><?php echo number_format($this -> inventorySingle -> Mileage, 0); ?></td>
						</tr>
						<tr>
							<td class='MainLabel'>Stock Number:</td>
							<td><?php echo $this -> inventorySingle -> stockNumber; ?></td>
						</tr>
						<tr>
							<td class='MainLabel'>Color:</td>
							<td><?php echo $this -> inventorySingle -> Color; ?></td>
						</tr>
						<tr>
							<td class='MainLabel'>Location:</td>
							<td><?php echo $this -> inventorySingle -> GetStoreAddress(); ?></td>
						</tr>
					</table>
				</div>
				<div style='margin-bottom:10px;'>
					<a href="javascript:void(0);" onclick="SpecialsController.GoToInventory('<?php echo $this -> inventorySingle -> GetURL() ?>');">
						<div class="ViewInventoryButton">View Vehicle</div>
					</a>
					
					
					<div style="clear:both"></div>
				</div>
			</div>
		</div>
		
	
		
	</div>
	
</div>





</body>	
</html>
