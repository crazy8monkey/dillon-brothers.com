<div class='SpecialMainImage'>
	<div class="ImageContainer">
		<img src="<?php echo PHOTO_URL ?>specials/<?php echo $this -> specialSingle -> folderName?>/<?php echo $this -> specialSingle -> SpecialImage?>" alt="<?php echo $this -> specialSingle -> SpecialTitle ?>" title="<?php echo $this -> specialSingle -> SpecialTitle ?>" />
		<div class="shadow"></div>
	</div>	
	
</div>	

<div class="container">
	<div class="row">
		<div class="col-md-12 BreadCrumbContainer">
			<a href="<?php echo PATH ?>specials">Dillon Brothers Specials</a><span style="margin:0px 5px;"><i class="fa fa-caret-right" aria-hidden="true"></i></span><?php echo $this -> specialSingle -> SpecialTitle ?>		
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="ContentHeader" style='margin-bottom: 0px;'>
				<h1 style='margin-bottom: 0px;'><?php echo $this -> specialSingle -> GetSpecialTitle() ?></h1>
			</div>
		</div>	
	</div>
	<!--<div class="row">
		<div class="col-md-12">
			<strong>Expires / <?php echo $this -> recordedTime -> formatDate($this -> specialSingle -> endDate) ?></strong>
		</div>
	</div> -->
	<div class="row">
		<div class="col-md-12">
			<div style='padding: 10px 0px; margin-top:10px;'>
				<?php echo $this -> specialSingle -> Description ?>	
			</div>
		</div>	
	</div>
</div>
<?php 
	function filterInventoryByIncentive(){
	    return ($inventoryRelatedIncentive == $currentIncentiveID);
	}

	function FilterRelatedBike() {
		
	}

?>

<?php  $prevCategory = null; ?>
<div class="container">
<?php foreach(array_chunk($this -> incentives['incentives'], 2, true) as $incentiveDuece) : ?>
	<div class="row">
	<?php foreach($incentiveDuece as $incentiveSingle): ?>
		<div class="col-md-6">
		<div class="incentivesContainer">		
			
				<?php 
					if($incentiveSingle['RelatedItemSpecialType'] == "Dollar Off") {
						$valueOff = "$" . $incentiveSingle['Value']. " Off";
					} else if($incentiveSingle['RelatedItemSpecialType'] == "Percentage Off") {
						$valueOff = $incentiveSingle['Value']. "% Off";
					} else if($incentiveSingle['RelatedItemSpecialType'] == 'Special Financing') {
						$valueOff = $incentiveSingle['Value'] . '% APR';
					}
				?>
				<div class="row">
					<div class="col-md-12" style='margin-bottom:5px'>
					
						<div style='float:left;'>
							<h2 class='IncentiveHeader'><?php echo $valueOff; ?></h2>	
						</div>
						
						<div class='expiredDate'>
							Expires: <?php echo $this -> recordedTime -> formatDate($incentiveSingle['ExpiredDate']) ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" style='margin-top:10px;'>
						<?php echo $incentiveSingle['Description'] ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" style='margin-top:10px; font-size: 10px; margin-bottom:15px;'>
						<div><strong>Disclaimer</strong></div>
						<?php echo $incentiveSingle['Disclaimer'] ?>
					</div>
				</div>
				<?php if($this -> specialSingle -> RelatedInventoryType == 1): ?>		
					<div class="specialInventoryList">
					<?php 
						
						$filteredInventory = array();
						
						
						foreach($this -> incentives['inventory'] as $relatedInventory) {
							if($relatedInventory['RelatedIncentiveID'] == $incentiveSingle['RelatedSpecialItemID']) {
								if($relatedInventory['FriendlyModelName'] != NULL) {
									$bikeName = $relatedInventory['Year'] . ' ' . $relatedInventory['Manufacturer']  . ' ' . $relatedInventory['FriendlyModelName'];		
								} else {
									$bikeName = $relatedInventory['Year'] . ' ' . $relatedInventory['Manufacturer']  . ' ' . $relatedInventory['ModelName'];
								}
								
								if($relatedInventory['Conditions'] == 0) {
									$url = PATH . 'inventory/new/' . $relatedInventory['inventorySeoURL'];
								} else {
									$url = PATH . 'inventory/used/' . $relatedInventory['inventorySeoURL'];
								}
								
								
								
								array_push($filteredInventory, array('BikeName' => $bikeName,
																	 'VinNumber' => $relatedInventory['VinNumber'],
																	 'PhotoName' => $relatedInventory['inventoryPhotoName'] . '-s.' . $relatedInventory['inventoryPhotoExt'],
																	 'Stock' => $relatedInventory['Stock'],
																	 'InventoryLink' => $url));	
							}
							
						}
						
						$UniqueVehicles = array_column($filteredInventory, 'BikeName');
						$UniqueVehicles = array_unique($UniqueVehicles);
					
					?>
					<?php foreach($UniqueVehicles as $inventory): ?>
						<div class="row" style='margin: 0px;'>
							<a href="javascript:void(0)" onclick="SpecialsController.OpenIncentive(this)">				
								<div class="col-md-12" style='position:relative; margin:3px 0px; padding: 0px;'>
									<div class="toggleButton">
										+
									</div>
									<div class="inventoryName"><strong><?php echo $inventory ?></strong></div>
									<div class='inventoryDivider'></div>
									<div style="clear:both"></div>
								</div>
							</a>
						</div>
						<?php 
							$BikeName = $inventory;
										
							$filteredBikes = array_filter($filteredInventory, function ($var) use($BikeName) {
								return $var['BikeName'] == $BikeName; 
							});
						
						?>
						<div class="incentiveContent">
							<?php foreach(array_chunk($filteredBikes, 2, true) as $bikeDuece) : ?>
								<div class="row">
									<?php foreach($bikeDuece as $bikeSingle): ?>
										<div class="col-md-6">
											<div style='margin-bottom:20px;'>
												<div class="row" style='margin: 0px;'>
													<div class="col-md-12" style='padding-left: 0px; padding-right: 0px;'>
														<img src='<?php echo PHOTO_URL ?>inventory/<?php echo $bikeSingle['VinNumber'] ?>/<?php echo $bikeSingle['PhotoName'] ?>' width='100%' alt='<?php echo $bikeSingle['BikeName'] ?>' title='<?php echo $bikeSingle['BikeName'] ?>' />
													</div>
												</div>
												<div class="row" style='margin: 0px;'>
													<div class="col-md-12 inventoryInfo" style='padding-left: 0px; padding-right: 0px;'>
														<table>
															<tr>
																<td>Stock #:</td>
																<td><?php echo $bikeSingle['Stock'] ?></td>
															</tr>
															<tr>
																<td>VIN:</td>
																<td><?php echo $bikeSingle['VinNumber'] ?></td>
															</tr>
														</table>
														<a href="<?php echo $bikeSingle['InventoryLink'] ?>">
															<div class="viewInventory">
																View Vehicle
															</div>	
														</a>
													</div>
												</div>	
											</div>
											
										</div>
									<?php endforeach; ?>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endforeach; ?>
					</div>
				<?php endif; ?>
				</div>	
		</div>
	<?php endforeach; ?>
	</div>
<?php endforeach; ?>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12" style='margin: 15px 0px;'>
			<?php echo $this -> specialSingle -> SpecialDescriptionFooter; ?>
		</div>
	</div>
	<?php //print_r($this -> specialSingle -> relatedStores); ?>
	<?php foreach(array_chunk($this -> specialSingle -> relatedStores, 3, true) as $storeArray): ?>
		<div class="row" style="padding-bottom:40px;">
			<div style='border-top:1px solid #cecece; margin:0px 15px;'></div>
			<?php foreach($storeArray as $storeSingle) : ?>	
				<div class="col-md-4">
					<div class="storeImage">
						<img src='<?php echo PHOTO_URL ?>Stores/<?php echo $storeSingle['storeImage'] ?>' />
					</div>
					<div class="contactInfo">
						<?php 
							$storeAddresses = explode(" / ", $storeSingle['StoreAddress']); 
							$storePhones = explode(" / ", $storeSingle['storePhone']);
							
							$StoreInformation = NULL;
							if(count($storeSingle['StoreAddress']) > 0) {
								foreach($storeAddresses as $key => $storeSingleAddress) {
									$StoreInformation .= $storeSingleAddress . "<br />" . $storePhones[$key] . "<br />" . $storeSingle['StoreEmail'] . '<br /><br />';	
								}	
							} else {
								$StoreInformation = $storeSingle['StoreAddress'] . "<br />" . $storeSingle['storePhone']. "<br /><br />"; 	
							}
							
						?>
						
						<?php 
						$removeB = str_replace("B:", "", $StoreInformation); 
						echo str_replace("A:", "", $removeB);
						?>
						
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endforeach; ?>
</div>


<script type="application/ld+json">
[{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "name": "Dillon Brothers Specials"
    }
  },{
    "@type": "ListItem",
    "position": 2,
    "item": {
      "name": "<?php echo $this -> specialSingle -> SpecialTitle ?>"
    }
  }]
},
{
  "@context" : "http://schema.org",
  "@type" : "saleEvent",
  "name" : "<?php echo $this -> specialSingle -> GetSpecialTitle() ?> <?php echo $this -> recordedTime -> formatDate($this -> specialSingle -> startDate) ?> through <?php echo $this -> recordedTime -> formatDate($this -> specialSingle -> endDate) ?>",
  "description" : "Current Special Going on at Dillon Brothers",
  "url" : "<?php echo $this -> specialSingle -> GetURL(); ?>",
  "image" : "<?php echo PHOTO_URL ?>specials/<?php echo $this -> specialSingle -> folderName?>/<?php echo $this -> specialSingle -> SpecialImage?>",
  "eventStatus" : "NA",
  "startDate": "<?php echo $this -> specialSingle -> SpecialPublishedDate ?>",
  "endDate": "<?php echo $this -> recordedTime -> formatDate($this -> specialSingle -> endDate) ?>T17:00:00",
  <?php echo $this -> specialSingle -> StructuredDataLocation() ?>,
  "offers" : [
  	<?php $count = count($this -> incentives['incentives']) ?>
  	<?php foreach($this -> incentives['incentives'] as $key => $incentiveSingle): ?>
  		{
  			"@type" : "AggregateOffer",
		    "availability": "http://schema.org/InStock",
		    "availabilityStarts": "<?php echo $this -> specialSingle -> SpecialPublishedDate ?>",
		    "priceCurrency": "USD",
		    "url" : "<?php echo $this -> specialSingle -> GetURL(); ?>",
		    "inventoryLevel": "Demand",
		    "price": "00",
		    "validThrough": "<?php echo $this -> recordedTime -> formatDate($incentiveSingle['ExpiredDate']) ?>T17:00:00",
		    "potentialAction": {
		    	"@type" : "Action",
		        "name": "Incentives: <?php echo $valueOff ?>",
		        "description": "<?php echo $incentiveSingle['Description'] ?>"
		        
		    }
		    <?php if($this -> specialSingle -> RelatedInventoryType == 1): ?>
		 	,"isRelatedTo":[
		 	
		 		<?php 
		 			$filteredInventory = array();
					
					foreach($this -> incentives['inventory'] as $relatedInventory) {
						if($relatedInventory['RelatedIncentiveID'] == $incentiveSingle['RelatedSpecialItemID']) {
							if($relatedInventory['Conditions'] == 0) {
								$condition = "New";
							} else {
								$condition = "Used";
							}
							
							if($relatedInventory['FriendlyModelName'] != NULL) {
								$bikeName = $relatedInventory['Year'] . ' ' . $relatedInventory['Manufacturer']  . ' ' . $relatedInventory['FriendlyModelName'];		
							} else {
								$bikeName = $relatedInventory['Year'] . ' ' . $relatedInventory['Manufacturer']  . ' ' . $relatedInventory['ModelName'];
							}
							
							if($relatedInventory['Conditions'] == 0) {
								$url = PATH . 'inventory/new/' . $relatedInventory['inventorySeoURL'];
							} else {
								$url = PATH . 'inventory/used/' . $relatedInventory['inventorySeoURL'];
							}
							
							
							array_push($filteredInventory, array('BikeName' => $condition . ' ' . $bikeName,
																 'VinNumber' => $relatedInventory['VinNumber'],
																 'PhotoName' => $relatedInventory['inventoryPhotoName'] . '-s.' . $relatedInventory['inventoryPhotoExt'],
																 'Stock' => $relatedInventory['Stock'],
																 'InventoryLink' => $url,
																 'ModelName' => $relatedInventory['ModelName'],
																 'Manufacturer' => $relatedInventory['Manufacturer'],
																 'Year' => $relatedInventory['Year'],
																 'Mileage' => $relatedInventory['Mileage'],
																 'Conditions' => $relatedInventory['Conditions'],
																 'MSRP' => $relatedInventory['MSRP']));	
						}
						
					}
		 		?>
		 		<?php $inventoryCount = count($filteredInventory); ?>
		 		<?php foreach($filteredInventory as $inventorykey => $related):?>
		 			{
			 			"@context": "http://schema.org",
						"@type": "Motorcycle",
						"model": "<?php echo $related['ModelName'] ?>",
						"modelDate": "<?php echo $related['Year'] ?>",
						"name": "<?php echo $related['BikeName'] ?>",
						"productID": "<?php echo $related['VinNumber'] ?>",
						"brand": {
						  	"@type": "Thing",
						    "name": "<?php echo $related['Manufacturer']; ?>"
						},
						"mileageFromOdometer": "<?php echo $related['Mileage'] ?>",
			  			"sku": "<?php echo $related['Stock'] ?>"
					  
					  
					}<?php if($inventorykey != $inventoryCount - 1): ?>,<?php endif; ?>
		 		<?php endforeach; ?>
		 	]
 		<?php endif; ?>
  		}<?php if($key != $count - 1): ?>,<?php endif; ?>
  	<?php endforeach; ?>
  ]
  
}

]

</script>

