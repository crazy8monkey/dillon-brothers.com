<div class="ErrorPageContent" style="padding-top:95px;">
	<div class="TopPageContent">
		<div style='margin-bottom:10px;'>
			<h1>404 <strong>PAGE NOT FOUND</strong></h1>	
		</div>
		<div>
			<img class='DillonBrotherLogo' src='<?php echo PATH ?>public/images/NoPhotoPlacementDimmed.png' />	
		</div>
		Oops! We couldn’t find the page you were looking for.<br />
		Please try the links below.
	</div>
	<div class="StoreLocations">
		<div class="store">
			<div class='NameElement'>
				Dillon Brothers			
				<div class='Name'>
					MotorSports
				</div>
			</div>
			<div class="buttons">	
				<a href="<?php echo $this -> MotorSportInventory('New') ?>"><div class="buttonSingle redButton">Search New Inventory</div></a>
				<a href="<?php echo $this -> MotorSportInventory('Used') ?>"><div class="buttonSingle redButton">Search Used Inventory</div></a>
				<div style="clear:both"></div>
			</div>
			<div class="link">
				<a href="http://www.dilloncycles.com/" target="_blank">http://www.dilloncycles.com/</a>
			</div>
			<div class="storeDetails MotorSportDetails">
				<strong>3848 N HWS Cleveland Blvd.<br />
				Omaha, NE 68116</strong><br /><br />
				<table>
					<tr>
						<td class='phoneLabel'>Phone:</td>
						<td>(402) 556-3333</td>
					</tr>
					<tr>
						<td class='phoneLabel'>Fax:</td>
						<td>(402) 556-1796</td>
					</tr>
				</table><br />
			</div>
		</div>
		<div class="store">
			<div class='NameElement'>
				Dillon Brothers
				<div class='Name'>
					Harley-Davidson
				</div>
			</div>
			<div class="buttons">
				<a href='<?php echo $this -> HarleyInventory('New') ?>' target='_blank'><div class="buttonSingle Harley">Search New Inventory</div></a>
				<a href="<?php echo $this -> HarleyInventory('Used') ?>"><div class="buttonSingle Harley">Search Used Inventory</div></a>
				<div style="clear:both"></div>
			</div>
			<div class="link">
				<a href="http://www.dillonharley.com/" target="_blank">http://www.dillonharley.com/</a>
			</div>
			<div class="storeDetails HarleyDetails">
				<div class="StoreLocationHalf">
					<strong>3838 N HWS Cleveland Blvd<br />
					Omaha, NE 68116</strong><br /><br />
					<table>
						<tr>
							<td class='phoneLabel'>Phone:</td>
							<td>(402) 289-5556</td>
						</tr>
						<tr>
							<td class='phoneLabel'>Fax:</td>
							<td>(402) 289-1931</td>
						</tr>
					</table><br />
				</div>
				<div class="StoreLocationHalf">
					<strong>2440 East 23rd Street<br />
					Fremont, NE 68025</strong><br /><br />
					<table>
						<tr>
							<td class='phoneLabel'>Phone:</td>
							<td>(402) 721-2007</td>
						</tr>
						<tr>
							<td class='phoneLabel'>Fax:</td>
							<td>(402) 721-2441</td>
						</tr>
					</table>
				</div>
				<div style="clear:both"></div>
			</div>
		</div>
		<div class="store">
			<div class='NameElement'>
				Dillon Brothers
				<div class='Name'>
					Indian Motorcycle
				</div>
			</div>
			<div class="buttons">
				<a href="<?php echo PATH ?>inventory/page/1#FilerApplied&Conditions=New&Stores=Dillon Brothers Indian&View=2By2"><div class="buttonSingle Indian">Search New Inventory</div></a>
				<a href="<?php echo PATH ?>inventory/page/1#FilerApplied&Conditions=Used&Stores=Dillon Brothers Indian&View=2By2"><div class="buttonSingle Indian">Search Used Inventory</div></a>
				<div style="clear:both"></div>
			</div>
			<div class="link">
				<a href="http://www.dillonbrothersindian.com/" target="_blank">http://www.dillonbrothersindian.com/</a>
			</div>
			<div class="storeDetails IndianDetails">
				<strong>3840 N 174th Ave<br />
				Omaha, NE 68116</strong><br /><br />
				<table>
					<tr>
						<td class='phoneLabel'>Phone:</td>
						<td>(402) 505-4233</td>
					</tr>
				</table>
			</div>
		</div>
		<div style='clear:both'></div>
	</div>
</div>
