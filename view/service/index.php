<div id="TopServicePage">
	<div class="HalfSection EntryServiceDeptText">
		<div class="ContentHeader">
			<h1>WELCOME TO OUR SERVICE DEPARTMENTS</h1>
		</div>
		<div>
			Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. 
			Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur 
			ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla 
			consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In 
			enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis 
			pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate 
			eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem 
			ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. 
			Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper 
			ultricies nisi. 
		</div>
	</div>
	<div class="HalfSection">
		<div class="ServiceDeptPicture"></div>
	</div>
	<div style='clear:both'></div>
</div>


<div class="grayBackground" style='padding: 15px 0px; margin-bottom:20px;'>
	<div class="container">
		<div class="row">
			<div class="col-md-12" style='font-size:20px; text-align:center'>
				Visit one of our Service Departments.
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="ServiceButtonContainer">
					<a href="http://www.dilloncycles.com/have-your-motorcycle-atv-or-utv-repair-with-the-best--service" target="_blank">
						<div class="buttonSingle HarleyButton">
							Harley-Davidson Service Dept.
						</div>		
					</a>
					<a href="http://www.dillonharley.com/repair-your-harley-motorcycle-with-us--service" target="_blank">
						<div class="buttonSingle redButton">
							Motorsport Service Dept.
						</div>
					</a>
					<a href="http://www.dillonbrothersindian.com/repair-your-indian-motorcycle-with-certified-technicians--service" target="_blank">
						<div class="buttonSingle IndianButton">
							Indian Motorcycle Service Dept.
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<form method='post' action="<?php echo PATH ?>ajax/servicedeptform" id="ServiceDeptForm">
		<div class="row">
			<div class="col-md-12" style='margin-bottom:20px;'>
				<h2>Contact our Service Department</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="inputID1">
					<div class="errorMessage"></div>
				</div>
			</div>
		</div>
		<div class="row" style='margin-bottom:10px;'>
			
			<div class="col-md-4">
				<div class='radioButton'><input type='radio' name='serviceStore[]' value='3:Omaha' onchange="Globals.removeValidationMessage(1)" /> Harley-Davidson Omaha</div>
				<div class='radioButton'><input type='radio' name='serviceStore[]' value='3:Fremont' onchange="Globals.removeValidationMessage(1)" /> Harley-Davidson Fremont</div>
			</div>
			<div class="col-md-4">
				<div class='radioButton'><input type='radio' name='serviceStore[]' value='2:Omaha' onchange="Globals.removeValidationMessage(1)" /> Motorsports</div>
				<div class='radioButton'><input type='radio' name='serviceStore[]' value='4:Omaha' onchange="Globals.removeValidationMessage(1)" /> Indian Motorcycle</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="formHeader">
					<div class="text">
						Contact Information 	
					</div>
					<div class="divider"></div>
					<div style='clear:both'></div>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-6">
						<div id="inputID2">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">First Name <span class='requiredIndicator'>*</span></div>
								<input type="text" name="serviceFirstName" onchange="Globals.removeValidationMessage(2)" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div id="inputID3">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">Last Name <span class='requiredIndicator'>*</span></div>
								<input type="text" name="serviceLastName" onchange="Globals.removeValidationMessage(3)" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="inputLine">
							<div class="inputLabel">Home Phone <span class='requiredIndicator'>*</span></div>
							<input type="text" name="serviceHomePhone" class="serviceHomePhone" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="inputLine">
							<div class="inputLabel">Cell Phone</div>
							<input type="text" name="serviceCellPhone" class="serviceCellPhone" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div id="inputID12">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">Email Address</div>
								<input type="text" name="serviceEmailAddress" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div class="inputLine">
							<div class="inputLabel">Street Address</div>
							<input type="text" name="serviceStreetAddress" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8">
						<div class="inputLine">
							<div class="inputLabel">City</div>
							<input type="text" name="serviceCity" />
						</div>
					</div>
					<div class="col-md-2">
						<div class="inputLine">
							<div class="inputLabel">State</div>
							<select name="serviceState">
								<option value=''></option>
								<?php foreach($this -> states() as $key => $stateSingle): ?>
									<option value='<?php echo $key ?>'><?php echo $key ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="inputLine">
							<div class="inputLabel">Zip</div>
							<input type="text" name="serviceZip" maxlength="5" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="formHeader">
					<div class="text">
						Vehicle Information
					</div>
					<div class="divider"></div>
					<div style='clear:both'></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-4">
						<div id="inputID4">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">Vehicle Year <span class='requiredIndicator'>*</span></div>
								<input type="text" name="serviceVehicleYear" onchange="Globals.removeValidationMessage(4)" />
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div id="inputID5">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">Vehicle Make <span class='requiredIndicator'>*</span></div>
								<input type="text" name="serviceVehicleMake" onchange="Globals.removeValidationMessage(5)" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div id="inputID6">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">Vehicle Model <span class='requiredIndicator'>*</span></div>
								<input type="text" name="serviceVehicleModel" onchange="Globals.removeValidationMessage(6)" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div class="inputLine">
							<div class="inputLabel">Miles</div>
							<input type="text" name="serviceVehicleMiles" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="inputLine">
							<div class="inputLabel">Vin Number</div>
							<input type="text" name="serviceVehicleVinNumber" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="formHeader">
					<div class="text">
						Service Details/History
					</div>
					<div class="divider"></div>
					<div style='clear:both'></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="inputID7">
					<div class="errorMessage"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="inputLine">
					<div class="inputLabel">Have we serviced your vehicle in the past? <span class='requiredIndicator'>*</span></div>
					<div class='radioButton'><input type='radio' name="servicePastServiceOption[]" value='1' onchange="Globals.removeValidationMessage(7); ServiceController.ShowServiceDetailInfo(this)" />Yes</div>
					<div class='radioButton'><input type='radio' name="servicePastServiceOption[]" value='0' onchange="Globals.removeValidationMessage(7); ServiceController.ShowServiceDetailInfo(this)" />No</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6" id="PastServiceInfo" style='display:none;'>
				<div class="row">
					<div class="col-md-12">
						<div id="inputID8">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">Last Service Date <span class='requiredIndicator'>*</span></div>
								<input type="text" name="servicePreviousDate" class='servicePreviousDate' onchange="Globals.removeValidationMessage(8);" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div id="inputID9">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">Work Done <span class='requiredIndicator'>*</span></div>
								<textarea name="servicePreviousWorkDone" onchange="Globals.removeValidationMessage(9)"></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div id="inputID10">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">Your Appointment Date <span class='requiredIndicator'>*</span></div>
								<input type="text" name="serviceNewServiceDate" class="serviceNewServiceDate" onchange="Globals.removeValidationMessage(10)" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div id="inputID11">
							<div class="errorMessage"></div>						
							<div class="inputLine">
								<div class="inputLabel">What needs to be done? <span class='requiredIndicator'>*</span></div>
								<textarea name="serviceNewWorkDone" onchange="Globals.removeValidationMessage(11)"></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div id="inputID13">
					<div class="errorMessage"></div>					
					<div class="inputLine">
						<div class="inputLabel">Are You Human?: <span class='requiredIndicator'>*</span></div>
						<img src="<?php echo PATH ?>view/captcha.php" />
						<input type="text" name="capta" style='margin-top:10px' />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<input type='submit' value='Send Request' class="blueButton" />
			</div>
		</div>
	</form>
</div>
<div class="grayBackground" style='padding: 15px 0px; margin:20px 0px 0px 0px;'>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				Dillon Brothers is committed to your privacy.
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style='margin-top:10px;'>
				<a href='<?php echo PATH ?>privacy'>View our Privacy Statement</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style='font-size:12px; margin-top:10px;'>
				
	
			</div>
		</div>
	</div>
</div>

