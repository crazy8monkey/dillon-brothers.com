<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html amp lang="en"> <!--<![endif]-->
	<head>
	
		<title>Join our Text club | Dillon Brothers</title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
		<meta name="robots" content="noindex">
		<meta http-equiv="Cache-control" content="public">
		<meta charset="utf-8">

		<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		
		
		<link rel="stylesheet" href="<?php echo PATH ?>public/css/TextClubController.css" />
									
		<script type="text/javascript" src="<?php echo PATH ?>public/js/Globals.js"></script>
		
		<script src="https://use.typekit.net/kpv4owm.js"></script>
		
		
					
				
		
	</head>
<body >
<div class="container">
	<div class="col-md-12">
		<iframe class="embed-responsive-item" src="http://closebyadmin.com/subscription/widget_subscribe.php?p=kh-d4" frameborder="0"></iframe>
	</div>
</div>

</body>
</html>