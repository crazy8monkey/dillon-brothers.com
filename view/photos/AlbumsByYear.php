<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery_lazyload/1.9.7/jquery.lazyload.min.js"></script>
<div class="YearPhotoSection">
	<?php if($this -> yearCategory -> YearPhoto != NULL):?>
		<div class="yearImage">
			<img src='<?php echo PHOTO_URL . $this -> yearCategory -> year . '/' . $this -> yearCategory -> YearPhoto?>' alt='<?php echo $this -> yearCategory -> year ?> Photos' title='<?php echo $this -> yearCategory -> year ?> Photos' />	
		</div>
	<?php else: ?>		
		<div class="noYearImage">
			<img src="<?php echo PATH ?>public/images/PhotoAlbumIcon.png">
			<div style='margin-top:15px; font-weight:bold;'><?php echo $this -> yearCategory -> year ?> Photos</div>
		</div>
	<?php endif; ?>	
</div>


	
	
	
<div class="container">	
	<div class="row">
		<div class="col-md-12" style="margin:15px 0px 20px 0px;">
			<a href="<?php echo PATH ?>photos">Photos</a><span style="margin:0px 5px;"><i class="fa fa-caret-right" aria-hidden="true"></i></span><?php echo $this -> yearCategory -> year ?>	
		</div>
	</div>

	<?php if(count($this -> albums) > 0):?>
	
		<?php 
		$prevSection = null;
		$yearNewsletters = array(); 
		$albumsByCategory = array(); 
		
		foreach($this -> albums as $albumCategory) {
			
		  	$NewSection = $albumCategory['AlbumType'];
	
			if($prevSection != $NewSection) {
				//echo "<strong>Array Push</strong><br />";
				array_push($albumsByCategory, array("section" => $NewSection,
													"albums" => $this -> albums));	
			}
			$prevSection = $NewSection; 
				
		} 
		
		
		//clear wrong category albums
		foreach($albumsByCategory as $key => $value) {
			$section = $value['section']; 
			foreach($value['albums'] as $albumKey => $album) {
				$albumCategory = $album['AlbumType'];
				if($section != $albumCategory) {
					unset($albumsByCategory[$key]['albums'][$albumKey]);
				}		
			}
		 } 
		 ?>
		
		
		<?php $prevCategory = NULL; ?>
		<?php foreach($albumsByCategory as $albumCategory) : ?>
			<?php 
			$showCategory = false; 
			if($prevCategory != $albumCategory['section']) {
				$showCategory = true; 
			}
			
			
			$prevCategory = $albumCategory['section'];
			?>
			
			<?php if($showCategory == true): ?>
			<div class="row">
				<div class="col-md-12">
					<div style="margin-bottom: 10px;">
						<strong><?php
							switch($albumCategory['section']) {
								case '1':
									echo "Events";
									break;		
								case '2':
									echo "Blog";
									break;
								case '3':
									echo "Miscellaneous";
									break;		
							}
						
						 ?></strong>		
					</div>
					
				</div>
			</div>	
			<?php endif; ?>
			
			<?php foreach(array_chunk($albumCategory['albums'], 4, true) as $albumSix) : ?>
				<div class="row" style='margin-bottom:10px;'>
					<?php foreach($albumSix as $albumSingle) : ?>
						<?php 
							$type = NULL;
							switch($albumSingle['AlbumType']) {
								case 1:
									$type = "/events/";
									$url = PATH . "photos/" . $albumSingle['ParentDirectory'] ."/event/" . $albumSingle['albumSEOurl'];									
									break;
								case 2:
									$type = "/blog/";									
									$url = PATH . "photos/" . $albumSingle['ParentDirectory'] ."/blog/" . $albumSingle['albumSEOurl'];
									break;	
								case 3:
									$type = "/misc/";
									$url = PATH . "photos/" . $albumSingle['ParentDirectory'] ."/misc/" . $albumSingle['albumSEOurl'];
									break;		
							}
						?>
						
						
						<div class="col-md-3">
							<a href="<?php echo $url ?>">
								<?php if(empty($albumSingle['albumThumbNail'])) :?>							
									<div class="PhotoAlbumElementEmpty">
										<div class="indicator">
											<img src="<?php echo PATH ?>public/images/PhotoAlbumIcon.png" />	
										</div>
									</div>
								<?php else: ?>
									<div class="PhotoAlbumElement">
										
										<img data-original="<?php echo PHOTO_URL . $albumSingle['ParentDirectory'] . $type . $albumSingle['albumFolderName'] . '/' . $albumSingle['photoName'] . '-s.' . $albumSingle['ext']?>" alt="<?php echo $albumSingle['altText'] ?>" title="<?php echo $albumSingle['title'] ?>" data-scale="best-fill" data-align="center"  />	
										
									</div>
									
								<?php endif; ?>
								<div class="albumName">
									<div><strong><?php echo $albumSingle['albumName'] ?></strong></div>
									<div style='font-size:12px'><?php echo $albumSingle['PhotoCount'] ?> Photo(s)</div>
								</div>
							</a>
						</div>
						<script type="application/ld+json">
						{
							"@context": "http://schema.org",
							"@type": "ImageGallery",
						    "name": "<?php echo $albumSingle['albumName'] ?>",
						    "url": "<?php echo $url ?>",
						    "datePublished": "<?php echo $albumSingle['albumPublishedDate'] ?>",
						    "dateModified": "<?php echo $albumSingle['albumModifedDate'] ?>",
						    "creator": "Dillon Brothers",
						    "dateCreated": "<?php echo $albumSingle['createdDate'] ?>T<?php echo $albumSingle['createdTime'] ?>",
						    "thumbnailUrl": "<?php echo PHOTO_URL . $albumSingle['ParentDirectory'] . $type . $albumSingle['albumFolderName'] . '/' . $albumSingle['photoName'] . '-s.' . $albumSingle['ext']?>"
						}
						</script>
						
					<?php endforeach; ?>
				</div>
			<?php endforeach; ?>
		
		<?php endforeach; ?>	
	<?php else:?>
		<div class="row" style='margin-bottom:10px;'>
			<div class="col-md-12">
				There are no albums present in this year.
			</div>
		</div>
	<?php endif;?>
	
</div>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [
  	{
	    "@type": "ListItem",
	    "position": 1,
	    "item": {
	      "name": "Dillon Brothers Photos",
	      "url": "<?php echo PATH ?>photos"
	    }
  	},{
	    "@type": "ListItem",
	    "position": 2,
	    "item": {
	      "name": "<?php echo $this -> yearCategory -> year?> Photos",
	      "url": "<?php echo PATH . 'photos/'. $this -> yearCategory -> year?>"
	    }
   }]
}
</script>

