<div class="container" style="padding-top:25px; padding-bottom:0px;">
	<div class="row">
		<div class="col-md-12 ContentHeader">
			<h1>OUR PHOTO GALLERY</h1>
		</div>
	</div>
</div>	
<div class="grayBackground" style='padding:10px; 0px;'>
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam non nunc egestas quam fringilla blandit vitae vitae urna. Cras vehicula risus nec condimentum sollicitudin. Quisque ac porttitor neque. Etiam vestibulum velit est, a faucibus elit porta quis. Vivamus mollis quis diam vitae ultricies. Proin id nisl varius lectus lobortis pellentesque. In scelerisque sapien sit amet libero semper, ac interdum metus placerat. Donec mattis accumsan accumsan. Sed eu dictum mauris.
			</div>
		</div>
	</div>
</div>
	
<div class="container" style="padding-top:25px;">	
	<div class="row" style='margin-bottom:10px;'>
		<div class="col-md-12">
			<?php foreach($this -> years as $yearSingle) : ?>									
				<div class="YearElement">
					<a href="<?php echo PATH ?>photos/<?php echo $yearSingle['YearText']; ?>" >
						<div style="text-align:center; padding:10px;">	
							<?php if($yearSingle['YearPhotoThumb'] != NULL) : ?>
								<img src="<?php echo PHOTO_URL. $yearSingle['YearText'] ?>/<?php echo $yearSingle['YearPhotoThumb'] ?>" />
							<?php else: ?>
								<div class="emptyYearThumb">
									<img src="<?php echo PATH ?>public/images/FolderIcon.png" />
								</div>
							<?php echo $yearSingle['YearText']; ?>
							<?php endif; ?>		
						</div>
					</a>	
				</div>
			<?php endforeach; ?>
		</div>
	</div>				
</div>


<script type="application/ld+json">

</script>


