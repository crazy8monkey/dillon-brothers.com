<?php

header('Content-type: application/xml');

$url_prefix = 'http://www.pontikis.net/blog/';
$blog_timezone = 'UTC';
$timezone_offset = '+06:00';
$W3C_datetime_format_php = 'Y-m-d\h:i:s'; // See http://www.w3.org/TR/NOTE-datetime
//$null_sitemap = '<urlset><url><loc></loc></url></urlset>';


echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';

echo '<url><loc>'. PATH . '</loc><changefreq>weekly</changefreq></url>';
echo '<url><loc>http://www.dilloncycles.com/</loc><changefreq>weekly</changefreq></url>';
echo '<url><loc>http://www.dillonharley.com/</loc><changefreq>weekly</changefreq></url>';
echo '<url><loc>http://www.dillonbrothersindian.com/</loc><changefreq>weekly</changefreq></url>';
echo '<url><loc>'. PATH . 'inventory</loc><changefreq>weekly</changefreq></url>';
echo '<url><loc>'. PATH . 'service</loc><changefreq>weekly</changefreq></url>';
echo '<url><loc>'. PATH . 'parts</loc><changefreq>weekly</changefreq></url>';
echo '<url><loc>'. PATH . 'shop</loc><changefreq>weekly</changefreq></url>';
foreach($this -> EcommerceCategories as $ecommerceSingle) {
	echo '<url><loc>'. PATH . 'shop/category/' . $ecommerceSingle['settingCategorySEOName'] . '</loc><changefreq>weekly</changefreq></url>';
	$categoryID = $ecommerceSingle['settingCategoryID'];
										
	$filteredCategoryProducts = array_filter($this -> EcommerceProducts, function ($var) use($categoryID) {
			return $var['productCategory'] == $categoryID; 
		}
	);	
	
	foreach($filteredCategoryProducts as $productSingle) {
		echo '<url><loc>'. PATH . 'shop/product/' . $productSingle['productSEOName'] . '</loc><changefreq>weekly</changefreq></url>';
	}
		
}
echo '<url><loc>'. PATH . 'specials</loc><changefreq>weekly</changefreq></url>';

foreach($this -> currentSpecials as $specialSingle)  {
	$url = NULL;
	if($specialSingle['StoreNames'] != NULL) {
		switch(true) {
			case stripos($specialSingle['StoreNames'], "MotorSports"):
				$url = PATH . 'specials/motorsport/' . $specialSingle['specialSEOurl'];					
				break;
			case stripos($specialSingle['StoreNames'], "Harley"):
				$url = PATH . 'specials/harley/' . $specialSingle['specialSEOurl'];
				break;
			case stripos($specialSingle['StoreNames'], "Indian"):
				$url = PATH . 'specials/indian/' . $specialSingle['specialSEOurl'];
				break;
		}
	} else {
		switch($specialSingle['specialType']) {
			case 2:
				$url = PATH . 'specials/apparelspecial/' . $specialSingle['specialSEOurl'];
				break;		
			case 3:
				$url = PATH . 'specials/partsspecial/' . $specialSingle['specialSEOurl'];
				break;		
			case 4:
				$url = PATH . 'specials/servicespecial/' . $specialSingle['specialSEOurl'];
				break;	
			case 5:
				$url = PATH . 'specials/storespecial/' . $specialSingle['specialSEOurl'];
				break;
		}
	}							
									
	echo '<url><loc>'. $url . '</loc><changefreq>weekly</changefreq></url>';							

}

echo '<url><loc>'. PATH . 'blog</loc><changefreq>daily</changefreq></url>';

foreach($this -> blogPosts as $blogSingle) {
	echo '<url><loc>'. PATH . 'blog/post/' . $blogSingle['blogPostSEOurl'] . '</loc><changefreq>daily</changefreq></url>';
} 

foreach($this -> blogCategories as $blogCategory) {
	echo '<url><loc>'. PATH . 'blog/category/' . $blogCategory['blogCategorySEOUrl'] . '</loc><changefreq>daily</changefreq></url>';
} 
echo '<url><loc>'. PATH . 'blog/category/uncategorized</loc><changefreq>daily</changefreq></url>';
echo '<url><loc>'. PATH . 'events</loc><changefreq>daily</changefreq></url>';

if(count($this -> CurrentEvents) > 0) { 
	foreach($this -> CurrentEvents as $event) { 
		$hyperLink = NULL;
		if($event['SubEvent'] == 1) {	
			$hyperLink = PATH . $event['SuperEventSEOUrl'] . '/#' . $event['seoUrl'];
		} else {
			$hyperLink = PATH .  $event['seoUrl'];
		}
		echo '<url><loc>'. $hyperLink . '</loc><changefreq>daily</changefreq></url>';							
	}
}

echo '<url><loc>'. PATH . 'photos</loc><changefreq>daily</changefreq></url>';

foreach($this -> PhotoYears as $year) {	
	echo '<url><loc>'. PATH . 'photos/' . $year['YearText'] .'</loc><changefreq>daily</changefreq></url>';
						
	$Year = $year['YearText'];
										
	$filteredAlbums = array_filter($this -> PhotoAlubms, function ($var) use($Year) {
			return $var['ParentDirectory'] == $Year; 
		}
	);
	if(count($filteredAlbums) > 0) {
		foreach($filteredAlbums as $albumSingle) {
			switch($albumSingle['AlbumType']) {
				case 1:
					$url = PATH . "photos/" . $albumSingle['ParentDirectory'] ."/event/" . $albumSingle['albumSEOurl'];									
					break;
				case 2:
					$url = PATH . "photos/" . $albumSingle['ParentDirectory'] ."/blog/" . $albumSingle['albumSEOurl'];
					break;	
				case 3:
					$url = PATH . "photos/" . $albumSingle['ParentDirectory'] ."/misc/" . $albumSingle['albumSEOurl'];
					break;		
			}
			
			echo '<url><loc>'. $url . '</loc><changefreq>daily</changefreq></url>';						
		}						
	} 
}

echo '<url><loc>'. PATH . 'newsletters</loc><changefreq>daily</changefreq></url>';
echo '<url><loc>'. PATH . 'rewards</loc><changefreq>daily</changefreq></url>';





if(count($this -> newsLetters) > 0) {
	foreach($this -> newsLetters as $newsLetter) { 	
		if(file_exists(NEWSLETTER_PATH. $newsLetter['folderName'] . '/index.html')) { 
			echo '<url><loc>'. NEWSLETTER_URL. $newsLetter['folderName'] . '</loc><changefreq>never</changefreq></url>';
		}
	}
}
echo '<url><loc>'. PATH . 'contact</loc><changefreq>daily</changefreq></url>';
echo '<url><loc>http://www.partsfish.com/</loc><changefreq>yearly</changefreq></url>';
echo '<url><loc>http://www.siddillon.com/</loc><changefreq>daily</changefreq></url>';
echo '<url><loc>http://learntoride.dillon-brothers.com/</loc><changefreq>daily</changefreq></url>';
echo '<url><loc>https://www.facebook.com/DillonBrothersOmaha/</loc><changefreq>daily</changefreq></url>';
echo '<url><loc>https://twitter.com/dillonbrothers</loc><changefreq>daily</changefreq></url>';
echo '<url><loc>https://www.youtube.com/user/dillonbrothers?feature=mhee</loc><changefreq>daily</changefreq></url>';

echo '</urlset>';
