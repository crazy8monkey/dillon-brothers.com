<div class="container">	
	<div class="row">
		<div class="col-md-12 ContentHeader">
			<h1>SITEMAP</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="PageContent">
				<div><a href="<?php echo PATH ?>">Home</a></div>
				<div class="Indent">
					<div><a href="http://www.dilloncycles.com/" target="_blank">http://www.dilloncycles.com/</a></div>
					<div><a href="http://www.dillonharley.com/" target="_blank">http://www.dillonharley.com/</a></div>
					<div><a href="http://www.dillonbrothersindian.com/" target="_blank">http://www.dillonbrothersindian.com/</a></div>
				</div>
			</div>
			<div class="PageContent">
				<div><a href="<?php echo PATH ?>brand">Brands We Carry</a></div>
				<div class="Indent">
					<div class="header">Vehicle Brands</div>
					<div class="Indent">
						<?php foreach($this -> OEMBrands as $OEMSingle): ?>
							<div><a href='<?php echo PATH ?>brand/<?php echo $OEMSingle['brandNameSEOUrl'] ?>'><?php echo $OEMSingle['BrandName'] ?></a></div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="Indent">
					<div class="header">Part/Apparel Brands</div>
					<div class="Indent">
						<?php foreach($this -> PartApparelBrands as $partBrandSingle): ?>
							<div><a href='<?php echo PATH ?>brand/<?php echo $partBrandSingle['brandName'] ?>'><?php echo $partBrandSingle['brandName'] ?></a></div>
						<?php endforeach; ?>
					</div>
					
				</div>
			</div>
			<div class="PageContent">
				<div><a href="<?php echo PATH ?>inventory">Inventory</a></div>
			</div>
			<div class="PageContent">
				<div><a href="<?php echo PATH ?>service">Service</a></div>
			</div>
			<div class="PageContent">
				<div><a href="<?php echo PATH ?>Parts">Parts</a></div>
			</div>
			<div class="PageContent">
				<div><a href="<?php echo PATH ?>shop">Shop</a></div>
				<div class="Indent">
					<?php foreach($this -> EcommerceCategories as $ecommerceSingle): ?>
						<div><a href="<?php echo PATH ?>shop/category/<?php echo $ecommerceSingle['settingCategorySEOName'] ?>" target="_blank"><?php echo $ecommerceSingle['settingCategoryName'] ?></a></div>
						
						<?php 
							
							$categoryID = $ecommerceSingle['settingCategoryID'];
										
							$filteredCategoryProducts = array_filter($this -> EcommerceProducts, function ($var) use($categoryID) {
								return $var['productCategory'] == $categoryID; 
								}
							);
						?>
						<div class="Indent">
							<?php foreach($filteredCategoryProducts as $productSingle): ?>
								<div><a href="<?php echo PATH ?>shop/product/<?php echo $productSingle['productSEOName'] ?>" target="_blank"><?php echo $productSingle['productName'] ?></a></div>
							<?php endforeach; ?>
						</diV>
						
					<?php endforeach; ?>
				</div>
				
				
			</div>
			<div class="PageContent">
				<div><a href="<?php echo PATH ?>specials">Specials</a></div>
				<div class="Indent">
					<?php foreach($this -> currentSpecials as $specialSingle) : ?>
						<?php 
							$url = NULL;
							if($specialSingle['StoreNames'] != NULL) {
								switch(true) {
									case stripos($specialSingle['StoreNames'], "MotorSports"):
										$url = PATH . 'specials/motorsport/' . $specialSingle['specialSEOurl'];
										break;
									case stripos($specialSingle['StoreNames'], "Harley"):
										$url = PATH . 'specials/harley/' . $specialSingle['specialSEOurl'];
										
										break;
									case stripos($specialSingle['StoreNames'], "Indian"):
										$url = PATH . 'specials/indian/' . $specialSingle['specialSEOurl'];
										break;
								}
							} else {
								switch($specialSingle['specialType']) {
									case 2:
										$url = PATH . 'specials/apparelspecial/' . $specialSingle['specialSEOurl'];
										break;		
									case 3:
										$url = PATH . 'specials/partsspecial/' . $specialSingle['specialSEOurl'];
										break;		
									case 4:
										$url = PATH . 'specials/servicespecial/' . $specialSingle['specialSEOurl'];
										break;	
									case 5:
										$url = PATH . 'specials/storespecial/' . $specialSingle['specialSEOurl'];
										break;
								}
							}
							
							switch($specialSingle['specialType']) {
								case '1':
									$specialTitle = $specialSingle['BrandName'] . ' Manufacture Incentive: '. $specialSingle['SpecialTitle'];
									break;
								case '2':
									if($specialSingle['PartsApparelID'] != 0) {
										$specialTitle = $special['brandName'] . " Apparel Incentive: " .$specialSingle['SpecialTitle'];	
									} else {
										$specialTitle = "Apparel Incentive: " .$special['SpecialTitle'];
									}
									break;
								case '3':
									if($special['PartsApparelID'] != 0) {
										$specialTitle = $specialSingle['brandName'] . " Parts Incentive: " .$specialSingle['SpecialTitle'];
									} else {
										$specialTitle = "Parts Incentive: " .$specialSingle['SpecialTitle'];
									}
									break;
								case '4':
									$specialTitle = "Service Incentive: " . $specialSingle['SpecialTitle'];
									break;
								case '5':
									$specialTitle = "Store Incentive: " . $specialSingle['SpecialTitle'];
									break;
							}
									
									
								
								?>
								<div><a href="<?php echo $url ?>"><?php echo $specialTitle; ?></a></div>
							
					<?php endforeach; ?>	
				</div>
			</div>
			
			<div class="PageContent">
				<div><a href="<?php echo PATH ?>blog">Blog</a></div>
				<div class="Indent">
					<?php if(count($this -> blogCategories) > 0) :?>
						<div class="header">Categories</div>
						<div><a href="<?php echo PATH ?>blog">All</a></div>
						<?php foreach($this -> blogCategories as $blogCategory) : ?>
							<div><a href="<?php echo PATH ?>blog/category/<?php echo $blogCategory['blogCategorySEOUrl']?>"><?php echo $blogCategory['blogCategoryName']?></a></div>
						<?php endforeach; ?>
						<div><a href="<?php echo PATH ?>blog/category/uncategorized">Uncategorized</a></div>
					<?php endif; ?>
					
				</div>
			</div>
			
			<div class="PageContent">
				<div><a href="<?php echo PATH ?>events">Events</a></div>
				<?php if(count($this -> CurrentEvents) > 0) : ?>
					<div class="Indent">						
						<?php foreach($this -> CurrentEvents as $event) : ?>
							<?php 
							$hyperLink = NULL;
							$EventUrlName = NULL;
								//SuperEventName
							if($event['SubEvent'] == 1) {	
								$hyperLink = PATH . $event['SuperEventSEOUrl'] . '/#' . $event['seoUrl'];
								$EventUrlName = $event['SuperEventName'] . ': ' . $event['eventName'];
							} else {
								$hyperLink = PATH .  $event['seoUrl'];
								$EventUrlName = $event['eventName'];
							}
								
								
							?>
							<div><a href="<?php echo $hyperLink ?>"><?php echo $EventUrlName ?></a></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<div class="PageContent">
				<div><a href="<?php echo PATH ?>photos">Photos</a></div>
				<div class="Indent">	
					<?php foreach($this -> PhotoYears as $year) : ?>	
						<div><a href="<?php echo PATH ?>photos/<?php echo $year['YearText'] ?>"><?php echo $year['YearText'] ?></a></div>
						
						<?php 
							$Year = $year['YearText'];
										
							$filteredAlbums = array_filter($this -> PhotoAlubms, function ($var) use($Year) {
								 return $var['ParentDirectory'] == $Year; 
								}
							);
						?>
						<?php if(count($filteredAlbums) > 0): ?>
							<div class="Indent">	
								<?php foreach($filteredAlbums as $albumSingle) : ?>
									<?php 
										switch($albumSingle['AlbumType']) {
											case 1:
												$url = PATH . "photos/" . $albumSingle['ParentDirectory'] ."/event/" . $albumSingle['albumSEOurl'];									
												break;
											case 2:
												$url = PATH . "photos/" . $albumSingle['ParentDirectory'] ."/blog/" . $albumSingle['albumSEOurl'];
												break;	
											case 3:
												$url = PATH . "photos/" . $albumSingle['ParentDirectory'] ."/misc/" . $albumSingle['albumSEOurl'];
												break;		
										}
									?>
									
									<div><a href="<?php echo $url ?>"><?php echo $albumSingle['albumName'] ?></a></div>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
						
					<?php endforeach; ?>				
				</div>
				
			
			</div>
			
			<div class="PageContent">
				<div><a href="<?php echo PATH ?>newsletters">Newsletters</a></div>
				<div class="Indent">
					<?php $prevYear= null; ?>
					<?php if(count($this -> newsLetters) > 0) : ?>		
						<?php foreach($this -> newsLetters as $newsLetter) : ?>	
							<?php $showYear = false; ?>
							<?php if(file_exists(NEWSLETTER_PATH. $newsLetter['folderName'] . '/index.html')) : ?>
								<?php  
								$DateSentYear = explode("-", $newsLetter['DateSent']);
								if($prevYear != $DateSentYear[0]) {
				                    $showYear = true;
				                } 
				                $prevYear = $DateSentYear[0];
                
                				?>
                				<?php if($showYear == true): ?>
								<div class="header" style="margin-top:10px;">
									<?php echo $DateSentYear[0] ?>	
								</div>
								<?php endif; ?>
								<div style="margin-left:20px;">
									<a href="<?php echo NEWSLETTER_URL. $newsLetter['folderName'] ?>/" target='_blank'>
										<?php echo $newsLetter['Name'] . ' ' . $newsLetter['DateSent']?>
									</a>
								</div>
								
							<?php endif; ?>
						<?php endforeach ?>
					<?php endif; ?>
				</div>
			</div>
			
			<div class="PageContent">
				<div><a href="<?php echo PATH ?>rewards">Rewards</a></div>
			</div>
			
			<div class="PageContent">
				<div><a href="<?php echo PATH ?>contact">Contact</a></div>
			</div>
			
			<div class="PageContent">
				<div class="header">Footer Links</div>
				<div class="Indent">
					<div><a href="http://www.partsfish.com/" target='_blank'>PartsFish</a></div>
					<div><a href="http://www.siddillon.com/" target='_blank'>Sid Dillon</a></div>
					<div><a href="http://learntoride.dillon-brothers.com/" target='_blank'>Learn To Ride</a></div>
					<div><a href="https://www.facebook.com/DillonBrothersOmaha" target='_blank'>Facebook</a></div>
					<div><a href="https://twitter.com/dillonbrothers" target='_blank'>Twitter</a></div>
					<div><a href="https://www.youtube.com/user/dillonbrothers?feature=mhee" target='_blank'>Youtube</a></div>
				</div>
			
			</div>
			
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="margin-top: 20px;">
			<div style="border-top:1px solid #cecece; margin-bottom: 10px;"></div>
			<a href="<?php echo PATH; ?>sitemap/xml" target="_blank">XML SiteMap</a>	
		</div>
		
	</div>
<br />
</div>

