var RewardsController = function() {
	
	function OpenLoginForm() {
		$(".MoreThanRewardsLoginAccount").show();
	}	
	
	function CloseLoginForm() {
		$(".MoreThanRewardsLoginAccount").hide();
	}
	
	function InitializePage() {
		
	}
		
	return {
		OpenLoginForm: OpenLoginForm,
		CloseLoginForm: CloseLoginForm,
		InitializePage: InitializePage
	}
}();
