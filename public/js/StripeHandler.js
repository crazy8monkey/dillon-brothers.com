var StripeHandler = function() {
	
	var FormElement;
	
	var address;
	var city;
	var providence;
	var postalCode;
							
	//shippingEmail
	var cardHolderName;	
	var creditCardNumber;
	var creditCardCVC;
	var creditCardExpirationMonth;
	var creditCardExpirationYear;
	
	var PostValue;
	
	function SetStripeVariables(form, addressElement, cityElement, provenceElement, postalcodeElement, CardHolderNameElement, CardNumberElement, CardCVCElement, CardExpirationMonthElement, CardExpirationYearElement, PostBoolean) {
		FormElement = form;
	
		address = addressElement;
		city = cityElement;
		providence = provenceElement;
		postalCode = postalcodeElement;
								
		//shippingEmail
		cardHolderName = CardHolderNameElement;	
		creditCardNumber = CardNumberElement;
		creditCardCVC = CardCVCElement;
		creditCardExpirationMonth = CardExpirationMonthElement;
		creditCardExpirationYear = CardExpirationYearElement;
		PostValue = PostBoolean;
	}
	
	function ValidateExpiration(){
        var month = $(creditCardExpirationMonth).val();
        var year = $(creditCardExpirationYear).val();
        if (month === ''){
            $(creditCardExpirationMonth).css('border-color', '#c30000');
            return false;
        }else {
            if (year === '') {
                $(creditCardExpirationYear).css('border-color', '#c30000');
                return false;
            } else if (year === new Date().getFullYear().toString()) {
                if (parseInt(month) < new Date().getMonth() + 1){ // We do the +1 here because months are zero-indexed
                    // ADAM: Here you should change this to show a message inline probably
                    // I just added this for your testing purposes. If I were good at design I'd do it
                    // But I'm not
                    alert('Please Enter a Future Month');
                    return false;
                }
            }
            else{
                $(creditCardExpirationMonth).css('border-color', 'green');
                $(creditCardExpirationYear).css('border-color', 'green');
                return true;
            }
        }
    }
	
	
	function ValidateCardNumber(){
        var retVal = false;
		   	
		// Feel free too apply classes or something to make this look how you like
		if(!Stripe.card.validateCardNumber($(creditCardNumber).val())){
        	if(document.getElementById("paymentCreditCardNumberInvalid") == null) {
				$("<div class='errorMessageText' id='paymentCreditCardNumberInvalid'>Credit Card Number is invalid</div>").insertBefore(creditCardNumber);
			}
            $(creditCardNumber).css('border-color', '#c30000');
        }else{
            $(creditCardNumber).css('border-color', 'green');
            $("#paymentCreditCardNumberInvalid").remove();
            retVal = true;
        }
		
        return retVal;
    }
	
	 function ValidateCVC(){
        if(!Stripe.card.validateCVC($(creditCardCVC).val())){
            $(creditCardCVC).css('border-color', '#c30000');
            return false;
        }else{
            $(creditCardCVC).css('border-color', 'green');
            return true;
        }
    }
	
	function stripeResponseHandler(status, response) {
		var $form = FormElement;
	  	if (response.error) {
	  		alert(response.error.message);

	    	// Show the errors on the form
	    	//$form.find('.payment-errors').text(response.error.message);
	    	console.log(response.error.message);
	    	$form.find('input[type="submit"]').prop('disabled', false);
	  	} else {
	  		$("#ecommerceLoadingObject").show();
	    	// response contains id and card, which contains additional card details
	    	var token = response.id;
	    	// Insert the token into the form so it gets submitted to the server
	    	var stripeHiddenTokenInput = $('<input type="hidden" name="stripeToken" />').val(token)
	    
	    	$form.append(stripeHiddenTokenInput);
	    
	    	//if form post action
	    	if(PostValue) {
	    		$form.submit();
	    	//if form get action	
	    	} else {
	    		$form.get(0).submit();	
	    	}	    
	  	}
	}
		
	function CreateToken() {
		Stripe.card.createToken({
	        name: $(cardHolderName).val(),
	        address_line1: $(address).val(),
	        address_city: $(city).val(),
	        address_state: $(providence).val(),
	        address_zip: $(postalCode).val(),
	        number: $(creditCardNumber).val(),
	        cvc: $(creditCardCVC).val(),
	        exp_year: $(creditCardExpirationYear).val(),
	        exp_month: $(creditCardExpirationMonth).val(),
	    	address_country: "US"
	    }, stripeResponseHandler);
	}	
			
	return {
		SetStripeVariables: SetStripeVariables,
		CreateToken: CreateToken,
		ValidateExpiration: ValidateExpiration,
		ValidateCardNumber: ValidateCardNumber,
		ValidateCVC: ValidateCVC
	}
}();
