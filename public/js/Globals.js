jQuery.fn.extend({
	    toggleText: function (a, b){
	        var that = this;
	            if (that.text() != a && that.text() != b){
	                that.text(a);
	            }
	            else
	            if (that.text() == a){
	                that.text(b);
	            }
	            else
	            if (that.text() == b){
	                that.text(a);
	            }
	        return this;
	    }
});
var Globals = function() {

	var channelUniqueName;
	var WorderID;
	
	
	function MultipleErrorResponses(responses) {
		if(responses.ValidationErrors) {
			var validationMessagesArray = responses.ValidationErrors;
			
			var ValidationErrors = responses.ValidationErrors.length;
			for (var i = 0; i < ValidationErrors; i++) {
			   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
			   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
			   $("#inputID" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
			   $("#inputID" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
			}
		}
		if(responses.redirect) {
 			Globals.redirect(responses.redirect); 
 		}
	}	
	
	function SetChannel(id) {
		channelUniqueName = id;
	}
	
	function SetWorker(worker) {
		WorderID = worker;
	}
	
	function GetChannel() {
		return channelUniqueName;
	}
	
	function GetWorker() {
		return WorderID;
	}
	
	function redirect(string, delay) {
		if(delay) {
			setTimeout(function(){
				window.location.assign(string); 
			}, delay);
		} else {
			window.location.assign(string);
		}
	}
	
	function OpenTextClubPopup() {
		$("#TextClubPopupContent").fadeIn('fast');
	}

	function CloseTextClubPopup() {
		$("#TextClubPopupContent").fadeOut('fast');
	}
	
	function WebsitePath() {
		return 'http://127.0.0.1/DillonBrothersMain/';
	}
	
	function ScrollToTop() {
		 $('body,html').animate({
	        scrollTop : 0 // Scroll to top of body
	    }, 500);
		
	}
	
	function CloseInventoryQuickLinks() {
		$(".Quicklinks").hide();
	}
	
	function OpenInventoryQuickLinks() {
		$(".Quicklinks").toggle();
	}
	
	function StartWebsite() {
		$('[data-toggle="tooltip"]').tooltip({html: true});
		$.getJSON(Globals.WebsitePath() + 'ajax/getchatchannel', function(data) {
			if(data.response) {
				SetChannel(data.channel);
				SetWorker(data.worker);
			} else {
				$("#SalesChatButton").hide();
				$("#ScrollUpToPage").css('bottom', '110px');	
			}
	    });
		
	}
	
	function OpenConversationWindow() {
		window.open(Globals.WebsitePath() + 'chat?id=' + GetWorker() + '&channel=' + GetChannel(),'sharer','height=500,width=500');
	}
	
	return {
		AjaxPost: function(formURL, postData, loadingElement, success) {
			$.ajax({
				url: formURL,
		        type: 'POST',
		        data: postData,
		        dataType: 'json',
				cache: false,
		        beforeSend: function(){
		            $(loadingElement).show();
    		    },
				success : success
			});	
		},
		AjaxFileUpload: function(formURL, postData, loadingElement, success) {
			$.ajax({
				url: formURL,
		        type: 'POST',
		        data: postData,
		        dataType: 'json',
				cache: false,
		        beforeSend: function(){
		            $(loadingElement).show();
    		    },
				success : success,
				cache: false,
		        contentType: false,
		        processData: false
			});	
		},
		removeValidationMessage: function(ID) {
			$("#inputID" + ID + " .errorMessage").html("");
			$("#inputID" + ID + " input").removeAttr("style");
			$("#inputID" + ID + " textarea").removeAttr("style");
			$("#inputID" + ID + "Name").remove()
		},
		ScrollToTop: function(element) {
			$('html, body').animate({
		        scrollTop: $(element).offset().top - 200
		    },1000);
		    $(".ajaxLoadingIndicator").hide();
		},
		MultipleErrorResponses: MultipleErrorResponses,
		OpenTextClubPopup: OpenTextClubPopup,
		CloseTextClubPopup: CloseTextClubPopup,
		WebsitePath: WebsitePath,
		ScrollToTop: ScrollToTop,
		OpenInventoryQuickLinks: OpenInventoryQuickLinks,
		CloseInventoryQuickLinks: CloseInventoryQuickLinks,
		StartWebsite: StartWebsite,
		OpenConversationWindow: OpenConversationWindow,
		redirect: redirect 
	}	
	
}();