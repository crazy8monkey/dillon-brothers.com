	Array.prototype.contains = function(obj) {
	    var i = this.length;
	    while (i--) {
	        if (this[i] === obj) {
	            return true;
	        }
	    }
	    return false;
	}

var InventoryController = function() {

	function OpenFilterList(Element) {
		$(Element).next().toggle();
		$(Element).find('.sectionHeader').toggleClass("Active");
		
		//$(".filterList").not($(Element).next()).hide();
		
		//$(".sectionHeader").not($(Element).find('.sectionHeader')).removeClass("Active");
	}
	
	function OpenBikeDescription() {
		$("#bikeDescriptionTab .tabSelected, #BikeDescription").show();
		$("#SpecTab .tabSelected, #BikeSpecsContent").hide();
	}
	
	function OpenBikeSpecs() {
		$("#SpecTab .tabSelected, #BikeSpecsContent").show();
		$("#bikeDescriptionTab .tabSelected, #BikeDescription").hide();
	}
	
	function SendToFriend() {
		$("#SendToFriendForm").submit(function(e){
			e.preventDefault();
			$(".InventoryPopupLoader").show();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, 
							 postData, 
							 "#uploadingPhotosText", 
							 function(responses) {
								if(responses.ValidationErrors) {
									var validationMessagesArray = responses.ValidationErrors;
									
									var ValidationErrors = responses.ValidationErrors.length;
									for (var i = 0; i < ValidationErrors; i++) {
									   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
									   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
									   $("#inputID" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
									   $("#inputID" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
									}
									
									alert("Please fix the errors on the screen");
									$("#uploadingPhotosText").hide();
									$(".InventoryPopupLoader").hide();
								}
								if(responses.emailsent) {
									$("#MessageShow").show();
									$(".InventoryPopupLoader").hide();
									$("#SuccessMessage").html("Your Email has been sent, thank you for choosing Dillon Brothers");
									setTimeout(function(){ window.close(); }, 1500);
									
						 		}
							 }
			)	
		});
	}
	
	function toggleList(rowNumber) {
		if(rowNumber == 2) {
			$("#CurrentInventoryListTwoByTwo").show();
			$("#CurrentInventoryListThreeByThree").hide();
			$(".TwoByTwo").addClass('SelectedList');
			$(".ThreeByThree").removeClass('SelectedList');
		} else if(rowNumber == 3) {
			$("#CurrentInventoryListTwoByTwo").hide();
			$("#CurrentInventoryListThreeByThree").show();
			$(".ThreeByThree").addClass('SelectedList');
			$(".TwoByTwo").removeClass('SelectedList');
		}
		//var postData = $("#FilterInventory").serialize();
		//$("#RowNumber").val(rowNumber);
		//$("#LoadingSpinnerObject").show();
		//var PageNumber = $("#PageNumber").val();
		//$("#CurrentInventoryList").html('');
		//Globals.AjaxPost(Globals.WebsitePath() + 'ajax/toggleInventoryList/' + rowNumber + '/' + PageNumber + '/false', postData, ".loadingIndicator", 
		//	function(responses) {
		//		$("#CurrentInventoryList").html(responses.html);
				//GenerateCategoryFilterItems(responses.Categories, responses.CategoryIDS);
				//GenerateManufactureFilterItems(responses.Manufacture);
				//GenerateModelFilterItems(responses.Model);	
				//ApplyCheckboxes()
		//		if(rowNumber == 2) {
		//			
		//		} else if(rowNumber == 3) {
		//			
		//		}
		//		$("#LoadingSpinnerObject").hide();
		//		window.location = responses.AppendUrl;
		//	}
					
		//);
		
		
	}
	
	function ApplyCheckboxes() {
		var HashValue = window.location.hash.split('&');
		
		
		
		HashValue.shift();		
		
		
		
		for (var i=HashValue.length-1; i>=0; i--) {
			if(HashValue[i].indexOf('Conditions=') >= 0) {
				HashValue[i] = HashValue[i].replace(/^.+=/, '');
				var ConditionSelected = HashValue[i];
				var ConditionSelectedArray = ConditionSelected.split(',');
			}
			if(HashValue[i].indexOf('Years=') >= 0) {
				HashValue[i] = HashValue[i].replace(/^.+=/, '');
				var YearsSelected = HashValue[i];
				var YearsSelectedArray = YearsSelected.split(',');
			}
			
			if(HashValue[i].indexOf('Categories=') >= 0) {
				HashValue[i] = HashValue[i].replace(/^.+=/, '');
				var CategorySelected = HashValue[i];
				var CategorySelectedArray = CategorySelected.split(',');
			}
			
			if(HashValue[i].indexOf('Stores=') >= 0) {
				HashValue[i] = HashValue[i].replace(/^.+=/, '');
				var StoresSelected = HashValue[i];
				var StoresSelectedArray = StoresSelected.split(',');
			}
			
			if(HashValue[i].indexOf('Manufactures=') >= 0) {
				HashValue[i] = HashValue[i].replace(/^.+=/, '');
				var ManufactureSelected = HashValue[i];
				var ManufactureSelectedArray = ManufactureSelected.split(',');
			}
			
			if(HashValue[i].indexOf('Colors=') >= 0) {
				HashValue[i] = HashValue[i].replace(/^.+=/, '');
				var ColorsSelected = HashValue[i];
				var ColorsSelectedArray = ColorsSelected.split(',');
			}
			
			if(HashValue[i].indexOf('MileageRange=') >= 0) {
				HashValue[i] = HashValue[i].replace(/^.+=/, '');
				var MileageRangesSelected = HashValue[i];
				var MileageRangesSelectedArray = MileageRangesSelected.split(',');
			}
			
			if(HashValue[i].indexOf('PriceRange=') >= 0) {
				HashValue[i] = HashValue[i].replace(/^.+=/, '');
				var PriceRangesSelected = HashValue[i];
				var PriceRangesSelectedArray = PriceRangesSelected.split(',');
			}
			
			if(HashValue[i].indexOf('EngineSizeRanges=') >= 0) {
				HashValue[i] = HashValue[i].replace(/^.+=/, '');
				var EngineSizeRangesSelected = HashValue[i];
				var EngineSizeRangesSelectedArray = EngineSizeRangesSelected.split(',');
			}
			
			if(HashValue[i].indexOf('View=') >= 0) {
				HashValue[i] = HashValue[i].replace(/^.+=/, '');
				var ViewSelected = HashValue[i];
			}
			
			
		}
		if(ConditionSelectedArray && ConditionSelectedArray.length) {
			$(".ConditionCheck").each(function() {
				if(ConditionSelectedArray.contains($(this).val())) {
					$(this).attr('checked', 'checked');
				}		
			})
		}
		
		if(YearsSelectedArray && YearsSelectedArray.length) {
			$(".YearCheck").each(function() {
				if(YearsSelectedArray.contains($(this).val())) {
					$(this).attr('checked', 'checked');
				}		
			});	
		}
		
		if(CategorySelectedArray && CategorySelectedArray.length) {
			$(".CategoryCheck").each(function() {
				if(CategorySelectedArray.contains($(this).val())) {
					$(this).attr('checked', 'checked');
				}		
			});	
		}
		
		if(StoresSelectedArray && StoresSelectedArray.length) {
			$(".StoreCheck").each(function() {
				if(StoresSelectedArray.contains($(this).val())) {
					$(this).attr('checked', 'checked');
				}		
			});	
		}
		
		if(ManufactureSelectedArray && ManufactureSelectedArray.length) {
			$(".ManufactureCheck").each(function() {
				if(ManufactureSelectedArray.contains($(this).val())) {
					$(this).attr('checked', 'checked');
				}		
			});	
		}
		
		if(ColorsSelectedArray && ColorsSelectedArray.length) {
			$(".ColorCheck").each(function() {
				if(ColorsSelectedArray.contains($(this).val())) {
					$(this).attr('checked', 'checked');
				}		
			});	
		}
		
		if(MileageRangesSelectedArray && MileageRangesSelectedArray.length) {
			$(".MileageRange").each(function() {
				if(MileageRangesSelectedArray.contains($(this).val())) {
					$(this).attr('checked', 'checked');
				}		
			});	
		}
		
		if(PriceRangesSelectedArray && PriceRangesSelectedArray.length) {
			$(".PriceRange").each(function() {
				if(PriceRangesSelectedArray.contains($(this).val())) {
					$(this).attr('checked', 'checked');
				}		
			});	
		}
		
		if(EngineSizeRangesSelectedArray && EngineSizeRangesSelectedArray.length) {
			$(".EngineCCRange").each(function() {
				if(EngineSizeRangesSelectedArray.contains($(this).val())) {
					$(this).attr('checked', 'checked');
				}		
			});	
		}
		
		if(ViewSelected) {
			if(ViewSelected == "2By2") {
				$("#RowNumber").val(2);
				$(".ThreeByThree ").removeClass("SelectedList");
				$(".TwoByTwo").addClass("SelectedList");
			} else if(ViewSelected == "3By3") {
				$("#RowNumber").val(3);
				$(".TwoByTwo").removeClass("SelectedList");
				$(".ThreeByThree ").addClass("SelectedList");
			}

		}
		
		
		
		
		
		//console.log(HashValue);
		//console.log(YearSelectedArray);
		//console.log(CategorySelectedArray);
		//console.log(ManufactureSelectedArray);
		
		
	}
		
	function InventoryListViewModel() {
		//var self = this;
		//$("#LoadingSpinnerObject").show();
		
		var postData = $("#FilterInventory").serialize();
		//self.inventoryList = ko.observableArray([]);
		
		Globals.AjaxPost(Globals.WebsitePath() + 'ajax/toggleInventoryList', postData, ".loadingIndicator", 
			function(responses) {
				//self.inventoryList(responses.html);			
					$("#LoadingSpinnerObject").hide();
					//$("#CurrentInventoryList").html();
					//$("strong#count").html(responses.count);
					window.location = responses.AppendUrl;
					
					$("#LoadingSpinnerObject").hide();
							
				}
					
			);
		
		//self.firstName = "test";
		//self.LastName = "test lastname"
	}	
	
	function PaginationLink(Number) {
		$("#LoadingSpinnerObject").show();
		var postData = $("#FilterInventory").serialize();
		$("#CurrentInventoryList, .paginationLinks").html('');
		var RowNumber = $("#RowNumber").val();
		
		Globals.AjaxPost(Globals.WebsitePath() + 'ajax/toggleInventoryList/' + RowNumber + '/' + Number + '/false', postData, ".loadingIndicator", 
			function(responses) {
				$("#LoadingSpinnerObject").hide();
				$("#CurrentInventoryList").html(responses.html);
				$(".paginationLinks").html(responses.Pagination);
				$("#PageNumber").val(Number);
				
				//$("strong#count").html(responses.count);
				window.location = responses.AppendUrl;
					
				document.title = 'Inventory | Page ' + Number + ' | Dillon Brothers';
					
				window.history.pushState({"html":responses.html,"pageTitle":'Inventory | Page ' + responses.PageNumber + ' | Dillon Brothers'},"", Globals.WebsitePath() + 'inventory/page/' + Number + responses.AppendUrl);
				$("#LoadingSpinnerObject").hide();
						
							
			}
					
		);
	}	
	
	function getTemplate(templateName) {
	  return document.querySelector(`#${templateName}-template`).innerHTML;
	}	
		

	function InitializeInventoryList(liveSite) {
		
		if(liveSite == true) {
			var search = instantsearch({
				// Replace with your own values
				appId: '943L0R18H9',
				apiKey: 'eba4ec01de1347a0bff85f56d79ad18d', // search only API key, no ADMIN key
				indexName: 'MainUnitInventory',
				urlSync: true
			});
			
			search.addWidget(
			  instantsearch.widgets.sortBySelector({
			    container: '#sort-by-container',
			    indices: [
			      {name: 'MainUnitInventory', label: 'Featured'},
			      {name: 'year_desc', label: 'Years - Descending'},
			      {name: 'year_asc', label: 'Years - Ascending'},
			      {name: 'price_desc', label: 'Price - Descending'},
			      {name: 'price_asc', label: 'Price - Ascending'},
			      {name: 'manufacture_desc', label: 'Manufacturer - Descending'},
			      {name: 'manufacture_asc', label: 'Manufacturer - Ascending'},
			      {name: 'mileage_desc', label: 'Mileage - Descending'},
			      {name: 'mileage_asc', label: 'Mileage - Ascending'}
			    ]
			  })
			);			
		} else {
			var search = instantsearch({
				// Replace with your own values
				appId: '943L0R18H9',
				apiKey: 'eba4ec01de1347a0bff85f56d79ad18d', // search only API key, no ADMIN key
				indexName: 'MainUnitInventoryTest',
				urlSync: true
			});
			
			search.addWidget(
			  instantsearch.widgets.sortBySelector({
			    container: '#sort-by-container',
			    indices: [
			      {name: 'MainUnitInventoryTest', label: 'Featured'},
			      {name: 'year_desc_test', label: 'Years - Descending'},
			      {name: 'year_asc_test', label: 'Years - Ascending'},
			      {name: 'price_desc_test', label: 'Price - Descending'},
			      {name: 'price_asc_test', label: 'Price - Ascending'},
			      {name: 'manufacture_desc_test', label: 'Manufacturer - Descending'},
			      {name: 'manufacture_asc_test', label: 'Manufacturer - Ascending'},
			      {name: 'mileage_desc_test', label: 'Mileage - Descending'},
			      {name: 'mileage_asc_test', label: 'Mileage - Ascending'}
			    ]
			  })
			);
		}
		
		search.addWidget(
		    instantsearch.widgets.hits({
		      container: '#CurrentInventoryListTwoByTwo',
		      hitsPerPage: 30,
		      templates: {
		      	item: getTemplate('inventoryTwoByTwo')
		      }
		    })
		);	
		
		search.addWidget(
		    instantsearch.widgets.hits({
		      container: '#CurrentInventoryListThreeByThree',
		      hitsPerPage: 30,
		      templates: {
		      	item: getTemplate('inventoryThreeByThree')
		      }
		    })
		);	
		
		search.addWidget(
		  instantsearch.widgets.stats({
		    container: '#count',
		    templates: {
		      body: '<strong>{{nbHits}}</strong> results'
		    }
		  })
		);
			
		search.addWidget(
			instantsearch.widgets.searchBox({
		    	container: '#muisearch',
		    	placeholder: 'Search Anything'
		  	})
		);
		
		search.addWidget(
		  instantsearch.widgets.refinementList({
		    container: '#ConditionFilterHtml',
		    attributeName: "Conditions"
		  })
		);
		
		search.addWidget(
		  instantsearch.widgets.refinementList({
		    container: '#YearsFilterHtml',
		    attributeName: "Year",
		    limit: 99,
		    sortBy: ['name:desc']
		  })
		);
		
		search.addWidget(
		  instantsearch.widgets.refinementList({
		    container: '#CategoryFilterHtml',
		    attributeName: "inventoryCategoryName"
		  })
		);
		
		search.addWidget(
		  instantsearch.widgets.refinementList({
		    container: '#StoresFilterHTML',
		    attributeName: "StoreName"
		  })
		);
		
		search.addWidget(
		  instantsearch.widgets.refinementList({
		    container: '#ManufactureFilterHTML',
		    attributeName: "Manufacturer"
		  })
		);
		
		search.addWidget(
		  instantsearch.widgets.refinementList({
		    container: '#ModelFilterHTML',
		    limit: 99,
		    attributeName: "FriendlyModelName"
		  })
		);
		
		search.addWidget(
		  instantsearch.widgets.refinementList({
		    container: '#ColorFilterHTML',
		    attributeName: "FilteredColors"
		  })
		);
		
		
		search.addWidget(
		    instantsearch.widgets.rangeSlider({
		      container: '#MileageFilterRanges',
		      attributeName: 'MileageInt'
			})
		);
		
		search.addWidget(
		    instantsearch.widgets.rangeSlider({
		      container: '#PriceRangeFilterHtml',
		      attributeName: 'PriceInt'
			})
		);
		
		search.addWidget(
		    instantsearch.widgets.rangeSlider({
		      container: '#EngineSizeRangeFilterHtml',
		      attributeName: 'EngineSizeCC'
			})
		);
		
		search.addWidget(
		    instantsearch.widgets.pagination({
		      container: '#PaginationTop',
		      scrollTo: '#muisearch',
		    })
		);
		
		search.addWidget(
		    instantsearch.widgets.pagination({
		      container: '#PaginationBottom',
		      scrollTo: '#muisearch',
		    })
		);
		
		search.addWidget(
		  instantsearch.widgets.clearAll({
		    container: '#ClearFilterBtn',
		    templates: {
		      link: 'Clear all filters'
		    },
		    cssClasses: {
		      root: 'btn btn-block btn-default'
		    },
		    autoHideContainer: true
		  })
		);
		
		
		search.start();
	}	
	
	function ClearFilters() {
		$('.FilterOptionsContent input').attr('checked', false); 
		window.history.pushState('page2', 'Our Inventory', Globals.WebsitePath() + 'inventory');
		//http://localhost/DillonBrothersMain/inventory
	}
	
	function ChangeOrder(element) {
		$("#OrderByValue").val($(element).val());
		$("#LoadingSpinnerObject").show();
		var postData = $("#FilterInventory").serialize();
		var RowNumber = $("#RowNumber").val();
		$("#CurrentInventoryList").html('');
		
		Globals.AjaxPost(Globals.WebsitePath() + 'ajax/toggleInventoryList/' + RowNumber + '/1/false', postData, ".loadingIndicator", 
				function(responses) {
					$("#LoadingSpinnerObject").hide();
					$("#CurrentInventoryList").html(responses.html);		
								
				}
						
			);
	}
	
	function OpenMobileFilters() {
		$(".FilterInventoryOptions").toggleClass('mobileFilersOpen');
		$(".filterSection, .clearFilters, .resultNumber").toggle();
	}
	
	function CloseMobileFilters() {
		$(".FilterInventoryOptions").removeClass('mobileFilersOpen');
		$(".filterSection, .clearFilters, .resultNumber").hide();
	}
	
	function QuickSearch(element) {
		$("#LoadingSpinnerObject").show();
		$("#CurrentInventoryList").html('');
		$("#hiddenQuickInventorySearch").val($(element).val());
		Globals.AjaxPost(Globals.WebsitePath() + 'ajax/QuickInventorySearch', {quickInventorySearch: $(element).val()}, ".loadingIndicator", 
				function(responses) {
					$("#CurrentInventoryList").html(responses.html);
					$(".paginationLinks").html(responses.Pagination);
					//$("#PageNumber").val(responses.PageNumber);
					//$("#YearsFilterHtml").html(responses.YearFilter);
					//$("#StoresFilterHTML").html(responses.StoreFilter);
					//$("#CategoryFilterHtml").html(responses.CategoryFilter);
					//$("#ManufactureFilterHTML").html(responses.ManufactureFilter);
					//$("#ColorFilterHTML").html(responses.ColorFilter);
					//$("#ConditionFilterHtml").html(responses.ConditionFilter);
					//$("#PriceRangeFilterHtml").html(responses.PriceFilter);
					//$("#EngineSizeRangeFilterHtml").html(responses.EngineSizeCCFilter);
					//$("#MileageFilterRanges").html(responses.MileageFilter);	
				
				
					
					$("strong#count").html(responses.count);
						
					document.title = 'Inventory | Page 1 | Dillon Brothers';
						
					window.history.pushState({"html":responses.html,"pageTitle":'Inventory | Page 1 | Dillon Brothers'},"", Globals.WebsitePath() + 'inventory/page/1');
					$("#LoadingSpinnerObject").hide();
							
				}
						
			);
	}
		
	return {
		OpenFilterList: OpenFilterList,
		OpenBikeDescription: OpenBikeDescription,
		OpenBikeSpecs: OpenBikeSpecs,
		SendToFriend: SendToFriend,
		toggleList: toggleList,
		InitializeInventoryList: InitializeInventoryList,
		PaginationLink: PaginationLink,
		ClearFilters: ClearFilters,
		ChangeOrder: ChangeOrder,
		OpenMobileFilters: OpenMobileFilters,
		CloseMobileFilters: CloseMobileFilters,
		QuickSearch: QuickSearch
	}
}();
