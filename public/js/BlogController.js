var BlogController = function() {
	
	function MultipleErrorResponses(responses) {
		if(responses.ValidationErrors) {
			var validationMessagesArray = responses.ValidationErrors;
			
			var ValidationErrors = responses.ValidationErrors.length;
			for (var i = 0; i < ValidationErrors; i++) {
			   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
			   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
			   $("#inputID" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
			   $("#inputID" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
			}
		}
		if(responses.success) {
			$("#CommentForm").fadeOut(function() {
				$("#SuccessMessage").html(responses.success).fadeIn();
			});
 		}
	}	
	
	
	function InitializeCommentsForm() {
		$("#BlogCommentForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, "#LoginLoading", MultipleErrorResponses)		
		});
	}
		
	return {
		InitializeCommentsForm: InitializeCommentsForm
	}
}();
