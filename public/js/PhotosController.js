var PhotosController = function() {
	//https://github.com/GestiXi/image-scale
	function InitializeAlbumsByYear() {
		$(".PhotoAlbumElement img").lazyload({effect:"fadeIn"})
		
		$(".PhotoAlbumElement img").imageScale();
	}
	
	function InitializeAlbum() {
		$(".photoSingle img").lazyload({effect:"fadeIn"})
		
		$(".photoSingle img").imageScale()
	}
		
	return {
		InitializeAlbumsByYear: InitializeAlbumsByYear,
		InitializeAlbum: InitializeAlbum
	}
}();
