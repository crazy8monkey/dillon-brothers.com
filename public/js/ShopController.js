var ShopController = function() {
	
	function ProductSinglePage() {
		$(document).click(function (e) {
			if ($(e.target).closest('.AddReviewForm, .AddReviewButton').length === 0) {
		    	$("#AddReviewForm").hide();
		    }
		});
		
		
		$("#SendToCartForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, "#LoginLoading", function(responses) {
				if(responses.ValidationErrors) {
					var validationMessagesArray = responses.ValidationErrors;
					
					var ValidationErrors = responses.ValidationErrors.length;
					for (var i = 0; i < ValidationErrors; i++) {
					   $("#" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
					   $("#" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
					}
				}
				
				if(responses.results) { 
					$("#AddedToCartMessage").fadeIn().delay(1500).fadeOut('slow');
					$("#CartCount").html(responses.results.count);
				}
			})		
		});
		
		
		$("#NewReviewForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, "#LoginLoading", function(responses) {
				if(responses.ValidationErrors) {
					var validationMessagesArray = responses.ValidationErrors;
					
					var ValidationErrors = responses.ValidationErrors.length;
					for (var i = 0; i < ValidationErrors; i++) {
						$("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);
					    $("#inputID" + validationMessagesArray[i].inputID + " input[type='text']").css("border", '1px solid #c30000');
					    $("#inputID" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
					    
					}
				}
				
				if(responses.ReviewedSaved) {
					$("#AddReviewFormContent").fadeOut(function() {
						$("#ReviewContentAdded").fadeIn();
						
						setTimeout(function(){
							location.reload();
						}, 2000);	
					})
				}
			})		
		});
		
	}
	
	function NewCredentialsForm() {
		$("#NewCredentiaislForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, "#LoginLoading", function(responses) {
				if(responses.ValidationErrors) {
					var validationMessagesArray = responses.ValidationErrors;
					
					var ValidationErrors = responses.ValidationErrors.length;
					for (var i = 0; i < ValidationErrors; i++) {
					   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
					   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
					}
				}
				
				if(responses.AccountSaved) { 
					$("#SavedForm").fadeIn();
					setTimeout(function(){
						window.location.assign(Globals.WebsitePath() + 'shop'); 
					}, 2000);	
				}
			})		
		});
	}	
	
	function RemoveValidationErrorMessages() {
		removeValidationError('.billingAddress', '#BillingAddressMessage');
		removeValidationError('.billingCity', '#BillingCityMessage');
		removeValidationError('.billingState', '#BillingStateMessage');
		removeValidationError('.billingZip', '#BillingZipMessage');
		removeValidationError('.CardHolderName', '#CardHolderNameMessage');
		removeValidationError('.CreditCardNumber', '#BillingCreditCardNumberMessage');
		removeValidationError('.ExpirationMonth', '#BillingExpirationMonthMessage');
		removeValidationError('.ExpirationYear', '#BillingExpirationYearMessage');
		removeValidationError('.CVCNumber', '#BillingCVCMessage');
	}
	
	function removeValidationError (inputElement, message) {
		$(inputElement).on("change", function() {
			$(message).remove();
			$(inputElement).css('border-color', '');
		})
		
	}
	
	function ValidateInput(element, id, ErrorMessage, placement) {
		if($(element).val() == "") {
	 		if(document.getElementById(id) == null) {
		   		$("<div class='errorMessageText' id='" + id + "'>" + ErrorMessage + "</div>").insertAfter(placement);
		   	}
		   	$(element).css('border-color', "#c30000");
            return false;
		} else {
			$("#" + id).remove();
			$(element).css('border-color', "");
            return true;
		}
	}
	
	function ValidateDropDown(element, id, ErrorMessage, placement) {
		if($(element).val() == 0) {
		   	if(document.getElementById(id) == null ) {
		   		$("<div class='errorMessageText' id='" + id + "'>" + ErrorMessage + "</div>").insertAfter(placement);
		   	}
		   	$(element).css('border-color', "#c30000");
            return false;
		}
		else {
			$("#" + id).remove();
			$(element).css('border-color', "");
            return true;
		}
	}	
	
	function ValidateForm() {
		var isValid = true;
		
		isValid = ValidateInput('.billingAddress', 'BillingAddressMessage', 'Please type in Your Billing Address', '.billingAddress') && isValid;
		isValid = ValidateInput('.billingCity', 'BillingCityMessage', 'Please type in Your Billing City', '.billingCity') && isValid;
		isValid = ValidateDropDown('.billingState', 'BillingStateMessage', 'Please type in Your Billing State', '.billingState') && isValid;
		isValid = ValidateInput('.billingZip', 'BillingZipMessage', 'Please type in Your Billing Zip', '.billingZip') && isValid;
		isValid = ValidateInput('.CardHolderName', 'CardHolderNameMessage', 'Please type in Card Holder Name', '.CardHolderName') && isValid;
		isValid = ValidateInput('.CreditCardNumber', 'BillingCreditCardNumberMessage', 'Please type in Your Credit Card Number', '.CreditCardNumber') && isValid;
		isValid = ValidateDropDown('.ExpirationMonth', 'BillingExpirationMonthMessage', 'Please type in Your Expiration Month', '.ExpirationMonth') && isValid;
		isValid = ValidateDropDown('.ExpirationYear', 'BillingExpirationYearMessage', 'Please type in Your Expiration Year', '.ExpirationYear') && isValid;
		isValid = ValidateInput('.CVCNumber', 'BillingCVCMessage', 'Please type in Your Credit Card CVC Number', '.CVCNumber') && isValid;
		return isValid;
	}
	
		
	function AccountSinglePage() {
		$("#AccountForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, "#LoginLoading", function(responses) {
				if(responses.ValidationErrors) {
					var validationMessagesArray = responses.ValidationErrors;
					
					var ValidationErrors = responses.ValidationErrors.length;
					for (var i = 0; i < ValidationErrors; i++) {
					   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
					   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
					}
				}
				
				if(responses.SecurityTokenPresent == false) {
						StripeHandler.SetStripeVariables($('#AccountForm'), //form
														 $('.billingAddress'), //addressValue
														 $('.billingCity'), //cityValue
														 $('.billingState'), //provenceValue
														 $('.billingZip'), //postalcodeValue
														 $('.CardHolderName'), //CardHolderNameValue
														 $('.CreditCardNumber'), //CardNumberValue
														 $('.CVCNumber'), //CardCVCValue
														 $('.ExpirationMonth'), //CardExpirationMonthValue
														 $('.ExpirationYear'),
														 true); //CardExpirationYearValue	
						RemoveValidationErrorMessages();
						if(ValidateForm() && StripeHandler.ValidateExpiration() && StripeHandler.ValidateCardNumber() && StripeHandler.ValidateCVC()) {
							StripeHandler.CreateToken();
						}
						
				}
				
				if(responses.AccountSaved) { 
					$("#SignUpForm").fadeOut(function() {
						$("#CongratsText").fadeIn();
					});
					$("#ecommerceLoadingObject").fadeOut();
					
					setTimeout(function(){
						window.location.assign(Globals.WebsitePath() + 'shop'); 
					}, 2000);					
				}
				
				
				
			})		
		});
	}	
	
	function AccountSingleInfoUpdate() {
		$(".accountPhoneNumber").mask("(999) 999-9999",{placeholder:" "});
		$("#AccountFormInfo").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
		
			Globals.AjaxPost(formURL, postData, "#LoginLoading", function(responses) {
				if(responses.ValidationErrors) {
					var validationMessagesArray = responses.ValidationErrors;
					
					var ValidationErrors = responses.ValidationErrors.length;
					for (var i = 0; i < ValidationErrors; i++) {
					   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
					   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
					}
				}
				
				if(responses.AccountUpdated) {
					$("#SavedBasicInfo").fadeIn().delay(2000).fadeOut()
				}
				
			})		
		});
	}
	
	function AccountSingleBillingUpdate() {
		$("#AccountBillingUpdate").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, "#LoginLoading", function(responses) {
				
				if(responses.SecurityTokenPresent == false) {
					StripeHandler.SetStripeVariables($('#AccountBillingUpdate'), //form
											 $('.billingAddress'), //addressValue
											 $('.billingCity'), //cityValue
											 $('.billingState'), //provenceValue
											 $('.billingZip'), //postalcodeValue
											 $('.CardHolderName'), //CardHolderNameValue
											 $('.CreditCardNumber'), //CardNumberValue
											 $('.CVCNumber'), //CardCVCValue
											 $('.ExpirationMonth'), //CardExpirationMonthValue
											 $('.ExpirationYear'),
											 true); //CardExpirationYearValue	
		    		// Disable the submit button to prevent repeated clicks
		    		RemoveValidationErrorMessages();	
		    		//stripe value checking
		    		if (ValidateForm() && StripeHandler.ValidateExpiration() && StripeHandler.ValidateCardNumber() && StripeHandler.ValidateCVC()) {			
			          StripeHandler.CreateToken();
		           } else {
		           		alert('Please fix the issues on the screen')
		           }
						
				}
				
				if(responses.BillingSaved) {
					$("#ecommerceLoadingObject").fadeOut(function() {
						$("#BillingUpdate").fadeIn();	
						$('.billingAddress, .billingCity, .billingZip, .CardHolderName, .CreditCardNumber, .CVCNumber').val('');
						$(".billingState, .ExpirationMonth,.ExpirationYear").val(0);
					});
					
				}
			})		
		});
	}
	
	function LoginFormPage() {
		$("#ShoppingCenterLoginForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, "#LoginLoading", function(responses) {
				if(responses.errorMessage) {
					$("#LoginErrorMessages").hide().fadeIn();
					$("#LoginErrorMessages .errorMessageTextPlacement").html(responses.errorMessage)
						
				}
				
				if(responses.redirect) {
					$("#LoginErrorMessages").hide().fadeIn();
					$("#LoginErrorMessages .errorMessageTextPlacement").removeClass('alert-danger');
					$("#LoginErrorMessages .errorMessageTextPlacement").addClass('alert-success');
					$("#LoginErrorMessages .errorMessageTextPlacement").html('Logging you in...');
					
					setTimeout(function(){
						window.location.replace(responses.redirect); 
					}, 2000);
				}
				
			})		
		});
	}
	
	function CloseNewReviewForm() {
		$("#AddReviewForm").hide();
		$("#ReviewContentAdded").hide();	
		$("#AddReviewFormContent").show();
		$("#AddReviewFormContent input").removeAttr('style');
	} 
	
	function OpenAddReviewPopup() {
		$("#AddReviewForm").show();
	}
	
	function getTemplate(templateName) {
	  return document.querySelector(`#${templateName}-template`).innerHTML;
	}	
	
	function ShoppingList(liveSite) {
		var indexNameText;
		
		if(liveSite == true) {
			indexNameText = 'ShoppingCartInventory';
		} else {
			indexNameText = 'ShoppingCartInventoryTest';
		}
		
		var search = instantsearch({
			// Replace with your own values
			appId: '943L0R18H9',
			apiKey: 'eba4ec01de1347a0bff85f56d79ad18d', // search only API key, no ADMIN key
			indexName: indexNameText,
			urlSync: true
		});
	
		search.addWidget(
		    instantsearch.widgets.hits({
		      container: '#ShoppingCartItems',
		      templates: {
		      	item: getTemplate('productSingle')
		      }
		    })
		);	

		search.addWidget(
			instantsearch.widgets.searchBox({
		    	container: '#shopsearch',
		    	placeholder: 'Search Anything'
		  	})
		);	
		
		search.start();		
		//
		
	}
		
	return {
		ProductSinglePage: ProductSinglePage,
		NewCredentialsForm: NewCredentialsForm,
		AccountSinglePage: AccountSinglePage,
		AccountSingleInfoUpdate: AccountSingleInfoUpdate,
		AccountSingleBillingUpdate: AccountSingleBillingUpdate,
		LoginFormPage: LoginFormPage,
		OpenAddReviewPopup: OpenAddReviewPopup,
		CloseNewReviewForm: CloseNewReviewForm,
		ShoppingList: ShoppingList 
	}
}();
