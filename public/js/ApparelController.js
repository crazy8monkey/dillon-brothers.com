var ApparelController = function() {
	
	function Initialize() {
		$("#ApparelDeptForm").submit(function(e){
			e.preventDefault();
			$("#LoadingSpinnerObject").show();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, "#LoginLoading", function(responses) {
				if(responses.ValidationErrors) {
					$("#LoadingSpinnerObject").hide();
					alert("Please fix the errors on the screen");
					var validationMessagesArray = responses.ValidationErrors;
					
					var ValidationErrors = responses.ValidationErrors.length;
					for (var i = 0; i < ValidationErrors; i++) {
					   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
					   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
					   $("#inputID" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
					   $("#inputID" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
					}
				}
				if(responses.testResponse) {
		 			alert("Thank you for filling out this form, someone will be in contact with you about your response");
		 			location.reload();
		 		}
			});		
		});
		
		$(".partsHomePhone, .partsCellPhone").mask("(999) 999-9999",{placeholder:" "});
		
	}	
	
	function ShowServiceDetailInfo(element) {
		$("#ServiceDetailInfo").show();
		
		if($(element).val() == 1) {
			$("#PastServiceInfo").show();
		} else {
			$("#PastServiceInfo").hide();
		}
	}
	
	function ShowApparelDeptForm() {
		$("#ApparelDeptFormContent").show();
	}
		
	return {
		Initialize: Initialize,
		ShowServiceDetailInfo: ShowServiceDetailInfo,
		ShowApparelDeptForm: ShowApparelDeptForm
	}
}();
