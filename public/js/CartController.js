var CartController = function() {
	
	function MoneyFormat(NumberValue) {
		return '$' + NumberValue.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	}
	
	function RemoveValidationErrorMessages() {
		removeValidationError('.billingAddress', '#BillingAddressMessage');
		removeValidationError('.billingCity', '#BillingCityMessage');
		removeValidationError('.billingState', '#BillingStateMessage');
		removeValidationError('.billingZip', '#BillingZipMessage');
		removeValidationError('.CardHolderName', '#CardHolderNameMessage');
		removeValidationError('.CreditCardNumber', '#BillingCreditCardNumberMessage');
		removeValidationError('.ExpirationMonth', '#BillingExpirationMonthMessage');
		removeValidationError('.ExpirationYear', '#BillingExpirationYearMessage');
		removeValidationError('.CVCNumber', '#BillingCVCMessage');
		removeValidationError('.CreditCardNumber', '#paymentCreditCardNumberInvalid');
	}
	
	function removeValidationError (inputElement, message) {
		$(inputElement).on("change", function() {
			$(message).remove();
			$(inputElement).css('border-color', '');
		})
		
	}
	
	function RemoveCartItem(key, element) {
		
		
		$.getJSON(Globals.WebsitePath() +  "ajax/removefromcart/" + key, function( data ) {
			$("#CartCount").html(data.results.count);
			$("#CartTotal").html("$" + data.results.total);
			location.reload();
		});
	}
	
	function ShowShippingForm(Element) {
		if($(Element).val() == 1) {
			$("#ShippingInformationElement").show();
		} else {
			$("#ShippingInformationElement").hide();
			$("#AddressMessage, #CityMessage, #StateMessage, #ZipMessage").remove();
			$("#ShippingInformationElement input, #ShippingInformationElement select").removeAttr('style');
		}
	}
	
	function ValidateInput(element, id, ErrorMessage, placement) {
		if($(element).val() == "") {
	 		if(document.getElementById(id) == null) {
		   		$("<div class='errorMessageText' id='" + id + "'>" + ErrorMessage + "</div>").insertAfter(placement);
		   	}
		   	$(element).css('border-color', "#c30000");
            return false;
		} else {
			$("#" + id).remove();
			$(element).css('border-color', "");
            return true;
		}
	}
	
	
	function ValidateDropDown(element, id, ErrorMessage, placement) {
		if($(element).val() == 0) {
		   	if(document.getElementById(id) == null ) {
		   		$("<div class='errorMessageText' id='" + id + "'>" + ErrorMessage + "</div>").insertAfter(placement);
		   	}
		   	$(element).css('border-color', "#c30000");
            return false;
		}
		else {
			$("#" + id).remove();
			$(element).css('border-color', "");
            return true;
		}
	}

	function ValidateEmail() {
		var ShippingEmail = $(".shippingEmail").val();
		var emailFilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (!emailFilter.test(ShippingEmail)) {
			if(document.getElementById("emailNotValid") == null) {
				$("<div class='errorMessageText' id='emailNotValid'>Email is not valid</div>").insertAfter(".shippingEmail");
			}
			$('.shippingEmail').css('border-color', "#c30000");
			return false
		}
		else {
			$("#emailNotValid").remove();
			$('.shippingEmail').css('border-color', "");
			return true;
		}
		
	}	

	function ValidateRadioInput(Element, id, ErrorMessage, placement) {
		var radios = $(Element + ":checked");
		
		
		if(radios.length == 0) {
			if(document.getElementById(id) == null) {
		   		$("<div class='errorMessageText' id='" + id + "'>" + ErrorMessage + "</div>").insertAfter(placement);
		   	}
			return false;
		} else {
			$("#" + id).remove();
			return true;
		}
	}
	
	
	function ValidateForm() {
		var isValid = true;
				
		isValid = ValidateInput('.CardHolderName', 'CardHolderNameMessage', 'Please type in Card Holder Name', '.CardHolderName') && isValid;
		isValid = ValidateInput('.billingAddress', 'BillingAddressMessage', 'Please type in Your Billing Address', '.billingAddress') && isValid;
		isValid = ValidateInput('.billingCity', 'BillingCityMessage', 'Please type in Your Billing City', '.billingCity') && isValid;
		isValid = ValidateDropDown('.billingState', 'BillingStateMessage', 'Please type in Your Billing State', '.billingState') && isValid;
		isValid = ValidateInput('.billingZip', 'BillingZipMessage', 'Please type in Your Billing Zip', '.billingZip') && isValid;
		isValid = ValidateInput('.CreditCardNumber', 'BillingCreditCardNumberMessage', 'Please type in Your Credit Card Number', '.CreditCardNumber') && isValid;
		isValid = ValidateDropDown('.ExpirationMonth', 'BillingExpirationMonthMessage', 'Please type in Your Expiration Month', '.ExpirationMonth') && isValid;
		isValid = ValidateDropDown('.ExpirationYear', 'BillingExpirationYearMessage', 'Please type in Your Expiration Year', '.ExpirationYear') && isValid;
		isValid = ValidateInput('.CVCNumber', 'BillingCVCMessage', 'Please type in Your Credit Card CVC Number', '.CVCNumber') && isValid;
		return isValid;
	}

	function CheckoutPage(NoSessionLoggedIn) {
		
		$(".phoneNumber").mask("(999) 999-9999",{placeholder:" "});
		RemoveValidationErrorMessages();
		
		$("#CheckoutForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, "#LoginLoading", function(responses) {
				if(responses.ValidationErrors) {
					var validationMessagesArray = responses.ValidationErrors;
					
					var ValidationErrors = responses.ValidationErrors.length;
					for (var i = 0; i < ValidationErrors; i++) {
					   $("#" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
					   $("#" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
					   $("#" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
					}
				}
				
				if(NoSessionLoggedIn == true) {
					if(responses.SecurityTokenPresent == false) {
						RemoveValidationErrorMessages();
						StripeHandler.SetStripeVariables($('#CheckoutForm'), //form
														 $('.billingAddress'), //addressValue
														 $('.billingCity'), //cityValue
														 $('.billingState'), //provenceValue
														 $('.billingZip'), //postalcodeValue
														 $('.CardHolderName'), //CardHolderNameValue
														 $('.CreditCardNumber'), //CardNumberValue
														 $('.CVCNumber'), //CardCVCValue
														 $('.ExpirationMonth'), //CardExpirationMonthValue
														 $('.ExpirationYear'),
														 true); //CardExpirationYearValue
						
						if (ValidateForm() && StripeHandler.ValidateExpiration() && StripeHandler.ValidateCardNumber() && StripeHandler.ValidateCVC()) {			
	    					StripeHandler.CreateToken();
        				}								 
														 	
					}
				}
				
				if(responses.redirect) { 
					window.location.assign(responses.redirect); 	
				}
			})		
		});
		
	}	
		
	function UpdateShippingRates(element) {
		var CartTotal = $("#CartTotalNumber").html().split("$").pop();
		if($(element).val() == 1) {
			
			$.getJSON(Globals.WebsitePath() +  "ajax/getrates", function( data ) {
				
				$('#ShippingRates').hide().html(MoneyFormat(parseFloat(data))).fadeIn('fast');	
				
				$('#GrandTotalRates').hide().html(MoneyFormat(+parseFloat(data) + +CartTotal)).fadeIn('fast');	
			});			
		} else {
			$('#ShippingRates').hide().html(MoneyFormat(parseFloat(0.00))).fadeIn('fast');	
				
			$('#GrandTotalRates').hide().html(MoneyFormat(+CartTotal)).fadeIn('fast');
		}

	}	
	
	function RemoveValidation(id) {
		
	}
	
	function Processing() {
		
		$("#ConfirmPage").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, "#LoginLoading", function(responses) {
				
				
				if(responses.redirect) { 
					$("#ecommerceLoadingObject").show();
					window.location.assign(responses.redirect); 	
				}
			})		
		});
		
	}
		
	function OpenMobileCartSummary() {
		$("#CheckoutCartSummary").toggleClass('MobileCartSummaryOpen');
	}	
	
	function CloseMobileCartSummary() {
		$("#CheckoutCartSummary").removeClass('MobileCartSummaryOpen');
	}	
		
	function InitializeCartPage() {
		$('.featuredProducts').slick({
		  dots: true,
		  infinite: false,
		  speed: 300,
		  slidesToShow: 4,
		  slidesToScroll: 4,
		  autoplaySpeed: 2000,
		  responsive: [
		    {
		      breakpoint: 1200,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3,
		        dots: true
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
		});
	} 	
		
	return {
		RemoveCartItem: RemoveCartItem,
		ShowShippingForm: ShowShippingForm,
		CheckoutPage: CheckoutPage,
		UpdateShippingRates: UpdateShippingRates,
		Processing: Processing,
		OpenMobileCartSummary: OpenMobileCartSummary,
		CloseMobileCartSummary: CloseMobileCartSummary,
		RemoveValidation: RemoveValidation,
		InitializeCartPage: InitializeCartPage
	}
}();
