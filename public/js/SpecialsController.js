jQuery.fn.extend({
	    toggleText: function (a, b){
	        var that = this;
	            if (that.text() != a && that.text() != b){
	                that.text(a);
	            }
	            else
	            if (that.text() == a){
	                that.text(b);
	            }
	            else
	            if (that.text() == b){
	                that.text(a);
	            }
	        return this;
	    }
});

var SpecialsController = function() {
	
	function OpenIncentive(Element) {
		$(Element).parent().next().toggle();	
		$(".incentiveContent").not($(Element).parent().next()).hide();
		
		$(Element).find('.toggleButton').toggleText('-', '+');
		$(".toggleButton").not($(Element).find(".toggleButton")).html('+');
	}
	
	function OpenInventory(stock) {
		newwindow=window.open(Globals.WebsitePath() + 'specials/inventory/' + stock ,'name','height=800,width=800');
		if (window.focus) {
			newwindow.focus()
		}
		return false;
	}
	
	function GoToInventory(url) {
		opener.location = url;
		window.close();
					
	}
	
	return {
		OpenIncentive: OpenIncentive,
		OpenInventory: OpenInventory,
		GoToInventory: GoToInventory
	}
}();
