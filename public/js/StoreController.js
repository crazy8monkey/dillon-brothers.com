jQuery.fn.extend({
	    toggleText: function (a, b){
	        var that = this;
	            if (that.text() != a && that.text() != b){
	                that.text(a);
	            }
	            else
	            if (that.text() == a){
	                that.text(b);
	            }
	            else
	            if (that.text() == b){
	                that.text(a);
	            }
	        return this;
	    }
});

var StoreController = function() {
	
	function InitializeSpecialSingle() {
		
	}
	
	function OpenSpecialLine(element) {
		$(element).find(".content").toggle();
		$(element).find('.expandIndicator').toggleText('-', '+');
		
		$(".VehicleSingle .content").not($(element).find(".content")).hide();
		$(".VehicleSingle .expandIndicator").not($(element).find(".expandIndicator")).html('+');
		//$(element).find('.expandIndicator').toggleText('-', '+');
	}
	
	return {
		InitializeSpecialSingle: InitializeSpecialSingle,
		OpenSpecialLine: OpenSpecialLine
	}
}();
