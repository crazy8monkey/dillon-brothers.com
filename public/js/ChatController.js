var ChatController = function() {
	
	var $chatWindow = $('#messagesLogged');
	var chatClient;
	var workerClient;
	var workspaceClient;
	var chatChannel;
	var taskQueueClient;
	
	var ChannelName;
	var uniqueChannelName;
	var CustomerName;
	var SalesmanName;
	
	var CurrentChatCount;
	
	var SalesManTwilioMemberID;
	
	
	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
	
	//channel name
	function SetChannelName(name) {
		ChannelName = name;
	}
	
	function GetChannelName() {
		return ChannelName
	}
	
	//unique name
	function SetUniqueChannelName(name) {
		uniqueChannelName = name;
	}
	
	function GetUniqueChannelName() {
		return uniqueChannelName;
	}
	
	//current count
	function SetCurrentCount(count) {
		CurrentChatCount = count;
	}
	
	function GetCurrentCount() {
		return CurrentChatCount;
	}
	
	//customer name
	function SetCustomerName(name) {
		CustomerName = name;
	}
	
	function GetCustomerName() {
		return CustomerName;
	}	
	
	//salesman name
	function SetSalesmanName(name) {
		SalesmanName = name;
	}
	
	function GetSalesmanName() {
		return SalesmanName;
	}
	
	
	function ScrollToBottom() {
		$('#ChatMessageContainer').scrollTop($('#ChatMessageContainer')[0].scrollHeight);
	}
	
	function InitializeProgrammableChat(CurrentCustomerName, WorkerID) {
		//workspaceToken
		
		
		$.getJSON(Globals.WebsitePath() + 'ajax/twiliotokens', {
	        device: 'browser',
	        customerName: CurrentCustomerName,
	        workerID: WorkerID
	    }, function(data) {
	    	chatClient = new Twilio.Chat.Client(data.chatchanneltoken);
        	workspaceClient = new Twilio.TaskRouter.Workspace(data.workspaceToken);
        	taskQueueClient = new Twilio.TaskRouter.TaskQueue(data.taskqueuetoken);
        	chatClient.getSubscribedChannels().then(JoinChannel);
        	workerClient = new Twilio.TaskRouter.Worker(data.workerToken);
        	
        	
        	
        	//console.log(workspaceClient);
        	//console.log(taskQueueClient);
	    });
	    
	    
	    var $input = $('#UserNewMessage');
		$input.on('keydown', function(e) {
			if (e.keyCode == 13) {
				//$.post(Globals.WebsitePath() + 'ajax/sendmessage', {NewMessage: $input.val(), taskID: $('#taskID').val()} );
				chatChannel.sendMessage($input.val(), {'taskID': getParameterByName('id'), 'fromCustomer': 1});
			   	$input.val('');
		   	}
		});
		
		$("button.blueButton").click(function() {
			chatChannel.sendMessage($input.val(), {'taskID': getParameterByName('id'), 'fromCustomer': 1});
			$input.val('');
		});
	}
	
	
	function InitializeTaskRouterWorker() {
		$.getJSON(Globals.WebsitePath() + 'ajax/twiliotokens', {
	        device: 'browser'
	    }, function(data) {
	        
	        registerTaskRouterCallbacks()
	    });
	}
	
	function initializeConversation(customerName, channelName, uniqueChannelName, currentMessageCount, salesmanName, WorkerID) {
		InitializeProgrammableChat(customerName,WorkerID);
		SetChannelName(channelName);
		SetUniqueChannelName(uniqueChannelName);
		SetCurrentCount(currentMessageCount);
		SetCustomerName(customerName);
		SetSalesmanName(salesmanName);
		ScrollToBottom();
		//AddNewMessage();	
	}	
	
	function print(infoMessage, asHtml) {
       var $msg = $('<div class="info">');
       if (asHtml) {
       		$msg.html(infoMessage);
       } else {
       		$msg.text(infoMessage);
       }
       $('#messagesLogged').append($msg);
       
    }
	
	
	function JoinChannel() {
        // Get the general chat channel, which is where all the messages are
        // sent in this simple application
        var promise = chatClient.getChannelByUniqueName(GetUniqueChannelName());
        promise.then(function(channel) {
            chatChannel = channel;
            //console.log('Found general channel:' + uniqueChannelName);
            console.log(chatChannel);
            setupChannel();
        });
    }
	
	function setupChannel() {
		var WorkSpaceCurrentTaskID;
		
        // Join the general channel
        chatChannel.join().then(function(channel) {
        	var CurrentIPAddress;
        	var NotificationBody;
        	$.get("http://ipinfo.io", function(response) {
			    CurrentIPAddress = response.ip;
			    console.log(CurrentIPAddress);
			    //alert(response.ip);
			}, "jsonp");
		    
        	
        	if(GetCurrentCount() == 0) {
        		print('<div class="welcomeMessage">Welcome To ' + GetChannelName() + '!, ' + GetSalesmanName() + ' be assisting you shortly!</div>', true);	
        		NotificationBody = "You have a new Chat Message Session";
        		
        	} else {
        		NotificationBody = GetCustomerName() + "is back in his chat session";
        		print('<div class="welcomeMessage">Welcome Back ' + GetCustomerName() + '!</div>' , true);	
        	}
        	ScrollToBottom();
        	console.log('66.37.246.122');
        	
        	//if(CurrentIPAddress == '66.37.246.122') {
        		//var notification = new Notification('Notification title', {
			//		body: NotificationBody,
			//	});
	        	
	        //    notification.show();		
        	//}
        	
        });

        // Listen for new messages sent to the channel
        chatChannel.on('messageAdded', function(message) {
        	if(message.attributes.taskID == getParameterByName('id')) {	
        		//closing the message
        		if(message.attributes.taskComplete != null) {
	        		print('<div class="welcomeMessage">' + GetSalesmanName() + ' has closed this conversation have a nice day!</div>', true);	
	        		setTimeout(function(){
						window.close(); 
					}, 1000);
					
	        	} else {
	        		printMessage(message.author, message.body, message);		
	        	}
	        	ScrollToBottom();
        	}
        	
        });
        
        chatChannel.on('typingStarted', function(member) {
		  console.log(member.identity + 'is currently typing.');
		});
        
        
        workspaceClient.on("connected", function() {
		    console.log("Websocket has connected");
		});
        
        
       
	   
       
        
        taskQueueClient.on("ready", function(taskQueue) {
		  //print('<div class="welcomeMessage">has ended the conversation</div>', true);
		  //console.log(taskQueue.sid)                // 'WQxxx'
		  //console.log(taskQueue.friendlyName)       // 'Simple FIFO Queue'
		  //console.log(taskQueue.targetWorkers)      // '1==1'
		  //console.log(taskQueue.maxReservedWorkers) // 20
		});
        
       
        
    }
    
    function printMessage(fromUser, message, msgObj) {
    	var className;
        if(msgObj.attributes.fromCustomer == 1) {
        	className = 'msgBubbleCustomer';
        } else {
        	className = 'msgBubbleDillonSalesMan';
        }
        
         var newMessageObj = '<div style="clear:both; margin-bottom:10px;">' +
										'<div class="' + className + '">' +
											 message +
										'</div>' + 
										'<div style="clear:both"></div>' +
									'</div>';
        
     
        $('#messagesLogged').append(newMessageObj);
        ScrollToBottom();
    }
	
	
	function initializeStartForm() {
		$("#StartChatForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			Globals.AjaxPost(formURL, postData, "#uploadingPhotosText", Globals.MultipleErrorResponses)	
		});
		
		$(".yourPhoneNumber").mask("(999) 999-9999",{placeholder:" "});
	}		
			
			
			
	return {
		initializeConversation: initializeConversation,
		initializeStartForm: initializeStartForm
	}
}();
