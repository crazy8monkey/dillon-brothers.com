var DonationController = function() {
	
	function Initialize() {
		$("#DonationRequestForm").submit(function(e){
			e.preventDefault();
			$("#LoadingSpinnerObject").show();
			
			
			var formURL = $(this).attr("action");
			var postData = new FormData($(this)[0]);
			
			
			Globals.AjaxFileUpload(formURL, postData, "#LoginLoading", function(responses) {
				if(responses.ValidationErrors) {
					$("#LoadingSpinnerObject").hide();
					alert("Please fix the errors on the screen");
					var validationMessagesArray = responses.ValidationErrors;
					
					var ValidationErrors = responses.ValidationErrors.length;
					for (var i = 0; i < ValidationErrors; i++) {
					   $("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(validationMessagesArray[i].errorMessage);	
					   $("#inputID" + validationMessagesArray[i].inputID + " input").css("border", '1px solid #c30000');
					   $("#inputID" + validationMessagesArray[i].inputID + " select").css("border", '1px solid #c30000');
					   $("#inputID" + validationMessagesArray[i].inputID + " textarea").css("border", '1px solid #c30000');
					}
				}
				if(responses.testResponse) {
		 			alert("Thank you for filling out this form, someone will be in contact with you about your response");
		 			location.reload();
		 		}
		 		
		 		
				
			})		
		});
		
		$(".donationDateofEvent").datepicker();
		$(".donationEventContactPhone").mask("(999) 999-9999",{placeholder:" "});
		
	}	
		
	return {
		Initialize: Initialize
	}
}();
