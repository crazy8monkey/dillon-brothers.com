var ContactController = function() {
	
	function OpenMotorSportHours(element) {
		$("#MotorsportHours").toggle();
		$(element).find(".expandText").toggleText('-', '+');	
		$("#HarleyHours, #IndianHours").hide();
		$(".expandText").not($(element).find('.expandText')).html('+');
	}
	
	function OpenHarleyHours(element) {
		$("#HarleyHours").toggle();
		$(element).find(".expandText").toggleText('-', '+');
		$("#MotorsportHours,#IndianHours").hide();
		$(".expandText").not($(element).find('.expandText')).html('+');
	}
	
	function OpenIndianHours(element) {
		$("#IndianHours").toggle();
		$(element).find(".expandText").toggleText('-', '+');	
		$("#HarleyHours, #MotorsportHours").hide();
		$(".expandText").not($(element).find('.expandText')).html('+');
	}
		
	return {
		OpenMotorSportHours: OpenMotorSportHours,
		OpenHarleyHours: OpenHarleyHours,
		OpenIndianHours: OpenIndianHours
	}
}();
