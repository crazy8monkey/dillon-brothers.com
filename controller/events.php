<?php

class Events extends Controller {
	
		
	public function __construct() {
		parent::__construct();
		$this -> view -> js = array(PATH . "public/js/photoswipe.min.js", PATH . "public/js/photoswipe-ui-default.min.js", PATH . "public/js/EventsController.js", PATH . 'public/js/moment.min.js', PATH . 'public/js/fullcalendar.min.js', PATH . 'public/js/gcal.js');
		$this -> view -> css = array(PATH . 'public/css/EventsController.css', PATH . 'public/css/fullcalendar.css');
	}
	
	
	public function index() {
		$currentEvents = new EventsList();
		$pdfList = EventPDF::WithCurrentYear(date("Y", $this -> time -> NebraskaTime()));
		$this -> view -> showPDFLink = $pdfList -> ShowLink;
		$this -> view -> pdfName = $pdfList -> pdfName;
		$this -> view -> title = "Events" . parent::DILLONBROTHERS;
		
		$this -> view -> SchemaMetaData = array("Name" => "Events: Dillon Brothers", 
												"Description" => "Dillon Brothers Events",
												"ImageURL" => PATH . "public/images/DillonBrothers.png");
		
		$this -> view -> TwitterMetaData = array("Title" => "Events: Dillon Brothers", 
												 "Description" => "Dillon Brothers Events",
												 "URL" => PATH . 'events',
												 "ImageURL" => PATH . "public/images/DillonBrothers.png");
												 
		$this -> view -> OpenGraphMetaData = array("Title" => "Events: Dillon Brothers", 
												   "Type" => "Dillon Brothers Events",
												   "URL" => PATH . 'events',
												   "Image" => PATH . "public/images/DillonBrothers.png",
												   "Description" => "",
												   "ArticlePublishTime" => "",
												   "ArticleModifiedTime" => "",
												   "ArticleSection" => "",
												   "ArticleTag" => "");												   
		
		$this -> view -> Canonical = PATH . 'events';
		$this -> view -> LatestEvents = $currentEvents -> LatestEvents();
												 										 
		$this -> view -> startJsFunction = array('EventsController.Initialize("' . PATH . '");');
		$this -> view -> render('events/index');		
	}
	
	public function IsEventExists($eventName) {
		try {
			$eventSingle = Event::WithSEOname($eventName);
			return true;
		} catch (PDOException $e) {
			return false;
		} catch (Exception $e){
            return false;
        }	
	}
	
	
	public function view($eventName) {
		try {
			
			$eventSingle = Event::WithSEOname($eventName);
			
			
			$pageData = new PageData();
			$subEventsList = new EventsList();
				
			$MainTitleName = NULL;
				
			$eventHTMLJSON = NULL;
			$eventID = NULL;
			$eventMetaDataDescription = NULL;
			$eventPublishedDate = NULL;
			$eventModifiedDate = NULL;
			$eventMetaUrl = NULL;
			$eventMetaURL = NULL;
			$eventImageURL = NULL;
				
			$photosSelectedData = NULL;
			$albumsSectedData = NULL;
			$relatedAlbums = NULL;
				
			if($eventSingle -> SuperEvent == 1) {
				$this -> view -> IsSubEvent = true;
				$this -> view -> startJsFunction = array('EventsController.GetSubEventName();');
				$this -> view -> SubEvents = $subEventsList -> GetSubEvents($eventSingle -> GetEventID());
				$this -> view -> title = $eventSingle -> eventName . " Events" . parent::DILLONBROTHERS;
				
					
				$relatedAlbums = $subEventsList -> GetSuperEventRelatedAlbums($eventSingle -> GetEventID());
				
				$eventIDS = NULL;
				$publishedSubDates = array();
				$modifiedSubDates = array();
				foreach($subEventsList -> GetSubEvents($eventSingle -> GetEventID()) as $eventSingleLoop) {
					$eventIDS .= $eventSingleLoop['eventID'] . ', ';
					array_push($publishedSubDates, $eventSingleLoop['publishDateFull']);
					array_push($modifiedSubDates, $eventSingleLoop['modifiedDateFull']);
				}
				sort($publishedSubDates);
				sort($modifiedSubDates);
					
					
				$eventPublishedDate = $publishedSubDates[0];
				$eventModifiedDate = $modifiedSubDates[count($modifiedSubDates)-1];
				
				$photosSelectedData = $pageData -> SuperEventPhotos(rtrim($eventIDS, ', '));
				$albumsSectedData = $pageData -> SuperEventAlbums(rtrim($eventIDS, ', '));
					
				$this -> view -> title = $eventSingle -> eventName;
					
				$eventMetaDataDescription = $eventSingle -> SuperEventDescription;
					 
					
			} else {
				$this -> view -> title = $eventSingle -> eventName .  parent::DILLONBROTHERS;
				$eventMetaDataDescription = $eventSingle -> GoogleEventDescription;
				$this -> view -> pageLayout = json_decode($eventSingle -> GetHtml(), true);
				$photosSelectedData = $pageData -> EventSinglePhotos($eventSingle -> GetEventID());
				$albumsSectedData = $pageData -> EventSingleAlbums($eventSingle -> GetEventID());
				
				$eventPublishedDate = $eventSingle -> PublishedDate;
				$eventModifiedDate = $eventSingle -> ModifiedDate;
				
				$eventMetaUrl = PATH . $eventSingle -> GetSEOUrl();
				
				$relatedAlbums = $subEventsList -> GetEventRelatedAlbums($eventName);
			}
				
				
				
				
			$this -> view -> SchemaMetaData = array("Name" => $eventSingle -> eventName . ": Dillon Brothers", 
													"Description" => $eventMetaDataDescription,
													"ImageURL" => $eventSingle -> GetMainEventPhoto());
		
			$this -> view -> TwitterMetaData = array("Title" => $eventSingle -> eventName . ": Dillon Brothers", 
													 "Description" => $eventMetaDataDescription,
													 "URL" => PATH . $eventName,
													 "ImageURL" => $eventSingle -> GetMainEventPhoto());
												 
			$this -> view -> OpenGraphMetaData = array("Title" => $eventSingle -> eventName . "Events: Dillon Brothers", 
													   "Type" => "place",
													   "URL" => PATH . $eventName,
													   "Image" => $eventSingle -> GetMainEventPhoto(),
													   "Description" => $eventMetaDataDescription,
													   "ArticlePublishTime" => $eventPublishedDate,
													   "ArticleModifiedTime" => $eventModifiedDate,
													   "ArticleSection" => "place",
													   "ArticleTag" => "event");												   
		
			$this -> view -> Canonical = PATH . $eventName;
			
			$this -> view -> MainEventTitle = $eventSingle -> eventName;
			
			$this -> view -> EventSingle = $eventSingle;
				
			$this -> view -> photosSelected = $photosSelectedData;
			$this -> view -> albumsSelected = $albumsSectedData;
			$this -> view -> relatedAlbums = $relatedAlbums;
			
			$this -> view -> render('events/single');	
			
			
			
			return true;
		} catch (PDOException $e) {
			return false;
		} catch (Exception $e){
            return false;
        }
		
		
	}
	
	public function preview($id) {
		$pageData = new PageData();
		$eventSingle = Event::WithID($id);
		
		//$subEvents = new EventsList();
		
		$this -> view -> title = $eventSingle -> eventName;
		
		//$this -> view -> MainEventTitle = $eventSingle -> eventName;
		
		
		//$this -> view -> EventSingle = $eventSingle;
		
		//$this -> view -> SubEvents = $subEvents -> GetSubEvents($eventSingle -> GetEventID());
		
		//$this -> view -> pageLayout = json_decode($eventSingle -> GetHtml(), true);
		
		//$this -> view -> photosSelected = $pageData -> EventSinglePhotos($eventSingle -> GetEventID());
		$this -> view -> render('events/previewSingle');
	}
	
	public function viewalbum($name) {
		$photoAlbum = PhotoAlbum::WithAlbumName($name);
		$photos = new PhotosList();
		$this -> view -> photos = $photos -> PhotosByAlbum($photoAlbum -> albumID);
		
		$photopathBase = NULL;
		
		switch($photoAlbum -> AlbumType) {
			case 1:
				$photopathBase = EVENT_PHOTOS_URL;	
				break;
			case 2:
				$photopathBase = MISC_PHOTO_URL;	
				break;
		}
		
		
		$this -> view -> photoPath = $photopathBase;
		$this -> view -> render('events/album', true);
	}

	public function GetSubEvent() {
		if(isset($_POST['subEventName'])) {
			return $_POST['subEventName'];	
		}	
		
	}

}
?>