<?php

class Chat extends Controller {
		
	public function __construct() {
		parent::__construct();
		
	}
	
	
	public function index() {
		$this -> view -> chatchannel = ChatChannel::WithTwilioChannelID($_GET['channel']);
		$this -> view -> render('chat/index', true);		
	}
	
	public function conversation() {
		$chatConversation = ChatConversation::WithTaskID($_GET['id']);
		$this -> view -> taskDetails = $chatConversation;
		
		$this -> view -> render('chat/conversation', true);
	}
	
	
	public function conversationTest() {
		$this -> view -> render('chat/conversatonTest', true);		
	}
	
	


}
?>