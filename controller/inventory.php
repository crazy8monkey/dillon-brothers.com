<?php

class Inventory extends Controller {
		
	public function __construct() {
		parent::__construct();
		$this -> view -> js = array("https://cdn.jsdelivr.net/instantsearch.js/1/instantsearch.min.js", PATH . "public/js/InventoryController.js");
		$this -> view -> css = array('https://cdn.jsdelivr.net/instantsearch.js/1/instantsearch.min.css', PATH . 'public/css/InventoryController.css');
	}
	
	
	public function index() {
		$this -> view -> title = "Inventory" . parent::DILLONBROTHERS;
		
			//$this -> view -> LatestEvents = $latestEvents -> LatestEvents();
		$this -> view -> SchemaMetaData = array("Name" => "Inventory " . parent::DILLONBROTHERS, 
												"Description" => "Dillon Brothers Photos",
												"ImageURL" => "http://www.dillon-brothers.com/images/dillon-brothers-motorsports-15.png");
		
		$this -> view -> TwitterMetaData = array("Title" => "Inventory " . parent::DILLONBROTHERS, 
												 "Description" => "Dillon Brothers Photos",
												 "URL" => PATH . 'photos',
												 "ImageURL" => "http://www.dillon-brothers.com/images/dillon-brothers-motorsports-15.png");
												 
		$this -> view -> OpenGraphMetaData = array("Title" => "Inventory " . parent::DILLONBROTHERS, 
												   "Type" => "product",
												   "URL" => PATH . 'inventory',
												   "Image" => "",
												   "Description" => "",
												   "ArticlePublishTime" => "",
												   "ArticleModifiedTime" => "",
												   "ArticleSection" => "",
												   "ArticleTag" => "");	
		
		$this -> view -> Canonical = PATH . 'inventory';					
		$this -> view -> bodyClassName = "MainInventoryList";	
		
		$this -> view -> startJsFunction = array('InventoryController.InitializeInventoryList();');
		$this -> view -> render('inventory/index');	
		
	}

	public function page($number) {
		if(!isset($number)) {
			$this -> redirect -> redirectPage(PATH. 'inventory/page/1');
		}
		$inventory = new InventoryList();
		
		
	
							
		$this -> view -> Canonical = PATH . 'inventory/page/'. $number;					
		$this -> view -> bodyClassName = "MainInventoryList";	
		
		if(count($inventory -> AllLiveInventoryNoLimit())) {				
			$this -> view -> LiveInventoryFilters = $inventory -> AllLiveInventoryNoLimit();										   
			$this -> view -> colors = $inventory -> Colors();
			$this -> view -> Mileage = $inventory -> Mileage();
			$this -> view -> PriceRange = $inventory -> PriceRange();
			$this -> view -> EngineSizeCC = $inventory -> EngineSizeCC();
			$this -> view -> filteredColors = $inventory -> Colors();
			$this -> view -> pageNumber = $number;
			$this -> view -> inventoryCount = $inventory -> Count();
			
			$this -> view -> CurrentInventory = $inventory -> AllLiveInventory($number);	
			$this -> view -> Pagination = $inventory -> PaginationLinks();		
			$this -> view -> MobilePagination = $inventory -> MobilePagination();
		}
		
		$this -> view -> startJsFunction = array('InventoryController.InitializeInventoryList();');
		$this -> view -> render('inventory/index');		
	}


	public function new($seoName) {
		$photos = new InventoryPhotoList();	
		$specLabels = new SpecLabelsList();
		$inventoryList = new InventoryList();
		
		$bikeName = explode("-", $seoName);
		$inventoryCurrent = InventoryObject::WithVin(end($bikeName));
		$currentPhotos = $photos -> PhotosByInventory($inventoryCurrent -> inventoryID);
		
		$this -> view -> title = $inventoryCurrent -> GetBikeName() . parent::DILLONBROTHERS;
		$this -> view -> inventorySingle = $inventoryCurrent;
		
		$this -> view -> specgroups = $specLabels -> Groups();
		$this -> view -> speclabels = $specLabels -> ByCategory($inventoryCurrent -> categoryID);
		
		$this -> view -> RelatedInventory = $inventoryList -> RelatedInventory(end($bikeName), 
																			   $inventoryCurrent -> StoreID, 
																			   $inventoryCurrent -> categoryID);
		
		$this -> view -> inventoryPhotos = $currentPhotos;
		
		if(count($currentPhotos) > 0) {
			$mainPhoto = PHOTO_URL . $inventoryCurrent -> VIN . '/'. $currentPhotos[0]['inventoryPhotoName'] . '-l.' . $currentPhotos[0]['inventoryPhotoExt'];
		} else {
			$mainPhoto = PATH . 'public/images/NoPhotoPlacement.png';	
		}
		
		
		
		$this -> view -> SchemaMetaData = array("Name" => $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												"Description" => $inventoryCurrent -> GetMetaDescription(),
												"ImageURL" => $mainPhoto);
		
		$this -> view -> TwitterMetaData = array("Title" => $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												 "Description" => $inventoryCurrent -> GetMetaDescription(),
												 "URL" => $inventoryCurrent -> GetURL(),
												 "ImageURL" => $mainPhoto);
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												   "Type" => "product.item",
												   "URL" => $inventoryCurrent -> GetURL(),
												   "Image" => $mainPhoto,
												   "Description" => $inventoryCurrent -> GetMetaDescription(),
												   "ArticlePublishTime" => $inventoryCurrent -> PublishedTime,
												   "ArticleModifiedTime" => $inventoryCurrent -> ModifiedTime,
												   "ArticleSection" => "Product",
												   "ArticleTag" => "Product");	
		
		$this -> view -> Canonical = $inventoryCurrent -> GetURL();
		$this -> view -> render('inventory/inventorySingle');
	}

	public function used($seoName) {
		$photos = new InventoryPhotoList();	
		$specLabels = new SpecLabelsList();
		$inventoryList = new InventoryList();
		
		$bikeName = explode("-", $seoName);
		$inventoryCurrent = InventoryObject::WithVin(end($bikeName));
		$currentPhotos = $photos -> PhotosByInventory($inventoryCurrent -> inventoryID);
		
		$this -> view -> title = $inventoryCurrent -> GetBikeName() . parent::DILLONBROTHERS;
		$this -> view -> inventorySingle = $inventoryCurrent;
		
		$this -> view -> specgroups = $specLabels -> Groups();
		$this -> view -> speclabels = $specLabels -> ByCategory($inventoryCurrent -> categoryID);
		
		$this -> view -> RelatedInventory = $inventoryList -> RelatedInventory(end($bikeName), 
																			   $inventoryCurrent -> StoreID, 
																			   $inventoryCurrent -> categoryID);
		
		$this -> view -> inventoryPhotos = $currentPhotos;
		
		if(count($currentPhotos) > 0) {
			$mainPhoto = PHOTO_URL . $inventoryCurrent -> VIN . '/'. $currentPhotos[0]['inventoryPhotoName'] . '-l.' . $currentPhotos[0]['inventoryPhotoExt'];
		} else {
			$mainPhoto = PATH . 'public/images/NoPhotoPlacement.png';	
		}
		
		
		
		$this -> view -> SchemaMetaData = array("Name" => $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												"Description" => $inventoryCurrent -> GetMetaDescription(),
												"ImageURL" => $mainPhoto);
		
		$this -> view -> TwitterMetaData = array("Title" => $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												 "Description" => $inventoryCurrent -> GetMetaDescription(),
												 "URL" => $inventoryCurrent -> GetURL(),
												 "ImageURL" => $mainPhoto);
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												   "Type" => "product.item",
												   "URL" => $inventoryCurrent -> GetURL(),
												   "Image" => $mainPhoto,
												   "Description" => $inventoryCurrent -> GetMetaDescription(),
												   "ArticlePublishTime" => $inventoryCurrent -> PublishedTime,
												   "ArticleModifiedTime" => $inventoryCurrent -> ModifiedTime,
												   "ArticleSection" => "Product",
												   "ArticleTag" => "Product");	
		$this -> view -> Canonical = $inventoryCurrent -> GetURL();
		
		$this -> view -> render('inventory/inventorySingle');
	}
	
	public function print($vin) {
		$photos = new InventoryPhotoList();	
		$specLabels = new SpecLabelsList();
		
		$inventoryCurrent = InventoryObject::WithVin($vin);
		
		$this -> view -> inventoryPrint = $inventoryCurrent;
		
		$this -> view -> specgroups = $specLabels -> Groups();
		$this -> view -> speclabels = $specLabels -> ByCategory($inventoryCurrent -> categoryID);
		
		
		
		$this -> view -> SchemaMetaData = array("Name" => "Print " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												"Description" => $inventoryCurrent -> GetMetaDescription() . " Print",
												"ImageURL" => $inventoryCurrent -> GetMainPhoto());
												
												
		$this -> view -> TwitterMetaData = array("Title" => "Print " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												 "Description" => $inventoryCurrent -> GetMetaDescription() . " Print",
												 "URL" => PATH . 'inventory/print/'. $vin,
												 "ImageURL" => $inventoryCurrent -> GetMainPhoto());	
		
		$this -> view -> OpenGraphMetaData = array("Title" => "Print " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												   "Type" => "Product",
												   "URL" => PATH . 'inventory/print/'. $vin,
												   "Image" => $inventoryCurrent -> GetMainPhoto(),
												   "Description" => $inventoryCurrent -> GetMetaDescription() . " Print",
												   "ArticlePublishTime" => $inventoryCurrent -> PublishedTime,
												   "ArticleModifiedTime" => $inventoryCurrent -> ModifiedTime,
												   "ArticleSection" => "Product",
												   "ArticleTag" => "Product");											 

		$this -> view -> Canonical = $inventoryCurrent -> GetURL();
		
		
		$this -> view -> render('inventory/inventoryPrintSingle', true);
		
	}
	
	public function emailfriend($vin) {
		$inventoryCurrent = InventoryObject::WithVin($vin);
		$this -> view -> title = "Email To a Friend: " . $inventoryCurrent -> GetCondition() . ' ' . $inventoryCurrent -> GetBikeName() . parent::DILLONBROTHERS;
		
		
		$this -> view -> SchemaMetaData = array("Name" => "Email " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												"Description" => $inventoryCurrent -> GetMetaDescription() . " Email",
												"ImageURL" => $inventoryCurrent -> GetMainPhoto());
												
												
		$this -> view -> TwitterMetaData = array("Title" => "Email " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												 "Description" => $inventoryCurrent -> GetMetaDescription() . " Email",
												 "URL" => PATH . 'inventory/emailfriend/'. $vin,
												 "ImageURL" => $inventoryCurrent -> GetMainPhoto());	
		
		$this -> view -> OpenGraphMetaData = array("Title" => "Email " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												   "Type" => "Product",
												   "URL" => PATH . 'inventory/emailfriend/'. $vin,
												   "Image" => $inventoryCurrent -> GetMainPhoto(),
												   "Description" => $inventoryCurrent -> GetMetaDescription() . " Email",
												   "ArticlePublishTime" => $inventoryCurrent -> PublishedTime,
												   "ArticleModifiedTime" => $inventoryCurrent -> ModifiedTime,
												   "ArticleSection" => "Product",
												   "ArticleTag" => "Product");											 

		$this -> view -> Canonical = $inventoryCurrent -> GetURL();
		
		$this -> view -> CurrentInventory = $inventoryCurrent;
		$this -> view -> render('inventory/inventorySingleSendToFriend', true);
		
	}

	public function payment($vin) {
		$inventoryCurrent = InventoryObject::WithVin($vin);
		$this -> view -> title = "Payment Calculator: " . $inventoryCurrent -> GetCondition() . ' ' . $inventoryCurrent -> GetBikeName() . parent::DILLONBROTHERS;
		
		
		$this -> view -> SchemaMetaData = array("Name" => "Payment Calculator " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												"Description" => $inventoryCurrent -> GetMetaDescription() . " Payment Calculator",
												"ImageURL" => $inventoryCurrent -> GetMainPhoto());
												
												
		$this -> view -> TwitterMetaData = array("Title" => "Payment Calculator " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												 "Description" => $inventoryCurrent -> GetMetaDescription() . " Payment Calculator",
												 "URL" => PATH . 'inventory/emailfriend/'. $vin,
												 "ImageURL" => $inventoryCurrent -> GetMainPhoto());	
		
		$this -> view -> OpenGraphMetaData = array("Title" => "Payment Calculator " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												   "Type" => "Product",
												   "URL" => PATH . 'inventory/emailfriend/'. $vin,
												   "Image" => $inventoryCurrent -> GetMainPhoto(),
												   "Description" => $inventoryCurrent -> GetMetaDescription() . " Payment Calculator",
												   "ArticlePublishTime" => $inventoryCurrent -> PublishedTime,
												   "ArticleModifiedTime" => $inventoryCurrent -> ModifiedTime,
												   "ArticleSection" => "Product",
												   "ArticleTag" => "Product");											 

		$this -> view -> Canonical = $inventoryCurrent -> GetURL();
		
		$this -> view -> CurrentInventory = $inventoryCurrent;
		$this -> view -> render('inventory/inventorySingleLoadCalculator', true);
	}

	public function onlineoffer($vin) {
		$inventoryCurrent = InventoryObject::WithVin($vin);
		$specialsList = new SpecialsList();
		$this -> view -> title = "Online Offer: " . $inventoryCurrent -> GetCondition() . ' ' . $inventoryCurrent -> GetBikeName() . parent::DILLONBROTHERS;
		
		
		$this -> view -> SchemaMetaData = array("Name" => "Email " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												"Description" => $inventoryCurrent -> GetMetaDescription() . " Email",
												"ImageURL" => $inventoryCurrent -> GetMainPhoto());
												
												
		$this -> view -> TwitterMetaData = array("Title" => "Email " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												 "Description" => $inventoryCurrent -> GetMetaDescription() . " Email",
												 "URL" => PATH . 'inventory/onlineoffer/'. $vin,
												 "ImageURL" => $inventoryCurrent -> GetMainPhoto());	
		
		$this -> view -> OpenGraphMetaData = array("Title" => "Email " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												   "Type" => "Product",
												   "URL" => PATH . 'inventory/onlineoffer/'. $vin,
												   "Image" => $inventoryCurrent -> GetMainPhoto(),
												   "Description" => $inventoryCurrent -> GetMetaDescription() . " Email",
												   "ArticlePublishTime" => $inventoryCurrent -> PublishedTime,
												   "ArticleModifiedTime" => $inventoryCurrent -> ModifiedTime,
												   "ArticleSection" => "Product",
												   "ArticleTag" => "Product");											 

		$this -> view -> Canonical = $inventoryCurrent -> GetURL();
		
		$this -> view -> currentSpecials = $specialsList -> SpecialsByInventory($inventoryCurrent -> stockNumber);
		$this -> view -> CurrentInventory = $inventoryCurrent;
		$this -> view -> render('inventory/inventorySingleOnlineOffer', true);
		
	}


	public function tradevalue($vin) {
		$inventoryCurrent = InventoryObject::WithVin($vin);
		$this -> view -> title = "Trade In Value: " . $inventoryCurrent -> GetCondition() . ' ' . $inventoryCurrent -> GetBikeName() . parent::DILLONBROTHERS;
		
		
		$this -> view -> SchemaMetaData = array("Name" => "Email " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												"Description" => $inventoryCurrent -> GetMetaDescription() . " Email",
												"ImageURL" => $inventoryCurrent -> GetMainPhoto());
												
												
		$this -> view -> TwitterMetaData = array("Title" => "Email " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												 "Description" => $inventoryCurrent -> GetMetaDescription() . " Email",
												 "URL" => PATH . 'inventory/tradevalue/'. $vin,
												 "ImageURL" => $inventoryCurrent -> GetMainPhoto());	
		
		$this -> view -> OpenGraphMetaData = array("Title" => "Email " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												   "Type" => "Product",
												   "URL" => PATH . 'inventory/tradevalue/'. $vin,
												   "Image" => $inventoryCurrent -> GetMainPhoto(),
												   "Description" => $inventoryCurrent -> GetMetaDescription() . " Email",
												   "ArticlePublishTime" => $inventoryCurrent -> PublishedTime,
												   "ArticleModifiedTime" => $inventoryCurrent -> ModifiedTime,
												   "ArticleSection" => "Product",
												   "ArticleTag" => "Product");											 

		$this -> view -> Canonical = $inventoryCurrent -> GetURL();
		
		$this -> view -> CurrentInventory = $inventoryCurrent;
		$this -> view -> render('inventory/inventorySingleTradeValue', true);
		
	}
	
	public function testride($vin) {
		$inventoryCurrent = InventoryObject::WithVin($vin);
		$this -> view -> title = "Schedule Test Ride: " . $inventoryCurrent -> GetCondition() . ' ' . $inventoryCurrent -> GetBikeName() . parent::DILLONBROTHERS;
		
		
		$this -> view -> SchemaMetaData = array("Name" => "Email " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												"Description" => $inventoryCurrent -> GetMetaDescription() . " Email",
												"ImageURL" => $inventoryCurrent -> GetMainPhoto());
												
												
		$this -> view -> TwitterMetaData = array("Title" => "Email " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												 "Description" => $inventoryCurrent -> GetMetaDescription() . " Email",
												 "URL" => PATH . 'inventory/testride/'. $vin,
												 "ImageURL" => $inventoryCurrent -> GetMainPhoto());	
		
		$this -> view -> OpenGraphMetaData = array("Title" => "Email " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												   "Type" => "Product",
												   "URL" => PATH . 'inventory/testride/'. $vin,
												   "Image" => $inventoryCurrent -> GetMainPhoto(),
												   "Description" => $inventoryCurrent -> GetMetaDescription() . " Email",
												   "ArticlePublishTime" => $inventoryCurrent -> PublishedTime,
												   "ArticleModifiedTime" => $inventoryCurrent -> ModifiedTime,
												   "ArticleSection" => "Product",
												   "ArticleTag" => "Product");											 

		$this -> view -> Canonical = $inventoryCurrent -> GetURL();
		
		$this -> view -> CurrentInventory = $inventoryCurrent;
		$this -> view -> render('inventory/inventorySingleScheduleRide', true);
		
	}	

	public function contact($vin) {
		$inventoryCurrent = InventoryObject::WithVin($vin);
		$this -> view -> title = "Contact Us: " . $inventoryCurrent -> GetCondition() . ' ' . $inventoryCurrent -> GetBikeName() . parent::DILLONBROTHERS;
		
		
		$this -> view -> SchemaMetaData = array("Name" => "Email " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												"Description" => $inventoryCurrent -> GetMetaDescription() . " Email",
												"ImageURL" => $inventoryCurrent -> GetMainPhoto());
												
												
		$this -> view -> TwitterMetaData = array("Title" => "Email " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												 "Description" => $inventoryCurrent -> GetMetaDescription() . " Email",
												 "URL" => PATH . 'inventory/contact/'. $vin,
												 "ImageURL" => $inventoryCurrent -> GetMainPhoto());	
		
		$this -> view -> OpenGraphMetaData = array("Title" => "Email " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												   "Type" => "Product",
												   "URL" => PATH . 'inventory/contact/'. $vin,
												   "Image" => $inventoryCurrent -> GetMainPhoto(),
												   "Description" => $inventoryCurrent -> GetMetaDescription() . " Email",
												   "ArticlePublishTime" => $inventoryCurrent -> PublishedTime,
												   "ArticleModifiedTime" => $inventoryCurrent -> ModifiedTime,
												   "ArticleSection" => "Product",
												   "ArticleTag" => "Product");											 

		$this -> view -> Canonical = $inventoryCurrent -> GetURL();
		
		$this -> view -> CurrentInventory = $inventoryCurrent;
		$this -> view -> render('inventory/inventorySingleContactUs', true);
		
	}		

	public function OnlineOfferTest($vin) {
		$inventoryCurrent = InventoryObject::WithVin($vin);
		$specialsList = new SpecialsList();
		$this -> view -> title = "Online Offer: " . $inventoryCurrent -> GetCondition() . ' ' . $inventoryCurrent -> GetBikeName() . parent::DILLONBROTHERS;
		
		
		$this -> view -> SchemaMetaData = array("Name" => "Email " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												"Description" => $inventoryCurrent -> GetMetaDescription() . " Email",
												"ImageURL" => $inventoryCurrent -> GetMainPhoto());
												
												
		$this -> view -> TwitterMetaData = array("Title" => "Email " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												 "Description" => $inventoryCurrent -> GetMetaDescription() . " Email",
												 "URL" => PATH . 'inventory/onlineoffer/'. $vin,
												 "ImageURL" => $inventoryCurrent -> GetMainPhoto());	
		
		$this -> view -> OpenGraphMetaData = array("Title" => "Email " . $inventoryCurrent -> GetBikeName() . ": Dillon Brothers", 
												   "Type" => "Product",
												   "URL" => PATH . 'inventory/onlineoffer/'. $vin,
												   "Image" => $inventoryCurrent -> GetMainPhoto(),
												   "Description" => $inventoryCurrent -> GetMetaDescription() . " Email",
												   "ArticlePublishTime" => $inventoryCurrent -> PublishedTime,
												   "ArticleModifiedTime" => $inventoryCurrent -> ModifiedTime,
												   "ArticleSection" => "Product",
												   "ArticleTag" => "Product");											 

		$this -> view -> Canonical = $inventoryCurrent -> GetURL();
		
		$this -> view -> currentSpecials = $specialsList -> SpecialsByInventory($inventoryCurrent -> stockNumber);
		$this -> view -> CurrentInventory = $inventoryCurrent;
		$this -> view -> render('inventory/inventorySingleOnlineOfferTest', true);
	}

}
?>