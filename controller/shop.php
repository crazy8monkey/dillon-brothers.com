<?php

class shop extends Controller {
	
	private $_categories;	
	private $_products;	
	private $_reviews;	
		
	public function __construct() {
		Session::init();	
		parent::__construct();
		$this -> _categories = new EcommerceCategoryList();
		$this -> _products = new EcommerceProductList();
		$this -> _reviews = new ReviewList();
		$this -> view -> css = array(PATH . 'public/css/ShopController.css');
	}
	
	
	public function index() {
		$pageName = 'Shopping Center' . parent::DILLONBROTHERS;
		$this -> view -> title = $pageName;
		
		$this -> view -> js = array(PATH . "public/js/ShopController.js");
		$this -> view -> SchemaMetaData = array("Name" => $pageName, 
												"Description" => "Dillon Brothers Omaha Shopping Center",
												"ImageURL" => PATH . "public/images/DillonBrothers.png");
		
		$this -> view -> TwitterMetaData = array("Title" => $pageName, 
												 "Description" => "Dillon Brothers Omaha Shopping Center",
												 "URL" => PATH . 'photos',
												 "ImageURL" => PATH . "public/images/DillonBrothers.png");
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $pageName, 
												   "Type" => "products",
												   "URL" => PATH . 'shop',
												   "Image" => PATH . "public/images/DillonBrothers.png",
												   "Description" => "Dillon Brothers Omaha Shopping Center",
												   "ArticlePublishTime" => $this -> _products -> AllProducts()[count($this -> _products -> AllProducts()) - 1]['productPublishedDate'],
												   "ArticleModifiedTime" => $this -> _products -> AllProducts()[0]['productModifiedTime'],
												   "ArticleSection" => "products",
												   "ArticleTag" => "ecommerce");	
		
		$this -> view -> shoppingCategories = $this -> _categories -> ActiveCategories();
		$this -> view -> shoppingProducts = $this -> _products -> AllProducts();
		$this -> view -> reviews = $this -> _reviews -> AllActiveReviewsByEcommerceProducts();
		$this -> view -> render('shop/index');	
	}
	
	public function product($seoName) {
		$productSingle = EcommerceProduct::WithSEOName($seoName);
		$this -> view -> js = array(PATH . "public/js/ShopController.js", "https://www.google.com/recaptcha/api.js");
		$titleName = $productSingle -> productName . ' | Shopping Center' . parent::DILLONBROTHERS;
		
		
		$this -> view -> title = $titleName;
		//$this -> view -> LatestEvents = $latestEvents -> LatestEvents();
		$this -> view -> SchemaMetaData = array("Name" => $titleName, 
												"Description" => "Dillon Brothers Omaha Shopping Center " . $productSingle -> productName,
												"ImageURL" => $productSingle -> GetProductPhoto());
		
		$this -> view -> TwitterMetaData = array("Title" => $titleName, 
												 "Description" => "Dillon Brothers Omaha Shopping Center " . $productSingle -> productName,
												 "URL" => PATH . 'shop/product/'. $seoName,
												 "ImageURL" => $productSingle -> GetProductPhoto());
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $titleName, 
												   "Type" => "product",
												   "URL" => PATH . 'shop/product/'. $seoName,
												   "Image" => $productSingle -> GetProductPhoto(),
												   "Description" => "Dillon Brothers Omaha Shopping Center " . $productSingle -> productName,
												   "ArticlePublishTime" => $productSingle -> PublishedDate,
												   "ArticleModifiedTime" => $productSingle -> ModifiedDate,
												   "ArticleSection" => "product",
												   "ArticleTag" => "Shopping Center");	
												   
												   
		$this -> view -> productSingle = $productSingle;
		$this -> view -> startJsFunction = array('ShopController.ProductSinglePage();');
		$this -> view -> render('shop/productSingle');	
	}

	public function category($seoName) {
		$categorySingle = EcommerceCategory::WithSEOName($seoName);
		$titleName = $categorySingle -> CategoryName . ' | Shopping Center' . parent::DILLONBROTHERS;
		$this -> view -> js = array(PATH . "public/js/ShopController.js");
		$this -> view -> title = $titleName;
		
		$this -> view -> SchemaMetaData = array("Name" => $titleName, 
												"Description" => "Dillon Brothers Shipping Center Category " . $categorySingle -> CategoryName,
												"ImageURL" => PATH . "public/images/DillonBrothers.png");
		
		$this -> view -> TwitterMetaData = array("Title" => $titleName, 
												 "Description" => "Dillon Brothers Shipping Center Category " . $categorySingle -> CategoryName,
												 "URL" => PATH . 'shop/category/'. $seoName,
												 "ImageURL" => PATH . "public/images/DillonBrothers.png");
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $titleName, 
												   "Type" => "category products",
												   "URL" => PATH . 'shop/category/'. $seoName,
												   "Image" => PATH . "public/images/DillonBrothers.png",
												   "Description" => "Dillon Brothers Shipping Center Category " . $categorySingle -> CategoryName,
												   "ArticlePublishTime" => $categorySingle -> GetPublishedDate(),
												   "ArticleModifiedTime" => $categorySingle -> GetModifiedDate(),
												   "ArticleSection" => "CategoryProducts",
												   "ArticleTag" => "");	
												   
		$this -> view -> shoppingCategories = $this -> _categories -> ActiveCategories();
		$this -> view -> shoppingProducts = $this -> _products -> ProductsByCategory($categorySingle -> CategoryID);
		$this -> view -> reviews = $this -> _reviews -> AllActiveReviewsByEcommerceProducts();
		$this -> view -> render('shop/index');											   
		
	}

	
	public function signup() {
		$this -> view -> title = "Sign up | Shopping Center" . parent::DILLONBROTHERS;
		$this -> view -> js = array("https://js.stripe.com/v2/", PATH . "public/js/StripeHandler.js", PATH . "public/js/ShopController.js");
		$this -> view -> startJsFunction = array("Stripe.setPublishableKey('" . STRIPE_PUBLISH_KEY . "'); ShopController.AccountSinglePage()");
		$this -> view -> render('shop/signup');
	}

	public function account($type = null) {
		Session::init();
		if(isset($type)) {
			$this -> view -> js = array("https://js.stripe.com/v2/", PATH . "public/js/StripeHandler.js", PATH . "public/js/ShopController.js");
			$this -> view -> title = "Update Your Billing | Shopping Center" . parent::DILLONBROTHERS;
			$this -> view -> startJsFunction = array("Stripe.setPublishableKey('" . STRIPE_PUBLISH_KEY . "'); ShopController.AccountSingleBillingUpdate()");
			$this -> view -> render('shop/accountSingleBilling');	
		} else {
			$this -> view -> title = "Your Account | Shopping Center" . parent::DILLONBROTHERS;
			$this -> view -> js = array(PATH . "public/js/ShopController.js", PATH . "public/js/maskedInput.js");
			$this -> view -> AccountSingle = EcommerceUserAccount::WithID($_SESSION['EcommerceUserAccount'] -> GetID());
			$this -> view -> startJsFunction = array("ShopController.AccountSingleInfoUpdate()");
			$this -> view -> render('shop/accountSingleBasic');	
		}
		
	}

	public function newcredentials() {
		$this -> view -> AccountSingle = EcommerceUserAccount::WithID($_GET['u']);
		$this -> view -> render('shop/credentialsReset', true);
	}
	
	public function login() {
		$this -> view -> title = "Login to your Account | Shopping Center" . parent::DILLONBROTHERS;
		$this -> view -> js = array(PATH . "public/js/ShopController.js");
		$this -> view -> startJsFunction = array("ShopController.LoginFormPage()");
		$this -> view -> render('shop/login');
	}
	
	public function logout() {
		Session::init();
		unset($_SESSION['EcommerceUserAccount']);
		unset($_SESSION['EcommerceUserAccountLoggedIn']);
		$this -> redirect -> redirectPage(PATH. 'shop');
		exit();
	}
	



	

}
?>