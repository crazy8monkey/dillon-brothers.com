<?php

class Ajax extends Controller {
		
	public function __construct() {
		parent::__construct();
		
	}
	
	public function index() {}
	
	public function LoadPhotos() {
		$photos = new PhotosList();
		echo $photos -> LoadPhotosByAlbum($_POST['start'], $_POST['limit'], $_POST['albumID']);
	}

	public function submitComment() {
		$comment = new BlogPostComment();
		$comment -> fullName = $_POST['commentFullName'];
		$comment -> CommentDescription = $_POST['commentContent'];
		$comment -> postID = Hash::mc_decrypt($_POST['post'], ENCRYPTION_KEY);
		
		if($comment -> Validate()) {
			$comment -> Save();
		}
	}


	public function inventorylead($type) {
		$inventoryCurrent = InventoryObject::WithVin(Hash::mc_decrypt($_POST['u'], ENCRYPTION_KEY));	
		switch($type) {
			case "sendToFriend":
				
				$inventoryCurrent -> firstName = $_POST['yourFirstName'];
				$inventoryCurrent -> lastName = $_POST['yourLastName'];
				$inventoryCurrent -> userEmail = $_POST['yourEmail'];
				$inventoryCurrent -> friendsEmail = $_POST['friendsEmail'];
				$inventoryCurrent -> PersonalMessage = $_POST['PersonalMessage'];
				break;
			case "onlineoffer":
				$inventoryCurrent -> firstName = $_POST['yourFirstName'];
				$inventoryCurrent -> lastName = $_POST['yourLastName'];
				$inventoryCurrent -> userEmail = $_POST['yourEmail'];
				$inventoryCurrent -> PersonalMessage = $_POST['PersonalMessage'];
				break;
			case "tradeinvalue":
				$inventoryCurrent -> firstName = $_POST['yourFirstName'];
				$inventoryCurrent -> lastName = $_POST['yourLastName'];
				$inventoryCurrent -> userEmail = $_POST['yourEmail'];	
				$inventoryCurrent -> phone = $_POST['yourPhone'];
				$inventoryCurrent -> TradeInYear = $_POST['TradeInYear'];
				$inventoryCurrent -> TradeInMake = $_POST['TradeInMake'];
				$inventoryCurrent -> TradeInModel = $_POST['TradeInModel'];
				$inventoryCurrent -> TradeInMileage = $_POST['TradeInMileage'];
				
				$inventoryCurrent -> addedAccessories = $_POST['AddeddAccessories'];
				$inventoryCurrent -> TradeComments = $_POST['TradeComments'];
				
				break;
			case "testride":
				$inventoryCurrent -> firstName = $_POST['yourFirstName'];
				$inventoryCurrent -> lastName = $_POST['yourLastName'];
				$inventoryCurrent -> userEmail = $_POST['yourEmail'];	
				$inventoryCurrent -> phone = $_POST['yourPhone'];
				$inventoryCurrent -> DateofRide = $_POST['testrideDate'];
				break;
			case "contactus":
				$inventoryCurrent -> firstName = $_POST['yourFirstName'];
				$inventoryCurrent -> lastName = $_POST['yourLastName'];
				$inventoryCurrent -> userEmail = $_POST['yourEmail'];	
				$inventoryCurrent -> phone = $_POST['yourPhone'];
				$inventoryCurrent -> CommentsConcerns = $_POST['commentsConcerns'];
				break;
		}
	
	
		
		if($inventoryCurrent -> ValidateLeadEmail($type)) {
			$inventoryCurrent -> SendEmail($type);
		}


	}

	public function toggleInventoryList($rowNumber = NULL, $pageNumber = NULL, $updateFilters) {
		$inventory = new InventoryObject();
		
		if($rowNumber != NULL) {
			$inventory -> RowNumber = $rowNumber;	
		} else {
			$inventory -> RowNumber = $_POST['RowNumber'];
		}
		
		if(isset($_POST['Condition'])) {
			$inventory -> FilterCondition = $_POST['Condition'];	
		}
		
		if(isset($_POST['store'])) {
			$inventory -> FilterStore = $_POST['store'];	
		}
		
		if(isset($_POST['manufacture'])) {
			$inventory -> FilterManufactures =$_POST['manufacture'];
		}
		
		if(isset($_POST['color'])) {
			$inventory -> FilterColors =$_POST['color'];
		}
		
		if(isset($_POST['Year'])) {
			$inventory -> FilterYears = $_POST['Year'];
		}
		
		if(isset($_POST['category'])) {
			$inventory -> FilterCategory = $_POST['category'];
		}
		
		if(isset($_POST['priceRange'])) {
			$inventory -> FilterPrice = $_POST['priceRange'];	
		}
		
		if(isset($_POST['engineSizeCCRange'])) {
			$inventory -> FilterEngineSizeCCRange = $_POST['engineSizeCCRange'];	
		}
		
		if(isset($_POST['mileageRange'])) {
			$inventory -> FilterMileage = $_POST['mileageRange'];	
		}
		
		if(!empty($_POST['hiddenQuickInventorySearch'])) {
			$inventory ->  FilterByString = $_POST['hiddenQuickInventorySearch'];
		}		
				
		$inventory ->  FilterOrderBy = $_POST['OrderByValue'];
		
		
		$inventory -> PageNumber = $pageNumber;
		
		
		echo json_encode($inventory -> ToggleInventory($updateFilters));
	}

	public function QuickInventorySearch() {
		$inventory = new InventoryObject();
		$inventory ->  FilterByString = $_POST['quickInventorySearch'];
		
		echo json_encode($inventory -> QuickSearch());
	}
	

	public function EventPhotoAlbum() {
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$photos = new PhotosList();
			//$decryptedID = html_entity_decode(urldecode($id));
			//echo $_GET['id'];
			echo json_encode($photos -> PhotosByAlbum(Hash::mc_decrypt($_GET['id'], ENCRYPTION_KEY), true));	
		}
		
	}

	public function addtocart() {
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$product = new EcommerceProduct();
			
			$product -> ProductSelect = Hash::mc_decrypt($_POST['productID'], ENCRYPTION_KEY);
			if(isset($_POST['ColorSelect'])) {
				$product -> ColorSelect = $_POST['ColorSelect'];	
			}
			
			if(isset($_POST['SizeSelect'])) {
				$product -> SizeSelect = $_POST['SizeSelect'];
			}
			
			if($product -> Validate()) {
				echo json_encode($product -> AddToCart());
			}
		}
	}
	
	public function removefromcart($key) {
		$product = new EcommerceProduct();
		$product -> CartKey = $key;
		echo json_encode($product -> RemoveCartItem());
	}
	
	public function getrates() {
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$ecommerceSettings = EcommerceSettingGeneral::Init();
			echo json_encode($ecommerceSettings -> GetFlatRate());	
		}
		
	}
	
	public function newcredentials() {
		$account = EcommerceUserAccount::WithID(Hash::mc_decrypt($_POST['u'], ENCRYPTION_KEY));
		$account -> AccountCreatedSource = 3;
		$account -> Username = $_POST['newUsername'];
		$account -> SetPassword($_POST['newPassword']);
		if($account -> Validate()) {
			$account -> Save();	
		}
	}
	
	public function account($type) {
		switch($type) {
			case "new":
				$account = new EcommerceUserAccount();
				$account -> AccountCreatedSource = 4;
				$account -> FirstName = $_POST['accountFirstName'];
				$account -> LastName = $_POST['accountLastName'];		
				$account -> Email = $_POST['accountEmail'];
				$account -> Phone = $_POST['accountPhoneNumber'];
				$account -> SetPassword($_POST['accountPassword']);
				
				if(isset($_POST['stripeToken'])) {
					$account -> SecurityToken = $_POST['stripeToken'];
				}
				
				if($account -> Validate()) {
					if($account -> ValidateSecurityToken()) {
						$account -> SaveSignup();	
					}	
				}		
				break;
			case "updatesingleinfo":
					Session::init();
					$AccountInfo = EcommerceUserAccount::WithID($_SESSION['EcommerceUserAccount'] -> GetID());
					$AccountInfo -> FirstName = $_POST['accountFirstName'];
					$AccountInfo -> LastName = $_POST['accountLastName'];	
					$AccountInfo -> Email = $_POST['accountEmail'];
					$AccountInfo -> Phone = $_POST['accountPhoneNumber'];
					if($AccountInfo -> ValidateAccountInfo()) {
						$AccountInfo -> SaveSingleAccountInfo();	
					}
				break;
			case "updatebillinginfo";
				Session::init();
				$AccountInfo = EcommerceUserAccount::WithID($_SESSION['EcommerceUserAccount'] -> GetID());
				
				if(isset($_POST['stripeToken'])) {
					$AccountInfo -> SecurityToken = $_POST['stripeToken'];
				}
				
				if($AccountInfo -> ValidateSecurityToken()) {
					$AccountInfo -> SaveBillingInfo();	
				}
				break;
			
		}
		
	}
	
	public function ecommerceloginaccount() {
		$loginAccount = new EcommerceUserAccount();
		$loginAccount -> Email = $_POST['emailLogin'];
		$loginAccount -> Password = $_POST['passwordLogin'];
		$loginAccount -> userLogin();		
	}
	
	public function SavePendingOrder() {
		Session::init();
		$ecommerceOrder = new EcommerceOrder();	
		
		if(isset($_POST['shipOption'])) {
			$ecommerceOrder -> StorePickup = $_POST['shipOption'];	
		}
		
		$ecommerceOrder -> ShippingAddress = $_POST['shippingAddress'];
		$ecommerceOrder -> ShippingCity = $_POST['shippingCity'];
		$ecommerceOrder -> ShippingState = $_POST['shippingState'];
		$ecommerceOrder -> ShippingZip = $_POST['shippingZip'];
		
		
		if(isset($_SESSION['EcommerceUserAccount'])) {
			$getAccountInfo = EcommerceUserAccount::WithID($_SESSION['EcommerceUserAccount'] -> GetID());
			
			$ecommerceOrder -> firstName = $getAccountInfo -> FirstName;
			$ecommerceOrder -> LastName = $getAccountInfo -> LastName;
			$ecommerceOrder -> PhoneNumber = $getAccountInfo -> Email;
			$ecommerceOrder -> Email = $getAccountInfo -> Phone;	
			$ecommerceOrder -> CustomerStripeID = $getAccountInfo -> StripeCustomerID;
			$ecommerceOrder -> CreateAccount = 0;	
			$ecommerceOrder -> SessionExists = 1;			
			
			if($ecommerceOrder -> Validate()) {
				$ecommerceOrder -> SavePersistentOrder();
			}
			
		} else {
			$ecommerceOrder -> firstName = $_POST['shippingFirstName'];
			$ecommerceOrder -> LastName = $_POST['shippingLastName'];
			$ecommerceOrder -> PhoneNumber = $_POST['phoneNumber'];
			$ecommerceOrder -> Email = $_POST['shippingEmail'];
			$ecommerceOrder -> CreateAccount = $_POST['createAccountOption'];	
			$ecommerceOrder -> SessionExists = 0;
			
			if(isset($_POST['stripeToken'])) {
				$ecommerceOrder -> StripeToken = $_POST['stripeToken'];
			}
			
			if($ecommerceOrder -> Validate()) {
				if($ecommerceOrder -> ValidateStripeToken()) {
					$ecommerceOrder -> SavePersistentOrder();
				}
			}	
		}
	}

	public function SaveOrder() {
		Session::init();
		$orderItems = array();
		$order = new EcommerceOrder();
		$order -> firstName = $_SESSION['NewOrder']['user-first-name'];
		$order -> LastName = $_SESSION['NewOrder']['user-first-last'];
		$order -> PhoneNumber = $_SESSION['NewOrder']['user-phone'];
		$order -> Email = $_SESSION['NewOrder']['user-email'];
		$order -> ShippingAddress = $_SESSION['NewOrder']['ship-address'];
		$order -> ShippingCity = $_SESSION['NewOrder']['ship-city'];
		$order -> ShippingState = $_SESSION['NewOrder']['ship-state'];
		$order -> ShippingZip = $_SESSION['NewOrder']['ship-zip'];
		$order -> StorePickup = $_SESSION['NewOrder']['ship-option']['0'];
		$order -> TaxRate = $_POST['taxRate'];
		$order -> ShippingRate = $_POST['shippingRate'];	
			
		if(isset($_SESSION['EcommerceUserAccountLoggedIn'])) {
			$order -> ShippingRate = $_POST['shippingRate'];	
			$order -> CreateAccount = 0;
			$order -> CustomerStripeID = $_SESSION['NewOrder']['customer-id'];
		} else {
			$order -> StripeToken = $_SESSION['NewOrder']['token'];
			$order -> CreateAccount = $_SESSION['NewOrder']['create-account'];
		}
		
		foreach($_SESSION['CurrentCartObject']['results']['cartItems'] as $itemSingle) {
			array_push($orderItems, array('product_id' => $itemSingle['product_id'],
					 					  'quantity' => $itemSingle['quantity'],
										  'price' => $itemSingle['price'],
										  'colorSelected' => $itemSingle['colorSelected'],
										  'sizeSelected' => $itemSingle['sizeSelected'],
										  'PartNumber' => $itemSingle['PartNumber']));
		} 

		$order -> orderItems = $orderItems;
		if(isset($_SESSION['EcommerceUserAccountLoggedIn'])) {
			$order -> SaveOrderChargeCustomer();	
		} else {
			$order -> Save();	
		}
		
	}

	public function productreview() {
		$newReview = new ReviewSingle();
		$newReview -> inventoryItemID = Hash::mc_decrypt($_POST['ecommerceProductID'], ENCRYPTION_KEY);
		if(isset($_POST['rating'])) {
			$newReview -> RatingNumber = $_POST['rating'];	
		}
		$newReview -> ReviewType = 1;
		
		Session::init();
		if(!isset($_SESSION['EcommerceUserAccountLoggedIn'])) {
			$newReview -> firstName = $_POST['reviewFirstName'];
			$newReview -> lastName = $_POST['reviewLastName'];	
		} else {
			$getAccountInfo = EcommerceUserAccount::WithID($_SESSION['EcommerceUserAccount'] -> GetID());
			$newReview -> firstName = $getAccountInfo -> FirstName;
			$newReview -> lastName = $getAccountInfo -> LastName;			
		}
		
		$newReview -> ReviewText = $_POST['reviewText'];
		
		if($newReview -> Validate()) {
			$newReview -> Save();
		} 
		
	}

	public function servicedeptform() {
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$service = new ServiceDept();
			if(isset($_POST['serviceStore'])) {
				$service -> Store = $_POST['serviceStore'];	
			}
			$service -> FirstName = $_POST['serviceFirstName'];
			$service -> LastName = $_POST['serviceLastName'];
			$service -> HomePhone = $_POST['serviceHomePhone'];
			$service -> CellPhone = $_POST['serviceCellPhone'];
			$service -> EmailAddress = $_POST['serviceEmailAddress'];
			$service -> StreetAddress = $_POST['serviceStreetAddress'];
			$service -> City = $_POST['serviceCity'];
			$service -> state = $_POST['serviceState'];
			$service -> Zip = $_POST['serviceZip'];
			//vehicle information
			$service -> VehicleYear = $_POST['serviceVehicleYear'];
			$service -> VehicleMake = $_POST['serviceVehicleMake'];
			$service -> VehicleModel = $_POST['serviceVehicleModel'];
			$service -> Miles = $_POST['serviceVehicleMiles'];
			$service -> VinNumber = $_POST['serviceVehicleVinNumber'];
			//service details
			if(isset($_POST['servicePastServiceOption'])) {
				$service -> PastServiceYesNo = $_POST['servicePastServiceOption'];	
			}
			$service -> LastServiceDate = $_POST['servicePreviousDate'];
			$service -> LastWorkDone = $_POST['servicePreviousWorkDone'];
			$service -> NewAppointmentDate = $_POST['serviceNewServiceDate'];
			$service -> NewWorkNeedsDone = $_POST['serviceNewWorkDone'];	
				
			if($service -> Validate()) {
				$service -> Save();
			}
		}
	} 

	public function partsdeptform() {
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$parts = new PartsDept();	
			
			if(isset($_POST['partsStore'])) {
				$parts -> Store = $_POST['partsStore'];	
			}
			
			$parts -> FirstName = $_POST['partsFirstName'];
			$parts -> LastName = $_POST['partsLastName'];
			$parts -> HomePhone = $_POST['partsHomePhone'];
			$parts -> CellPhone = $_POST['partsCellPhone'];
			$parts -> EmailAddress = $_POST['partsEmailAddress'];
			$parts -> StreetAddress = $_POST['partsStreetAddress'];
			$parts -> City = $_POST['partsCity'];
			$parts -> state = $_POST['partsState'];
			$parts -> Zip = $_POST['partsZip'];
			//vehicle information
			$parts -> VehicleYear = $_POST['partsVehicleYear'];
			$parts -> VehicleMake = $_POST['partsVehicleMake'];
			$parts -> VehicleModel = $_POST['partsVehicleModel'];
			$parts -> Miles = $_POST['partsVehicleMiles'];
			$parts -> VinNumber = $_POST['partsVehicleVinNumber'];
			//service details
			$parts -> PartsNeeded = $_POST['partNeededRequest'];
			$parts -> PartNumberRequest = $_POST['partNumberRequest'];
			
			if($parts -> Validate()) {
				$parts -> Save();
			}
		}
	}

	public function donationform() {
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$donation = new DonationReq();
			
			$donation -> DillonBrotherCustomer = $_POST['donationNameList'];
			$donation -> ContactName = $_POST['donationEventContactName'];
			$donation -> ContactPhone = $_POST['donationEventContactPhone'];
			$donation -> ContactEmail = $_POST['donationEventContactEmail'];
			//event info
			$donation -> NameOfEvent = $_POST['donationEventName'];
			$donation -> TypeOfEvent = $_POST['donationEventType'];
			$donation -> TaxID = $_POST['donationTaxIDNumber'];
			$donation -> Benifiting = $_POST['donationBenefiting'];
			$donation -> DateOfEvent = $_POST['donationDateofEvent'];
			$donation -> AnticipatedAttendance = $_POST['donationAnticipatedAttendance'];
			//addional info
			$donation -> Attachments = $_FILES['donationAttachments']['name'];			
			$donation -> AttachmentsTemp = $_FILES['donationAttachments']['tmp_name'];
			
			$donation -> AdditionalComments = $_POST['donationAdditionalComments'];
			
			if($donation -> Validate()) {
				$donation -> Save();
			}
			
		}
	}

	public function appareldeptform() {
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$apparelDept = new ApparelDept();
			
			if(isset($_POST['apparelStore'])) {
				$apparelDept -> Store = $_POST['apparelStore'];	
			}
			
			$apparelDept -> FirstName = $_POST['apparelFirstName'];
			$apparelDept -> LastName = $_POST['apparelLastName'];
			$apparelDept -> HomePhone = $_POST['apparelHomePhone'];
			$apparelDept -> CellPhone = $_POST['apparelCellPhone'];
			$apparelDept -> EmailAddress = $_POST['apparelEmailAddress'];
			$apparelDept -> StreetAddress = $_POST['apparelStreetAddress'];
			$apparelDept -> City = $_POST['apparelCity'];
			$apparelDept -> state = $_POST['apparelState'];
			$apparelDept -> Zip = $_POST['apparelZip'];
			$apparelDept -> AdditionalComments = $_POST['apparelAdditionalComments'];
			
			if($apparelDept -> Validate()) {
				$apparelDept -> Save();
			}
		}
	}
	
	public function twiliotokens() {
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$twilioHandler = new TwilioHandler();
			$twilioHandler -> acessTokenCustomerName = $_GET['customerName'];
			$twilioHandler -> CurrentWorkerID = $_GET['workerID'];
			
			echo json_encode(array('workerToken' => $twilioHandler -> GenerateWorkerTaskRouterToken(),
								   'workspaceToken' => $twilioHandler -> GenerateWorkspaceTaskRouterToken(),
								   'chatchanneltoken' => $twilioHandler -> GenerateAccessToken(),
								   'taskqueuetoken' => $twilioHandler -> GenerateTaskQueueRouterToken()));			
		}
		
	}
	
	public function ChatTokens() {
		$twilioHandler = new TwilioHandler();
		
		echo json_encode($twilioHandler -> GenerateAccessToken());
	}
	
	public function startchat() {
		$conversation = new ChatConversation();
		$conversation -> CustomerFirstName = $_POST['yourFirstName'];
		$conversation -> CustomerLastname = $_POST['yourLastName'];
		$conversation -> CustomerPhone = $_POST['yourPhoneNumber'];
		$conversation -> CustomerEmail = $_POST['yourEmail'];
		$conversation -> ChannelID = $_POST['channel'];
		$conversation -> WorkerID = $_POST['worker'];
		if($conversation -> Validate()) {
			$conversation -> StartChatConversation();	
		}
		
	}
	
	public function sendmessage() {
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$referrer = $_SERVER['HTTP_REFERER'];
			$conversation = ChatConversation::WithTaskID($_POST['taskID']);
			$conversation -> NewMessage = $_POST['NewMessage'];
			$conversation -> SaveMessage();
		}
	}


	public function getchatchannel() {
		//if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$chatChannel = new ChatChannel();
			echo json_encode($chatChannel -> GetChatChannelDetails());
		//}
		
		
		
	}




}
?>