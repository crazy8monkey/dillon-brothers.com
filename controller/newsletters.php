<?php

class Newsletters extends Controller {
		
	public function __construct() {
		parent::__construct();
		//$this -> view -> js = array(PATH . "public/js/NewslettersController.js");
		$this -> view -> css = array(PATH . 'public/css/NewslettersController.css');
	}
	
	
	public function index() {
		$latestEvents = new EventsList();
		$newsLetters = new NewsletterList();
		
		$this -> view -> title = "Newsletters" . parent::DILLONBROTHERS;
		//$this -> view -> LatestEvents = $latestEvents -> LatestEvents();
		$this -> view -> SchemaMetaData = array("Name" => "Newsletters: Dillon Brothers", 
												"Description" => "Dillon Brothers Newsletters",
												"ImageURL" => "http://www.dillon-brothers.com/images/dillon-brothers-motorsports-15.png");
		
		$this -> view -> TwitterMetaData = array("Title" => "Newsletters: Dillon Brothers", 
												 "Description" => "Dillon Brothers Newsletters",
												 "URL" => PATH . 'events',
												 "ImageURL" => "http://www.dillon-brothers.com/images/dillon-brothers-motorsports-15.png");
												 
		$this -> view -> OpenGraphMetaData = array("Title" => "Newsletters: Dillon Brothers", 
												   "Type" => "Dillon Brothers Newsletters",
												   "URL" => PATH . 'Newsletters',
												   "Image" => "",
												   "Description" => "",
												   "ArticlePublishTime" => "",
												   "ArticleModifiedTime" => "",
												   "ArticleSection" => "",
												   "ArticleTag" => "");												   
												 										 
		//$this -> view -> startJsFunction = array('EventsController.Initialize("' . PATH . '");');
		$this -> view -> newsLetters = $newsLetters -> Newsletters();
		$this -> view -> render('newsletters/index');		
	}
	
	public function view($eventName, $subEvent = NULL) {
		$eventSingle = Event::WithSEOname($eventName);
		
		$subEvents = new EventsList();
		
		
		$MetaDataDecoded = json_decode($eventSingle -> MetaData);
		$this -> view -> title = $eventSingle -> eventName;
		
		$this -> view -> MainEventTitle = $eventSingle -> eventName;
		
		
		
		$this -> view -> SchemaMetaData = $MetaDataDecoded ->{'SchemaMetaData'};
		$this -> view -> TwitterMetaData = $MetaDataDecoded ->{'TwitterMetaData'};
		$this -> view -> OpenGraphMetaData = $MetaDataDecoded ->{'OpenGraphMetaData'};
		
		$this -> view -> EventSingle = $eventSingle;
		
		$this -> view -> SubEvents = $subEvents -> GetSubEvents($eventSingle -> GetEventID());
		
		$this -> view -> pageLayout = json_decode($eventSingle -> GetHtml(), true);
		
		
		$this -> view -> render('events/single');
	}
	
	


}
?>