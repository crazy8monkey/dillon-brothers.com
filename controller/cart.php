<?php

class cart extends Controller {
	
	private $_colors;	
	private $_products;
	private $_sizes;	
		
	public function __construct() {
		parent::__construct();
		$this -> view -> js = array(PATH . "public/js/slick.min.js", "https://js.stripe.com/v2/", PATH . "public/js/StripeHandler.js", PATH . "public/js/CartController.js", PATH . "public/js/maskedInput.js");
		$this -> view -> css = array(PATH . 'public/css/CartController.css', PATH . 'public/css/slick.css', PATH . 'public/css/slick-theme.css');
		$this -> _colors = new EcommerceColorList();	
		$this -> _products = new EcommerceProductList();
		$this -> _sizes = new EcommerceSizeList();	
	}
	
	
	public function index() {
		Session::init();
		$this -> view -> title = 'Your Cart' . parent::DILLONBROTHERS;
		
		if(isset($_SESSION['CurrentCartObject']) && count($_SESSION['CurrentCartObject']['results']['cartItems']) > 0) {
			$productIDs = '';
			$colorIDs = '';	
			$sizeSelectedIDs = '';
			foreach($_SESSION['CurrentCartObject']['results']['cartItems'] as $itemSingle) {
				$colorIDs .= $itemSingle['colorSelected'] . ', ';	
				$productIDs .= $itemSingle['product_id'] . ', ';	
				$sizeSelectedIDs .= $itemSingle['sizeSelected'] . ', ';
			}
			
			if(!empty($colorIDs)) {
				$this -> view -> colorsSelected = $this -> _colors -> SelectedColors(rtrim($colorIDs, ', '));		
			}
			
			$this -> view -> productsSelected = $this -> _products -> SelectedProducts(rtrim($productIDs, ', '));
			$this -> view -> sizesSelected = $this -> _sizes -> SelectedSizes(rtrim($sizeSelectedIDs, ', '));
			
		} else {
			$this -> view -> startJsFunction = array('CartController.InitializeCartPage();');
			$this -> view -> featuredproducts = $this -> _products -> FeaturedProducts();	
		}
						
		$this -> view -> render('cart/index');	
	}
	
	public function checkout() {
		Session::init();
		$this -> view -> title = 'Checkout' . parent::DILLONBROTHERS;		
		$this -> view -> bodyClassName = "checkoutPage";
		
		if(isset($_SESSION['EcommerceUserAccountLoggedIn'])) {
			$this -> view -> AccountSingle = EcommerceUserAccount::WithID($_SESSION['EcommerceUserAccount'] -> GetID());	
			$this -> view -> startJsFunction = array("Stripe.setPublishableKey('" . STRIPE_PUBLISH_KEY . "');", 'CartController.CheckoutPage();');		
		} else {
			$this -> view -> showBillingInformation = '';
			$this -> view -> startJsFunction = array("Stripe.setPublishableKey('" . STRIPE_PUBLISH_KEY . "');", 'CartController.CheckoutPage(true);');
			
		}
		
		if(isset($_SESSION['CurrentCartObject'])) {
			$productIDs = '';
			$colorIDs = '';	
			$sizeSelectedIDs = '';
			foreach($_SESSION['CurrentCartObject']['results']['cartItems'] as $itemSingle) {
				$colorIDs .= $itemSingle['colorSelected'] . ', ';	
				$productIDs .= $itemSingle['product_id'] . ', ';	
				$sizeSelectedIDs .= $itemSingle['sizeSelected'] . ', ';
			}
			
			if(!empty($colorIDs)) {
				$this -> view -> colorsSelected = $this -> _colors -> SelectedColors(rtrim($colorIDs, ', '));		
			}
			
			$this -> view -> productsSelected = $this -> _products -> SelectedProducts(rtrim($productIDs, ', '));
			$this -> view -> sizesSelected = $this -> _sizes -> SelectedSizes(rtrim($sizeSelectedIDs, ', '));
			
		} else {
			$this -> redirect -> redirectPage(PATH. 'cart');
		}
		
		$this -> view -> render('cart/checkout');
	}
	
	public function confirm() {
		$ecommerceSettings = EcommerceSettingGeneral::Init();
		Session::init();
		$this -> view -> title = 'Confirm' . parent::DILLONBROTHERS;		
		$this -> view -> bodyClassName = "checkoutPage";
		if(isset($_SESSION['CurrentCartObject'])) {
			$productIDs = '';
			$colorIDs = '';	
			$sizeSelectedIDs = '';
			foreach($_SESSION['CurrentCartObject']['results']['cartItems'] as $itemSingle) {
				$colorIDs .= $itemSingle['colorSelected'] . ', ';	
				$productIDs .= $itemSingle['product_id'] . ', ';	
				$sizeSelectedIDs .= $itemSingle['sizeSelected'] . ', ';
			}
			
			if(!empty($colorIDs)) {
				$this -> view -> colorsSelected = $this -> _colors -> SelectedColors(rtrim($colorIDs, ', '));		
			}
			
			$this -> view -> productsSelected = $this -> _products -> SelectedProducts(rtrim($productIDs, ', '));
			$this -> view -> sizesSelected = $this -> _sizes -> SelectedSizes(rtrim($sizeSelectedIDs, ', '));
			$this -> view -> shippingRates = $ecommerceSettings -> GetFlatRate();
			
		} else {
			$this -> redirect -> redirectPage(PATH. 'cart');
		}
		
		$this -> view -> render('cart/confirm');
	}
	

	public function thanks() {
		$this -> view -> render('cart/thanks', true);
	}
	
	
	

}
?>