<?php

class CatchAll extends Controller {
		
	public function __construct() {
		parent::__construct();
	}
	
	
	public function index() {}
	
	public function view($value) {
		$shortLink = ShortLinks::WithSEOName($value);
		
		if($shortLink -> BlogID == 0 && $shortLink -> EventID == 0) {
			if($shortLink -> SubDomain) {
				$redirectString = $shortLink -> OriginatedSource;
			} else {
				$redirectString = PATH .  $shortLink -> OriginatedSource;	
			}
			
		} else {
			if($shortLink -> EventID != 0) {
				$redirectString = PATH .  $shortLink -> OriginatedSource;
			}
					
			if($shortLink -> BlogID != 0) {
				if($shortLink -> ShortCodeLinkActive == 1) {
					$redirectString = PATH .  $value;	
				} else {
					$redirectString = PATH . 'blog/post/' .  $shortLink -> OriginatedSource;
				}
			}	
		}
		
		switch($shortLink -> ActionStatus) {
			case "410Redirect":
				$this -> redirect -> Redirect410($redirectString);
				break;	
			case "301Redirect":
				$this -> redirect -> Redirect301($redirectString);
				break;
			case "CurrentContent":
				switch($shortLink -> ShortCodeLinkActive) {
					case 0:
						if($shortLink -> BlogID != 0) {
							
							$this -> redirect -> redirectPage(PATH. 'blog/post/'. $shortLink -> OriginatedSource);	
						}
						break;
					case 1:
						if($shortLink -> BlogID != 0) {
					
							$blogPost = BlogPost::WithID($shortLink -> BlogID);
							$this -> view -> css = array(PATH. 'public/css/BlogController.css');
							$this -> view -> js = array(PATH . "public/js/BlogController.js", "https://www.google.com/recaptcha/api.js");
							
						
							//if($blogPost -> NotBlogPostName == false) {
							$comments = new BlogPostCommentsList();
								
								
							$this -> view -> startJsFunction = array('BlogController.InitializeCommentsForm();');
							$this -> view -> title = $blogPost -> blogPostName . parent::DILLONBROTHERS;
									
							$MetaDataDecoded = json_decode($blogPost -> MetaData);
									
							$this -> view -> SchemaMetaData = array("Name" => $blogPost -> blogPostName . ": Dillon Brothers", 
																	"Description" => "Dillon Brothers Current Blog " . $blogPost -> blogPostName . " Omaha Fremont Nebraska",
																	"ImageURL" => $blogPost -> GetBlogImage());
								
							$this -> view -> TwitterMetaData = array("Title" => $blogPost -> blogPostName . ": Dillon Brothers", 
																	 "Description" => "Dillon Brothers Current Blog " . $blogPost -> blogPostName . " Omaha Fremont Nebraska",
																	 "URL" => PATH . 'blog/' . $blogPost -> blogPostSEOUrl,
																	 "ImageURL" => $blogPost -> GetBlogImage());
																			 
							$this -> view -> OpenGraphMetaData = array("Title" => $blogPost -> blogPostName . ": Dillon Brothers", 
																	   "Type" => "blog",
																	   "URL" => PATH . 'blog/' . $blogPost -> blogPostSEOUrl,
																	   "Image" => $blogPost -> GetBlogImage(),
																	   "Description" => "Dillon Brothers Current Blog " . $blogPost -> blogPostName . " Omaha Fremont Nebraska",
																	   "ArticlePublishTime" => $blogPost -> blogPublishedDateFull,
																	   "ArticleModifiedTime" => $blogPost -> modifiedDateFull,
																	   "ArticleSection" => "Article",
																	   "ArticleTag" => "Article");	
														
							$this -> view -> Canonical = PATH . $value;	
										
									
							$this -> view -> postSingle = $blogPost;
							$this -> view -> comments = $comments -> CommentsByPost($blogPost -> blogPostID);
								
								
							$this -> view -> render('blog/single');	
						}
						
						if($shortLink -> EventID != 0) {
							$eventSingle = Event::WithID($shortLink -> EventID);	
							
							$this -> view -> js = array(PATH . "public/js/photoswipe.min.js", PATH . "public/js/photoswipe-ui-default.min.js", PATH . "public/js/EventsController.js", PATH . 'public/js/moment.min.js', PATH . 'public/js/fullcalendar.min.js', PATH . 'public/js/gcal.js');
							$this -> view -> css = array(PATH . 'public/css/EventsController.css', PATH . 'public/css/fullcalendar.css');
							$pageData = new PageData();
							$subEventsList = new EventsList();
								
							$MainTitleName = NULL;
								
							$eventHTMLJSON = NULL;
							$eventID = NULL;
							$eventMetaDataDescription = NULL;
							$eventPublishedDate = NULL;
							$eventModifiedDate = NULL;
							$eventMetaUrl = NULL;
							$eventMetaURL = NULL;
							$eventImageURL = NULL;
								
							$photosSelectedData = NULL;
							$albumsSectedData = NULL;
							$relatedAlbums = NULL;
								
							if($eventSingle -> SuperEvent == 1) {
								$this -> view -> startJsFunction = array('EventsController.GetSubEventName();');
								$this -> view -> SubEvents = $subEventsList -> GetSubEvents($eventSingle -> GetEventID());
								$this -> view -> title = $eventSingle -> eventName . " Events" . parent::DILLONBROTHERS;
								
									
								$relatedAlbums = $subEventsList -> GetSuperEventRelatedAlbums($eventSingle -> GetEventID());
								
								$eventIDS = NULL;
								$publishedSubDates = array();
								$modifiedSubDates = array();
								foreach($subEventsList -> GetSubEvents($eventSingle -> GetEventID()) as $eventSingleLoop) {
									$eventIDS .= $eventSingleLoop['eventID'] . ', ';
									array_push($publishedSubDates, $eventSingleLoop['publishDateFull']);
									array_push($modifiedSubDates, $eventSingleLoop['modifiedDateFull']);
								}
								sort($publishedSubDates);
								sort($modifiedSubDates);
									
									
								$eventPublishedDate = $publishedSubDates[0];
								$eventModifiedDate = $modifiedSubDates[count($modifiedSubDates)-1];
								
								$photosSelectedData = $pageData -> SuperEventPhotos(rtrim($eventIDS, ', '));
								$albumsSectedData = $pageData -> SuperEventAlbums(rtrim($eventIDS, ', '));
									
								$this -> view -> title = $eventSingle -> eventName;
									
								$eventMetaDataDescription = $eventSingle -> SuperEventDescription;
									 
									
							} else {
								$this -> view -> title = $eventSingle -> eventName .  parent::DILLONBROTHERS;
								$eventMetaDataDescription = $eventSingle -> GoogleEventDescription;
								$this -> view -> pageLayout = json_decode($eventSingle -> GetHtml(), true);
								$photosSelectedData = $pageData -> EventSinglePhotos($eventSingle -> GetEventID());
								$albumsSectedData = $pageData -> EventSingleAlbums($eventSingle -> GetEventID());
								
								$eventPublishedDate = $eventSingle -> PublishedDate;
								$eventModifiedDate = $eventSingle -> ModifiedDate;
								
								$eventMetaUrl = PATH . $eventSingle -> GetSEOUrl();
								
								$relatedAlbums = $subEventsList -> GetEventRelatedAlbums($value);
							}
								
								
								
								
							$this -> view -> SchemaMetaData = array("Name" => $eventSingle -> eventName . ": Dillon Brothers", 
																	"Description" => $eventMetaDataDescription,
																	"ImageURL" => $eventSingle -> GetMainEventPhoto());
						
							$this -> view -> TwitterMetaData = array("Title" => $eventSingle -> eventName . ": Dillon Brothers", 
																	 "Description" => $eventMetaDataDescription,
																	 "URL" => PATH . $value,
																	 "ImageURL" => $eventSingle -> GetMainEventPhoto());
																 
							$this -> view -> OpenGraphMetaData = array("Title" => $eventSingle -> eventName . "Events: Dillon Brothers", 
																	   "Type" => "place",
																	   "URL" => PATH . $value,
																	   "Image" => $eventSingle -> GetMainEventPhoto(),
																	   "Description" => $eventMetaDataDescription,
																	   "ArticlePublishTime" => $eventPublishedDate,
																	   "ArticleModifiedTime" => $eventModifiedDate,
																	   "ArticleSection" => "place",
																	   "ArticleTag" => "event");												   
						
							$this -> view -> Canonical = PATH . $value;
							
							$this -> view -> MainEventTitle = $eventSingle -> eventName;
							
							$this -> view -> EventSingle = $eventSingle;
								
							$this -> view -> photosSelected = $photosSelectedData;
							$this -> view -> albumsSelected = $albumsSectedData;
							$this -> view -> relatedAlbums = $relatedAlbums;
							
							$this -> view -> render('events/single');
							
						}
						break;
				}
				
				break;
		}
	
	}
	
	

}
?>