<?php

class Donation extends Controller {
		
	public function __construct() {
		parent::__construct();
		$this -> view -> js = array(PATH . "public/js/DonationController.js", "https://code.jquery.com/ui/1.12.1/jquery-ui.js", PATH . "public/js/maskedInput.js", "https://www.google.com/recaptcha/api.js");
		$this -> view -> css = array(PATH . 'public/css/DonationController.css', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
	}
	
	public function index() {
		$this -> view -> title = 'Donation Request' . parent::DILLONBROTHERS; 	
		$this -> view -> startJsFunction = array('DonationController.Initialize();');				 										 
		$this -> view -> render('donation/index');		
	}
}
?>