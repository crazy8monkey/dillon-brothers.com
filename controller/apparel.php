<?php

class Apparel extends Controller {
	
		
	public function __construct() {
		parent::__construct();
		$this -> view -> js = array(PATH . "public/js/ApparelController.js", PATH . "public/js/maskedInput.js", "https://www.google.com/recaptcha/api.js");
		$this -> view -> css = array(PATH . 'public/css/ApparelController.css');
	}
	
	
	public function index() {
		Session::init();
		//$specialsList = new SpecialsList();
		$pageTitle = "Apparel Department" . parent::DILLONBROTHERS;
		
		$this -> view -> title = $pageTitle;
		
		
		$this -> view -> SchemaMetaData = array("Name" => $pageTitle, 
												"Description" => "Dillon Brothers Apparel Departments Omaha Fremont Nebraska",
												"ImageURL" => PATH . "public/images/DillonBrothers.png");
		
		$this -> view -> TwitterMetaData = array("Title" => $pageTitle, 
												 "Description" => "Dillon Brothers Apparel Departments Omaha Fremont Nebraska",
												 "URL" => PATH . 'parts',
												 "ImageURL" => PATH . "public/images/DillonBrothers.png");
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $pageTitle, 
												   "Type" => "page",
												   "URL" => PATH . 'parts',
												   "Image" => PATH . "public/images/DillonBrothers.png",
												   "Description" => "Dillon Brothers Apparel Departments Omaha Fremont Nebraska",
												   "ArticlePublishTime" => "",
												   "ArticleModifiedTime" => "",
												   "ArticleSection" => "",
												   "ArticleTag" => "");												   
		
		$this -> view -> Canonical = PATH . 'parts';
		$this -> view -> startJsFunction = array('ApparelController.Initialize();');										 										 
		$this -> view -> render('apparel/index');		
	}
	


}
?>