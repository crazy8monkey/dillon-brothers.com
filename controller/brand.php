<?php

class Brand extends Controller {
		
	public function __construct() {
		parent::__construct();
		$this -> view -> css = array(PATH. 'public/css/BrandController.css');
		$this -> view -> js = array(PATH . "public/js/BrandController.js");
	}
	
	public function index() {
		$brands = new BrandsList();
		$this -> view -> title = 'Brands We Carry' . parent::DILLONBROTHERS;
		$this -> view -> OEMBrands = $brands -> OEMBrands();
		$this -> view -> PartApparelBrands = $brands -> PartApparelBrands();
		$this -> view -> BrandsList = $brands -> AllOEMAndPartApparelBrands();
		
		$this -> view -> SchemaMetaData = array("Name" => "Brands We Carry at Dillon Brothers",
												"ImageURL" => PATH . "public/images/DillonBrothers.png");
		
		$this -> view -> TwitterMetaData = array("Title" => "Brands We Carry at Dillon Brothers", 
												 "Description" => "Dillon Brothers Omaha Fremont Brands We Carry",
												 "URL" => PATH . 'brand',
												 "ImageURL" => PATH . "public/images/DillonBrothers.png");
												 
		$this -> view -> OpenGraphMetaData = array("Title" => "Brands We Carry at Dillon Brothers", 
												   "Type" => "business.business",
												   "URL" => PATH . 'brand',
												   "Image" => PATH . "public/images/DillonBrothers.png",
												   "Description" => "Dillon Brothers Omaha Fremont Brands We Carry",
												   "ArticlePublishTime" => "",
												   "ArticleModifiedTime" => "",
												   "ArticleSection" => "",
												   "ArticleTag" => "");	
		
		
		$this -> view -> render('brand/index');
	}
	
	public function view($seoName) {
		$brandPage = BrandWeCarry::WithSEOName($seoName);	
		$brandContent = new BrandContentList();
		if($brandPage -> ISOEMBrand == 1) {
			$OEMBrand = OEMBrand::WithID($brandPage -> BrandID);
			$titleString = 'About Our '. $OEMBrand -> BrandName . ' Inventory';
			
			$this -> view -> ISOEMBrandPage = '';
			$this -> view -> OEMContent = $OEMBrand;
			
			$brandImage = PHOTO_URL . 'brands/' . $OEMBrand -> BrandImage;
			$metaDescription = "Dillon Brothers Omaha Fremont " . $OEMBrand -> BrandName . ' Vehicles';
			$contentList = $brandContent -> ContentByOEMBrand($brandPage -> BrandID);
			
		} else if($brandPage -> ISOEMBrand == 0) {
			$PartApparelBrand = PartApparelBrand::WithSEOName($seoName);	
			
			$this -> view -> ISPartApparelBrandPage = '';
			$this -> view -> PartApparelBrandContent = $PartApparelBrand;
			
			$titleString = 'About Our '. $PartApparelBrand -> BrandName . ' Inventory';
			$brandImage = PHOTO_URL . 'PartsBrand/' . $PartApparelBrand -> BrandImage;
			$metaDescription = "Dillon Brothers Omaha Fremont " . $PartApparelBrand -> BrandName . ' Vehicles';
			$contentList = $brandContent -> ContentByPartApparelBrand($PartApparelBrand -> BrandID);
		}
		
		$this -> view -> title = $titleString . parent::DILLONBROTHERS;
		
		$this -> view -> SchemaMetaData = array("Name" => $titleString . ":Dillon Brothers",
												"ImageURL" => $brandImage);
		
		$this -> view -> TwitterMetaData = array("Title" => $titleString . ":Dillon Brothers", 
												 "Description" => $metaDescription,
												 "URL" => PATH . 'brand',
												 "ImageURL" => $brandImage);
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $titleString . ":Dillon Brothers", 
												   "Type" => "business.business",
												   "URL" => PATH . 'brand',
												   "Image" => $brandImage,
												   "Description" => $metaDescription,
												   "ArticlePublishTime" => "",
												   "ArticleModifiedTime" => "",
												   "ArticleSection" => "",
												   "ArticleTag" => "");
		$this -> view -> BrandContent = $contentList;
		$this -> view -> render('brand/single');
	}

	
	public function pagecontent($brand, $contentName) {
		$brandContent = BrandContent::WithSEOName($brand, $contentName);
		$this -> view -> title = $brandContent -> LandingPageTitle . parent::DILLONBROTHERS;
		$this -> view -> pagecontent = $brandContent;
		$this -> view -> render('brand/brandcontent');
	}
	


}
?>