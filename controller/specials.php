<?php

class Specials extends Controller {
	
		
	public function __construct() {
		parent::__construct();
		$this -> view -> js = array(PATH . "public/js/SpecialsController.js");
		$this -> view -> css = array(PATH . 'public/css/SpecialsController.css');
	}
	
	
	public function index() {
		$pageTitle = "Specials";
		$specialsList = new SpecialsList();
		
		$this -> view -> title = $pageTitle . parent::DILLONBROTHERS;
		
		$this -> view -> currentSpecials = $specialsList -> AllSpecials();
		
		$this -> view -> SchemaMetaData = array("Name" => $pageTitle, 
												"Description" => "Dillon Brothers Current Specials Omaha Fremont Nebraska",
												"ImageURL" => PATH . "public/images/NoPhotoPlacement.png");
		
		$this -> view -> TwitterMetaData = array("Title" => $pageTitle, 
												 "Description" => "Dillon Brothers Current Specials Omaha Fremont Nebraska",
												 "URL" => PATH . 'specials',
												 "ImageURL" => PATH . "public/images/NoPhotoPlacement.png");
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $pageTitle, 
												   "Type" => "website",
												   "URL" => PATH . 'events',
												   "Image" => PATH . "public/images/NoPhotoPlacement.png",
												   "Description" => "Dillon Brothers Current Specials Omaha Fremont Nebraska",
												   "ArticlePublishTime" => "",
												   "ArticleModifiedTime" => "",
												   "ArticleSection" => "",
												   "ArticleTag" => "");												   
		
		$this -> view -> Canonical = PATH . 'specials';
												 										 
		$this -> view -> render('specials/index');		
	}
	
	public function motorsport($seoName) {
		$specialSingle = Special::WithSEOname($seoName);	
			
		$pageTitle = $specialSingle -> SpecialTitle. " Special: Dillon Brothers";
		$specialsList = new SpecialsList();
		$relatedInvenctives = new RelatedSpecialItemsList();
		
		$this -> view -> title = $pageTitle . parent::DILLONBROTHERS;
		
		$this -> view -> specialSingle = $specialSingle;
		
		$incentivesList = NULL;
		
		if($specialSingle -> RelatedInventoryType == 1) {
			$incentivesList = $relatedInvenctives -> RelatedItemsBySpecial($specialSingle -> SpecialID, true);
		} else {
			$incentivesList = $relatedInvenctives -> RelatedItemsBySpecial($specialSingle -> SpecialID);
		}
		
		$this -> view -> incentives = $incentivesList;
		
		
		$this -> view -> SchemaMetaData = array("Name" => $pageTitle, 
												"Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												"ImageURL" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage);
		
		$this -> view -> TwitterMetaData = array("Title" => $pageTitle, 
												 "Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												 "URL" => PATH . 'specials/motorsport/' . $seoName,
												 "ImageURL" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage);
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $pageTitle, 
												   "Type" => "website",
												   "URL" => PATH . 'specials/motorsport/' . $seoName,
												   "Image" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage,
												   "Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												   "ArticlePublishTime" => $specialSingle -> SpecialPublishedDate,
												   "ArticleModifiedTime" => $specialSingle -> SpecialModifiedDate,
												   "ArticleSection" => "",
												   "ArticleTag" => "");												   
		
		$this -> view -> Canonical = PATH . 'specials/motorsport/' . $seoName;
			
		$this -> view -> render('specials/single');
	}
	
	public function harley($seoName) {
		$specialSingle = Special::WithSEOname($seoName);	
			
		$pageTitle = $specialSingle -> SpecialTitle. " Special: Dillon Brothers";
		$specialsList = new SpecialsList();
		$relatedInvenctives = new RelatedSpecialItemsList();
		
		$this -> view -> title = $pageTitle . parent::DILLONBROTHERS;
		
		$this -> view -> specialSingle = $specialSingle;
		
		$incentivesList = NULL;
		
		if($specialSingle -> RelatedInventoryType == 1) {
			$incentivesList = $relatedInvenctives -> RelatedItemsBySpecial($specialSingle -> SpecialID, true);
		} else {
			$incentivesList = $relatedInvenctives -> RelatedItemsBySpecial($specialSingle -> SpecialID);
		}
		
		$this -> view -> incentives = $incentivesList;
		
		
		$this -> view -> SchemaMetaData = array("Name" => $pageTitle, 
												"Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												"ImageURL" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage);
		
		$this -> view -> TwitterMetaData = array("Title" => $pageTitle, 
												 "Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												 "URL" => PATH . 'specials/motorsport/' . $seoName,
												 "ImageURL" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage);
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $pageTitle, 
												   "Type" => "website",
												   "URL" => PATH . 'specials/motorsport/' . $seoName,
												   "Image" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage,
												   "Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												   "ArticlePublishTime" => $specialSingle -> SpecialPublishedDate,
												   "ArticleModifiedTime" => $specialSingle -> SpecialModifiedDate,
												   "ArticleSection" => "",
												   "ArticleTag" => "");												   
		
		$this -> view -> Canonical = PATH . 'specials/harley/' . $seoName;
			
		$this -> view -> render('specials/single');
	}
	
	public function indian($seoName) {
		$specialSingle = Special::WithSEOname($seoName);	
			
		$pageTitle = $specialSingle -> SpecialTitle. " Special: Dillon Brothers";
		$specialsList = new SpecialsList();
		$relatedInvenctives = new RelatedSpecialItemsList();
		
		$this -> view -> title = $pageTitle . parent::DILLONBROTHERS;
		
		$this -> view -> specialSingle = $specialSingle;
		
		$incentivesList = NULL;
		
		if($specialSingle -> RelatedInventoryType == 1) {
			$incentivesList = $relatedInvenctives -> RelatedItemsBySpecial($specialSingle -> SpecialID, true);
		} else {
			$incentivesList = $relatedInvenctives -> RelatedItemsBySpecial($specialSingle -> SpecialID);
		}
		
		$this -> view -> incentives = $incentivesList;
		
		
		$this -> view -> SchemaMetaData = array("Name" => $pageTitle, 
												"Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												"ImageURL" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage);
		
		$this -> view -> TwitterMetaData = array("Title" => $pageTitle, 
												 "Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												 "URL" => PATH . 'specials/motorsport/' . $seoName,
												 "ImageURL" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage);
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $pageTitle, 
												   "Type" => "website",
												   "URL" => PATH . 'specials/motorsport/' . $seoName,
												   "Image" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage,
												   "Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												   "ArticlePublishTime" => $specialSingle -> SpecialPublishedDate,
												   "ArticleModifiedTime" => $specialSingle -> SpecialModifiedDate,
												   "ArticleSection" => "",
												   "ArticleTag" => "");												   
		
		$this -> view -> Canonical = PATH . 'specials/indian/' . $seoName;
			
		$this -> view -> render('specials/single');
	}
	
	
	public function storespecial($seoName) {
		$specialSingle = Special::WithSEOname($seoName);	
			
		$pageTitle = $specialSingle -> SpecialTitle. " Special: Dillon Brothers";
		$specialsList = new SpecialsList();
		$relatedInvenctives = new RelatedSpecialItemsList();
		
		$this -> view -> title = $pageTitle . parent::DILLONBROTHERS;
		
		$this -> view -> specialSingle = $specialSingle;
		
		$incentivesList = NULL;
		
		if($specialSingle -> RelatedInventoryType == 1) {
			$incentivesList = $relatedInvenctives -> RelatedItemsBySpecial($specialSingle -> SpecialID, true);
		} else {
			$incentivesList = $relatedInvenctives -> RelatedItemsBySpecial($specialSingle -> SpecialID);
		}
		
		$this -> view -> incentives = $incentivesList;
		
		
		$this -> view -> SchemaMetaData = array("Name" => $pageTitle, 
												"Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												"ImageURL" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage);
		
		$this -> view -> TwitterMetaData = array("Title" => $pageTitle, 
												 "Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												 "URL" => PATH . 'specials/motorsport/' . $seoName,
												 "ImageURL" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage);
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $pageTitle, 
												   "Type" => "website",
												   "URL" => PATH . 'specials/motorsport/' . $seoName,
												   "Image" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage,
												   "Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												   "ArticlePublishTime" => $specialSingle -> SpecialPublishedDate,
												   "ArticleModifiedTime" => $specialSingle -> SpecialModifiedDate,
												   "ArticleSection" => "",
												   "ArticleTag" => "");												   
		
		$this -> view -> Canonical = PATH . 'specials/storespecial/' . $seoName;
			
		$this -> view -> render('specials/single');
	}
	
	public function servicespecial($seoName) {
		$specialSingle = Special::WithSEOname($seoName);	
			
		$pageTitle = $specialSingle -> SpecialTitle. " Special: Dillon Brothers";
		$specialsList = new SpecialsList();
		$relatedInvenctives = new RelatedSpecialItemsList();
		
		$this -> view -> title = $pageTitle . parent::DILLONBROTHERS;
		
		$this -> view -> specialSingle = $specialSingle;
		
		$incentivesList = NULL;
		
		if($specialSingle -> RelatedInventoryType == 1) {
			$incentivesList = $relatedInvenctives -> RelatedItemsBySpecial($specialSingle -> SpecialID, true);
		} else {
			$incentivesList = $relatedInvenctives -> RelatedItemsBySpecial($specialSingle -> SpecialID);
		}
		
		$this -> view -> incentives = $incentivesList;
		
		
		$this -> view -> SchemaMetaData = array("Name" => $pageTitle, 
												"Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												"ImageURL" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage);
		
		$this -> view -> TwitterMetaData = array("Title" => $pageTitle, 
												 "Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												 "URL" => PATH . 'specials/motorsport/' . $seoName,
												 "ImageURL" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage);
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $pageTitle, 
												   "Type" => "website",
												   "URL" => PATH . 'specials/motorsport/' . $seoName,
												   "Image" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage,
												   "Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												   "ArticlePublishTime" => $specialSingle -> SpecialPublishedDate,
												   "ArticleModifiedTime" => $specialSingle -> SpecialModifiedDate,
												   "ArticleSection" => "",
												   "ArticleTag" => "");												   
		
		$this -> view -> Canonical = PATH . 'specials/servicespecial/' . $seoName;
			
		$this -> view -> render('specials/single');
	}	
	
	public function partsspecial($seoName) {
		$specialSingle = Special::WithSEOname($seoName);	
			
		$pageTitle = $specialSingle -> GetSpecialTitle(). " Special: Dillon Brothers";
		$specialsList = new SpecialsList();
		$relatedInvenctives = new RelatedSpecialItemsList();
		
		$this -> view -> title = $pageTitle . parent::DILLONBROTHERS;
		
		$this -> view -> specialSingle = $specialSingle;
		
		$incentivesList = NULL;
		
		if($specialSingle -> RelatedInventoryType == 1) {
			$incentivesList = $relatedInvenctives -> RelatedItemsBySpecial($specialSingle -> SpecialID, true);
		} else {
			$incentivesList = $relatedInvenctives -> RelatedItemsBySpecial($specialSingle -> SpecialID);
		}
		
		$this -> view -> incentives = $incentivesList;
		
		
		$this -> view -> SchemaMetaData = array("Name" => $pageTitle, 
												"Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												"ImageURL" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage);
		
		$this -> view -> TwitterMetaData = array("Title" => $pageTitle, 
												 "Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												 "URL" => PATH . 'specials/motorsport/' . $seoName,
												 "ImageURL" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage);
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $pageTitle, 
												   "Type" => "website",
												   "URL" => PATH . 'specials/motorsport/' . $seoName,
												   "Image" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage,
												   "Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												   "ArticlePublishTime" => $specialSingle -> SpecialPublishedDate,
												   "ArticleModifiedTime" => $specialSingle -> SpecialModifiedDate,
												   "ArticleSection" => "",
												   "ArticleTag" => "");												   
		
		$this -> view -> Canonical = PATH . 'specials/partsspecial/' . $seoName;
			
		$this -> view -> render('specials/single');
	}	
	
	public function apparelspecial($seoName) {
		$specialSingle = Special::WithSEOname($seoName);	
			
		$pageTitle = $specialSingle -> GetSpecialTitle(). " Special: Dillon Brothers";
		$specialsList = new SpecialsList();
		$relatedInvenctives = new RelatedSpecialItemsList();
		
		$this -> view -> title = $pageTitle . parent::DILLONBROTHERS;
		
		$this -> view -> specialSingle = $specialSingle;
		
		$incentivesList = NULL;
		
		if($specialSingle -> RelatedInventoryType == 1) {
			$incentivesList = $relatedInvenctives -> RelatedItemsBySpecial($specialSingle -> SpecialID, true);
		} else {
			$incentivesList = $relatedInvenctives -> RelatedItemsBySpecial($specialSingle -> SpecialID);
		}
		
		$this -> view -> incentives = $incentivesList;
		
		
		$this -> view -> SchemaMetaData = array("Name" => $pageTitle, 
												"Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												"ImageURL" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage);
		
		$this -> view -> TwitterMetaData = array("Title" => $pageTitle, 
												 "Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												 "URL" => PATH . 'specials/motorsport/' . $seoName,
												 "ImageURL" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage);
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $pageTitle, 
												   "Type" => "website",
												   "URL" => PATH . 'specials/motorsport/' . $seoName,
												   "Image" => PHOTO_URL . "specials/" . $specialSingle -> folderName . '/' . $specialSingle -> SpecialImage,
												   "Description" => "Dillon Brothers Current " . $specialSingle -> GetSpecialTitle() . " Special Omaha Nebraska",
												   "ArticlePublishTime" => $specialSingle -> SpecialPublishedDate,
												   "ArticleModifiedTime" => $specialSingle -> SpecialModifiedDate,
												   "ArticleSection" => "",
												   "ArticleTag" => "");												   
		
		$this -> view -> Canonical = PATH . 'specials/apparelspecial/' . $seoName;
			
		$this -> view -> render('specials/single');
	}		
	
	public function inventory($stock) {
		$photos = new InventoryPhotoList();	
		$specLabels = new SpecLabelsList();
		$inventoryList = new InventoryList();
		
		$inventoryCurrent = InventoryObject::WithVin($stock);
		$currentPhotos = $photos -> PhotosByInventory($inventoryCurrent -> inventoryID);
		
		
		
		switch($inventoryCurrent -> RelatedIncentiveType) {
			case "Percentage Off":
				$valueText = $inventoryCurrent -> RelatedIncentiveValue . '% Off';
				break;
			case "Dollar Off":
				$valueText = '$'. $inventoryCurrent -> RelatedIncentiveValue . ' Off';
				break;
			case "Special Financing":
				$valueText = $inventoryCurrent -> RelatedIncentiveValue . '% APR';
				break;
		}
		
		$PopupTitle = "Special: " . $inventoryCurrent -> RelatedSpecialName . ' | ' . $inventoryCurrent -> GetBikeName() . ' | ' . $valueText;
		
		$this -> view -> title = $PopupTitle . parent::DILLONBROTHERS;
		$this -> view -> inventorySingle = $inventoryCurrent;
		$this -> view -> speclabels = $specLabels -> ByCategory($inventoryCurrent -> categoryID);
		
		
		$this -> view -> inventoryPhotos = $currentPhotos;
		
		if(count($currentPhotos) > 0) {
			$mainPhoto = PHOTO_URL . $inventoryCurrent -> VIN . '/'. $currentPhotos[0]['inventoryPhotoName'] . '-l.' . $currentPhotos[0]['inventoryPhotoExt'];
		} else {
			$mainPhoto = PATH . 'public/images/NoPhotoPlacement.png';	
		}
		
		
		
		$this -> view -> SchemaMetaData = array("Name" => $PopupTitle . ": Dillon Brothers", 
												"Description" => $inventoryCurrent -> GetMetaDescription(),
												"ImageURL" => $mainPhoto);
		
		$this -> view -> TwitterMetaData = array("Title" => $PopupTitle . ": Dillon Brothers", 
												 "Description" => $inventoryCurrent -> GetMetaDescription(),
												 "URL" => $inventoryCurrent -> GetURL(),
												 "ImageURL" => $mainPhoto);
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $PopupTitle . ": Dillon Brothers", 
												   "Type" => "product.item",
												   "URL" => $inventoryCurrent -> GetURL(),
												   "Image" => $mainPhoto,
												   "Description" => $inventoryCurrent -> GetMetaDescription(),
												   "ArticlePublishTime" => $inventoryCurrent -> PublishedTime,
												   "ArticleModifiedTime" => $inventoryCurrent -> ModifiedTime,
												   "ArticleSection" => "Product",
												   "ArticleTag" => "Product");	
		
		$this -> view -> Canonical = $inventoryCurrent -> GetURL();
		
		$this -> view -> render('specials/relatedinventory', true);
	}
	
	
	

	

	

}
?>