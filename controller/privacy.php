<?php

class Privacy extends Controller {
	
		
	public function __construct() {
		parent::__construct();
		//$this -> view -> js = array(PATH . "public/js/PrivacyController.js");
		$this -> view -> css = array(PATH . 'public/css/PrivacyController.css');
	}
	
	
	public function index() {
		$this -> view -> title = "Privacy Statement" . parent::DILLONBROTHERS;
		$this -> view -> Canonical = PATH . 'privacy';
		$this -> view -> render('privacy/index');		
	}

}
?>