<?php

class PageError extends Controller {
		
	public function __construct() {
		parent::__construct();
		$this -> view -> css = array(PATH . 'public/css/ErrorController.css');
	}
	
	
	public function index() {
		$this -> view -> title = "404 Page not Found" . parent::DILLONBROTHERS;	
		$this -> view -> render('error/index');
	}

	

}
?>