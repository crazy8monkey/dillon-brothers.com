<?php

class Parts extends Controller {
	
		
	public function __construct() {
		parent::__construct();
		$this -> view -> js = array(PATH . "public/js/PartsController.js", PATH . "public/js/maskedInput.js", "https://www.google.com/recaptcha/api.js");
		$this -> view -> css = array(PATH . 'public/css/PartsController.css');
	}
	
	
	public function index() {
		Session::init();
		//$specialsList = new SpecialsList();
		$pageTitle = "Parts Department" . parent::DILLONBROTHERS;
		
		$this -> view -> title = $pageTitle;
		
		
		$this -> view -> SchemaMetaData = array("Name" => $pageTitle, 
												"Description" => "Dillon Brothers Parts Departments Omaha Fremont Nebraska",
												"ImageURL" => PATH . "public/images/DillonBrothers.png");
		
		$this -> view -> TwitterMetaData = array("Title" => $pageTitle, 
												 "Description" => "Dillon Brothers Parts Departments Omaha Fremont Nebraska",
												 "URL" => PATH . 'parts',
												 "ImageURL" => PATH . "public/images/DillonBrothers.png");
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $pageTitle, 
												   "Type" => "page",
												   "URL" => PATH . 'parts',
												   "Image" => PATH . "public/images/DillonBrothers.png",
												   "Description" => "Dillon Brothers Parts Departments Omaha Fremont Nebraska",
												   "ArticlePublishTime" => "",
												   "ArticleModifiedTime" => "",
												   "ArticleSection" => "",
												   "ArticleTag" => "");												   
		
		$this -> view -> Canonical = PATH . 'parts';
		$this -> view -> startJsFunction = array('PartsController.Initialize();');										 										 
		$this -> view -> render('parts/index');		
	}
	


}
?>