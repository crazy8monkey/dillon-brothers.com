<?php

class Photos extends Controller {
		
	public function __construct() {
		parent::__construct();
		$this -> view -> js = array(PATH . "public/js/PhotosController.js", PATH . "public/js/image-scale.min.js");
		$this -> view -> css = array(PATH . 'public/css/PhotoController.css');
	}
	
	
	public function index() {
		$Years = new YearPhotosList();
		//$newsLetters = new NewsletterList();
		
		$this -> view -> title = "Photos" . parent::DILLONBROTHERS;
		//$this -> view -> LatestEvents = $latestEvents -> LatestEvents();
		$this -> view -> SchemaMetaData = array("Name" => "Photos: Dillon Brothers", 
												"Description" => "Dillon Brothers Photos",
												"ImageURL" => "http://www.dillon-brothers.com/images/dillon-brothers-motorsports-15.png");
		
		$this -> view -> TwitterMetaData = array("Title" => "Photos: Dillon Brothers", 
												 "Description" => "Dillon Brothers Photos",
												 "URL" => PATH . 'photos',
												 "ImageURL" => "http://www.dillon-brothers.com/images/dillon-brothers-motorsports-15.png");
												 
		$this -> view -> OpenGraphMetaData = array("Title" => "Photos: Dillon Brothers", 
												   "Type" => "Dillon Brothers Photos",
												   "URL" => PATH . 'photos',
												   "Image" => "",
												   "Description" => "",
												   "ArticlePublishTime" => "",
												   "ArticleModifiedTime" => "",
												   "ArticleSection" => "",
												   "ArticleTag" => "");	
												   
		
		$this -> view -> years = $Years -> Years();								   											   
		$this -> view -> render('photos/index');		
	}

	public function year($year) {
		$photoAlbums = new PhotoAlbumsList();
		$yearObject = PhotoYearDirectory::WithYear($year);
		$this -> view -> title = $year . " Photos" . parent::DILLONBROTHERS;
		//$this -> view -> LatestEvents = $latestEvents -> LatestEvents();
		$this -> view -> SchemaMetaData = array("Name" => $year. " Photos: Dillon Brothers", 
												"Description" => "Dillon Brothers " .$year . " Photos",
												"ImageURL" => "http://www.dillon-brothers.com/images/dillon-brothers-motorsports-15.png");
		
		$this -> view -> TwitterMetaData = array("Title" => $year. " Photos: Dillon Brothers", 
												 "Description" => "Dillon Brothers " .$year . " Photos",
												 "URL" => PATH . 'photos/'. $year,
												 "ImageURL" => "http://www.dillon-brothers.com/images/dillon-brothers-motorsports-15.png");
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $year. " Photos: Dillon Brothers", 
												   "Type" => "Dillon Brothers Photos",
												   "URL" => PATH . 'photos/'. $year,
												   "Image" => "",
												   "Description" => "Dillon Brothers " .$year . " Photos",
												   "ArticlePublishTime" => "",
												   "ArticleModifiedTime" => "",
												   "ArticleSection" => "",
												   "ArticleTag" => "");	
		
		
		$this -> view -> startJsFunction = array('PhotosController.InitializeAlbumsByYear();');
		$this -> view -> albums = $photoAlbums -> AlbumsByYear($year);
		$this -> view -> yearCategory = $yearObject;
		
		$this -> view -> render('photos/AlbumsByYear');		
	}

	public function album($year, $type, $albumName) {
		
		switch($type) {
			case "event":
				$intValue = 1;
				break;
			case "blog":
				$intValue = 2;
				break;
			case "misc":
				$intValue = 3;
				break;
			default:
				$intValue = 4;
				break;
		}
		
		
		$album = PhotoAlbum::WithSEOName($year, $intValue, $albumName);
		$photos = new PhotosList();
		$photoPath = NULL;
		
		
		$this -> view -> title = $album -> _albumName . ": Dillon Brothers";
		$this -> view -> albumDetails = $album; 
		
		$this -> view -> albumName = $album -> _albumName;
		$this -> view -> year = $album -> year;
		$this -> view -> albumID = $album -> albumID;
		
		$this -> view -> SchemaMetaData = array("Name" => $year. " Photos: Dillon Brothers", 
												"Description" => "Dillon Brothers " .$year . " Photos " . $albumName,
												"ImageURL" => PHOTO_URL . $year . '/' . $album -> folerAlbumName . '/' . $album -> photoName . '-l.'. $album -> photoExt);
		
		$this -> view -> TwitterMetaData = array("Title" => $year. " Photos: Dillon Brothers", 
												 "Description" => "Dillon Brothers " .$year . " Photos " . $albumName,
												 "URL" => PHOTO_URL . $year . '/' . $album -> folerAlbumName . '/' . $album -> photoName . '-l.'. $album -> photoExt,
												 "ImageURL" => "http://www.dillon-brothers.com/images/dillon-brothers-motorsports-15.png");
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $year. " Photos: Dillon Brothers", 
												   "Type" => "Dillon Brothers Photos",
												   "URL" => PATH . 'photos/'. $year . '/' . $type . '/'. $albumName,
												   "Image" => PHOTO_URL . $year . '/' . $album -> folerAlbumName . '/' . $album -> photoName . '-l.'. $album -> photoExt,
												   "Description" => "Dillon Brothers " .$year . " Photos " . $albumName,
												   "ArticlePublishTime" => $album -> publishedDate,
												   "ArticleModifiedTime" => $album -> modifiedDate,
												   "ArticleSection" => "photos",
												   "ArticleTag" => "photos");	
												   
		$this -> view -> photos	= $photos -> PhotosByAlbumStatic($album -> albumID);									   
		$this -> view -> type = $type;
												   
		$this -> view -> startJsFunction = array('PhotosController.InitializeAlbum();');
		$this -> view -> render('photos/albumView');
	}


	



}
?>