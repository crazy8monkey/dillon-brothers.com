<?php


class Index extends Controller {
		
	public function __construct() {
		parent::__construct();
		$this -> view -> css = array(PATH . 'public/css/IndexController.css', PATH . 'public/css/slick.css', PATH . 'public/css/slick-theme.css');
		$this -> view -> js = array(PATH . "public/js/slick.min.js", PATH . 'public/js/IndexController.js');
	}
	
	
	public function index() {
		$featuredInventory = new InventoryList();
		$events = new EventsList();
		$specials = new SpecialsList();
		$this -> view -> title = "Dillon Brothers";
		$this -> view -> SchemaMetaData = array("Name" => "Dillon Brothers", 
												"Description" => "Dillon Brothers Omaha Fremont ATV, Motorcycle, Side By Sides",
												"ImageURL" => PATH . 'public/images/DillonBrothers.png');
		
		$this -> view -> TwitterMetaData = array("Title" => "Dillon Brothers", 
												 "Description" => "Dillon Brothers Omaha Fremont ATV, Motorcycle, Side By Sides",
												 "URL" => PATH,
												 "ImageURL" => PATH . 'public/images/DillonBrothers.png');
												 
		$this -> view -> OpenGraphMetaData = array("Title" => "Dillon Brothers", 
												   "Type" => "page",
												   "URL" => PATH,
												   "Image" => PATH . 'public/images/DillonBrothers.png',
												   "Description" => "Dillon Brothers Omaha Fremont ATV, Motorcycle, Side By Sides",
												   "ArticlePublishTime" => "",
												   "ArticleModifiedTime" => "",
												   "ArticleSection" => "",
												   "ArticleTag" => "");			
		
		$this -> view -> startJsFunction = array('IndexController.InitializeHomePage();');
		$this -> view -> featuredevent = $events -> GetRandomEvent();
		$this -> view -> featuredSpecial = $specials -> GetRandomSpecial();
		$this -> view -> featuredInventory = $featuredInventory -> FeaturedInventory();										   
		$this -> view -> render('index/index');		
	}
	


}
?>