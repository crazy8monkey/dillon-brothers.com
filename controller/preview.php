<?php

class Preview extends Controller {
	
		
	public function __construct() {
		parent::__construct();
	}
	
	
	public function index() {
		
	}
	
	
	public function eventsingle($id) {
		$eventSingle = Event::PreviewID($id);
		$subEventsList = new EventsList();
		$pageData = new PageData();
		$this -> view -> js = array(PATH . "public/js/photoswipe.min.js", PATH . "public/js/photoswipe-ui-default.min.js", PATH . "public/js/EventsController.js", PATH . 'public/js/moment.min.js', PATH . 'public/js/fullcalendar.min.js', PATH . 'public/js/gcal.js');
		$this -> view -> css = array(PATH . 'public/css/EventsController.css');
		
		$photosSelectedData = $pageData -> EventSinglePhotos($eventSingle -> GetEventID());
		$albumsSectedData = $pageData -> EventSingleAlbums($eventSingle -> GetEventID());
		
		
		$this -> view -> EventSingle = $eventSingle;
		
		$this -> view -> SubEvents = $subEventsList -> GetSubEvents($eventSingle -> SuperEventID);
		
		if($eventSingle -> SubEvent == 1) {
			$relatedAlbums = $subEventsList -> GetSuperEventRelatedAlbums($eventSingle -> SuperEventID);	
		} else {
			$relatedAlbums = $subEventsList -> GetEventRelatedAlbums($eventSingle -> SeoURL);	
		}
		
		$this -> view -> photosSelected = $photosSelectedData;
		$this -> view -> albumsSelected = $albumsSectedData;
		$this -> view -> relatedAlbums = $relatedAlbums;
		$this -> view -> render('preview/eventsingle');
	}
	
	
}
?>