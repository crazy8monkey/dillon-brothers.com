<?php

class Blog extends Controller {
	
	private $_blogPostList;	
	private $_blogCategoryList;	
		
	public function __construct() {
		parent::__construct();
		$this -> _blogPostList = new BlogPostList();
		$this -> _blogCategoryList = new BlogCategoryList();
		$this -> view -> css = array(PATH. 'public/css/BlogController.css');
		$this -> view -> js = array(PATH . "public/js/BlogController.js", "https://www.google.com/recaptcha/api.js");
	}
	
	
	public function index() {
		$this -> redirect -> redirectPage(PATH. 'blog/page/1');		
	}

	public function page($number) {
		
		$this -> view -> title = "Blog" . parent::DILLONBROTHERS;
		$this -> view -> SchemaMetaData = array("Name" => "Blog: Dillon Brothers", 
												"Description" => "Dillon Brothers Blog",
												"ImageURL" => PATH . "public/images/DillonBrothers.png");
		
		$this -> view -> TwitterMetaData = array("Title" => "Blog: Dillon Brothers", 
												 "Description" => "Dillon Brothers Blog",
												 "URL" => PATH . 'blog',
												 "ImageURL" => PATH . "public/images/DillonBrothers.png");
												 
		$this -> view -> OpenGraphMetaData = array("Title" => "blog: Dillon Brothers", 
												   "Type" => "Dillon Brothers Blog",
												   "URL" => PATH . 'blog',
												   "Image" => PATH . "public/images/DillonBrothers.png",
												   "Description" => "Dillon Brothers Omaha Fremont Blog",
												   "ArticlePublishTime" => "",
												   "ArticleModifiedTime" => "",
												   "ArticleSection" => "",
												   "ArticleTag" => "");		
		$this -> _blogPostList -> Page = $number;										   
												   
		$this -> view -> blogposts = $this -> _blogPostList -> AllBlogPlosts();		
		$this -> view -> categories	= $this -> _blogCategoryList -> AllBlogCategories();							   												 										 
		$this -> view -> render('blog/index');
	}

	public function category($category, $number) {
		$blogCategories = new BlogPostList();
		$categories = new BlogCategoryList();
		
		$categoryID = NULL;
		$pageTitle = NULL;
		$blogPosts = new BlogPostList();
		
		
		$this -> view -> categoryView = "";
		
		switch($category) {
			case "uncategorized":
				$redirectPath = PATH. 'blog/category/uncategorized/1';
				$categoryID = 0;	
				$categoryName = "Uncategorized";
				$pageTitle = "Blog Category Uncategorized" . parent::DILLONBROTHERS;
				$metaName = "Blog Category / Uncategorized: Dillon Brothers"; 
				$metaDescription = "Dillon Brothers Current Blog Category Uncategorized Blog Posts Omaha Fremont Nebraska";
				$metaUrl = PATH . 'blog/category/uncategorized';
				
				if(count($blogPosts -> UncategorizedPosts()) > 0) {
					$metaPublishedDate = $blogPosts -> UncategorizedPosts()[0]['dateModifiedDate'];	
					$metaModifiedDate = $blogPosts -> UncategorizedPosts()[count($blogPosts -> UncategorizedPosts())-1]['dateModifiedDate'];
				} else {
					$metaPublishedDate = '';	
					$metaModifiedDate = '';
				}
					
				break;
			default:
				$redirectPath = PATH. 'blog/category/' . $category .'/1';
				$blogCategory = BlogCategory::WithSEOName($category);
				$categoryID = $blogCategory -> categoryID;
				$categoryName = $blogCategory -> blogCategoryName;
				$pageTitle = 'Blog Category ' . $blogCategory -> blogCategoryName . parent::DILLONBROTHERS;
				$metaName = "Blog Category / " . $blogCategory -> blogCategoryName . ": Dillon Brothers"; 
				$metaDescription = "Dillon Brothers Current Blog Category " . $blogCategory -> blogCategoryName . " Blog Posts Omaha Fremont Nebraska";
				$metaUrl = PATH . 'blog/category/' . $category;
				
				if(count($blogPosts -> PostsByCategory($blogCategory -> categoryID)) > 0) {
					$metaPublishedDate = $blogPosts -> PostsByCategory($blogCategory -> categoryID)[0]['dateModifiedDate'];	
					$metaModifiedDate = $blogPosts -> PostsByCategory($blogCategory -> categoryID)[count($blogPosts -> PostsByCategory($blogCategory -> categoryID))-1]['dateModifiedDate'];
				} else {
					$metaPublishedDate = '';	
					$metaModifiedDate = '';
				}
				
				break;
		}
		
		if(!isset($number)) {
			$this -> redirect -> redirectPage($redirectPath);	
		}
			

		$blogCategories -> Page = $number;
		$blogCategories -> PaginationPath = substr($redirectPath, 0, -2);
		
		$this -> view -> SchemaMetaData = array("Name" => $metaName, 
												"Description" => $metaDescription,
												"ImageURL" => PATH . "public/images/DillonBrothers.png");
		
		$this -> view -> TwitterMetaData = array("Title" => $pageTitle, 
												 "Description" => $metaDescription,
												 "URL" => $metaUrl,
												 "ImageURL" => PATH . "public/images/DillonBrothers.png");										  
		
		$this -> view -> OpenGraphMetaData = array("Title" => $pageTitle, 
												   "Type" => "blog",
												   "URL" => $metaUrl,
												   "Image" => PATH . "public/images/DillonBrothers.png",
												   "Description" => $metaDescription,
												   "ArticlePublishTime" => $metaPublishedDate,
												   "ArticleModifiedTime" => $metaModifiedDate,
												   "ArticleSection" => "Blog",
												   "ArticleTag" => "Blog");	
		
		
		$this -> view -> Canonical = $metaUrl;	
		
		$this -> view -> title = $pageTitle;
		$this -> view -> categoryName = $categoryName;
		
		
		$this -> view -> categories	= $categories -> AllBlogCategories();							   												 										 
		$this -> view -> blogposts = $blogCategories -> CategorizedBlogPosts($categoryID);
		$this -> view -> render('blog/index');
	}
	
	public function post($blogName) {
		$blogPost = BlogPost::WithSEOName($blogName);
		
		$comments = new BlogPostCommentsList();
			
			
		$this -> view -> startJsFunction = array('BlogController.InitializeCommentsForm();');
		$this -> view -> title = $blogPost -> blogPostName . parent::DILLONBROTHERS;
			
		$MetaDataDecoded = json_decode($blogPost -> MetaData);
			
		$this -> view -> SchemaMetaData = array("Name" => $blogPost -> blogPostName . ": Dillon Brothers", 
												"Description" => "Dillon Brothers Current Blog " . $blogPost -> blogPostName . " Omaha Fremont Nebraska",
												"ImageURL" => $blogPost -> GetBlogImage());
			
		$this -> view -> TwitterMetaData = array("Title" => $blogPost -> blogPostName . ": Dillon Brothers", 
												 "Description" => "Dillon Brothers Current Blog " . $blogPost -> blogPostName . " Omaha Fremont Nebraska",
												 "URL" => PATH . 'blog/' . $blogPost -> blogPostSEOUrl,
												 "ImageURL" => $blogPost -> GetBlogImage());
													 
		$this -> view -> OpenGraphMetaData = array("Title" => $blogPost -> blogPostName . ": Dillon Brothers", 
												   "Type" => "blog",
												   "URL" => PATH . 'blog/' . $blogPost -> blogPostSEOUrl,
												   "Image" => $blogPost -> GetBlogImage(),
												   "Description" => "Dillon Brothers Current Blog " . $blogPost -> blogPostName . " Omaha Fremont Nebraska",
												   "ArticlePublishTime" => $blogPost -> blogPublishedDateFull,
												   "ArticleModifiedTime" => $blogPost -> modifiedDateFull,
												   "ArticleSection" => "Article",
												   "ArticleTag" => "Article");	
		if(!empty($blogPost -> OptionalShortURL)) {
			$this -> view -> Canonical = PATH . $blogPost -> OptionalShortURL;		
		} else {
			$this -> view -> Canonical = PATH . 'blog/post/' . $blogPost -> blogPostSEOUrl;
		}
								
		
				
			
		$this -> view -> postSingle = $blogPost;
		$this -> view -> comments = $comments -> CommentsByPost($blogPost -> blogPostID);
			
		$this -> view -> render('blog/single');	
		
		
	}
	
	
	public function author($name, $number) {
		$blogPosts = new BlogPostList();
		$categories = new BlogCategoryList();
		
		
		switch($name) {
			case "dillon-brothers":
				$redirectPath = PATH. 'blog/author/dillon-brothers/1';
				$AuthorName = str_replace("-", " ", $name);
				break;
			default:
				$base64Decode = base64_decode(str_pad(strtr($name, '-_', '+/'), strlen($name) % 4, '=', STR_PAD_RIGHT));
				$redirectPath = PATH. 'blog/author/' . $name . '/1';
				
				if(preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $base64Decode)) {
					$posts = $blogPosts -> PostsByAuthor($base64Decode, $name);	
					$AuthorName = $posts['blogposts'][0]['firstName'] . ' ' . substr($posts['blogposts'][0]['lastName'], 0, 1);	
				} else {
					throw new Exception('Blog Author ID does not exist Value(userID: '. $base64Decode . ' )');	
				}
				break;		
		}
		
		if(!isset($number)) {
			$this -> redirect -> redirectPage($redirectPath);	
		}
			

		$blogPosts -> Page = $number;
		$blogPosts -> PaginationPath = substr($redirectPath, 0, -2);
		
		$this -> view -> AuthorName = $AuthorName;
		$this -> view -> title = "Articles Written By ". $AuthorName . parent::DILLONBROTHERS;
		
		$url = PATH . 'blog/author/' . $name . '/' . $number;
		$this -> view -> SchemaMetaData = array("Name" => "Written By " . $AuthorName . ": Dillon Brothers", 
												"Description" => "Dillon Brothers Current Blog  Omaha Fremont Nebraska",
												"ImageURL" => PATH . "public/images/DillonBrothers.png");
		
		$this -> view -> TwitterMetaData = array("Title" => "Written By " . $AuthorName . ": Dillon Brothers", 
												 "Description" => "Dillon Brothers Current Blog  Omaha Fremont Nebraska",
												 "URL" => $url,
												 "ImageURL" => PATH . "public/images/DillonBrothers.png");
												 
		$this -> view -> OpenGraphMetaData = array("Title" => "Written By " . $AuthorName . ": Dillon Brothers", 
												   "Type" => "blog",
												   "URL" => $url,
												   "Image" => PATH . "public/images/DillonBrothers.png",
												   "Description" => "Dillon Brothers Current Blog  Omaha Fremont Nebraska",
												   "ArticlePublishTime" => '',
												   "ArticleModifiedTime" => '',
												   "ArticleSection" => "Article",
												   "ArticleTag" => "Article");	
												   
												   
		$this -> view -> Canonical = $url;											   
		$blogPosts -> PaginationPath = PATH . "blog/author/dillon-brothers";
		
		switch($name) {
			case "dillon-brothers":
				$this -> view -> blogposts = $blogPosts -> PostsByNotShownAuthor();	
				break;
			default:
				$this -> view -> blogposts = $blogPosts -> PostsByAuthor($base64Decode, $name);
				break;
		}
		
		$this -> view -> categories	= $categories -> AllBlogCategories();
		$this -> view -> render('blog/index');
	}
	

	

}
?>