<?php

class Sitemap extends Controller {
	
	private $_newsLetters;
	private $_specialsList;
	private $_LatestEvents;	
	private $_blogPosts;
	private $_blogCategories;
	private $_YearPhotos;	
	private $_PhotoAlbums;	
	private $_EcommerceCategories;
	private $_EcommerceProducts;
	private $_Brands;
		
	public function __construct() {
		parent::__construct();
		$this -> _newsLetters = new NewsletterList();
		$this -> _specialsList = new SpecialsList();
		$this -> _LatestEvents = new EventsList();
		$this -> _blogPosts = new BlogPostList();
		$this -> _blogCategories = new BlogCategoryList();
		$this -> _YearPhotos = new YearPhotosList();
		$this -> _PhotoAlbums = new PhotoAlbumsList();
		$this -> _EcommerceCategories = new EcommerceCategoryList();
		$this -> _EcommerceProducts = new EcommerceProductList();
		$this -> _Brands = new BrandsList();
		$this -> view -> css = array(PATH . 'public/css/SiteMapController.css');
	}
	
	
	public function index() {
		
		$this -> view -> title = "Sitemap: Dillon Brothers";
		//$this -> view -> LatestEvents = $latestEvents -> LatestEvents();
		$this -> view -> SchemaMetaData = array("Name" => "Sitemap: Dillon Brothers", 
												"Description" => "Dillon Brothers Newsletters",
												"ImageURL" => "http://www.dillon-brothers.com/images/dillon-brothers-motorsports-15.png");
		
		$this -> view -> TwitterMetaData = array("Title" => "Sitemap: Dillon Brothers", 
												 "Description" => "Dillon Brothers Sitemap",
												 "URL" => PATH . 'sitemap',
												 "ImageURL" => "http://www.dillon-brothers.com/images/dillon-brothers-motorsports-15.png");
												 
		$this -> view -> OpenGraphMetaData = array("Title" => "Sitemap: Dillon Brothers", 
												   "Type" => "Dillon Brothers Sitemap",
												   "URL" => PATH . 'Sitemap',
												   "Image" => "",
												   "Description" => "",
												   "ArticlePublishTime" => "",
												   "ArticleModifiedTime" => "",
												   "ArticleSection" => "",
												   "ArticleTag" => "");												   
												 										 
		//$this -> view -> startJsFunction = array('EventsController.Initialize("' . PATH . '");');
		$this -> view -> newsLetters = $this -> _newsLetters -> Newsletters();
		$this -> view -> currentSpecials = $this -> _specialsList -> AllSpecials();
		$this -> view -> CurrentEvents = $this -> _LatestEvents -> LatestEvents();
		$this -> view -> blogPosts = $this -> _blogPosts -> AllBlogPlosts();
		$this -> view -> blogCategories = $this -> _blogCategories -> AllBlogCategories();
		$this -> view -> PhotoYears = $this -> _YearPhotos -> Years();
		$this -> view -> PhotoAlubms = $this -> _PhotoAlbums -> AllAlbums();
		$this -> view -> EcommerceCategories = $this -> _EcommerceCategories -> ActiveCategories();
		$this -> view -> EcommerceProducts = $this -> _EcommerceProducts -> AllProducts();
		$this -> view -> OEMBrands = $this -> _Brands -> OEMBrands();
		$this -> view -> PartApparelBrands = $this -> _Brands -> PartApparelBrands();
		$this -> view -> render('sitemap/index');		
	}

	public function xml() {
		$this -> view -> title = PATH . "sitemap/xml";
		
		$this -> view -> newsLetters = $this -> _newsLetters -> Newsletters();
		$this -> view -> currentSpecials = $this -> _specialsList -> AllSpecials();
		$this -> view -> CurrentEvents = $this -> _LatestEvents -> LatestEvents();
		$this -> view -> blogPosts = $this -> _blogPosts -> AllBlogPlosts();
		$this -> view -> blogCategories =$this -> _blogCategories -> AllBlogCategories();
		$this -> view -> PhotoYears = $this -> _YearPhotos -> Years();
		$this -> view -> PhotoAlubms = $this -> _PhotoAlbums -> AllAlbums();
		$this -> view -> EcommerceCategories = $this -> _EcommerceCategories -> ActiveCategories();
		$this -> view -> EcommerceProducts = $this -> _EcommerceProducts -> AllProducts();
		$this -> view -> render('sitemap/xml', true);
	}
	
	


}
?>