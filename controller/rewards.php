<?php

class Rewards extends Controller {
	
		
	public function __construct() {
		parent::__construct();
		$this -> view -> js = array(PATH . "public/js/RewardsController.js");
		$this -> view -> css = array(PATH . 'public/css/RewardsController.css');
	}
	
	
	public function index() {
		$this -> view -> title = "Dillon Brothers VIB Rewards Program";
		//$this -> view -> LatestEvents = $latestEvents -> LatestEvents();
		$this -> view -> SchemaMetaData = array("Name" => "Dillon Brothers VIB Rewards Program", 
												"Description" => "Dillon Brothers VIB Rewards Program available at all our Motorcycle Dealerships. Redeem your rewards points and save.",
												"ImageURL" => PATH . "public/images/vib-rewards-banner.jpg");
		
		$this -> view -> TwitterMetaData = array("Title" => "Dillon Brothers VIB Rewards Program Specials #rewards #dillonbrothers", 
												 "Description" => "Dillon Brothers VIB Rewards Program available at Dillon Brothers Harley-Davidson, Dillon Brothers MotorSports or Dillon Brothers Indian Motorcycle.",
												 "URL" => PATH . 'sitemap',
												 "ImageURL" => PATH . "public/images/vib-rewards-banner.jpg");
												 
		$this -> view -> OpenGraphMetaData = array("Title" => "Dillon Brothers VIB Rewards Program Specials", 
												   "Type" => "article",
												   "URL" => PATH . 'rewards',
												   "Image" => PATH . "public/images/vib-rewards-banner.jpg",
												   "Description" => "Dillon Brothers VIB Rewards Program available at Dillon Brothers Harley-Davidson, Dillon Brothers MotorSports or Dillon Brothers Indian Motorcycle.",
												   "ArticlePublishTime" => "",
												   "ArticleModifiedTime" => "",
												   "ArticleSection" => "Events",
												   "ArticleTag" => "VIB Rewards Program");												   
												 										 
		
		$this -> view -> startJsFunction = array('RewardsController.InitializePage();');
		$this -> view -> render('rewards/index');		
	}
	

}
?>