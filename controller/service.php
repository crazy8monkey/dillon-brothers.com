<?php

class Service extends Controller {
	
		
	public function __construct() {
		parent::__construct();
		$this -> view -> js = array(PATH . "public/js/ServiceController.js", "https://code.jquery.com/ui/1.12.1/jquery-ui.js", PATH . "public/js/maskedInput.js", "https://www.google.com/recaptcha/api.js");
		$this -> view -> css = array(PATH . 'public/css/ServiceController.css', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
	}
	
	
	public function index() {
		Session::init();
		//$specialsList = new SpecialsList();
		$pageTitle = "Service Department" . parent::DILLONBROTHERS;
		
		$this -> view -> title = $pageTitle;
		
		
		$this -> view -> SchemaMetaData = array("Name" => $pageTitle, 
												"Description" => "Dillon Brothers Service Departments Omaha Fremont Nebraska",
												"ImageURL" => PATH . "public/images/DillonBrothers.png");
		
		$this -> view -> TwitterMetaData = array("Title" => $pageTitle, 
												 "Description" => "Dillon Brothers Service Departments Omaha Fremont Nebraska",
												 "URL" => PATH . 'service',
												 "ImageURL" => PATH . "public/images/DillonBrothers.png");
												 
		$this -> view -> OpenGraphMetaData = array("Title" => $pageTitle, 
												   "Type" => "page",
												   "URL" => PATH . 'service',
												   "Image" => PATH . "public/images/DillonBrothers.png",
												   "Description" => "Dillon Brothers Service Departments Omaha Fremont Nebraska",
												   "ArticlePublishTime" => "",
												   "ArticleModifiedTime" => "",
												   "ArticleSection" => "",
												   "ArticleTag" => "");												   
		
		$this -> view -> Canonical = PATH . 'service';
		$this -> view -> startJsFunction = array('ServiceController.Initialize();');										 										 
		$this -> view -> render('service/index');		
	}
	


}
?>