<?php

class Contact extends Controller {
		
	public function __construct() {
		parent::__construct();
		$this -> view -> js = array(PATH . "public/js/ContactController.js");
		$this -> view -> css = array(PATH . 'public/css/ContactController.css');
	}
	
	
	public function index() {
		$latestEvents = new EventsList();
		$newsLetters = new NewsletterList();
		
		$this -> view -> title = "Contact" . parent::DILLONBROTHERS;
		//$this -> view -> LatestEvents = $latestEvents -> LatestEvents();
		$this -> view -> SchemaMetaData = array("Name" => "Contact: Dillon Brothers", 
												"Description" => "Dillon Brothers Contact",
												"ImageURL" => "http://www.dillon-brothers.com/images/dillon-brothers-motorsports-15.png");
		
		$this -> view -> TwitterMetaData = array("Title" => "Contact: Dillon Brothers", 
												 "Description" => "Dillon Brothers Contact",
												 "URL" => PATH . 'Contact',
												 "ImageURL" => "http://www.dillon-brothers.com/images/dillon-brothers-motorsports-15.png");
												 
		$this -> view -> OpenGraphMetaData = array("Title" => "Contact: Dillon Brothers", 
												   "Type" => "Dillon Brothers Contact",
												   "URL" => PATH . 'Contact',
												   "Image" => "",
												   "Description" => "",
												   "ArticlePublishTime" => "",
												   "ArticleModifiedTime" => "",
												   "ArticleSection" => "",
												   "ArticleTag" => "");												   
												 										 
		$this -> view -> render('contact/index');		
	}
	
	


}
?>